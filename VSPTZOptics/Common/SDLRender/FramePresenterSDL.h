/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = ThreadController.cpp                                        */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018 Areth Software Ltd.        All rights reserved.                   */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class definations				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Areth Software Ltd., the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0:  15-Dec-2018  : Initial Version
*/
/******************************************************************************/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#pragma once

/* Application header files */
#include "CommonExportMacro.h"
#include "DefineBase.h"
#include "ThreadCtl/CriticalSection.h"
#include "SDL.h"
#include "SDL_events.h"
#include "SDL_Render.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

/* System header files */
#include <vector>

/* Lib header files */
extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#  define __STDC_CONSTANT_MACROS
#endif
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>
}
using namespace ShareLibrary::Base;
using namespace ShareLibrary::Model;
using namespace System::Collections::Generic;
using namespace System::Drawing;
class Resolution2D
{
public:
	int width;
	int height;
};

typedef struct
{
	SDL_Texture* sdlTexture;
	char		name[STR_LENGTH_64];
}SDLMessage;

class ISF_API FramePresenterSDL
{
private:
	CriticalSection sdlCrs;
	int zoomPosX;
	int zoomPosY;
	double zoomFactor;
	SDL_Rect srcRect;
	SDL_Rect wndRect;
	SDL_Rect displayRect;
	SDL_Rect minimumDisplayRect;
	double lastZoomFactor;
public:
	bool isInit;
	bool isSuppendReder;
	SDL_Event sdl_Event;
	SDL_Window *screen;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	Uint8 *yPlane, *uPlane, *vPlane;
	size_t yPlaneSz, uvPlaneSz;
	int uvPitch;
	SwsContext *sws_ctx;
	HWND		handle;
	int windowW, windowH;
	Resolution2D resolution;
	DISPLAY_RATIO_ENUM displayRatio;
	TTF_Font* pFont;
	std::vector<UINT64> messageList;

	FramePresenterSDL();
	FramePresenterSDL(HWND _hwnd);
	~FramePresenterSDL();
	VOID Reset();
	VOID Destroy(bool _isDestroyWindow);
	Std_ReturnType	RenderAVFrame(AVFrame* _pavFrame, bool _isPresent = true);
	Std_ReturnType  RenderAVFrameInfo(AVFrame* _pavFrame, bool _isPresent = true);
	Std_ReturnType	InitFramePresenterSDL(AVFrame* _pavFrame);
	Std_ReturnType  RenderDefaultFrame();
	Std_ReturnType  RenderRects(SDL_Rect* _rect, Uint8 _r, Uint8 _g, Uint8 _b, char* _name, int _ind, bool _isPresent = false);
	void	RenderPresent();
	SDL_Rect* GetDisplaySize();
	SDL_Rect* GetMinimunDisplaySize();
	void GetDisplayRatio(float* _w, float* _h);
	void  SetZoomPosition(int _x, int _y);
	void  SetZoomFactor(double _zoomFactor);
};
