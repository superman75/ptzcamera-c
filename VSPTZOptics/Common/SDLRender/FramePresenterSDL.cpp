#include "stdafx.h"
#include "FramePresenterSDL.h"

using namespace ShareLibrary::Base;

using namespace std;
using namespace System;
using namespace System::Diagnostics;
using namespace System::Runtime::ExceptionServices;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

Std_ReturnType ResizeEventWatcher(void* data, SDL_Event* event)
{
	try
	{
		if (event->type == SDL_WINDOWEVENT &&
			event->window.event == SDL_WINDOWEVENT_RESIZED) {
			SDL_Renderer* renderer = (SDL_Renderer*)data;
			if (renderer != NULL)
			{
				SDL_RenderClear(renderer);
				SDL_RenderPresent(renderer);
				//LogWriter::Instance->WriteLog(2, "resizing.....\n", __LINE__, __FUNCDNAME__);
			}
		}
		return E_OK;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + "\r\n" + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
}

Std_ReturnType FramePresenterSDL::InitFramePresenterSDL(AVFrame* _avFrame)
{
	if (_avFrame == NULL || this->handle == NULL)
		return E_NOT_OK;
	if (screen == NULL)
	{
		screen = SDL_CreateWindowFrom(this->handle);
		if (!screen)
		{
			return E_NOT_OK;;
		}
	}
	// Allocate a place to put our YUV image on that screen
	if (renderer == NULL)
	{
		renderer = SDL_CreateRenderer((SDL_Window*)screen, -1, SDL_RENDERER_ACCELERATED);
		if (!renderer) {
			return E_NOT_OK;
		}
	}
	int width = resolution.width;
	int height = resolution.height;
	if ((!isInit || width*height == 0) && (width != _avFrame->width || height != _avFrame->height))
	{
		windowW = windowH = 0;
		if (isInit == false) {
			isInit = true;
		}
		int screenW, screenH;
		SDL_GetWindowSize(screen, &screenW, &screenH);
		if (screenH * screenW != 0)
		{
			height = resolution.height = screenH;
			width = resolution.width = screenH * ((double)_avFrame->width/ (double)_avFrame->height);
			if (width > screenW)
			{
				width = resolution.width = screenW;
				height = resolution.height = screenW * ((double)_avFrame->height / (double)_avFrame->width);
			}
		}
		else
		{
			width = resolution.width = _avFrame->width;
			height = resolution.height = _avFrame->height;
		}
		if (width % 2 != 0 || height % 2 != 0) //width and height must be even number
		{
			height = resolution.height = ((int)height / 2 * 2);
			width = resolution.width = ((int)width / 2 * 2);
		}
		/*if(_avFrame->width % 2 !=0 || _avFrame->height%2 != 0) //width and height must be even number
		{
			height = resolution.height =  ((int)_avFrame->height/2 * 2);
			width = resolution.width = ((int)_avFrame->width / 2 * 2);	
		}
		else
		{
			width = resolution.width =  _avFrame->width;
			height = resolution.height = _avFrame->height;
		}*/
		srcRect.x = srcRect.y = 0;
		srcRect.w = width;
		srcRect.h = height;
		enum AVPixelFormat inFormat = AV_PIX_FMT_NONE;
		inFormat = (AVPixelFormat)_avFrame->format;
		if (texture != NULL) {
			SDL_DestroyTexture(texture);
			texture = NULL;
		}
		texture = SDL_CreateTexture(
			renderer,
			SDL_PIXELFORMAT_YV12,
			SDL_TEXTUREACCESS_STREAMING,
			width,
			height
		);
		if (!texture) {
			return E_NOT_OK;
		}
		// initialize SWS context for software scaling
		if (sws_ctx != NULL) {
			sws_freeContext(sws_ctx);
			sws_ctx = NULL;
		}
		sws_ctx = sws_getContext(_avFrame->width, _avFrame->height,
			inFormat, width, height,
			AV_PIX_FMT_YUVJ420P,
			SWS_BICUBIC,
			NULL,
			NULL,
			NULL);
		if (yPlane != NULL) {
			free(yPlane);
			yPlane = NULL;
		}
		if (uPlane != NULL) {
			free(uPlane);
			uPlane = NULL;
		}
		if (vPlane != NULL) {
			free(vPlane);
			vPlane = NULL;
		}
		// set up YV12 pixel array (12 bits per pixel)
		yPlaneSz = width * height;
		uvPlaneSz = width * height / 4;
		yPlane = (Uint8*)malloc(yPlaneSz);
		uPlane = (Uint8*)malloc(uvPlaneSz);
		vPlane = (Uint8*)malloc(uvPlaneSz);
		if (!yPlane || !uPlane || !vPlane) {
			return E_NOT_OK;
		}
		uvPitch = width / 2;
	}
	return E_OK;
}

Std_ReturnType FramePresenterSDL::RenderAVFrameInfo(AVFrame* _pavFrame, bool _isPresent)
{
	try
	{
		sdlCrs.Enter();
		return RenderAVFrame(_pavFrame, _isPresent);
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(1, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally
	{
		sdlCrs.Leave();
	}
	return E_NOT_OK;
}

FramePresenterSDL::FramePresenterSDL(HWND _hwnd)
{
	handle = _hwnd;
	Reset();
}

FramePresenterSDL::FramePresenterSDL()
{
	Reset();
}

FramePresenterSDL::~FramePresenterSDL()
{
	Destroy(true);
}

void FramePresenterSDL::Reset()
{
	isInit = false;
	isSuppendReder = false;
	screen = NULL;
	renderer = NULL;
	texture = NULL;
	yPlane = NULL;
	uPlane = NULL;
	vPlane = NULL;
	sws_ctx = NULL;
	uvPitch = 0;
	windowW = windowH = 0;
	displayRatio = DISPLAY_RATIO_ENUM::ORIGINAL_RATIO;
	zoomFactor = 1;
	lastZoomFactor = -1;
	zoomPosX = zoomPosY = 0;
	pFont = NULL;
	resolution.width = resolution.height = 0;
	messageList.reserve(8);//reserver 8 items
	for (int i = 0; i < 8; i++)
	{
		messageList.push_back(0);//init for messageList;
	}
	memset(&displayRect, 0, sizeof(SDL_Rect));
	memset(&minimumDisplayRect, 0, sizeof(SDL_Rect));
	memset(&wndRect, 0, sizeof(SDL_Rect));
}

void FramePresenterSDL::Destroy(bool _isDestroyWindow = true)
{
	try
	{
		if (yPlane != NULL) {
			free(yPlane);
			yPlane = NULL;
		}
		if (uPlane != NULL) {
			free(uPlane);
			uPlane = NULL;
		}
		if (vPlane != NULL) {
			free(vPlane);
			vPlane = NULL;
		}
		if (sws_ctx != NULL) {
			sws_freeContext(sws_ctx);
			sws_ctx = NULL;
		}
		if (texture != NULL) {
			SDL_DestroyTexture(texture);
			texture = NULL;
		}
		if (renderer != NULL) {
			SDL_DestroyRenderer(renderer);
			renderer = NULL;
		}
		if (_isDestroyWindow) {
			if (screen != NULL) {
				SDL_DestroyWindow(screen);
				screen = NULL;
			}
		}

		for (int i; i< messageList.size(); i++)
		{
			SDLMessage* pMessage = (SDLMessage*)messageList[i];
			if(pMessage != NULL && pMessage->sdlTexture)
				SDL_DestroyTexture(pMessage->sdlTexture);
			delete pMessage;
		}
		messageList.clear();
		isInit = false;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}	
}

Std_ReturnType FramePresenterSDL::RenderDefaultFrame()
{
	try
	{
		sdlCrs.Enter();
		if (renderer != NULL)
		{
			if (isSuppendReder)
				return E_OK;
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderClear(renderer);
			SDL_RenderPresent(renderer);
		}
		return E_OK;
	}
	catch (Exception^ e)
	{
		String^ log = "RenderAVFrame crash";
		if (e != nullptr)
			log += e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(1, log, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally
	{
		sdlCrs.Leave();
	}
}

Std_ReturnType FramePresenterSDL::RenderRects(SDL_Rect* _rect, Uint8 _r, Uint8 _g, Uint8 _b, char* _name, int _ind, bool _isPresent)
{
	try
	{
		sdlCrs.Enter();
		if (renderer != NULL)
		{
			if (isSuppendReder)
				return E_OK;
#pragma region Draw Rectangle
			// Render rect
			
			int scale = 3;
			_rect->x = (int)((float)_rect->x / (float)scale);
			_rect->y = (int)((float)_rect->y / (float)scale);
			_rect->w = (int)((float)_rect->w / (float)scale);
			_rect->h = (int)((float)_rect->h / (float)scale);
			SDL_RenderSetScale(renderer, scale, scale);
			SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 128);
			SDL_RenderFillRect(renderer, _rect);

			SDL_SetRenderDrawColor(renderer, _r, _g, _b, 255);
			SDL_RenderDrawRect(renderer, _rect);			
#pragma endregion Draw Rectangle
#pragma region Draw Text
			SDL_Rect Message_rect; //create a rect
			Message_rect.x = _rect->x + 3;  //controls the rect's x coordinate 
			Message_rect.y = _rect->y; // controls the rect's y coordinte
			int nChar = 0;
			for (char* p = _name; *p != STR_NULL; p++)
			{
				nChar++;
			}
			Message_rect.w = 3 * nChar; // controls the width of the rect
			Message_rect.h = 8; // controls the height of the rect
			if (pFont == NULL)
				pFont = TTF_OpenFont("micross.ttf", 24); //this opens a font style and sets a size
			if (pFont != NULL)
			{
				SDLMessage* pMessage = (SDLMessage*)messageList[_ind];
				if (pMessage != NULL)
				{
					if (strcmp(pMessage->name, _name))//change name
					{
						SDL_DestroyTexture(pMessage->sdlTexture);
						delete pMessage;
						pMessage = NULL;
					}
				}
				if (pMessage == NULL)
				{
					SDL_Color cl = { 255, 255, 255 };
					SDL_Surface* surfaceMessage = TTF_RenderText_Solid(pFont, _name, cl); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
					if(surfaceMessage != NULL)
					{
						pMessage = new SDLMessage();
						pMessage->sdlTexture = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture
						sprintf(pMessage->name, "%s", _name);
						if (pMessage != NULL)
							messageList[_ind] = (UINT64)pMessage;
						SDL_FreeSurface(surfaceMessage);
					}
				}
				if (pMessage != NULL && pMessage->sdlTexture)
				{
					SDL_RenderCopy(renderer, pMessage->sdlTexture, NULL, &Message_rect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture
				}
					
			}
#pragma endregion Draw Text
			if(_isPresent)
				SDL_RenderPresent(renderer);
		}
		return E_OK;
	}
	catch (Exception^ e)
	{
		String^ log = "RenderAVFrame crash";
		if (e != nullptr)
			log += e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(1, log, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally
	{
		sdlCrs.Leave();
	}
}

void FramePresenterSDL::RenderPresent()
{
	try
	{
		sdlCrs.Enter();
		if(renderer!=NULL)
			SDL_RenderPresent(renderer);
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(1, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return;
	}
	finally
	{
		sdlCrs.Leave();
	}
	return;
}

SDL_Rect* FramePresenterSDL::GetDisplaySize()
{
	displayRect.h = wndRect.h;
	switch (displayRatio)
	{
	case ShareLibrary::Base::DISPLAY_RATIO_ENUM::FULL_SCREEN:
		displayRect.h = wndRect.w;
		break;
	case ShareLibrary::Base::DISPLAY_RATIO_ENUM::ORIGINAL_RATIO:
		displayRect.w = (int)((float)srcRect.w * (float)wndRect.h / (float)srcRect.h);
		if (displayRect.w > wndRect.w)
		{
			displayRect.w = wndRect.w;
			displayRect.h = wndRect.w * ((float)srcRect.h / (float)srcRect.w);
		}
		break;
	case ShareLibrary::Base::DISPLAY_RATIO_ENUM::RATIO_16_9:
		displayRect.w = (int)((float)16 * (float)wndRect.h / (float)9);
		if (displayRect.w > wndRect.w)
		{
			displayRect.w = wndRect.w;
			displayRect.h = wndRect.w * ((float)9 / (float)16);
		}
		break;
	case ShareLibrary::Base::DISPLAY_RATIO_ENUM::RATIO_4_3:
		displayRect.w = (int)((float)4 * (float)wndRect.h / (float)3);
		if (displayRect.w > wndRect.w)
		{
			displayRect.w = wndRect.w;
			displayRect.h = wndRect.w * ((float)3 / (float)4);
		}
		break;
	case ShareLibrary::Base::DISPLAY_RATIO_ENUM::RATIO_16_10:
		displayRect.w = (int)((float)16 * (float)wndRect.h / (float)10);
		if (displayRect.w > wndRect.w)
		{
			displayRect.w = wndRect.w;
			displayRect.h = wndRect.w * ((float)10 / (float)16);
		}
		break;
	default:
		break;
	}
	displayRect.x = displayRect.y = 0;
	return &displayRect;
}

SDL_Rect* FramePresenterSDL::GetMinimunDisplaySize()
{
	return &minimumDisplayRect;
}

void FramePresenterSDL::GetDisplayRatio(float* _w, float* _h)
{
	*_w = (float)displayRect.w / (float)srcRect.w;
	*_h = (float)displayRect.h / (float)srcRect.h;
}

void FramePresenterSDL::SetZoomPosition(int _x, int _y)
{
	sdlCrs.Enter();
	zoomPosX = _x;
	zoomPosY = _y;
	sdlCrs.Leave();
}

void FramePresenterSDL::SetZoomFactor(double _zoomFactor)
{
	sdlCrs.Enter();
	zoomFactor = _zoomFactor;
	sdlCrs.Leave();
}

Std_ReturnType FramePresenterSDL::RenderAVFrame(AVFrame * _pavFrame, bool _isPresent)
{
	try
	{
		if (isSuppendReder)
			return E_OK;
		AVFrame * avFrame = _pavFrame;
		if (InitFramePresenterSDL(avFrame) != E_OK)
			return E_NOT_OK;
		int width = resolution.width;
		int height = resolution.height;
		if (width * height != 0)
		{
			AVFrame pict;
			pict.data[0] = yPlane;
			pict.data[1] = uPlane;
			pict.data[2] = vPlane;
			pict.linesize[0] = width;
			pict.linesize[1] = uvPitch;
			pict.linesize[2] = uvPitch;
			// Convert the image into YUV format that SDL uses
			sws_scale(sws_ctx, (uint8_t const * const *)avFrame->data,
				avFrame->linesize, 0, avFrame->height, pict.data,
				pict.linesize);
			SDL_UpdateYUVTexture(
				texture,
				NULL,
				yPlane,
				width,
				uPlane,
				uvPitch,
				vPlane,
				uvPitch
			);
#pragma region Resize Screen
			int screenW = 0, screenH = 0;
			int w = 0, h = 0;
			int x = 0, y = 0;
			SDL_GetWindowSize(screen, &screenW, &screenH);
			w = screenW;
			h = screenH;
			wndRect.w = screenW;
			wndRect.h = screenH;
			switch (displayRatio)
			{
			case DISPLAY_RATIO_ENUM::FULL_SCREEN:
				break;
			case DISPLAY_RATIO_ENUM::ORIGINAL_RATIO:
				w = h * width / height;
				break;
			case DISPLAY_RATIO_ENUM::RATIO_16_9:
				w = h * 16 / 9;
				break;
			case DISPLAY_RATIO_ENUM::RATIO_4_3:
				w = h * 4 / 3;
				break;
			case DISPLAY_RATIO_ENUM::RATIO_16_10:
				w = h * 16 / 10;
				break;
			}
			if (w * h != windowW * windowH)
			{
				windowW = w;
				windowH = h;
				if ((minimumDisplayRect.w * minimumDisplayRect.h == 0) || (windowW < minimumDisplayRect.w || windowH < minimumDisplayRect.h))
				{
					minimumDisplayRect.w = windowW;
					minimumDisplayRect.h = windowH;
				}
				SDL_RenderSetLogicalSize(renderer, (w/2*2), (h/2*2));				
			}
#pragma endregion Resize Screen
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderClear(renderer);
#pragma region Zoom
			if(zoomFactor > 1)
			{
				if(lastZoomFactor != zoomFactor)
				{
					lastZoomFactor = zoomFactor;
					double avgW = width / 2;
					double avgH = height / 2;
					int srcZoomPosX = srcRect.x + zoomPosX * srcRect.w / w;
					int srcZoomPosY = srcRect.y + zoomPosY * srcRect.h / h;
					double newW = srcZoomPosX <= avgW ? (double)srcZoomPosX * 2 : (double)(width - srcZoomPosX) * 2;
					double newH = srcZoomPosY <= avgH ? (double)srcZoomPosY * 2 : (double)(height - srcZoomPosY) * 2;
					if (newW <= newH){
						double tempH = newW * height / width;
						newH = tempH;
					}
					else{
						double tempW = newH * width / height;
						newW = tempW;
					}
					if (newW * newH != 0)
					{
						newW = newW / zoomFactor;
						newH = newH / zoomFactor;
						srcRect.x = srcZoomPosX - newW / 2;
						srcRect.y = srcZoomPosY - newH / 2;
						srcRect.w = newW;
						srcRect.h = newH;						
					}
				}
			}
			else
			{
				srcRect.x = 0;
				srcRect.y = 0;
				srcRect.w = width;
				srcRect.h = height;
			}
			SDL_RenderCopy(renderer, texture, &srcRect, NULL);
			if (w * h > 1 && zoomFactor > 1)
			{
				SDL_Rect destRect;
				destRect.w = w / 3;
				destRect.h = destRect.w * h / w;
				destRect.x = 0;
				destRect.y = 0;
				SDL_RenderCopy(renderer, texture, NULL, &destRect);
				SDL_Rect r;
				double ratioW = (double)destRect.w / (double)width;
				double ratioH = (double)destRect.h / (double)height;
				r.w = srcRect.w * ratioW;
				r.h = srcRect.h * ratioH;
				r.x = srcRect.x * ratioW;
				r.y = srcRect.y * ratioH;
				SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
				SDL_RenderDrawRect(renderer, &r);
				SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
			}
#pragma endregion Zoom
			if(_isPresent)
				SDL_RenderPresent(renderer);
		}
		return E_OK;
	}
	catch (Exception^ e)
	{
		String^ log = "RenderAVFrame crash";
		if (e != nullptr)
			log += e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(1, log, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
}
