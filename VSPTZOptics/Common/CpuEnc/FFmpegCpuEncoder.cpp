#include "stdafx.h"
#include "FFmpegCpuEncoder.h"
using namespace ShareLibrary::Base;

VOID FFmpegCpuEncoder::FfmpegFreeFrame(AVFrame* _avFrame, bool _isFreePackets)
{
	if (_avFrame != NULL)
	{
		av_frame_free(&_avFrame);
	}
	if (_isFreePackets)
		ReleasePacket();
}

Std_ReturnType FFmpegCpuEncoder::AVAsyncEncoder(int*_got_packet, AVFrame* _avFrame)
{
	int ret = -1;
	*_got_packet = 0;
	this->ReleasePacket();
	if (_avFrame)
	{
	resend:
		if(_avFrame->format != AV_PIX_FMT_YUVJ420P) //if format is not AV_PIX_FMT_YUVJ420P so we need to convert to AV_PIX_FMT_YUVJ420P before encode
		{
			AVFrame* outframe = av_frame_alloc();
			av_image_alloc(outframe->data, outframe->linesize, _avFrame->width, _avFrame->height, AV_PIX_FMT_YUV420P, 1);
			if (swsCtx == NULL)
				swsCtx = sws_getContext(_avFrame->width, _avFrame->height,
				(AVPixelFormat)_avFrame->format, _avFrame->width, _avFrame->height,
					AV_PIX_FMT_YUVJ420P, 0, 0, 0, 0);

			sws_scale(swsCtx,
				_avFrame->data,
				_avFrame->linesize,
				0,
				_avFrame->height,
				outframe->data,
				outframe->linesize);
			outframe->pts = nextPts++;
			ret = avcodec_send_frame(pCodecCtx, outframe);
			av_freep(&outframe->data[0]);
			av_frame_free(&outframe);
		}
		else
		{
			ret = avcodec_send_frame(pCodecCtx, _avFrame);
		}
		// In particular, we don't expect AVERROR(EAGAIN), because we read all
		// decoded frames with avcodec_receive_packet() until done.
		if (ret == AVERROR(EAGAIN)) {
			if (*_got_packet == 0)
			{
				AVPacket* avPacket = av_packet_alloc();
				av_init_packet(avPacket);
				avPacket->size = 0;
				avPacket->data = NULL;
				ret = avcodec_receive_packet(pCodecCtx, avPacket);
				if (ret < 0 && ret != AVERROR(EAGAIN)) {
					av_packet_free(&avPacket);
					return E_NOT_OK;
				}
				if (ret >= 0) {
					this->avPacketList.push_back((UINT64)avPacket);
					*_got_packet = 1;
					LogWriter::Instance->WriteLog(2, "Resend AVFrame", __LINE__, __FUNCDNAME__);
					goto resend;
				}
				else
					av_packet_free(&avPacket);
			}
			return E_OK;
		}
		else if (ret < 0 && ret != AVERROR_EOF)
		{
			int err = AVERROR(EINVAL);
			err = AVERROR(ENOMEM);
			return E_NOT_OK;
		}
	}
	if (*_got_packet == 0)
	{
		do
		{
			AVPacket* avPacket = av_packet_alloc();
			av_init_packet(avPacket);
			avPacket->size = 0;
			avPacket->data = NULL;
			ret = avcodec_receive_packet(pCodecCtx, avPacket);
			int err = AVERROR(EINVAL);
			err = AVERROR(EAGAIN);
			err = AVERROR(ENOMEM);
			if (ret < 0 && ret != AVERROR(EAGAIN))
			{
				av_packet_free(&avPacket);
				return E_NOT_OK;
			}
			if (ret >= 0)
			{
				avPacket->pts = av_rescale_q_rnd(avPacket->pts, pCodecCtx->time_base, pCodecCtx->time_base, AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
				this->avPacketList.push_back((UINT64)avPacket);				
				*_got_packet = 1;
			}
			else
				av_packet_free(&avPacket);
		} while (ret >= 0);
	}
	return E_OK;
}

VOID FFmpegCpuEncoder::Reset()
{
	pCodecCtx = NULL;
	pCodec = NULL;
	avPacketList.reserve(5);// reserver 5 items
	nextPts = 0;
	swsCtx = NULL;
	isInit = false;
}

VOID FFmpegCpuEncoder::Destroy()
{
	ReleasePacket();
	if (pCodecCtx != NULL) {
		avcodec_free_context(&pCodecCtx);
		pCodecCtx = NULL;
	}
	if (swsCtx != NULL)
	{
		sws_freeContext(swsCtx);
		swsCtx = NULL;
	}
}

VOID FFmpegCpuEncoder::ReleasePacket()
{
	for (int i = 0; i < avPacketList.size(); i++)
	{
		AVPacket* avPacket = (AVPacket*)avPacketList[i];
		av_packet_free(&avPacket);
	}
	avPacketList.clear();
}

FFmpegCpuEncoder::FFmpegCpuEncoder()
{
	Reset();
}

FFmpegCpuEncoder::~FFmpegCpuEncoder()
{
	Destroy();
}

Std_ReturnType FFmpegCpuEncoder::FFmpegCpuEncoderInit(AVCodecParameters* _avCodecParam, AVCodecID _codecID)
{
	codecId = _codecID;
	pCodec = avcodec_find_encoder(codecId);
	if (!pCodec) {
		LogWriter::Instance->WriteLog(2, "avcodec_find_encoder error", __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	pCodecCtx = avcodec_alloc_context3(pCodec);
	/* put sample parameters */
	pCodecCtx->bit_rate = 400000;
	/* resolution must be a multiple of two */
	pCodecCtx->width = _avCodecParam->width;
	pCodecCtx->height = _avCodecParam->height;
	/* frames per second */
	AVRational avRatioTB = { avRatioTB.num = 1, avRatioTB.den = 25 };
	pCodecCtx->time_base = avRatioTB;
	AVRational avRatioFR = { avRatioFR.num = 25, avRatioFR.den = 1 };
	//pCodecCtx->framerate = avRatioFR;
	if(_codecID != AV_CODEC_ID_MJPEG)
	{
		pCodecCtx->gop_size = 10; /* emit one intra frame every ten frames */
		pCodecCtx->max_b_frames = 1;
	}
	pCodecCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
	pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	/* open it */
	int err = avcodec_open2(pCodecCtx, pCodec, NULL);
	if (err < 0) {
		LogWriter::Instance->WriteLog(2, "avcodec_open2 error " + err, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}

	pCodecCtx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
	isInit = true;
	return E_OK;
}

Std_ReturnType FFmpegCpuEncoder::FFmpegRunAsynEncoder(AVFrame* _avFrame)
{
	try
	{
		int         packetFinished = 0;
		if (_avFrame != NULL)
		{
			if (AVAsyncEncoder(&packetFinished, _avFrame) != E_OK)
			{
				ReleasePacket();
				return E_NOT_OK;
			}
			if (packetFinished)
			{
				return E_OK;
			}
			else
			{
				return E_CONTINUE;
			}
		}
		else
		{
			return E_NOT_OK;
		}
	}
	catch (System::Exception^ e)
	{
		if (e != nullptr)
		{
			LogWriter::Instance->WriteLog(1, e->Message + e->StackTrace, __LINE__, __FUNCDNAME__);
		}
		return E_NOT_OK;
	}
	return E_NOT_OK;
}
