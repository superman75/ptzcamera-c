#pragma once
#include "CommonExportMacro.h"
#include "DefineBase.h"

#include <memory>
#include <vector>

extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#  define __STDC_CONSTANT_MACROS
#endif
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
}


class ISF_API FFmpegCpuEncoder
{
private:
	VOID FfmpegFreeFrame(AVFrame* _avFrame, bool _isFreePackets = false);
	Std_ReturnType AVAsyncEncoder(int*_got_packet, AVFrame* _avFrame);
	AVCodecID		codecId;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	SwsContext		*swsCtx;
	int				nextPts;

	VOID Reset();
	VOID Destroy();
	
public:
	std::vector<UINT64> avPacketList;
	bool isInit;
	FFmpegCpuEncoder();
	~FFmpegCpuEncoder();
	VOID ReleasePacket();
	Std_ReturnType FFmpegCpuEncoderInit(AVCodecParameters* _avCodecParam, AVCodecID _codecID);
	Std_ReturnType FFmpegRunAsynEncoder(AVFrame* _avFrame);
};
