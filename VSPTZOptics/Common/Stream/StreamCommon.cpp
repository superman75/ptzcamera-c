#include "stdafx.h"
#include "StreamCommon.h"
using namespace ShareLibrary::Base;
using namespace std;
using namespace System;

StreamCommon::StreamCommon()
{
	Reset();
}

StreamCommon::~StreamCommon()
{
	if (pFormatCtx != NULL) {
		avformat_close_input(&pFormatCtx);
		pFormatCtx = NULL;
	}
	if (avCodecParameters.extradata != NULL && avCodecParameters.extradata_size > 0) {
		av_free(avCodecParameters.extradata);
		memset(&avCodecParameters, 0, sizeof(AVCodecParameters));
	}
}

VOID StreamCommon::Reset()
{
	cameraID[0] = STR_NULL;
	packet.data = NULL;
	packet.size = 0;
	isGetIFrame = false;
	isFileStream = false;
	pFormatCtx = NULL;
	pFrame = NULL;
	memset(&avCodecParameters, 0, sizeof(AVCodecParameters));
	videoStreamInd = -1;
	video_codec_id = AV_CODEC_ID_NONE;
	audio_codec_id = AV_CODEC_ID_NONE;
	fps = 0;
	curFps = 0;
	maxFps = 0;
	time_tStart = 0;
	timeStart = timeEnd = 0;
	isEnableWriteLog = false;
	
	isInitForDecode = false;
	isInitForEncode = false;
	avPacketList.reserve(10);
}

static int interrupt_cb(void *ctx)
{
	AVFormatContext* formatContext = reinterpret_cast<AVFormatContext*>(ctx);
	//timeout after 5 seconds of no activity
	if (formatContext != NULL && formatContext->timestamp > 0 && (GetTickCount() - formatContext->timestamp > 5000))
		return 1;
	return 0;
}

UINT32 FFMPEGReadFrame(StreamCommon * _streamCommon)
{
	if (_streamCommon->packet.data != NULL)
	{
		av_packet_unref(&_streamCommon->packet);
	}
	av_init_packet(&_streamCommon->packet);
	_streamCommon->packet.size = 0;
	_streamCommon->packet.data = NULL;
	_streamCommon->pFormatCtx->timestamp = GetTickCount();
	int ret = AVERROR(EAGAIN);
	while (AVERROR(EAGAIN) == ret)
		ret = av_read_frame(_streamCommon->pFormatCtx, &_streamCommon->packet);
	if (ret < 0)
	{
		char err[AV_ERROR_MAX_STRING_SIZE];
		av_make_error_string(err, AV_ERROR_MAX_STRING_SIZE, ret);
		String^ errStr = gcnew String(err);
		LogWriter::Instance->WriteLog(2, "Error in function av_read_frame() return = " + errStr, __LINE__, __FUNCDNAME__);
		return (int)RESULT_ENUM::RS_NOT_OK;
	}
	if (_streamCommon->packet.stream_index != _streamCommon->videoStreamInd)
	{
		av_packet_unref(&_streamCommon->packet);
		_streamCommon->packet.data = NULL;
		_streamCommon->packet.size = 0;
		return (int)RESULT_ENUM::RS_CONTINUE;
	}
	else if (_streamCommon->packet.size > 0)
	{
		if (_streamCommon->isFileStream)
		{
			if (_streamCommon->fps <= 0)
				_streamCommon->fps = 60;
			if (_streamCommon->fps > 0)
			{
				if (_streamCommon->timeStart == 0)
				{
					_streamCommon->timeStart = GetTickCount();
				}
				else
				{
					_streamCommon->timeEnd = GetTickCount();
					int duaration = _streamCommon->timeEnd - _streamCommon->timeStart;
					int timeSleep = 1000 / _streamCommon->fps;
					int timeNeedToSleep = timeSleep - duaration;
					if (timeNeedToSleep > 0)
						Sleep(timeNeedToSleep);
					_streamCommon->timeStart = GetTickCount();
				}
			}
		}
		struct __timeb64 nTimeb;
		_ftime64_s(&nTimeb);
		_streamCommon->packet.duration = (int)(1000.0 * (nTimeb.time - _streamCommon->time_b.time)
			+ (nTimeb.millitm - _streamCommon->time_b.millitm));
		memcpy(&_streamCommon->time_b, &nTimeb, sizeof(__timeb64));
		if (_streamCommon->time_tStart == 0)
			_streamCommon->time_tStart = _streamCommon->time_b.time;
		return (int)RESULT_ENUM::RS_OK;
	}
	else
	{
		return (int)RESULT_ENUM::RS_NOT_OK;
	}
}

void FfmpegFreePacket(StreamCommon* _cameraCommon, AVPacket &_packet, uint8_t* &_packetPointer, bool _isFreePFrame)
{
	_packet.data = _packetPointer;
	if (_packet.data != NULL)
	{
		av_packet_unref(&_packet);
		_packetPointer = NULL;
	}
	else
	{
		if (_packetPointer != NULL)
		{
			delete[] _packetPointer;
			_packetPointer = NULL;
			_packet.data = NULL;
		}
	}
	if (_isFreePFrame)
	{
		if (_cameraCommon->pFrame != NULL)
		{
			av_frame_free(&_cameraCommon->pFrame);
			_cameraCommon->pFrame = NULL;
		}
	}
}

UINT32 StreamCommon::OpenVideoRTPStream(char* streamRequest)
{
	AVDictionary *opts = NULL;
	try
	{
		if (streamRequest[0] == STR_NULL)
			return (int)RESULT_ENUM::RS_NOT_OK;
		char strErrLog[STR_LENGTH_256 * 2];
		const int BUFSIZE = 5000000;
		av_dict_set_int(&opts, "rw_timeout", 10000000, 0);
		av_dict_set_int(&opts, "tcp_nodelay", 1, 0);
		av_dict_set_int(&opts, "stimeout", 10000000, 0);
		av_dict_set(&opts, "user_agent", "Mozilla/5.0", 0);

		av_dict_set(&opts, "rtsp_transport", "tcp", 0);
		av_dict_set(&opts, "rtsp_flags", "prefer_tcp", 0);
		av_dict_set_int(&opts, "buffer_size", BUFSIZE, 0);

		int err = -1;
		int loop = 2;
		while (err != 0 && loop > 0)
		{
			if (pFormatCtx != NULL) {
				avformat_close_input(&pFormatCtx);
				pFormatCtx = NULL;
			}
			pFormatCtx = avformat_alloc_context();
			pFormatCtx->timestamp = GetTickCount();
			pFormatCtx->interrupt_callback.opaque = pFormatCtx;
			pFormatCtx->interrupt_callback.callback = interrupt_cb;
			pFormatCtx->max_analyze_duration = 0;
			int err = -1;
			if(strcmp(streamRequest, "desktop") == 0)
			{
				/*test*/
				AVDictionary* options = NULL;
				AVInputFormat * pAVInputFormat = av_find_input_format("gdigrab");
				if (pAVInputFormat != NULL)
				{
					err = avformat_open_input(&pFormatCtx, "desktop", pAVInputFormat, &options);
				}
				else
					return (int)RESULT_ENUM::RS_NOT_OK;
			}
			else
				err = avformat_open_input(&pFormatCtx, streamRequest, NULL, &opts);
			if (err == 0)
			{
				//Retrieve stream information
				//avformat_find_stream_info
				//This function has been commented due to deadlock 
				avformat_find_stream_info(pFormatCtx, NULL);
				pFormatCtx->flags |= AVFMT_FLAG_NONBLOCK;
				//pFormatCtx->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
				pFormatCtx->flags |= AVFMT_FLAG_NOBUFFER;
				// Dump information about file onto standard error
				av_dump_format(pFormatCtx, 0, streamRequest, 0);
				// Find the first video stream		
				if (opts != NULL) {
					av_dict_free(&opts);
					opts = NULL;
				}
				for (int i = 0; i < pFormatCtx->nb_streams; i++)
				{
					if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
					{
						videoStreamInd = i;
						video_codec_id = pFormatCtx->streams[i]->codecpar->codec_id;
						if (avCodecParameters.extradata != NULL && avCodecParameters.extradata_size > 0) {
							av_free(avCodecParameters.extradata);
							memset(&avCodecParameters, 0, sizeof(AVCodecParameters));
						}
						avcodec_parameters_copy(&avCodecParameters, pFormatCtx->streams[i]->codecpar);
						int temp = (int)avCodecParameters.codec_id;
						LogWriter::Instance->WriteLog(1, "Codec id = " + temp, __LINE__, __FUNCDNAME__);
						break;
					}
				}
				return (int)RESULT_ENUM::RS_OK;
			}
			else
			{
				char strErr[STR_LENGTH_256];
				av_strerror(err, strErr, STR_LENGTH_256);
				if (std::abs(err) == AVERROR(AVERROR_INVALIDDATA)) {
					return (int)RESULT_ENUM::RS_INVALID_DATA_INPUT; //failed	
				}
				loop--;
			}
		}
		return (int)RESULT_ENUM::RS_NOT_OK;
	}
	catch (...)
	{
		if (opts != NULL)
			av_dict_free(&opts);
	}
}

UINT32 StreamCommon::AVPacketReadFrame()
{
	try
	{
		int res = (int)FFMPEGReadFrame(this);
		return res;
	}
	catch (...)
	{
		return (int)RESULT_ENUM::RS_NOT_OK;
	}
	return (int)RESULT_ENUM::RS_NOT_OK;
}

VOID StreamCommon::ReleasePacket()
{
	for (int i = 0; i < avPacketList.size(); i++)
	{
		AVPacket* avPacket = (AVPacket*)avPacketList[i];
		av_packet_free(&avPacket);
	}
	avPacketList.clear();
}

bool IsNumber(String^ str)
{
	try
	{
		Convert::ToInt32(str);
		return true;
	}
	catch (Exception^)
	{
		return false;
	}
}

String^ ReplaceRtspPortForUri(String^ _uri, int _rtspPort)
{
	String^ uri = gcnew String(_uri);
	if (_rtspPort <= 0 && _rtspPort >= 65536) //invalid port
	{
		return uri;
	}

	try
	{
		if (_uri->Substring(0, 7) != "rtsp://")
		{
			return _uri;
		}
		//rtsp://admin:admin@192.168.1.91:591/av0_0
		String^ newUri = "rtsp://";
		String^ subUri = uri->Substring(7); //skip "rtsp://" => admin:admin@192.168.1.91:591/av0_0

		cli::array<String^>^ arrStr = subUri->Split('/'); // admin:admin@192.168.1.91:591 and av0_0 and ...

		cli::array<String^>^ arrStrRtspPort = arrStr[0]->Split(':'); //admin; admin@192.168.1.91 and 591
		String^ rtspPortStr = arrStrRtspPort[arrStrRtspPort->Length - 1];

		if (IsNumber(rtspPortStr)) //replace this port by new port
		{
			//rtsp://admin:admin@192.168.1.91:591/av0_0
			String^ newArrStr_0 = "";
			for (int i = 0; i < arrStrRtspPort->Length - 1; i++)
			{
				newArrStr_0 += arrStrRtspPort[i] + ":";
			}
			newArrStr_0 += _rtspPort;// 192.168.1.91:rtspPort
			newUri += newArrStr_0; // rtsp://192.168.1.91:rtspPort

			for (int i = 1; i < arrStr->Length; i++)
			{
				newUri += "/" + arrStr[i];
			}
		}
		else
		{
			//rtsp://admin:@diemhensaigonquan.dyndns.org/user=admin_password=tlJwpbo6_channel=1_stream=0.sdp?real_stream
			String^ newArrStr_0 = arrStr[0] + ":" + _rtspPort;
			newUri += newArrStr_0;
			for (int i = 1; i < arrStr->Length; i++)
			{
				newUri += "/" + arrStr[i];
			}
		}
		return newUri;
	}
	catch (Exception^)
	{
		return uri;
	}
}
