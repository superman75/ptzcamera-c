#pragma once
#include "CommonExportMacro.h"
#include "DefineBase.h"
#include "SDLRender\FramePresenterSDL.h"
#include <memory>
#include <list>
#include <sys\timeb.h> 
#include "CpuDec/FFmpegCpuDecoder.h"
#include "CpuEnc/FFmpegCpuEncoder.h"

extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#  define __STDC_CONSTANT_MACROS
#endif
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>
#include <libavdevice/avdevice.h>
}
using namespace std;
using namespace System;

class ISF_API StreamCommon
{
public:
	char cameraID[STR_LENGTH_64];
	int videoStreamInd;
	Resolution2D resolution;
	AVFormatContext *pFormatCtx;
	AVFrame			*pFrame;
	AVCodecParameters avCodecParameters;
	AVPacket		packet;
	AVCodecID		video_codec_id;
	AVCodecID		audio_codec_id;

	__timeb64		time_b;
	__time64_t		time_tStart;
	bool			isGetIFrame;
	bool			isFileStream;
	int				deviceType;
	double			fps;
	int				curFps;
	int				maxFps;
	DWORD			timeStart;
	DWORD			timeEnd;
	bool			isEnableWriteLog;
	int				logLevel;


	/* Define for decode here */
	bool isInitForDecode;
	FFmpegCpuDecoder cpuDecoder;

	/* Define for encode here */
	bool isInitForEncode;
	FFmpegCpuEncoder cpuEncoder;
	std::vector<UINT64> avPacketList;

	StreamCommon();
	~StreamCommon();
	VOID Reset();
	UINT32 OpenVideoRTPStream(char* streamRequest);
	UINT32 AVPacketReadFrame();
	VOID ReleasePacket();
};

bool IsNumber(String^ str);
String^ ReplaceRtspPortForUri(String^ _uri, int _rtspPort);
