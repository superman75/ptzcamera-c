#include "stdafx.h"

#include "Common.h"




Common::CommonRef::CommonRef()
{

}

Common::CommonRef::~CommonRef()
{

}

int Common::CommonRef::InitLibs()
{
	//No need to init FFMPEG libs anymore from version 4.0
	//Init SDL - begin
	//Initialize PNG loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		LogWriter::Instance->WriteLog(2, "SDL_image could not initialize!", __LINE__, __FUNCDNAME__);
	}
	//Initialize SDL_ttf
	if (TTF_Init() == -1)
	{
		LogWriter::Instance->WriteLog(2, "SDL_ttf could not initialize!", __LINE__, __FUNCDNAME__);
	}
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");
	SDL_Init(SDL_INIT_EVERYTHING); //TienVo added for SDL init			
	SDL_version compiled;
	SDL_version linked;
	SDL_VERSION(&compiled);
	SDL_GetVersion(&linked);
	//LogWriter::Instance->WriteLog(2, "We compiled against SDL version " + compiled.major + "." + compiled.minor + "."
		//+ compiled.patch, __LINE__, __FUNCDNAME__);
	//LogWriter::Instance->WriteLog(2, "But we linked against SDL version " + linked.major + "." + linked.minor + "."
		//+ linked.patch, __LINE__, __FUNCDNAME__);
	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2"))
	{
		LogWriter::Instance->WriteLog(2, "SDL_SetHint fail", __LINE__, __FUNCDNAME__);
	}
	//Init SDL - end
	return 0;
}
