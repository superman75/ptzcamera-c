#pragma once

#include "CommonExportMacro.h"
#include "DefineBase.h"

class ISF_API TimeOutHandler
{
private:
	DWORD timeStart;
	bool isSupervise;
public:
	TimeOutHandler();
	~TimeOutHandler();
	void Reset();
	bool IsTimeout();
	static int DecodeIsrCb(void * t);
};
