#include "stdafx.h"

TimeOutHandler::TimeOutHandler()
{
	Reset();
}

TimeOutHandler::~TimeOutHandler()
{
	
}

void TimeOutHandler::Reset()
{
	timeStart = GetTickCount();
	isSupervise = true;
}

bool TimeOutHandler::IsTimeout()
{
	try
	{
		if (isSupervise && (GetTickCount() - timeStart > 5000))
		{
			isSupervise = false;
			return true;
		}
		else
		{
			return false;
		}
	}
	catch (...)
	{
		return false;
	}
}

int TimeOutHandler::DecodeIsrCb(void * t)
{
	try
	{
		AVFormatContext* formatContext = reinterpret_cast<AVFormatContext*>(t);
		if (t && static_cast<TimeOutHandler *>(t)->IsTimeout())
		{
			return 1;
		}
		return 0;
	}
	catch (...)
	{
		return 1;
	}
}
