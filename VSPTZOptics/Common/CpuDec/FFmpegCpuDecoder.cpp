#include "stdafx.h"
#include "FFmpegCpuDecoder.h"

using namespace ShareLibrary::Base;

FFmpegCpuDecoder::FFmpegCpuDecoder()
{
	Reset();
}

FFmpegCpuDecoder::~FFmpegCpuDecoder()
{
	Destroy();
}

AVCodecParameters* FFmpegCpuDecoder::GetAVCodecPar()
{
	return &avCodecParameters;
}

VOID FFmpegCpuDecoder::Reset()
{
	pCodecCtx = NULL;
	pCodec = NULL;
	pFormatCtx = NULL;
	memset(&avCodecParameters, 0, sizeof(AVCodecParameters));
	avFrameList.reserve(5);// reserver 5 items
}

VOID FFmpegCpuDecoder::Destroy()
{
	ReleaseFrame();
	if (pFormatCtx != NULL){
		avformat_close_input(&pFormatCtx);
		pFormatCtx = NULL;
	}
	else{
		if (pCodecCtx != NULL){
			avcodec_free_context(&pCodecCtx);
			pCodecCtx = NULL;
		}
	}
	if (avCodecParameters.extradata != NULL && avCodecParameters.extradata_size > 0){
		delete avCodecParameters.extradata;
		avCodecParameters.extradata = NULL;
		memset(&avCodecParameters, 0, sizeof(AVCodecParameters));
	}
}

VOID FFmpegCpuDecoder::ReleaseFrame()
{
	for (int i=0; i < avFrameList.size(); i++)
	{
		AVFrame* avFrame = (AVFrame*)avFrameList[i];
		av_frame_free(&avFrame);
	}
	avFrameList.clear();
}


VOID FFmpegCpuDecoder::FfmpegFreePacket(AVPacket* _packet, uint8_t* &_packetPointer, bool _isFreePFrame, bool _isMalocPacket)
{
	if (_isMalocPacket)
		av_packet_unref(_packet);
	else{
		if (_packetPointer != NULL){
			delete[] _packetPointer;
			_packetPointer = NULL;			
		}
	}
	_packet->data = NULL;
	_packet->size = 0;
	if (_isFreePFrame)
	{
		this->ReleaseFrame();
	}
}


Std_ReturnType FFmpegCpuDecoder::FFmpegCpuDecoderInit(AVCodecParameters* _avCodecParam)
{
	try
	{
		this->Destroy();
		if (_avCodecParam != NULL)
		{
			memcpy(&this->avCodecParameters, _avCodecParam, sizeof(AVCodecParameters));
			this->avCodecParameters.extradata = _avCodecParam->extradata;
			_avCodecParam->extradata = NULL;
			_avCodecParam->extradata_size = 0;
			this->codecId = (AVCodecID)this->avCodecParameters.codec_id;
		}
		if (this->codecId <= AV_CODEC_ID_NONE)
			return E_NOT_OK;
		this->pCodec = avcodec_find_decoder((AVCodecID)this->codecId);
		if (!this->pCodec)
		{
			LogWriter::Instance->WriteLog(1, "Error in function avcodec_find_encoder()", __LINE__, __FUNCDNAME__);
			return E_NOT_OK;
		}
		this->pCodecCtx = avcodec_alloc_context3(this->pCodec);
		if (!this->pCodecCtx)
		{
			LogWriter::Instance->WriteLog(1, "Error in function avcodec_alloc_context3()", __LINE__, __FUNCDNAME__);
			return E_NOT_OK;
		}
		if (_avCodecParam != NULL && this->avCodecParameters.codec_id != AV_CODEC_ID_NONE)
		{			
			if (avcodec_parameters_to_context(this->pCodecCtx, &this->avCodecParameters) < 0)
			{
				LogWriter::Instance->WriteLog(1, "Error in function avcodec_parameters_to_context()", __LINE__, __FUNCDNAME__);
				return E_NOT_OK;
			}
		}
		else
		{
			this->pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;
		}
		this->pCodecCtx->workaround_bugs = 1;
		this->pCodecCtx->flags2 |= AV_CODEC_FLAG2_FAST | AV_CODEC_FLAG_LOW_DELAY;// | ffmpeg.AV_CODEC_FLAG2_CHUNKS;

		//if ((this->pCodec->capabilities & AV_CODEC_CAP_DR1) != 0)
			//this->pCodecCtx->flags |= CODEC_FLAG_EMU_EDGE;
		if ((this->pCodec->capabilities & AV_CODEC_CAP_TRUNCATED) == AV_CODEC_CAP_TRUNCATED)
			this->pCodecCtx->flags |= AV_CODEC_FLAG_TRUNCATED;

		if (avcodec_open2(this->pCodecCtx, this->pCodec, NULL) < 0)
		{
			LogWriter::Instance->WriteLog(1, "Error in function avcodec_open()", __LINE__, __FUNCDNAME__);
			return E_NOT_OK;
		}
		return E_OK;
	}
	catch (...)
	{
		return E_NOT_OK;
	}
	return E_NOT_OK;
}


Std_ReturnType FFmpegCpuDecoder::AVAsyncDecoder(int *_got_frame, AVPacket *_pkt)
{
	int ret = -1;
	*_got_frame = 0;
	this->ReleaseFrame();
	if (_pkt)
	{
	resend:
		ret = avcodec_send_packet(this->pCodecCtx, _pkt);
		// In particular, we don't expect AVERROR(EAGAIN), because we read all
		// decoded frames with avcodec_receive_frame() until done.
		if (ret == AVERROR(EAGAIN)) {
			if (*_got_frame == 0)
			{
				AVFrame* avFrame = av_frame_alloc();
				ret = avcodec_receive_frame(this->pCodecCtx, avFrame);
				if (ret < 0 && ret != AVERROR(EAGAIN)) {
					av_frame_free(&avFrame);
					return E_NOT_OK;
				}
				if (ret >= 0) {
					this->avFrameList.push_back((UINT64)avFrame);
					*_got_frame = 1;
					LogWriter::Instance->WriteLog(2, "Resend AVpacket", __LINE__, __FUNCDNAME__);
					goto resend;
				}
				else
					av_frame_free(&avFrame);
			}
			return E_OK;
		}
		else if (ret < 0 && ret != AVERROR_EOF)
			return E_NOT_OK;
	}
	if (*_got_frame == 0)
	{
		do
		{
			AVFrame* avFrame = av_frame_alloc();
			ret = avcodec_receive_frame(this->pCodecCtx, avFrame);
			if (ret < 0 && ret != AVERROR(EAGAIN))
			{
				av_frame_free(&avFrame);
				return E_NOT_OK;
			}
			if (ret >= 0)
			{
				this->avFrameList.push_back((UINT64)avFrame);
				*_got_frame = 1;
			}
			else
				av_frame_free(&avFrame);
		} while (ret >= 0);
	}
	return E_OK;
}

Std_ReturnType FFmpegCpuDecoder::FFmpegRunAsynDecoder(AVPacket* _pPacket, bool _isMalocPacket)
{
	try
	{
		int         frameFinished = 0;
		uint8_t*	packetPointer;
		if (_pPacket != NULL)
		{
			packetPointer = _pPacket->data;
			if (AVAsyncDecoder(&frameFinished, _pPacket) != E_OK)
			{
				FfmpegFreePacket(_pPacket, packetPointer, true, _isMalocPacket);
				return E_NOT_OK;
			}
			if (frameFinished)
			{
				FfmpegFreePacket(_pPacket, packetPointer, false, _isMalocPacket);
				return E_OK;
			}
			else
			{
				FfmpegFreePacket(_pPacket, packetPointer, false, _isMalocPacket);
				return E_CONTINUE;
			}
		}
		else
		{
			return E_NOT_OK;
		}
	}
	catch (System::Exception^ e)
	{
		if (e != nullptr)
		{
			LogWriter::Instance->WriteLog(1, e->Message + e->StackTrace, __LINE__, __FUNCDNAME__);
		}
		return E_NOT_OK;
	}
	return E_NOT_OK;
}
