#pragma once

#include "CommonExportMacro.h"
#include "DefineBase.h"

#include <memory>
#include <vector>

extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#  define __STDC_CONSTANT_MACROS
#endif
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>
}


class ISF_API FFmpegCpuDecoder
{
private:
	VOID FFmpegCpuDecoder::FfmpegFreePacket(AVPacket* _packet, uint8_t* &_packetPointer, bool _isFreePFrame, bool _isMalocPacket = false);
	Std_ReturnType FFmpegCpuDecoder::AVAsyncDecoder(int *_got_frame, AVPacket *_pkt);
	AVCodecID		codecId;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;

	AVFormatContext		*pFormatCtx;
	AVCodecParameters	avCodecParameters;
	VOID Reset();
	VOID Destroy();
public:
	std::vector<UINT64> avFrameList;
	FFmpegCpuDecoder();
	~FFmpegCpuDecoder();
	Std_ReturnType FFmpegCpuDecoder::FFmpegCpuDecoderInit(AVCodecParameters* _avCodecParam);
	Std_ReturnType FFmpegCpuDecoder::FFmpegRunAsynDecoder(AVPacket* _pPacket, bool _isMalocPacket = false);
	AVCodecParameters* GetAVCodecPar();
	VOID ReleaseFrame();
};
