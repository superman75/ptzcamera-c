#pragma once
#ifndef __DEFINE_BASE_
#define __DEFINE_BASE_

//DECLARE TYPE DEFINATIONS
#ifdef HW_ACC_ENABLED
typedef struct CUFrameStruct {
	int width, height;
	unsigned char *pFrame;
}CUFrame;

#endif

//DECLARE DEFINITIONS AND ENUMS

#define		PI		3.14159

#define		STD_ON								1
#define		STD_OFF								0

#define     E_OK								int(0)
#define		E_CONTINUE							int(1)
#define		E_NOT_OK							int(-1)

typedef		int								Std_ReturnType;

#define		MAX_CLIENT_CONNECTIONS			61	//MAXIMUM_WAIT_OBJECTS - 3
#define		MAX_IP_CAMERA_SUPPORT			256
#define		MAX_CHANNEL_SUPPORT				256

//For Frame Differencing Algorithm - Begin
#define		PIXEL_MOTION_THRESHOLD				30
#define		FRAME_MOTION_THRESHOLD_PERCENT		1 //1%
//For Frame Differencing Algorithm - End

#define		HTTP_SUCCESSFUL_CODE			200
#define		MAX_HTTP_REQUEST_STR_LENGTH		1000
#define		MAX_HTTP_HEADER_LENGTH			3000
#define		MAX_HTTP_ONELINE_LENGTH			1000

#define		LENGTH_OF_BLOCK_RECEIVE			5120	// = 5KB
#define		MAX_HEADER_LINE_LENGTH			1000	
#define		WINDOW_HOR_SCAN_THREHOLD		3
#define		WINDOW_VIR_SCAN_THREHOLD		5
#define		MIN_TIME_STAND_PTZ				2000 //ms
#define		MIN_TIME_STAND_PTZ2				200 //ms
#define		MAX_TIME_STAND_PTZ				10000 //ms
#define		STEP_TIME_STAND_PTZ				700 //ms
#define		NUM_STEPS_PER_PTZ				3
#define		NUM_SCALE_SPEED_PTZ				9
#define		GROUP_OF_PICTURE				12

#define		MAX_IO_CONTROL_SUPPORT		    100 //van thoa added
#define		MAX_SMART_CONTROL_CONTACT		8 //van thoa added
#define		MAX_LOVAD_CONTROL_CONTACT		10 //van thoa added

#define		DEFAULT_NUM_OF_DIVISIONS			4

#define		DEFAULT_MAIN_PORT_CLIENT			10000
#define		DEFAULT_VIDEO_PORT_CLIENT			10001
#define		DEFAULT_SUB_VIDEO_PORT_CLIENT		10002
#define		DEFAULT_AUDIO_PORT_CLIENT			10010
#define		DEFAULT_HTTP_PORT					10080
#define		DEFAULT_MAX_CMM_DATA_PACKAGE_SIZE	10485760 //10MB
#define		MAX_LIST_REC_PACKAGE_SIZE			2* 1024 * 1024//2 M

#define		MAX_CAMERA_RESOLUTION_SUPPORT					30
#define		MAX_CAMERA_PROFILE_SUPPORT						30
#define		MAX_TIME_WAIT_FOR_IP_CAMERA_THREAD				INFINITE
#define		MAX_TIME_WAIT_FOR_TERMINATE_IP_CAMERA_THREAD	20000 //20s
#define		MAX_TIME_WAIT_FOR_FRAME							5000 //5S	

//FOR STRING LENGTH - BEGIN
#define		STR_LENGTH_16						16
#define		STR_LENGTH_32						32
#define		STR_LENGTH_64						64
#define		STR_LENGTH_128						128
#define		STR_LENGTH_256						256
//FOR STRING LENGTH - END

#define		MAX_USER_ACCOUNT					20
#define		MAX_USER_NAME_LENGTH				STR_LENGTH_64
#define		MAX_PASSWORD_LENGTH					STR_LENGTH_64


#define PRE_RECORD_POINT						10 //sec
#define POST_RECORD_POINT						10 //sec
#define RECORD_FILE_NAME_IPCAMERA				"Record.cfg"
#define RECORD_FILE_NAME_IPCAMERA_INFO			"CamInfo.cfg" //TienVo added on Dec 29, 2012
#define RECORD_FILE_NAME_CHANNEL_INFO			"ChanInfo.cfg" //TienVo added on Jan 27, 2013
#define RECORD_FILE_NAME_CHANNEL_INFO_BK		"ChanInfo.cfg_BK" //TienVo added on Jan 27, 2013
#define MAX_DAY_OF_MONTH						32
#define MAX_MONTH_OF_YEAR						13 //TienVo added on Sep 12, 2012
#define MAX_TIME_REC_OF_DAY						100
#define NAME_RECORD								"CAMERA" //TienVo added on Jun 30, 2012
#define DAY_RECORD								"%Y_%m_%d" //TienVo added on Jan 24, 2013
#define HOUR_REC								"%H" //TienVo added on Jan 24, 2013
#define MINUTE_REC								"%M" //TienVo added on Jan 24, 2013
#define SECOND_REC								"%S" //TienVo added on Jan 24, 2013
#define TYPE_REC_OFFSET							0
#define TYPE_REC_MASK							0x03 //00000011b
#define SEG_REC_OFFSET							2
#define SEG_REC_MASK							0x0C //00001100b
#define REC_OFFSET								4
#define REC_MASK								0x30 //00110000b
#define MODE_REC_OFFSET							6
#define MODE_REC_MASK							0xC0 //11000000b
#define MOTION_REC_OFFSET						8
#define MOTION_REC_MASK							0x0100 //0000000100000000b
#define EVENT_REC_OFFSET						9
#define EVENT_REC_MASK							0x0200 //0000001000000000b

#define TYPE_PB_OFFSET							0
#define TYPE_BP_MASK							0x03 //00000011b
#define PB_OFFSET								2
#define PB_MASK									0x0C //00001100b
#define MIN_SPACE_ALLOW_REC						1024 //MB
#define UPDATE_REC_INFO_PERIOD					20 //MB
//TienVo added - Jun 20, 2012 - END
//FOR CONFIGURE FILE PATH - BEGIN
//TienVo added for camera info - begin
#define IP_EVENT_OFFSET									0
#define MP_OFFSET										1
#define CLIENT_PROCESS_OFFSET							2
#define IP_OFFSET										3
#define SNAPSHOT_EVENT_OFFSET							4
//TienVo added for camera info - end

#define LOG_FILES_DIRECTORY								"Logs\\"
#define LOG_FILE_NAME_RUNTIME_ERROR						"runtime_error_logs.txt"
#define LOG_FILE_NAME_RUNTIME_ERROR_DVR_SUPERVISOR		"runtime_error_logs_dvr_supervisor.txt"
#define MAX_LOG_FILE_SIZE								1024*1024*5   //5MB
#define MAX_LOG_FILE_REC_SIZE							1024*1024*5   //5MB
//TienVo added
#define LOG_PB_FILE_NAME_RUNTIME_ERROR					"runtime_play_back_logs.txt"
#define LOG_REC_FILE_NAME_RUNTIME_ERROR					"runtime_record_logs.txt"
#define LOG_BACKUP_FILE_NAME_RUNTIME_ERROR				"runtime_backup_logs.txt"
#define LOG_ADD_FILE_NAME_RUNTIME_ERROR					"runtime_address_logs.txt"

#define STR_NULL										'\0'
#define	DVR_SERVER_PROCESS_NAME							"DVMSServer.exe"
#define	DVR_SUPERVISOR_PROCESS_NAME						"DMVSSupervisor.exe"
#define	MOBOTIX_APP_PROCESS_NAME						"MobotixLV.exe"
#define	KEY_REGISTRY_PROCESS_NAME						"KeyRegistry.exe"
#define PROCESS_ACCESS_RIGHT_FULL						PROCESS_QUERY_INFORMATION | PROCESS_TERMINATE | PROCESS_CREATE_PROCESS
#define  DEFAULT_ALARM_SOUND_STR						"Default"
#define	CRASH_ERROR_REPORT_PROCESS_NAME					"werfault.exe"

#define     MAX_FRAME_PER_MIN							60*50 
#define     MAX_FRAME_PER_5_MIN							5*60*100 
#define     SECOND_PER_5_MIN							60*5 
#define		MAX_FRAME_SIZE								50*1024 //100 KB
#define		MAX_NUM_THREAD_REC							20 

#define DEFAULT_MAX_QUEUE_LENGTH_MAIN_THREAD					500
#define DEFAULT_MAX_QUEUE_LENGTH_VIDEO_ANALYTICS_THREAD			30
#define DEFAULT_MAX_QUEUE_LENGTH_SEND_VIDEO_TO_CLIENT_THREAD	125

/* TienVo added Begin */
#define MAX_MSG_IN_QUEUE_ALLOW_REC		50 //frame
#define MAX_TIME_RECORD_BLOCK 2*60*1000 //ms
#define MIN_TIME_RECORD_BLOCK 2*1000 //ms Added on 12 June, 2012 by TienVo
#define MAX_WATCH_DOG_TIMER		  20
#define VIDEO_BLOCK_LENGTH 30
#define MAX_FRAME_DECODE 100

#endif
