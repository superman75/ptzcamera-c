/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = ThreadController.h                                          */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018 Areth Software Ltd.        All rights reserved.					  */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class decleration				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Areth Software Ltd., the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0:  15-Dec-2018  : Initial Version
*/
/******************************************************************************/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#pragma once
#include "CommonExportMacro.h"
#include "CriticalSection.h"
#include <queue>
#include <string>

/*******************************************************************************
**                      Using Namespace Section                              **
*******************************************************************************/
using namespace std;


/*******************************************************************************
**                      Class Defination Section							  **
*******************************************************************************/

class ISF_API ThreadController
{
private:
	/* Critical section to protect RAM data access in thread process */
	CriticalSection crsRamProtection;
	/* Flag checking whether thread is running */
	bool blIsThreadRunning;	
	/* Thread name to write log message */
	string strThreadName;
	/* List of thread parameter used in thread process */
	vector<UINT64> vArgList;
	/* Free all memory of thread */
	void ResetVars();
public:
	/* Flag to indidate thread already started */
	bool blIsThreadStarted;	
	/* Hold Thread handler */ 
	HANDLE hThreadHandler;
	/* Hold Thread address */
	UINT32 u32threadAddress;	
	/* Wake up event of thread */
	HANDLE hWakeUpThread;
	/* Flag to indicate all data in thread process already intialized */
	BOOLEAN blIsDataInit;
	/* Queue data used in thread */
	CriticalSection crsBuffer;
	deque<UINT64>		queueBuffer;

	/* Public function in thread param class */
	void InitThreadArgs(const UINT64* _pu64Params, UINT8 _u8NumPars);
	void Start(unsigned(__stdcall* _fpThreadProcess)(void*));
	void Stop();
	void SetIsThreadRunning(bool _blValue);
	bool GetIsThreadRunning();
	string GetThreadName();
	void SetThreadName(string _strThreadName);
	template<class T> void ClearQueue()
	{
		/* Remove all elments in queque */
		while (queueBuffer.size() > 0)
		{
			T* tObjFromQueue = (T*)queueBuffer.front();
			queueBuffer.pop_front();
			delete tObjFromQueue;
		}
		/* Clear buffer queue */
		queueBuffer.clear();
		/*std::queue<UINT64> empty;
		std::swap(queueBuffer, empty);*/
	};

	/* Class constructor and destructor */
	ThreadController();
	~ThreadController();
};
