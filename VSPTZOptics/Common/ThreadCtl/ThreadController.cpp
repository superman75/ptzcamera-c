/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = ThreadController.cpp                                        */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018 Areth Software Ltd.        All rights reserved.                   */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class definations				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Areth Software Ltd., the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0:  15-Dec-2018  : Initial Version
*/
/******************************************************************************/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "stdafx.h"
#include "ThreadController.h"
#include <process.h>
#include <string>

/*******************************************************************************
**                      Using Namespace Section                              **
*******************************************************************************/
using namespace ShareLibrary::Base;
using namespace System;


/******************************************************************************
**                   Class Methods Defination Section						 **
*******************************************************************************/
string ThreadController::GetThreadName()
{
	return strThreadName;
}

ThreadController::ThreadController()
{
	ResetVars();
}

void ThreadController::SetThreadName(string _strThreadName)
{
	strThreadName = _strThreadName;
}

ThreadController::~ThreadController()
{
	Stop();
}

void ThreadController::ResetVars()
{
	u32threadAddress = 0;
	blIsDataInit = false;
	blIsThreadRunning = false;
	blIsThreadStarted = false;
	hThreadHandler = NULL;
	hWakeUpThread =
		CreateEvent(NULL, false, false, NULL);
	vArgList.reserve(5);//Reserve 5 items element.	
}

void ThreadController::SetIsThreadRunning(bool _blValue)
{
	try {
		crsRamProtection.Enter();
		blIsThreadRunning = _blValue;
	}
	finally{
		crsRamProtection.Leave();
	}
}

bool ThreadController::GetIsThreadRunning()
{
	return blIsThreadRunning;
}

void ThreadController::InitThreadArgs(const UINT64* _pu64ArgList, UINT8 _u8NumArgs)
{
	/* Store parameters for thread process */
	UINT8 u8ArgIdx = 0;
	/* Push all parameters of thread process in list */
	while (u8ArgIdx < _u8NumArgs)
	{
		vArgList.push_back(_pu64ArgList[u8ArgIdx]);
		u8ArgIdx++;
	}
}

void ThreadController::Start(unsigned(__stdcall* _fpThreadProcess)(void*))
{
	if (GetIsThreadRunning() == false)
	{
		SetIsThreadRunning(true);
		hThreadHandler = (HANDLE)_beginthreadex(NULL, 0, 
			(unsigned(__stdcall*)(void*))_fpThreadProcess, (void*)&this->vArgList[0], 0, &u32threadAddress);
		if (hThreadHandler == NULL)
		{
			u32threadAddress = 0;
			SetIsThreadRunning(false);
		}
	}
}

void ThreadController::Stop()
{
	if (GetIsThreadRunning() == true)
	{
		SetIsThreadRunning(false);
		SetEvent(hWakeUpThread);
		DWORD waitResult = WaitForSingleObject(hThreadHandler, 20000);
		if (waitResult == WAIT_TIMEOUT)
		{
			LogWriter::Instance->WriteLog(2, gcnew String(strThreadName.c_str()), __LINE__, __FUNCDNAME__);
			throw(gcnew Exception(gcnew String(strThreadName.c_str()) + " crash"));
		}
		else
		{
			CloseHandle(hThreadHandler);
			hThreadHandler = NULL;
		}
	}
}
