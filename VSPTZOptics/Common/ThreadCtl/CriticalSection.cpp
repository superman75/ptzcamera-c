#include "stdafx.h"
#include "CriticalSection.h"

CriticalSection::CriticalSection(DWORD _spinCount)
{
	/*
	_spinCount is good for multi-processor systems and for short duration Critical Section.default Default is 4000.
	But it need assigned another values for each case, need test this more.
	By calling InitializeCriticalSection() => SpinCount default is 0 (zero)
	*/
	InitializeCriticalSectionAndSpinCount(&this->criticalSection, _spinCount);
}

CriticalSection::~CriticalSection()
{
	DeleteCriticalSection(&this->criticalSection);
}

void CriticalSection::Enter()
{
	EnterCriticalSection(&this->criticalSection);
}

void CriticalSection::Leave()
{
	LeaveCriticalSection(&this->criticalSection);
}