#pragma once
#ifndef __MY_CRITICAL_SECTION__
#define __MY_CRITICAL_SECTION__

#define _WINSOCKAPI_    // stops windows.h including winsock.h
#include "CommonExportMacro.h"
#include <windows.h> //Last

class ISF_API CriticalSection
{
private:
	CRITICAL_SECTION	criticalSection;

public:
	CriticalSection(DWORD _spinCount = 4000);
	~CriticalSection();
	void Enter();
	void Leave();
};

#endif
