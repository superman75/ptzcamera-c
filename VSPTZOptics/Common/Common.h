#pragma once
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
using namespace System;
using namespace ShareLibrary::Base;

namespace Common {

	public ref class CommonRef
	{
	public:
		CommonRef();
		~CommonRef();
		int InitLibs();
	};
}
