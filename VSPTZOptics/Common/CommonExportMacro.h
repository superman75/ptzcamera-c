#ifndef __COMMON_EXPORT_MACRO__
#define __COMMON_EXPORT_MACRO__

#ifdef COMMON_EXPORTS
#define ISF_API __declspec(dllexport)
#else
#define ISF_API __declspec(dllimport)
#endif

#endif