/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = ThreadController.h                                          */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018 Areth Software Ltd.        All rights reserved.					  */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class decleration				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Areth Software Ltd., the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0:  15-Dec-2018  : Initial Version
*/
/******************************************************************************/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#pragma once

/* Application header files */
#include "CommonExportMacro.h"
#include "DefineBase.h"

/* Lib header files */
extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#  define __STDC_CONSTANT_MACROS
#endif
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
}

/*******************************************************************************
**                      Using Namespace Section                              **
*******************************************************************************/
using namespace ShareLibrary::Base;

/*******************************************************************************
**                      Class Defination Section							  **
*******************************************************************************/
class ISF_API AVPacketInfo
{
private:
public:
	CriticalSection crs;
	PACKET_MODE_ENUM packetMode;
	AVPacket avPacket;
	bool isPacketRef;
	AVCodecParameters* codecPar;
	AVPacketInfo();
	~AVPacketInfo();
	VOID Destroy();
};


class ISF_API AVFrameInfo
{
private:
public:
	CriticalSection crs;
	AVFrame* avFrame;
	bool isImageAlloc;

#ifdef HW_ACC_ENABLED
	CUFrame cuFrame;
#endif

	AVFrameInfo();
	~AVFrameInfo();
	VOID Destroy();
};

ISF_API void AVPacketCopy(AVPacket * _dest, AVPacket* _src, bool _isLinkData = false);
