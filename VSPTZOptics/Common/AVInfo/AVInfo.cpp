﻿/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = ThreadController.cpp                                        */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018 Areth Software Ltd.        All rights reserved.                   */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class definations				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Areth Software Ltd., the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0:  15-Dec-2018  : Initial Version
*/
/******************************************************************************/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "stdafx.h"
#include "ThreadCtl/CriticalSection.h"
#include "AVInfo.h"


/*******************************************************************************
**                      Class Defination Section							  **
*******************************************************************************/
AVFrameInfo::AVFrameInfo()
{
	avFrame = NULL;
#ifdef HW_ACC_ENABLED
	cuFrame.pFrame = NULL;
#endif
	isImageAlloc = false;
}

AVFrameInfo::~AVFrameInfo()
{
	Destroy();
}

void AVFrameInfo::Destroy()
{
	crs.Enter();
	if (avFrame != NULL)
	{
		if (isImageAlloc)
		{
			av_freep(&avFrame->data[0]);
		}
		av_frame_free(&avFrame);
		avFrame = NULL;
	}
#ifdef HW_ACC_ENABLED
	if (cuFrame.pFrame != NULL)
	{
		cuFrame.pFrame = NULL;
	}
#endif
	crs.Leave();
}


AVPacketInfo::AVPacketInfo()
{
	codecPar = NULL;
	avPacket.data = NULL;
	avPacket.size = 0;
	packetMode = PACKET_MODE_ENUM::DRAW_DATA;
	isPacketRef = false;
}

AVPacketInfo::~AVPacketInfo()
{
	Destroy();
}

void AVPacketInfo::Destroy()
{
	if (codecPar != NULL){
		if (codecPar->extradata != NULL && codecPar->extradata_size > 0){
			delete[] codecPar->extradata;
			codecPar->extradata = NULL;
			codecPar->extradata_size = 0;
		}
		delete codecPar;
		codecPar = NULL;
	}
	if (avPacket.data != NULL && avPacket.size > 0){
		if (!isPacketRef)
			delete[] avPacket.data;
		else
			av_packet_unref(&avPacket);
		avPacket.data = NULL;
		avPacket.size = 0;
	}
}

void AVPacketCopy(AVPacket * _dest, AVPacket* _src, bool _isLinkData)
{
	if (_src != NULL && _dest != NULL)
	{
		_dest->pts = _src->pts;
		_dest->dts = _src->dts;
		_dest->size = _src->size;
		_dest->stream_index = _src->stream_index;
		_dest->flags = _src->flags;
		_dest->side_data_elems = _src->side_data_elems;
		_dest->duration = _src->duration;
		_dest->pos = _src->pos;
		_dest->buf = NULL;
		_dest->side_data = NULL;
		if (_isLinkData)
			_dest->data = _src->data;
		else
			_dest->data = NULL;
	}
}
