﻿using CameraView.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraView.Base
{
    public class ViewEventArgs : EventArgs
    {
        public CamView CView { get; set; }
        public EVENT_TYPE EventType { get; set; }
    }
}
