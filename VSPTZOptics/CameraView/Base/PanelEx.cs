﻿using CameraView.Model;
using NsRenderHandlerRef;
using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraView.Base
{
    public partial class PanelEx : DBPanel, IDisposable
    {
        public PanelEx()
        {
            isUsing = false;
            updatedTime = DateTime.Now;
            this.Dock = DockStyle.Fill;
            this.BackColor = Color.Black;
            this.Size = new Size(300, 300);
            this.Margin = new Padding(5, 5, 5, 5);
            this.AllowDrop = true;
            this.zoomFactor = 1;
        }
        private int index;
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        private CamView camView;
        public CamView CView
        {
            get { return camView; }
            set { camView = value; }
        }
        private DateTime updatedTime = DateTime.Now;
        public DateTime UpdatedTime
        {
            get { return updatedTime; }
            set { updatedTime = value; }
        }
        private bool isUsing;
        public bool IsUsing
        {
            get { return isUsing; }
            set
            {
                updatedTime = DateTime.Now;
                isUsing = value;
                if (Render != null)
                {
                    if (isUsing)
                        Render.SetSuppendRender(false);
                    else
                        Render.SetSuppendRender(true);
                }
                else
                {
                    LogWriter.Instance.WriteLog(2, "Object has been disposed");
                }
            }
        }
        private double zoomFactor;
        public double ZoomFactor
        {
            get { return zoomFactor; }
            set
            {
                updatedTime = DateTime.Now;
                zoomFactor = value;

                new Thread(delegate ()
                {
                    if (Render != null)
                    {
                        render.SetZoomFactor(zoomFactor);
                    }
                    else
                    {
                        LogWriter.Instance.WriteLog(2, "Object has been disposed");
                    }
                }).Start();
            }
        }
        private Point zoomPosition;
        public Point ZoomPosition
        {
            get { return zoomPosition; }
            set
            {
                updatedTime = DateTime.Now;
                zoomPosition = value;
                new Thread(delegate ()
                {
                    if (Render != null)
                    {
                        render.SetZoomPosition(zoomPosition.X, zoomPosition.Y);
                    }
                    else
                    {
                        LogWriter.Instance.WriteLog(2, "Object has been disposed");
                    }
                }).Start();

            }
        }
        private string screenID;
        public string ScreenID
        {
            get { return screenID; }
            set { screenID = value; }
        }

        private bool bHWAccel;
        public bool BHWAccel
        {
            get { return bHWAccel; }
            set
            {
                bHWAccel = value;
                if (render != null)
                    render.SetHWAccel(bHWAccel);
            }
        }
        private DISPLAY_RATIO_ENUM displayRatio;
        public DISPLAY_RATIO_ENUM DisplayRatio
        {
            get { return displayRatio; }
            set
            {
                displayRatio = value;
                if (render != null)
                    render.SetDisplayRatio(displayRatio);
            }
        }
        private RenderHandlerRef render;
        public RenderHandlerRef Render
        {
            get { return render; }
            set { render = value; }
        }

        private Point position;
        public Point Position
        {
            get { return position; }
            set { position = value; }
        }
        public PanelEx(CamView _camView, DISPLAY_RATIO_ENUM _displayRatio = DISPLAY_RATIO_ENUM.ORIGINAL_RATIO, string _screenID = null, bool _bHWAccel = false)
        {
            bHWAccel = _bHWAccel;
            screenID = _screenID;
            displayRatio = _displayRatio;
            render = new RenderHandlerRef(this.Handle, _displayRatio, bHWAccel);
            camView = _camView;
            isUsing = false;
            updatedTime = DateTime.Now;
            this.Dock = DockStyle.Fill;
            this.BackColor = Color.Black;
            this.Size = new Size(300, 300);
            this.Margin = new Padding(5, 5, 5, 5);
            this.AllowDrop = true;
            this.zoomFactor = 1;
        }
        ~PanelEx()
        {
            Dispose(false);
        }
        public void SetSuppendRender(bool _isSuppend)
        {
            if (Render != null)
            {
                Render.SetSuppendRender(_isSuppend);
            }
        }
        public Rectangle GetDisplaySize()
        {
            return (Rectangle)render.GetDisplaySize();
        }
        public void GetDisplayRatio(ref float _w, ref float _h)
        {
            render.GetDisplayRatio(ref _w, ref _h);
        }
        protected override void WndProc(ref Message m)
        {
            const int WM_NCHITTEST = 0x0084;
            const int HTTRANSPARENT = (-1);

            if (m.Msg == WM_NCHITTEST)
            {
                m.Result = (IntPtr)HTTRANSPARENT;
            }
            else
            {
                base.WndProc(ref m);
            }
        }
        private bool disposed = false;
        protected override void Dispose(bool _disposing)
        {
            if (!disposed)
            {
                if (_disposing)
                {
                    //LogWriter.Instance.WriteLog(2, "Disposed panel");
                    // Dispose any managed objects
                    // ...
                    camView = null;
                    if (render != null)
                    {
                        render.Dispose();
                        render = null;
                    }
                }
                // Now disposed of any unmanaged objects
                // ...
                disposed = true;
            }
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
