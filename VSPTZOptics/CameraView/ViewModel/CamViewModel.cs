﻿using CameraView.Model;
using Settings.Model;
using Settings.ViewModel;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraView.ViewModel
{
    public class CamViewModel
    {
        private static CamViewModel instance;
        private List<CamView> camViewList;
        CamViewModel()
        {
            camViewList = new List<CamView>();
            LoadCamera();
        }
        public List<CamView> CamViewList
        {
            get { return camViewList; }
            set { camViewList = value; }
        }
        public static CamViewModel Instance
        {
            get
            {
                if (instance == null)
                    instance = new CamViewModel();
                return instance;
            }
        }
        public void LoadCamera()
        {
            camViewList.Clear();
            foreach(Camera cam in ProfileViewModel.Instance.SelectedProfile.CameraList)
            {
                CamView camView;
                if(cam.Index < 8)
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.PTZ_OPTICS;
                    camView = new CamView(cam);
                    camViewList.Add(camView);
                    cam.CameraType = CAMERA_TYPE_ENUM.NDI;
                    camView = new CamView(cam);
                    camViewList.Add(camView);
                    cam.CameraType = CAMERA_TYPE_ENUM.USB;
                    camView = new CamView(cam);
                    camViewList.Add(camView);
                }
                else if (cam.Index == 8) //live camera
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.ZCAM;
                    camView = new CamView(cam, false);
                    camViewList.Add(camView);
                    cam.CameraType = CAMERA_TYPE_ENUM.NDI;
                    camView = new CamView(cam, false);
                    camViewList.Add(camView);
                    cam.CameraType = CAMERA_TYPE_ENUM.USB;
                    camView = new CamView(cam, false);
                    camViewList.Add(camView);
                }
                else // image
                {
                    camView = new CamView(cam, false);
                    camViewList.Add(camView);
                }
            }
        }

        public CamView FindActiveCamView(int _cameraInd)
        {
            return camViewList.Find(x => x.Cam.Index == _cameraInd && 
                                         (x.CameraType == CAMERA_TYPE_ENUM.STATIC_IMAGE || 
                                         ((x.CameraType == CAMERA_TYPE_ENUM.PTZ_OPTICS || x.CameraType == CAMERA_TYPE_ENUM.ZCAM) && x.Cam.IsIPCamera) ||
                                         (x.CameraType == CAMERA_TYPE_ENUM.NDI && x.Cam.IsNDICamera) ||
                                         (x.CameraType == CAMERA_TYPE_ENUM.USB && x.Cam.IsUSBCamera)));
        }

        public void UpdateActiveCamView(int _cameraInd)
        {
            foreach (CamView cView in camViewList)
            {
                if (_cameraInd == -1 || cView.Cam.Index == _cameraInd)
                {
                    bool isActive = false;
                    switch (cView.CameraType)
                    {
                        case CAMERA_TYPE_ENUM.PTZ_OPTICS:
                            if (cView.Cam.IsIPCamera)
                                isActive = true;
                            else
                                isActive = false;
                            break;
                        case CAMERA_TYPE_ENUM.ZCAM:
                            if (cView.Cam.IsIPCamera)
                                isActive = true;
                            else
                                isActive = false;
                            break;
                        case CAMERA_TYPE_ENUM.NDI:
                            if (cView.Cam.IsNDICamera)
                                isActive = true;
                            else
                                isActive = false;
                            break;
                        case CAMERA_TYPE_ENUM.USB:
                            if (cView.Cam.IsUSBCamera)
                                isActive = true;
                            else
                                isActive = false;
                            break;
                        default:
                            isActive = true;
                            break;
                    }
                    if (isActive && cView.Cam.IsConnect)
                        cView.ConnectControl();
                    else
                        cView.DisconnectControl();
                }
            }
        }
    }
}
