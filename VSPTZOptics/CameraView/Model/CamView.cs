﻿using CameraView.Base;
using NsRenderDecRef;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CameraView.Model
{
    using CameraControl.Model;
    public class CamView : IDisposable
    {
        private bool disposed = false;
        private Camera cam;
        private CamControl camControl;
        private StreamView mainStream;
        private StreamView subStream;
        private CAMERA_TYPE_ENUM cameraType;
        public CAMERA_TYPE_ENUM CameraType
        {
            get { return cameraType; }
            set { cameraType = value; }
        }
        public StreamView MainStream
        {
            get { return mainStream; }
            set { mainStream = value; }
        }
        public StreamView SubStream
        {
            get { return subStream; }
            set { subStream = value; }
        }

        public Camera Cam{
            get { return cam; }
            set { cam = value; }
        }
        public CamControl CamControl
        {
            get { return camControl; }
            set { camControl = value; }
        }
        public CamView(Camera _camera, bool _isControl = true)
        {
            cam = _camera;
            cameraType = _camera.CameraType;
            switch (_camera.CameraType)
            {
                case CAMERA_TYPE_ENUM.PTZ_OPTICS:
                    if(_isControl)
                        camControl = new PTZOpticsControl(cam);
                    mainStream = new StreamView(cam, CAMERA_STREAM_ENUM.MAIN);
                    subStream = new StreamView(cam, CAMERA_STREAM_ENUM.SUB);
                    break;
                case CAMERA_TYPE_ENUM.NDI:
                    if (_isControl)
                        camControl = new NDIControl(cam);
                    mainStream = new StreamView(cam, CAMERA_STREAM_ENUM.MAIN);
                    subStream = mainStream;
                    break;
                case CAMERA_TYPE_ENUM.USB:
                    /*if (_isControl)
                        camControl = new PTZOpticsControl(cam);*/
                    mainStream = new StreamView(cam, CAMERA_STREAM_ENUM.MAIN);
                    subStream = new StreamView(cam, CAMERA_STREAM_ENUM.SUB);
                    break;
                case CAMERA_TYPE_ENUM.ZCAM:
                    mainStream = new StreamView(cam, CAMERA_STREAM_ENUM.MAIN);
                    subStream = null;
                    break;
                case CAMERA_TYPE_ENUM.STATIC_IMAGE:
                    mainStream = new StreamView(cam, CAMERA_STREAM_ENUM.MAIN);
                    subStream = null;
                    break;
                default:
                    break;
            }
        }
        ~CamView()
        {
            Dispose(false);
        }
        
        public void ConnectControl()
        {
            if (camControl != null)
                camControl.ConnectCamera();
        }

        public void DisconnectControl()
        {
            if (camControl != null)
                camControl.DisconnectCamera();
        }
        public void StopStream()
        {
            if(mainStream != null)
                mainStream.ResetChannelPanel();
            if(subStream != null)
                subStream.ResetChannelPanel();           
        }
        protected virtual void Dispose(bool _disposing)
        {
            if (!disposed)
            {
                if (_disposing)
                {
                    // Dispose any managed objects
                    // ...
                    switch (cameraType)
                    {
                        case CAMERA_TYPE_ENUM.PTZ_OPTICS:
                            if (mainStream != null)
                                mainStream.Dispose();
                            if (subStream != null)
                                subStream.Dispose();
                            break;
                        case CAMERA_TYPE_ENUM.NDI:
                            if (mainStream != null)
                                mainStream.Dispose();                            
                            break;
                        case CAMERA_TYPE_ENUM.USB:
                            if (mainStream != null)
                                mainStream.Dispose();
                            if (subStream != null)
                                subStream.Dispose();
                            break;
                        case CAMERA_TYPE_ENUM.ZCAM:
                            if (mainStream != null)
                                mainStream.Dispose();
                            if (subStream != null)
                                subStream.Dispose();
                            break;
                        case CAMERA_TYPE_ENUM.STATIC_IMAGE:
                            if (mainStream != null)
                                mainStream.Dispose();
                            if (subStream != null)
                                subStream.Dispose();
                            break;
                        default:
                            break;
                    }                    
                }
                // Now disposed of any unmanaged objects
                // ...
                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
