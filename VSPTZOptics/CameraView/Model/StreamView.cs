﻿using CameraView.Base;
using NsRenderDecRef;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CameraView.Model
{
    using CameraControl.Model;
    public class StreamView : IDisposable
    {
        private System.Timers.Timer timer = null;
        private object streamViewLock;
        private bool isStop = false;
        private bool disposed = false;
        private bool bHWAccel;
        private object renderDecRefLock;
        private RenderDecRef renderDecRef;
        private object statusLock;
        private CAMERA_STREAM_ENUM stream;
        private List<PanelEx> chanelPanelList;
        private Camera cam;
        public CAMERA_STREAM_ENUM Stream
        {
            get { return stream; }
            set { stream = value; }
        }
        private CAMERA_TYPE_ENUM cameraType;
        public StreamView(Camera _camera, CAMERA_STREAM_ENUM _stream)
        {
            cam = _camera;
            stream = _stream;
            cameraType = _camera.CameraType;
            streamViewLock = new object();
            statusLock = new object();
            chanelPanelList = new List<PanelEx>();
            renderDecRefLock = new object();
            renderDecRef = new RenderDecRef(_camera, cameraType, _stream, bHWAccel);
            InitTimer();
        }
        ~StreamView()
        {
            Dispose(false);
        }
        public void InitTimer()
        {
            timer = new System.Timers.Timer(); // fire every 1 second
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 7000;
            timer.Enabled = true;
            timer.Start();
        }
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock (streamViewLock)
                {
                    if (!isStop)
                    {
                        if (cam.IsNDIOutput && stream == CAMERA_STREAM_ENUM.MAIN &&
                            (cam.IsIPCamera && (cameraType == CAMERA_TYPE_ENUM.PTZ_OPTICS || cameraType == CAMERA_TYPE_ENUM.ZCAM)))
                        {
                            if (renderDecRef == null)
                                renderDecRef = new RenderDecRef(cam, cameraType, stream, bHWAccel);
                            if (renderDecRef != null && cam.MainStreamStatus != STATUS_ENUM.CONNECTED)
                                renderDecRef.Start(true, true, true);
                        }
                        else
                        {
                            if (!CheckWorkingChannelPanel()) //no panel is mapped for render 
                            {
                                ClearResource();
                            }
                            else
                            {
                                STATUS_ENUM status = stream == CAMERA_STREAM_ENUM.MAIN ? cam.MainStreamStatus : cam.SubStreamStatus;
                                if (cam.IsConnect && status != STATUS_ENUM.CONNECTED)
                                {
                                    lock (renderDecRefLock)
                                    {
                                        renderDecRef.Start(true, true, true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }
        private void Stop_Timer()
        {
            isStop = true;
            timer.Stop();
        }
        public void EnqueueDecode(byte[] _buffer, int _offset, int _length, PACKET_MODE_ENUM _packetMode)
        {
            lock (renderDecRefLock)
            {
                if (renderDecRef != null)
                    renderDecRef.EnqueueDecode(_buffer, _offset, _length, _packetMode);
            }
        }
        public void Start(){
            lock (renderDecRefLock)
            {
                if(renderDecRef == null)
                    renderDecRef = new RenderDecRef(cam, cameraType, stream, bHWAccel);
                if (renderDecRef != null)
                {
                    renderDecRef.Start(true, true, true);
                }
            }
        }
        public void Stop(){
            new Thread(delegate ()
            {
                lock (renderDecRefLock)
                {
                    if (renderDecRef != null)
                        renderDecRef.Stop();
                }
            }).Start();            
        }
        public void ClearResource(){
            lock (streamViewLock)
            {
                foreach (PanelEx panel in chanelPanelList)
                {
                    renderDecRef.RemoveRender(panel.Render);
                    panel.Dispose();
                }
                chanelPanelList.Clear();
                lock (renderDecRefLock)
                {
                    if (renderDecRef != null)
                    {
                        renderDecRef.Dispose();
                        renderDecRef = null;
                    }
                }
            }
        }
        public List<PanelEx> ChannePanelList
        {
            get { return chanelPanelList; }
        }
        public void AddChannelPanel(PanelEx _panel){
            new Thread(delegate (){
                lock (streamViewLock){
                    string screenID = _panel.ScreenID;
                    if (chanelPanelList.Find(x => x == _panel) == null) {
                        chanelPanelList.Add(_panel);
                        lock (renderDecRefLock){
                            if (renderDecRef == null)
                                renderDecRef = new RenderDecRef(cam, cameraType, stream, bHWAccel);
                            renderDecRef.AddRender(_panel.Render);
                            if (cam.IsConnect)
                            {
                                renderDecRef.Start(true, true, true);
                            }
                        }
                    }
                }
            }).Start();
        }
        public void RemoveChannelPanel(PanelEx _panel, bool _isDispose = true){
            new Thread(delegate (){
                lock (streamViewLock){
                    if (chanelPanelList.Find(x => x == _panel) != null){
                        lock (renderDecRefLock)
                        {
                            if (renderDecRef != null)
                                renderDecRef.RemoveRender(_panel.Render);
                        }
                        chanelPanelList.Remove(_panel);
                        if (_isDispose)
                            _panel.Dispose();
                    }
                }
            }).Start();
        }
        public void ResetChannelPanel(string _screenID = null){
            try
            {
                foreach (PanelEx panel in chanelPanelList)
                {
                    if (panel.ScreenID == _screenID)
                    {
                        panel.IsUsing = false;
                        //RemoveChannelPanel(panel); //Remove this panel out of render list
                    }
                }
            }
            catch(Exception e)
            {
                LogWriter.Instance.WriteLog(2, e.Message + e.StackTrace);
            }
        }
        private bool CheckWorkingChannelPanel(){
            lock (streamViewLock)
            {
                foreach (PanelEx panel in chanelPanelList)
                {
                    TimeSpan difference = DateTime.Now - panel.UpdatedTime;
                    if (!(panel.IsUsing == false && difference.TotalMilliseconds > 10000))
                        return true;
                }
                return false;
            }
        }
        public PanelEx GetChannelPanel(string _screenID = null, bool _bHWAccel = false){
            try
            {
                foreach (PanelEx panel in chanelPanelList)
                {
                    TimeSpan difference = DateTime.Now - panel.UpdatedTime;
                    if (difference.TotalMilliseconds > 5000 && panel.IsUsing == false && panel.ScreenID == _screenID && _bHWAccel == panel.BHWAccel)
                    {
                        panel.UpdatedTime = DateTime.Now;
                        return panel;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                LogWriter.Instance.WriteLog(2, e.Message + e.StackTrace);
                return null;
            }
        }
        public event EventHandler<ViewEventArgs> camViewEvent;
        protected virtual void OnViewEvent(ViewEventArgs e)
        {
            EventHandler<ViewEventArgs> handler = camViewEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected virtual void Dispose(bool _disposing)
        {
            if (!disposed)
            {
                if (_disposing)
                {
                    // Dispose any managed objects
                    // ...
                    Stop_Timer();
                    ClearResource();
                }
                // Now disposed of any unmanaged objects
                // ...
                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
