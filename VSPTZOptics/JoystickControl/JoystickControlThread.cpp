#include "stdafx.h"
#include "JoystickControlCore.h"

using namespace System;
using namespace ShareLibrary::Base;
using namespace ShareLibrary::Model;
using namespace System::Runtime::ExceptionServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security;


JoystickDevice^ GetJoystickDevice(int _id)
{
	try
	{
		JoystickDevice^ joystickDevice = gcnew JoystickDevice();
		SDL_Joystick* sdlJoystick = SDL_JoystickOpen(_id);
		if (sdlJoystick != NULL) {
			joystickDevice->Blacklisted = false;
			joystickDevice->Name = gcnew String(SDL_JoystickName(sdlJoystick));

			/* Get joystick properties */
			int povs = SDL_JoystickNumHats(sdlJoystick);
			int axis = SDL_JoystickNumAxes(sdlJoystick);
			int buttons = SDL_JoystickNumButtons(sdlJoystick);

			/* Initialize POVs */
			for (int i = 0; i < povs; ++i)
				joystickDevice->Povs->Add(0);

			/* Initialize axes */
			for (int i = 0; i < axis; ++i)
				joystickDevice->Axis->Add(0);

			/* Initialize buttons */
			for (int i = 0; i < buttons; ++i)
				joystickDevice->Buttons->Add(false);
			//LogWriter::Instance->WriteLog(2, "name " + joystickDevice->Name + " num hats " + povs + " num axes " + axis + " num buttons " + buttons, __LINE__, __FUNCDNAME__);
		}
		else
			LogWriter::Instance->WriteLog(2, "Cannot find joystick", __LINE__, __FUNCDNAME__);

		return joystickDevice;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return nullptr;
	}	

}

void RaiseButtonEvent(Joystick^ _joystick, const SDL_Event* _sdlEvent)
{
	JoystickButtonEvent^ jsButtonEvent = gcnew JoystickButtonEvent();
	jsButtonEvent->Button = _sdlEvent->jbutton.button;
	jsButtonEvent->Pressed = _sdlEvent->jbutton.state == SDL_PRESSED;
	//jsButtonEvent->Joystick = GetJoystickDevice(_sdlEvent->jdevice.which);
	_joystick->RaiseButtonEvent(jsButtonEvent);
	LogWriter::Instance->WriteLog(2, "RaiseButtonEvent button" + jsButtonEvent->Button + " isPress " + jsButtonEvent->Pressed, __LINE__, __FUNCDNAME__);
}

void RaiseAxisEvent(Joystick^ _joystick, const SDL_Event* _sdlEvent)
{
	JoystickAxisEvent^ jsAxisEvent = gcnew JoystickAxisEvent();
	jsAxisEvent->Axis = _sdlEvent->caxis.axis;
	jsAxisEvent->Value = static_cast<double> (_sdlEvent->caxis.value) / 32767;
	//jsAxisEvent->Joystick = GetJoystickDevice(_sdlEvent->jdevice.which);
	_joystick->RaiseAxisEvent(jsAxisEvent);
	LogWriter::Instance->WriteLog(2, "RaiseAxisEvent axis" + jsAxisEvent->Axis + " Value " + jsAxisEvent->Value, __LINE__, __FUNCDNAME__);
}

void RaisePOVEvent(Joystick^ _joystick, const SDL_Event* _sdlEvent)
{
	JoystickPOVEvent^ jsPOVEvent = gcnew JoystickPOVEvent();
	jsPOVEvent->Pov = (int)_sdlEvent->jhat.hat;
	//jsPOVEvent->Joystick = GetJoystickDevice(_sdlEvent->jdevice.which);

	switch (_sdlEvent->jhat.value) {
	case SDL_HAT_RIGHTUP:
		jsPOVEvent->Angle = 45;
		break;
	case SDL_HAT_RIGHTDOWN:
		jsPOVEvent->Angle = 135;
		break;
	case SDL_HAT_LEFTDOWN:
		jsPOVEvent->Angle = 225;
		break;
	case SDL_HAT_LEFTUP:
		jsPOVEvent->Angle = 315;
		break;
	case SDL_HAT_UP:
		jsPOVEvent->Angle = 0;
		break;
	case SDL_HAT_RIGHT:
		jsPOVEvent->Angle = 90;
		break;
	case SDL_HAT_DOWN:
		jsPOVEvent->Angle = 180;
		break;
	case SDL_HAT_LEFT:
		jsPOVEvent->Angle = 270;
		break;
	default:
		jsPOVEvent->Angle = -1;
		break;
	}
	_joystick->RaisePOVEvent(jsPOVEvent);
	LogWriter::Instance->WriteLog(2, "RaisePOVEvent Pov " + jsPOVEvent->Pov + " angle " + jsPOVEvent->Pov, __LINE__, __FUNCDNAME__);
}


[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
UINT32 __stdcall SDLProcessThread(void* pArguments)
{
	UINT64* params = (UINT64*)pArguments;
	JoystickControlCore* joystickControlCore = (JoystickControlCore*)params[0];
	ThreadController* sdlThread = &joystickControlCore->sdlThread;
	void* joystickPtr = (void*)params[1];
	IntPtr pointer(joystickPtr);
	GCHandle joystickHandle = GCHandle::FromIntPtr(pointer);
	Joystick^ joystick = (Joystick^)joystickHandle.Target;
	try
	{
Reconnect:
		SDL_Event event;
		while (sdlThread->GetIsThreadRunning()) {
			int numJoystick = SDL_NumJoysticks();
			if (numJoystick > 0)
			{
				SDL_JoystickEventState(SDL_ENABLE);
				SDL_GameController* sdlGameController = NULL;
				for (int i = 0; i < numJoystick; i++)
				{
					SDL_JoystickOpen(i);
				}
				while (sdlThread->GetIsThreadRunning())
				{
					try
					{
						while (SDL_PollEvent(&event))
						{
							switch (event.type) {
							case SDL_JOYDEVICEADDED:
								goto Reconnect;
								break;
							case SDL_JOYDEVICEREMOVED:
								break;
							case SDL_JOYAXISMOTION:
								RaiseAxisEvent(joystick, &event);
								break;
							case SDL_JOYBUTTONUP:
								RaiseButtonEvent(joystick, &event);
								break;
							case SDL_JOYBUTTONDOWN:
								RaiseButtonEvent(joystick, &event);
								break;
							case SDL_JOYHATMOTION:
								RaisePOVEvent(joystick, &event);
								break;
							default:
								break;
							}
						}
						Sleep(150);
					}
					catch (Exception^ ex)
					{
						LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
					}					
				}
			}
			Sleep(1000);
		}
	}
	catch (Exception^ e) {
		String^ log = "";
		if (e != nullptr)
			log += " " + e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally{
		sdlThread->SetIsThreadRunning(false);
	}
}