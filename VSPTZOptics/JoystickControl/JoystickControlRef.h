#pragma once
#include "JoystickControlCore.h"
using namespace System;
using namespace ShareLibrary::Model;
using namespace System::Runtime::InteropServices;

namespace JoystickControl {
	public ref class JoystickControlRef
	{
	private:
		IntPtr joystickControlObj;
		GCHandle joystickHandle;
	public:
		JoystickControlRef();
		JoystickControlRef(Joystick^ _joystick);
		~JoystickControlRef();
		void Start();
		void Stop();
	};
}
