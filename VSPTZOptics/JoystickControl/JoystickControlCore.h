#pragma once
#include "SDL.h"
#include "SDL_events.h"
#include "ThreadCtl/ThreadController.h"
#include "DefineBase.h"

class JoystickControlCore
{
private:

public:
	ThreadController sdlThread;
	JoystickControlCore();
	JoystickControlCore(VOID* _pJoystick);
	~JoystickControlCore();
	void Reset();
	void Destroy();
	void Start();
	void Stop();

};
