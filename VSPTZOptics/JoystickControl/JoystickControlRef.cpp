#include "stdafx.h"
#include "JoystickControlRef.h"

JoystickControl::JoystickControlRef::JoystickControlRef()
{
	JoystickControlCore* core = new JoystickControlCore();
	joystickControlObj = (IntPtr)core;
}

JoystickControl::JoystickControlRef::JoystickControlRef(Joystick^ _joystick)
{
	joystickHandle = GCHandle::Alloc(_joystick);
	IntPtr intPtrJoystick = GCHandle::ToIntPtr(joystickHandle);
	JoystickControlCore* core = new JoystickControlCore(intPtrJoystick.ToPointer());
	joystickControlObj = (IntPtr)core;
}

JoystickControl::JoystickControlRef::~JoystickControlRef()
{
	JoystickControlCore* core = (JoystickControlCore*)joystickControlObj.ToPointer();
	if (core != NULL)
		delete core;

}

void JoystickControl::JoystickControlRef::Start()
{
	JoystickControlCore* core = (JoystickControlCore*)joystickControlObj.ToPointer();
	if (core != NULL)
		core->Start();
}

void JoystickControl::JoystickControlRef::Stop()
{
	JoystickControlCore* core = (JoystickControlCore*)joystickControlObj.ToPointer();
	if (core != NULL)
		core->Stop();
}
