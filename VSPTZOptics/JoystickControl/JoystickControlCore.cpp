#include "stdafx.h"
#include "JoystickControlCore.h"

extern UINT32 __stdcall SDLProcessThread(void* pArguments);

JoystickControlCore::JoystickControlCore()
{
	Reset();
	sdlThread.SetThreadName("SDL Thread");
	UINT64 threadPars[] = { (UINT64)this};
	sdlThread.InitThreadArgs(threadPars, (sizeof(sdlThread) / sizeof(UINT64)));
}

JoystickControlCore::JoystickControlCore(VOID* _pJoystick)
{
	Reset();
	sdlThread.SetThreadName("SDL Thread");
	UINT64 threadPars[] = { (UINT64)this, 	(UINT64)_pJoystick };
	sdlThread.InitThreadArgs(threadPars, (sizeof(sdlThread) / sizeof(UINT64)));
}

JoystickControlCore::~JoystickControlCore()
{
	Destroy();
}

void JoystickControlCore::Reset()
{

}

void JoystickControlCore::Destroy()
{

}

void JoystickControlCore::Start()
{
	sdlThread.Start(&SDLProcessThread);
}

void JoystickControlCore::Stop()
{
	sdlThread.Stop();
}
