﻿using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Settings.Model
{
    public class Profile
    {
        private string fileNamePath;
        public string FileNamePath { get { return fileNamePath; } }

        private List<Camera> cameraList;
        public List<Camera> CameraList { get { return cameraList; } set{cameraList = value;} }

        private PtzCamera ptz;
        public PtzCamera PTZ { get { return ptz; } set { ptz = value; } }

        private List<Stage> stageList;
        public List<Stage> StageList { get { return stageList; } set { stageList = value; } }

        private bool isStageLiveView;
        public bool IsStageLiveView { get { return isStageLiveView; } set { isStageLiveView = value; } }

        private bool isChange;
        public bool IsChange { get { return isChange; }
            set {
                isChange = value; }
        }
        public Profile(string _fileNamePath)
        {
            fileNamePath = _fileNamePath;
            CameraList = new List<Camera>();
            for (int i = 0; i < 10; i++) //8 Optics camera + 1 Zcam
            {
                Camera cam = new Camera(); //Should load from config
                if (i < 8)
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.PTZ_OPTICS;
                    cam.Name = "Camera #" + (i + 1);
                }
                else if (i==8)
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.ZCAM;
                    cam.Name = "Live camera";
                }
                else
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.STATIC_IMAGE;
                    cam.Name = "Static image";
                }
                cam.Index = i;
                CameraList.Add(cam);
            }
            PTZ = new PtzCamera();
            StageList = new List<Stage>();
            for (int i = 0; i < 8; i++) //8 Optics camera
            {
                Stage stage = new Stage(i);
                stage.Name = "Sp+" + i;
                StageList.Add(stage);
            }
            IsChange = false;
        }
        public void ResetProfile()
        {
            fileNamePath = null;
            int ind = 0;
            foreach (Camera cam in cameraList)
            {
                cam.Reset();
                if (ind < 8)
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.PTZ_OPTICS;
                    cam.Name = "Camera #" + (ind + 1);
                }
                else if (ind == 8)
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.ZCAM;
                    cam.Name = "Live camera";
                }
                else
                {
                    cam.CameraType = CAMERA_TYPE_ENUM.STATIC_IMAGE;
                    cam.Name = "Static image";
                }
                cam.Index = ind;
                ind++;
            }
            foreach (Stage stage in stageList)
            {
                stage.Reset();
            }
            PTZ.Reset();
            IsChange = false;
        }
        public void SetPath(string _fileNamePath)
        {
            fileNamePath = _fileNamePath;
        }
        public void LoadProfile()
        {
            if(File.Exists(fileNamePath))
            {
                FileStream fs = null;
                XmlDocument xmlDoc = new XmlDocument();
                XmlNodeList xmlProfileNode;
                try
                {
                    fs = new FileStream(fileNamePath, FileMode.Open, FileAccess.Read);
                    xmlDoc.Load(fs);
                    fs.Close();//Tien Vo added to close file;
                    xmlProfileNode = xmlDoc.GetElementsByTagName("Profile");
                    if (xmlProfileNode != null && xmlProfileNode.Count > 0)
                    {
                        XmlNode cameraNode = xmlProfileNode[0].SelectSingleNode("Camera");
                        if(cameraNode != null)
                        {
                            XmlNodeList xmlCameraInfoNodes = cameraNode.SelectNodes("CameraInfo");
                            if (xmlCameraInfoNodes != null && xmlCameraInfoNodes.Count > 0)
                            {
                                int cameraInd = 0;
                                foreach (XmlNode cameraInfoNode in xmlCameraInfoNodes)
                                {
                                    if(cameraInfoNode.SelectSingleNode("Name") != null && !string.IsNullOrEmpty(cameraInfoNode["Name"].InnerText)) CameraList[cameraInd].Name = cameraInfoNode["Name"].InnerText;

                                    CameraList[cameraInd].IsIPCamera = cameraInfoNode.SelectSingleNode("IPCamera") != null ? Boolean.Parse(cameraInfoNode["IPCamera"].InnerText.ToString()) : false;
                                    CameraList[cameraInd].IsNDICamera = cameraInfoNode.SelectSingleNode("NDICamera") != null ? Boolean.Parse(cameraInfoNode["NDICamera"].InnerText.ToString()) : false;
                                    CameraList[cameraInd].IsUSBCamera = cameraInfoNode.SelectSingleNode("USBCamera") != null ? Boolean.Parse(cameraInfoNode["USBCamera"].InnerText.ToString()) : false;
                                    if (!(CameraList[cameraInd].IsIPCamera || CameraList[cameraInd].IsNDICamera || CameraList[cameraInd].IsUSBCamera))
                                        CameraList[cameraInd].IsIPCamera = true;

                                    CameraList[cameraInd].Ip_Domain = cameraInfoNode.SelectSingleNode("IP") != null ? cameraInfoNode["IP"].InnerText : "";
                                    CameraList[cameraInd].NDICameraName = cameraInfoNode.SelectSingleNode("NDICameraName") != null ? cameraInfoNode["NDICameraName"].InnerText : "";
                                    CameraList[cameraInd].USBCameraName = cameraInfoNode.SelectSingleNode("USBCameraName") != null ? cameraInfoNode["USBCameraName"].InnerText : "";
                                    CameraList[cameraInd].ImageName = cameraInfoNode.SelectSingleNode("ImageName") != null ? cameraInfoNode["ImageName"].InnerText : "";
                                    CameraList[cameraInd].IsNDIOutput = cameraInfoNode.SelectSingleNode("NDIOutput") != null ? Boolean.Parse(cameraInfoNode["NDIOutput"].InnerText.ToString()) : false;
                                    CameraList[cameraInd].IsConnect = cameraInfoNode.SelectSingleNode("Connect") != null ? Boolean.Parse(cameraInfoNode["Connect"].InnerText.ToString()) : false;

                                    XmlNode presetNode = cameraInfoNode.SelectSingleNode("Preset");
                                    if (presetNode != null)
                                    {
                                        XmlNodeList presetNameNodes = presetNode.SelectNodes("PresetName");
                                        int presetInd = 1;
                                        if (presetNameNodes != null && presetNameNodes.Count > 0)
                                        {
                                            foreach (XmlNode presetNameNode in presetNameNodes)
                                            {
                                                if(presetNameNode.SelectSingleNode("PresetKey") != null && presetNameNode.SelectSingleNode("PresetValue") !=null)
                                                {
                                                    int presetKey = int.Parse(presetNameNode.SelectSingleNode("PresetKey").InnerText);
                                                    string presetValue = presetNameNode.SelectSingleNode("PresetValue").InnerText;
                                                    CameraList[cameraInd].PTZ.PresetNameList.Add(presetKey, presetValue);
                                                    presetInd++;
                                                }
                                            }
                                        }
                                    }
                                    cameraInd++;
                                }
                            }
                        }
                        //PTZ - begin
                        XmlNode ptzNode = xmlProfileNode[0].SelectSingleNode("PTZ");
                        if (ptzNode != null)
                        {
                            PTZ.PanSpeed = ptzNode.SelectSingleNode("PanSpeed") != null ? int.Parse(ptzNode["PanSpeed"].InnerText) : 5;
                            PTZ.TiltSpeed = ptzNode.SelectSingleNode("TiltSpeed") != null ? int.Parse(ptzNode["TiltSpeed"].InnerText) : 5;
                            PTZ.ZoomSpeed = ptzNode.SelectSingleNode("ZoomSpeed") != null ? int.Parse(ptzNode["ZoomSpeed"].InnerText) : 0;
                            PTZ.FocusSpeed = ptzNode.SelectSingleNode("FocusSpeed") != null ? int.Parse(ptzNode["FocusSpeed"].InnerText) : 0;
                            PTZ.SetPresetNumber = ptzNode.SelectSingleNode("SetPresetNumber") != null ? int.Parse(ptzNode["SetPresetNumber"].InnerText) : 1;
                            PTZ.ReCallPresetNumber = ptzNode.SelectSingleNode("ReCallPresetNumber") != null ? int.Parse(ptzNode["ReCallPresetNumber"].InnerText) : 1;
                            PTZ.IsAutoFocus = ptzNode.SelectSingleNode("IsAutoFocus") != null ? Boolean.Parse(ptzNode["IsAutoFocus"].InnerText) : true;
                            PTZ.IsXBOX = ptzNode.SelectSingleNode("IsXBOX") != null ? Boolean.Parse(ptzNode["IsXBOX"].InnerText) : true;
                        }
                        //PTZ - end
                        //Stage - begin
                        XmlNode stageNode = xmlProfileNode[0].SelectSingleNode("Stage");
                        if (stageNode != null)
                        {
                            XmlNodeList stageInfoNodes = stageNode.SelectNodes("StageInfo");
                            if (stageInfoNodes != null && stageInfoNodes.Count > 0)
                            {
                                int stageInd = 0;
                                foreach (XmlNode stageInfoNode in stageInfoNodes)
                                {
                                    XmlNodeList presetNumberNodes = stageInfoNode.SelectNodes("PresetNumber");
                                    if (presetNumberNodes != null && presetNumberNodes.Count > 0)
                                    {
                                        int cameraInd = 0;
                                        foreach (XmlNode presetNumberNode in presetNumberNodes)
                                        {
                                            StageList[stageInd].PresetNumberList[cameraInd] = int.Parse(presetNumberNode.InnerText);
                                            cameraInd++;
                                        }
                                    }
                                    int rectX = stageInfoNode.SelectSingleNode("X") != null ? int.Parse(stageInfoNode["X"].InnerText) : 0;
                                    int rectY = stageInfoNode.SelectSingleNode("Y") != null ? int.Parse(stageInfoNode["Y"].InnerText) : 0;
                                    int rectW = stageInfoNode.SelectSingleNode("W") != null ? int.Parse(stageInfoNode["W"].InnerText) : 0;
                                    int rectH = stageInfoNode.SelectSingleNode("H") != null ? int.Parse(stageInfoNode["H"].InnerText) : 0;
                                    StageList[stageInd].Rect = new System.Drawing.Rectangle(rectX, rectY, rectW, rectH);

                                    StageList[stageInd].WindowW = stageInfoNode.SelectSingleNode("WindowW") != null ? int.Parse(stageInfoNode["WindowW"].InnerText) : 0;
                                    StageList[stageInd].WindowH = stageInfoNode.SelectSingleNode("WindowH") != null ? int.Parse(stageInfoNode["WindowH"].InnerText) : 0;

                                    StageList[stageInd].IsShowOnStageView = stageInfoNode.SelectSingleNode("IsShowOnStageView") != null ? Boolean.Parse(stageInfoNode["IsShowOnStageView"].InnerText) : false;
                                    StageList[stageInd].HTTPTrigger = stageInfoNode.SelectSingleNode("HTTPTrigger") != null ? stageInfoNode["HTTPTrigger"].InnerText : "";
                                    if(stageInfoNode.SelectSingleNode("Name") != null) StageList[stageInd].Name = stageInfoNode["Name"].InnerText;
                                    StageList[stageInd].IsLock = stageInfoNode.SelectSingleNode("IsLock") != null ? Boolean.Parse(stageInfoNode["IsLock"].InnerText) : false;
                                    stageInd++;
                                }
                            }

                            isStageLiveView = stageNode.SelectSingleNode("IsStageLiveView") != null ? Boolean.Parse(stageNode["IsStageLiveView"].InnerText) : true;
                        }
                        //Stage - end
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.Instance.WriteLog(2, ex.Message + "\t" + ex.StackTrace);
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public bool SaveProfile()
        {
            bool isSaveOK = true;
            XmlTextWriter writer = null;
            if (string.IsNullOrEmpty(fileNamePath))
                return false;
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(fileNamePath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(fileNamePath));
                writer = new XmlTextWriter(fileNamePath, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("Profile");
                CreateProfileNode(writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            catch (Exception ex)
            {
                isSaveOK = false;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }

            return isSaveOK;
        }
        public void CreateProfileNode(XmlTextWriter writer)
        {
            writer.WriteStartElement("Camera"); //Camera start
            foreach (Camera cam in CameraList)
            {
                writer.WriteStartElement("CameraInfo");

                writer.WriteStartElement("Name");
                writer.WriteString(cam.Name);
                writer.WriteEndElement();

                writer.WriteStartElement("IPCamera");
                writer.WriteString(cam.IsIPCamera.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("NDICamera");
                writer.WriteString(cam.IsNDICamera.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("USBCamera");
                writer.WriteString(cam.IsUSBCamera.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("IP");
                writer.WriteString(cam.Ip_Domain);
                writer.WriteEndElement();

                writer.WriteStartElement("NDICameraName");
                writer.WriteString(cam.NDICameraName);
                writer.WriteEndElement();

                writer.WriteStartElement("USBCameraName");
                writer.WriteString(cam.USBCameraName);
                writer.WriteEndElement();

                writer.WriteStartElement("ImageName");
                writer.WriteString(cam.ImageName);
                writer.WriteEndElement();


                writer.WriteStartElement("NDIOutput");
                writer.WriteString(cam.IsNDIOutput.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("Connect");
                writer.WriteString(cam.IsConnect.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("Preset"); //Preset start
                foreach (KeyValuePair<int, string> entry in cam.PTZ.PresetNameList)
                {
                    if(entry.Key > 0)
                    {
                        writer.WriteStartElement("PresetName");

                        writer.WriteStartElement("PresetKey");
                        writer.WriteString(entry.Key.ToString());
                        writer.WriteEndElement();

                        writer.WriteStartElement("PresetValue");
                        writer.WriteString(entry.Value);
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();//Preset end

                writer.WriteEndElement();
            }            
            writer.WriteEndElement(); //Camera end

            writer.WriteStartElement("PTZ"); //PTZ start

            writer.WriteStartElement("PanSpeed");
            writer.WriteString(PTZ.PanSpeed.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("TiltSpeed");
            writer.WriteString(PTZ.PanSpeed.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("ZoomSpeed");
            writer.WriteString(PTZ.ZoomSpeed.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("FocusSpeed");
            writer.WriteString(PTZ.FocusSpeed.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("SetPresetNumber");
            writer.WriteString(PTZ.SetPresetNumber.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("ReCallPresetNumber");
            writer.WriteString(PTZ.ReCallPresetNumber.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsAutoFocus");
            writer.WriteString(PTZ.IsAutoFocus.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsXBOX");
            writer.WriteString(PTZ.IsXBOX.ToString());
            writer.WriteEndElement();

            writer.WriteEndElement(); //PTZ end

            writer.WriteStartElement("Stage"); //Stage begin
            foreach(Stage stage in StageList)
            {
                writer.WriteStartElement("StageInfo"); //Stage Info begin
               
                for (int cameraInd = 0; cameraInd < 8; cameraInd++)
                {
                    writer.WriteStartElement("PresetNumber"); //PresetNumber begin
                    writer.WriteString(stage.PresetNumberList[cameraInd].ToString());
                    writer.WriteEndElement(); //PresetNumber end
                }             

                writer.WriteStartElement("X"); //Rectangle X begin
                writer.WriteString(stage.Rect.X.ToString());
                writer.WriteEndElement(); //Rectangle X end

                writer.WriteStartElement("Y"); //Rectangle y begin
                writer.WriteString(stage.Rect.Y.ToString());
                writer.WriteEndElement(); //Rectangle y end

                writer.WriteStartElement("W"); //Rectangle w begin
                writer.WriteString(stage.Rect.Width.ToString());
                writer.WriteEndElement(); //Rectangle w end

                writer.WriteStartElement("H"); //Rectangle h begin
                writer.WriteString(stage.Rect.Height.ToString());
                writer.WriteEndElement(); //Rectangle h end

                writer.WriteStartElement("WindowW"); //WindowW begin
                writer.WriteString(stage.WindowW.ToString());
                writer.WriteEndElement(); //WindowW end

                writer.WriteStartElement("WindowH"); //WindowH begin
                writer.WriteString(stage.WindowH.ToString());
                writer.WriteEndElement(); //WindowH end

                writer.WriteStartElement("IsShowOnStageView"); //IsShowOnStageView begin
                writer.WriteString(stage.IsShowOnStageView.ToString());
                writer.WriteEndElement(); //IsShowOnStageView end

                writer.WriteStartElement("HTTPTrigger"); //HTTPTrigger begin
                writer.WriteString(stage.HTTPTrigger.ToString());
                writer.WriteEndElement(); //HTTPTrigger end

                writer.WriteStartElement("Name"); //Name begin
                writer.WriteString(stage.Name.ToString());
                writer.WriteEndElement(); //Name end

                writer.WriteStartElement("IsLock"); //is lock begin
                writer.WriteString(stage.IsLock.ToString());
                writer.WriteEndElement(); //Name end

                writer.WriteEndElement(); //Stage Info end

               
            }
            writer.WriteStartElement("IsStageLiveView"); //IsStageLiveView begin
            writer.WriteString(IsStageLiveView.ToString());
            writer.WriteEndElement(); //IsStageLiveView end

            writer.WriteEndElement(); //Stage end
        }
    }
}
