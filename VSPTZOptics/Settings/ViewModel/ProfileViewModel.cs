﻿using Settings.Model;
using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Settings.ViewModel
{
    public class ProfileViewModel
    {
        private static ProfileViewModel instance;
        private string configDir;
        private string configName;
        private List<string> profileNameList;
        private string selectedProfileName;
        private Profile selectedProfile;
        public Profile SelectedProfile { get { return selectedProfile; } set { selectedProfile = value; } }
        public float DpiX { get; set; }
        public float DpiY { get; set; }

        private string roamingApplicationData;
        private string appName;
        private string appSettingsLocation;
        public string AppSettingsLocation { get { return appSettingsLocation; } set { appSettingsLocation = value; } }

        private string imageDir;
        public string ImageDir { get { return imageDir; } set { imageDir = value; } }

        private bool isPresetSetup;
        public bool IsPresetSetup { get { return isPresetSetup; } set { isPresetSetup = value; } }

        private bool isChangeSetup;
        public bool IsChangeSetup { get { return isChangeSetup; } set { isChangeSetup = value; } }
        ProfileViewModel()
        {
            roamingApplicationData = Environment.ExpandEnvironmentVariables("%appdata%");//this gives C:\Users\<userName>\AppData\Roaming
            appName = "PTZOpticsARMulticam";
            appSettingsLocation = roamingApplicationData + "\\" + appName + "\\";

            imageDir = appSettingsLocation + "Image\\";
            configDir = appSettingsLocation + "Config\\";
            configName = "Config.xml";
            selectedProfileName = null;
            profileNameList = new List<string>();
            LoadConfig();
            LoadSelectedProfile(selectedProfileName);
        }
        public List<string> ProfileNameList
        {
            get { return profileNameList; }
            set { profileNameList = value; }
        }
        public void AddProfileName(string _profileName)
        {
            if(!string.IsNullOrEmpty(_profileName))
            {
                var value = ProfileNameList.Find(x => x == _profileName);
                if (value == null)
                    ProfileNameList.Add(_profileName);
            }
        }
        public static ProfileViewModel Instance
        {
            get
            {
                if (instance == null)
                    instance = new ProfileViewModel();
                return instance;
            }
        }
        public bool SaveConfig()
        {
            bool isSaveOK = true;
            XmlTextWriter writer = null;
            if(!System.IO.Directory.Exists(configDir)) //config path not found
                System.IO.Directory.CreateDirectory(configDir);
            string fileNamePath = configDir + configName;
            try
            {
                writer = new XmlTextWriter(fileNamePath, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("Config");
                CreateConfigNode(writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            catch
            {
                isSaveOK = false;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
            return isSaveOK;
        }
        public void CreateConfigNode(XmlTextWriter writer)
        {
            writer.WriteStartElement("SelectedProfile");
            if(selectedProfile != null)
                writer.WriteString(selectedProfile.FileNamePath);
            else
                writer.WriteString(selectedProfileName);
            writer.WriteEndElement();
            foreach (string profileName in profileNameList)
            {
                writer.WriteStartElement("Profile");
                writer.WriteString(profileName);
                writer.WriteEndElement();
            }
        }
        public void LoadConfig()
        {
            if (File.Exists(configDir + configName))
            {
                FileStream fs = null;
                XmlDataDocument xmlDoc = new XmlDataDocument();
                XmlNodeList xmlConfigNode;
                try
                {
                    fs = new FileStream(configDir + configName, FileMode.Open, FileAccess.Read);
                    xmlDoc.Load(fs);
                    fs.Close();//Tien Vo added to close file;
                    xmlConfigNode = xmlDoc.GetElementsByTagName("Config");
                    if (xmlConfigNode != null && xmlConfigNode.Count > 0)
                    {
                        XmlNodeList xmlSelectedProfileNodes = xmlDoc.GetElementsByTagName("SelectedProfile");
                        if (xmlSelectedProfileNodes != null && xmlSelectedProfileNodes.Count > 0)
                        {
                            selectedProfileName = xmlSelectedProfileNodes[0].InnerText;
                            AddProfileName(selectedProfileName);
                        }
                        XmlNodeList xmlProfileNodes = xmlDoc.GetElementsByTagName("Profile");
                        if (xmlProfileNodes != null && xmlProfileNodes.Count > 0)
                        {
                            foreach(XmlNode profile in xmlProfileNodes)
                            {
                                AddProfileName(profile.InnerText);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.Instance.WriteLog(2, ex.Message + "\t" + ex.StackTrace);
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public void LoadSelectedProfile(string _profileNamePath)
        {
            if (selectedProfile == null)
                selectedProfile = new Profile(_profileNamePath);
            else
                selectedProfile.SetPath(_profileNamePath);
            selectedProfile.LoadProfile();
        }
    }
}
