﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareLibrary.Model
{
    public class PtzCamera: BaseModel
    {
        private int setPresetNumber;
        public int SetPresetNumber { get { return setPresetNumber; } set{ setPresetNumber = value; OnPropertyChanged("SetPresetNumber"); } }

        private int reCallPresetNumber;
        public int ReCallPresetNumber { get { return reCallPresetNumber; } set { reCallPresetNumber = value; OnPropertyChanged("ReCallPresetNumber"); } }

        private int panSpeed;
        public int PanSpeed { get { return panSpeed; } set { panSpeed = value; OnPropertyChanged("PanSpeed"); } }

        private int tiltSpeed;
        public int TiltSpeed { get { return tiltSpeed; } set { tiltSpeed = value; OnPropertyChanged("TiltSpeed"); } }

        private int zoomSpeed;
        public int ZoomSpeed { get { return zoomSpeed; } set { zoomSpeed = value; OnPropertyChanged("ZoomSpeed"); } }

        private int focusSpeed;
        public int FocusSpeed { get { return focusSpeed; } set { focusSpeed = value; OnPropertyChanged("FocusSpeed"); } }

        private bool isAutoFocus;
        public bool IsAutoFocus { get { return isAutoFocus; } set { isAutoFocus = value; OnPropertyChanged("IsAutoFocus"); } }

        private bool isXBOX;
        public bool IsXBOX { get { return isXBOX; } set { isXBOX = value; OnPropertyChanged("IsXBOX"); } }

        private Dictionary<int, string> presetNameList;
        public Dictionary<int, string> PresetNameList { get { return presetNameList; } set { presetNameList = value; } }
        public PtzCamera()
        {
            Reset();
        }
        public void Reset()
        {
            SetPresetNumber = 1;
            ReCallPresetNumber = 1;
            PanSpeed = 5;
            TiltSpeed = 5;
            ZoomSpeed = 5;
            FocusSpeed = 5;
            IsAutoFocus = true;
            IsXBOX = false;
            PresetNameList = new Dictionary<int, string>();
            PresetNameList.Add(0, "No Movement");
        }
    }
}
