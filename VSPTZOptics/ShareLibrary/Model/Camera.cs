﻿using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ShareLibrary.Model
{

    public class Camera : BaseModel
    {
        public Camera()
        {
            Reset();
        }
        public void Reset()
        {
            this.ID = "";
            this.CameraType = CAMERA_TYPE_ENUM.PTZ_OPTICS;
            this.CameraStatus = STATUS_ENUM.UNKNOW;
            this.MainUrl = "";
            this.SubUrl = "";
            this.Ip_Domain = "";
            this.ViewMode = VIEW_MODE_ENUM.LIVE;
            this.ControlStatus = STATUS_ENUM.UNKNOW;
            this.SocketPort = 5678;
            this.PTZ = new PtzCamera();
            this.StageList = null;
            this.IsConnect = false;
            this.IsReconnect = false;
            this.isNDIOutput = false;
            this.IsIPCamera = true;
            this.IsNDICamera = false;
            this.IsUSBCamera = false;
            this.NDICameraName = "";
            this.USBCameraName = "";
            this.ImageName = "";
            this.Name = "";
        }
        public Camera Clone()
        {
            Camera newCam = new Camera();
            newCam.Index = this.Index;
            newCam.CameraType = this.CameraType;
            newCam.CameraStatus = this.cameraStatus;
            newCam.MainUrl = this.MainUrl;
            newCam.SubUrl = this.SubUrl;
            newCam.Ip_Domain = this.Ip_Domain;
            newCam.ViewMode = this.ViewMode;
            newCam.ControlStatus = this.ControlStatus;
            newCam.SocketPort = this.SocketPort;
            newCam.IsConnect = this.IsConnect;
            newCam.IsReconnect = this.IsReconnect;
            newCam.IsNDICamera = this.IsNDICamera;
            newCam.IsIPCamera = this.IsIPCamera;
            newCam.IsUSBCamera = this.IsUSBCamera;
            newCam.IsNDIOutput = this.IsNDIOutput;
            newCam.NDICameraName = this.NDICameraName;
            newCam.USBCameraName = this.USBCameraName;
            newCam.ImageName = this.ImageName;
            newCam.Name = this.Name;
            return newCam;
        }
        public void Copy(Camera _src)
        {
            this.Index = _src.Index;
            this.CameraType = _src.CameraType;
            this.CameraStatus = _src.cameraStatus;
            this.MainUrl = _src.MainUrl;
            this.SubUrl = _src.SubUrl;
            this.Ip_Domain = _src.Ip_Domain;
            this.ViewMode = _src.ViewMode;
            this.ControlStatus = _src.ControlStatus;
            this.SocketPort = _src.SocketPort;
            this.IsConnect = _src.IsConnect;
            this.IsReconnect = _src.IsReconnect;
            this.IsNDICamera = _src.IsNDICamera;
            this.IsIPCamera = _src.IsIPCamera;
            this.IsUSBCamera = _src.IsUSBCamera;
            this.IsNDIOutput = _src.IsNDIOutput;
            this.NDICameraName = _src.NDICameraName;
            this.USBCameraName = _src.USBCameraName;
            this.ImageName = _src.ImageName;
            this.Name = _src.Name;
        }
        private string id; //Unique ID
        public string ID
        {
            get { return id; }
            set { id = value;}
        }
        private int index; 
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        private string socketID; //Unique ID
        public string SocketID
        {
            get { return socketID; }
            set { socketID = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set {
                if(name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value;}
        }

        private string cameraGroup;
        public string CameraGroup
        {
            get { return cameraGroup; }
            set { cameraGroup = value;}
        }

        string ip_Domain;
        public string Ip_Domain
        {
            get { return ip_Domain; }
            set {
                if(ip_Domain != value)
                {
                    if(!string.IsNullOrEmpty(value))
                    {
                        subUrl = "rtsp://" + value + "/2"; //"rtsp://admin:admin123@minhxuonghost.dyndns.org:554/cam/realmonitor?channel=1&amp;subtype=00";//"rtsp://" + ip_Domain + "/2";
                        mainUrl = "rtsp://" + value + "/1"; //"rtsp://admin:admin123@minhxuonghost.dyndns.org:554/cam/realmonitor?channel=1&amp;subtype=00";//"rtsp://" + ip_Domain + "/1";
                        //ControlStatus = STATUS_ENUM.DISCONNECTED;
                        if (!string.IsNullOrEmpty(ip_Domain))
                            isReconnect = true;
                    }
                    else
                    {
                        //ControlStatus = STATUS_ENUM.UNKNOW;
                        MainStreamStatus = STATUS_ENUM.UNKNOW;
                        SubStreamStatus = STATUS_ENUM.UNKNOW;
                    }
                    ip_Domain = value;
                }                
            }
        }

        private string ndiCameraName;
        public string NDICameraName
        {
            get { return ndiCameraName; }
            set {
                if (ndiCameraName != value)
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (!string.IsNullOrEmpty(ndiCameraName))
                            isReconnect = true;
                    }
                    else
                    {
                        MainStreamStatus = STATUS_ENUM.UNKNOW;
                        SubStreamStatus = STATUS_ENUM.UNKNOW;
                    }
                    ndiCameraName = value;
                }
                OnPropertyChanged("NDICameraName");
            }
        }

        private string usbCameraName;
        public string USBCameraName
        {
            get { return usbCameraName; }
            set {
                if (usbCameraName != value)
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (!string.IsNullOrEmpty(usbCameraName))
                            isReconnect = true;
                    }
                    else
                    {
                        MainStreamStatus = STATUS_ENUM.UNKNOW;
                        SubStreamStatus = STATUS_ENUM.UNKNOW;
                    }
                    usbCameraName = value;
                }
                OnPropertyChanged("USBCameraName"); }
        }

        private string imageName;
        public string ImageName
        {
            get { return imageName; }
            set {
                if (imageName != value)
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (!string.IsNullOrEmpty(imageName))
                            isReconnect = true;
                    }
                    else
                    {
                        MainStreamStatus = STATUS_ENUM.UNKNOW;
                        SubStreamStatus = STATUS_ENUM.UNKNOW;
                    }
                    imageName = value;
                }
                OnPropertyChanged("ImageName"); }
        }

        private bool isReconnect;
        public bool IsReconnect
        {
            get { return isReconnect; }
            set
            {
                if (isReconnect != value)
                {
                    isReconnect = value;
                }
            }
        }

        private CAMERA_TYPE_ENUM cameraType;
        public CAMERA_TYPE_ENUM CameraType
        {
            get { return cameraType; }
            set { cameraType = value;}
        }       

        private bool isHWAccelarate; //enable HW acceleration for using GPU
        public bool IsHWAccelarate
        {
            get { return isHWAccelarate; }
            set { isHWAccelarate = value;}
        }        

        private CAMERA_STREAM_ENUM liveStream;
        public CAMERA_STREAM_ENUM LiveStream
        {
            get { return liveStream; }
            set { liveStream = value;}
        }

        private int rtspPort;
        public int RtspPort
        {
            get { return rtspPort; }
            set { rtspPort = value;}
        }

        private int onvifPort;
        public int OnvifPort
        {
            get { return onvifPort; }
            set { onvifPort = value; }
        }

        private int httpPort;
        public int HttpPort
        {
            get { return httpPort; }
            set { httpPort = value;}
        }

        private int socketPort;
        public int SocketPort
        {
            get { return socketPort; }
            set { socketPort = value; }
        }

        private string mainUrl;
        public string MainUrl
        {
            get { return mainUrl; }
            set { mainUrl = value;}
        }

        private string subUrl;
        public string SubUrl
        {
            get { return subUrl; }
            set { subUrl = value;}
        }

        private string mainEncoding;
        public string MainEncoding
        {
            get { return mainEncoding; }
            set { mainEncoding = value; }
        }

        private string subEncoding;
        public string SubEncoding
        {
            get { return subEncoding; }
            set { subEncoding = value; }
        }

        private string mainResolution;
        public string MainResolution
        {
            get { return mainResolution; }
            set { mainResolution = value;}
        }

        private string subResolution;
        public string SubResolution
        {
            get { return subResolution; }
            set { subResolution = value;}
        }

        private string macAddress;
        public string MacAddress
        {
            get { return macAddress; }
            set { macAddress = value;}
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
            set { userName = value;}
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { password = value;}
        }

        private STATUS_ENUM cameraStatus;
        public STATUS_ENUM CameraStatus
        {
            get { return cameraStatus; }
            set { cameraStatus = value; OnPropertyChanged("CameraStatus"); }
        }

        private STATUS_ENUM mainStreamStatus;
        public STATUS_ENUM MainStreamStatus
        {
            get { return mainStreamStatus; }
            set { mainStreamStatus = value; OnPropertyChanged("MainStreamStatus"); }
        }

        private STATUS_ENUM subStreamStatus;
        public STATUS_ENUM SubStreamStatus
        {
            get { return subStreamStatus; }
            set { subStreamStatus = value; OnPropertyChanged("SubStreamStatus"); }
        }

        private STATUS_ENUM controlStatus;
        public STATUS_ENUM ControlStatus
        {
            get { return controlStatus; }
            set { controlStatus = value; OnPropertyChanged("ControlStatus"); }
        }

        private bool isSnapshot;
        public bool IsSnapshot
        {
            get { return isSnapshot; }
            set {
                if(isSnapshot != value)
                {
                    isSnapshot = value;
                }
            }
        }

        private string imagePath;
        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; }
        }

        private VIEW_MODE_ENUM viewMode;
        public VIEW_MODE_ENUM ViewMode
        {
            get { return viewMode; }
            set { viewMode = value; }
        }

        private double longitude;
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value;}
        }

        private double latitude;
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value;}
        }

        private bool isNDIOutput;
        public bool IsNDIOutput
        {
            get { return isNDIOutput; }
            set { isNDIOutput = value; OnPropertyChanged("IsNDIOutput"); }
        }

        private bool isIPCamera;
        public bool IsIPCamera
        {
            get { return isIPCamera; }
            set { isIPCamera = value; OnPropertyChanged("IsIPCamera"); }
        }

        private bool isNDICamera;
        public bool IsNDICamera
        {
            get { return isNDICamera; }
            set { isNDICamera = value; OnPropertyChanged("IsNDICamera"); }
        }

        private bool isUSBCamera;
        public bool IsUSBCamera
        {
            get { return isUSBCamera; }
            set { isUSBCamera = value; OnPropertyChanged("IsUSBCamera"); }
        }

        private bool isConnect;
        public bool IsConnect
        {
            get { return isConnect; }
            set { isConnect = value;
                IsDisConnect = !isConnect;
                OnPropertyChanged("IsConnect");
            }
        }

        private bool isDisConnect;
        public bool IsDisConnect
        {
            get { return isDisConnect; }
            set { isDisConnect = value; OnPropertyChanged("IsDisConnect"); }
        }

        private PtzCamera ptz;
        public PtzCamera PTZ
        {
            get { return ptz; }
            set { ptz = value; }
        }

        private List<Stage> stageList;
        public List<Stage> StageList
        {
            get { return stageList; }
            set { stageList = value; }
        }

        public event EventHandler<CameraEventArgs> CameraEvent;
        protected virtual void OnCameraEvent(CameraEventArgs e)
        {
            EventHandler<CameraEventArgs> handler = CameraEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
