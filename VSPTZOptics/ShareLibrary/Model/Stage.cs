﻿using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareLibrary.Model
{
    public class Stage
    {
        private int index;
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private List<int> presetNumberList;
        public List<int> PresetNumberList
        {
            get { return presetNumberList; }
            set { presetNumberList = value; }
        }

        private Rectangle rect;
        public Rectangle Rect { get { return rect; }
            set {
                rect = value;
            }
        }

        private Color cl;
        public Color Cl { get { return cl; } set { cl = value; } }

        private int scale;
        public int Scale { get { return scale; } set { scale = value; } }

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
            }
        }

        private bool isCallStagePreset;
        public bool IsCallStagePreset
        {
            get { return isCallStagePreset; }
            set
            {
                isCallStagePreset = value;
                if (isCallStagePreset == true)
                {
                    StageEventArgs args = new StageEventArgs();
                    args.EventType = STAGE_EVENT_TYPE.CALL_PRESETS;
                    OnStageEvent(args);
                }
            }
        }
        private bool isShowOnStageView;
        public bool IsShowOnStageView { get { return isShowOnStageView; } set { isShowOnStageView = value; } }

        private string httpTrigger;
        public string HTTPTrigger { get { return httpTrigger; } set { httpTrigger = value; } }

        private int windowW;
        public int WindowW
        {
            get { return windowW; }
            set { windowW = value; }
        }
        private int windowH;
        public int WindowH
        {
            get { return windowH; }
            set { windowH = value; }
        }

        private bool isLock;
        public bool IsLock
        {
            get { return isLock; }
            set
            {
                isLock = value;
            }
        }
        public Stage(int _ind)
        {
            index = _ind;
            Reset();
        }
        public void Reset()
        {
            presetNumberList = new List<int>();
            for (int i = 0; i < 8; i++)
                presetNumberList.Add(0);//0 - no movement
            Rect = new Rectangle(80*index, 0, 80, 80);
            Cl = Color.Black;
            httpTrigger = "";
            windowW = windowH = 0;
            Name = "Sp" + (index+1);
            IsShowOnStageView = false;
            IsLock = false;
            IsSelected = false;
        }
        public event EventHandler<StageEventArgs> StageEvent;
        protected virtual void OnStageEvent(StageEventArgs e)
        {
            EventHandler<StageEventArgs> handler = StageEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
