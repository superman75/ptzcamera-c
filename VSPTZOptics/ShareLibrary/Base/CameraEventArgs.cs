﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareLibrary.Base
{

    public class CameraEventArgs : EventArgs
    {
        public CAMERA_EVENT_TYPE EventType { get; set; }
        public string ImagePath { get; set; }
    }
}
