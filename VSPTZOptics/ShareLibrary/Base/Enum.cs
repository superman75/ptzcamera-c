﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareLibrary.Base
{
    public enum LIMITATION_ENUM
    {
        MAX_CAMERA_SUPPORT = 8,
        MAX_CLIENT_MESSAGE_QUEUE = 10,
        MAX_AVPACKET_QUEUE = 500,
        MAX_PFRAME_QUEUE = 120, 
    }
    public enum CAMERA_TYPE_ENUM
    {
        PTZ_OPTICS = 0,
        ZCAM,
        STATIC_IMAGE,
        NDI,
        USB,
        UNKNOWN
    }
    public enum CAMERA_STREAM_ENUM
    {
        MAIN = 0,
        SUB,
        AUTO,
        UNKNOWN
    }
    public enum STATUS_ENUM
    {
        UNKNOW = 0,
        CONNECTED,
        DISCONNECTED
    }
    public enum DIVISIONS_ENUM
    {
        DIVISION_1 = 1,
        DIVISION_2 = 2,
        DIVISION_3 = 3,
        DIVISION_4 = 4,
        DIVISION_6 = 6,
        DIVISION_8 = 8,
        DIVISION_9 = 9,
        DIVISION_10 = 10,
        DIVISION_13 = 13,
        DIVISION_16 = 16,
        DIVISION_25 = 25,
        DIVISION_36 = 36,
        DIVISION_49 = 49,
        DIVISION_64 = 64
    }
    public enum RESULT_ENUM
    {
        RS_NOT_OK = -1,
        RS_OK,
        RS_CONTINUE,
        RS_STOP,
        RS_END,
        RS_AU_NOT_OK,
        RS_VI_NOT_OK,
        RS_EQUAL,
        RS_GREATER,
        RS_LESS,
        RS_IGNORE,
        RS_INVALID_DATA_INPUT,
        RS_BUFFRING
    }
 
    public enum CLIENT_TYPE_ENUM
    {
        UNKNOWN_CLIENT_TYPE = 0,
        MOBILE_CLIENT,
        PC_CLIENT,
        CAM_MOBOTIX
    }
    public enum FRAME_TYPE_ENUM
    {
        I_FRAME = 0,
        P_FRAME,
        B_FRAME,
        S_FRAME,
        UN_FRAME
    }
    public enum CODEC_TYPE_ENUM
    {
        JPEG = 0,
        MPEG4,
        H264,
        H265,
        CODEC_TYPE_UNKNOWN,
        NUM_OF_CODEC_TYPE
    }

    public enum VIEW_MODE_ENUM
    {
        LIVE = 0,
        STATIC,
        PLAYBACK,
        BACKUP,
        IMP,
        EMAP,
        RECORD
    }
    public enum PACKET_MODE_ENUM
    {
        INIT = 0,
        DRAW_DATA
    };
    public enum DISPLAY_RATIO_ENUM
    {
        FULL_SCREEN,
        ORIGINAL_RATIO,
        RATIO_16_9,
        RATIO_4_3,
        RATIO_16_10
    }
    public enum ASPECT_RATIO
    {
        WIDTH = 16,
        HEIGHT= 10
    }
    public enum APP_ASPECT_RATIO
    {
        WIDTH = 4,
        HEIGHT = 3
    }
    public enum SETUP_MODE_ENUM
    {
        CAMERA_SETUP,
        PRESET_SETUP,
        STAGE_SETUP
    }
    public enum CAMERA_EVENT_TYPE
    {
        SNAPSHOT
    }
    public enum STAGE_EVENT_TYPE
    {
        CALL_PRESETS
    }
    public enum JOYSTICK_EVENT_TYPE
    {
        POV_EVENT,
        AXIS_EVENT,
        BUTTON_EVENT
    }
    public enum JOYSTICK_BUTTON_ENUM
    {
        BTN_START = 9,
        BTN_BACK = 8,
        BTN_LB = 4,
        BTN_LT = 6,
        BTN_RB = 5,
        BTN_RT = 7,
        BTN_X = 3,
        BTN_Y = 0,
        BTN_A = 2,
        BTN_B = 1,
        BTN_TL = 10,
        BTN_TR = 11
    }
    public struct JOYSTICK
    {
        public const double STICK_THRESHOLD = 0.2F;
    }

    public enum IMAGE_TYPE_ENUM
    {
        JPEG,
        PNG
    }
    public enum MESSAGE_STATUS_ENUM
    {
        PENDING,
        SENT,
        RESPONDED
    }
    public enum PTZ_CONTROL_ENUM
    {
        MOVE_LEFT = 0,
        MOVE_RIGHT,
        MOVE_TOP,
        MOVE_BOTTOM,
        MOVE_HOME,
        STOP_MOVING,
        ZOOM_IN,
        ZOOM_OUT,
        STOP_ZOOMING,
        FOCUS_IN,
        FOCUS_OUT,
        STOP_FOCUSING,
        AUTO_FOCUS,
        RECAL_PRESET,
        SET_PRESET
    }
}
