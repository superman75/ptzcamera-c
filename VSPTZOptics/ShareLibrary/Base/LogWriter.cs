﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace ShareLibrary.Base
{
    public class LogWriter
    {
        private static LogWriter _instance = null;
        private static string _fileName = null;
        private static object _lock = new object();
        private static bool _isWriteDetail = true;
        private static int _logLevel = 2;

        public LogWriter()
        {
            var directory = AppDomain.CurrentDomain.BaseDirectory; // Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string logFolder = Path.Combine(directory, "Logs");

            bool isError = false;
            if (!Directory.Exists(logFolder))
            {
                try
                {
                    Directory.CreateDirectory(logFolder);
                }
                catch
                {
                    isError = true;
                }
            }

            if (!isError)
            {
                logFolder = Path.Combine(logFolder, DateTime.Now.ToString("yyyy_MM_dd"));
                if (!Directory.Exists(logFolder))
                {
                    try
                    {
                        Directory.CreateDirectory(logFolder);
                    }
                    catch
                    {
                        isError = true;
                    }
                }
            }

            if (!isError)
            {
                _fileName = Path.Combine(logFolder, "ptz_optics_log.txt");
                if (!File.Exists(_fileName))
                {
                    try
                    {
                        File.Create(_fileName).Close();
                    }
                    catch { }
                }
            }
        }

        public void WriteLog(int logLevel, string logMessage, [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string caller = null)
        {
            if (logLevel < _logLevel)
                return;
            lock (_lock)
            {
                if (File.Exists(_fileName))
                {
                    try
                    {
                        using (StreamWriter w = File.AppendText(_fileName))
                        {
                            if(_isWriteDetail)
                                Log(logMessage + " lineNumber["+lineNumber+"], caller["+caller+"]", w);
                            else
                                Log(logMessage, w);
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        File.Create(_fileName).Close();
                    }
                    catch { }
                }
            }
        }

        private void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("{0}", logMessage);
            w.WriteLine("---------------------------------------------------------------------------------------------");
        }

        public static LogWriter Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogWriter();
                }
                return _instance;
            }
        }
    }
}
