﻿using AsyncSocket;
using AsyncSocket.Base;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CameraControl.Model
{
    public class CamControl
    {
       
        private Camera cameraInfo;
        private bool isStop;
        private Queue<object> sendMessageQueue; //list of data to be sent
        private object queueLock;
        private Thread threadSendData;
        ManualResetEvent oSignalEvent;
        private System.Timers.Timer timer = null;
        private object timerLock = null;
        public Camera Cam
        {
            get { return cameraInfo; }
            set { cameraInfo = value; }
        }
        public CamControl()
        {

        }
        public CamControl(Camera _cameraInfo)
        {
            cameraInfo = _cameraInfo;
            isStop = false;
            sendMessageQueue = new Queue<object>((int)LIMITATION_ENUM.MAX_CLIENT_MESSAGE_QUEUE);
            queueLock = new object();
            oSignalEvent = new ManualResetEvent(false);
            threadSendData = new Thread(SendDataThread);
            threadSendData.SetApartmentState(ApartmentState.MTA);
            threadSendData.Start();
            InitTimer();
        }
        public void InitTimer()
        {
            timer = new System.Timers.Timer(); // fire every 1 second
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 5000;
            timer.Enabled = true;
            timerLock = new object();
            timer.Start();
        }
        private void Stop_Timer()
        {
            timer.Stop();
        }
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock(timerLock)
                {
                    if (!isStop)
                        CheckForReconnnectCamera();
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteLog(2, ex.Message + ex.StackTrace);
            }
        }
        public bool Enqueue(object _message)
        {
            bool res = false;
            lock (queueLock)
            {
                if (sendMessageQueue.Count >= (int)LIMITATION_ENUM.MAX_CLIENT_MESSAGE_QUEUE)
                {
                    LogWriter.Instance.WriteLog(2, "Queue is full count = " + sendMessageQueue.Count);
                    res = false;
                }
                else
                {
                    sendMessageQueue.Enqueue(_message);
                    res = true;
                }
            }
            oSignalEvent.Set();
            oSignalEvent.Reset();//Once we call the Set() method on the ManualResetEvent object, its boolean remains true. To reset the value we can use Reset() method. Reset method change the boolean value to false.
            return res;
        }
        public object Dequeue()
        {
            lock (queueLock)
            {
                if (sendMessageQueue.Count > 0)
                {
                    return sendMessageQueue.Dequeue();
                }
                else
                {
                    return null;
                }
            }
        }
        public void ClearQueue()
        {
            lock (queueLock)
            {
                sendMessageQueue.Clear();
            }
        }
        public virtual void SendData(object _message)
        {

        }
        private void SendDataThread()
        {
            LogWriter.Instance.WriteLog(1, "SendDataToServer start");
            object sendMessage;
            while (!isStop)
            {
                try
                {
                    lock (queueLock)
                    {
                        if (sendMessageQueue.Count > 0)
                        {
                            sendMessage = sendMessageQueue.Dequeue();
                        }
                        else
                            sendMessage = null;
                    }
                    if (sendMessage != null)
                    {
                        SendData(sendMessage);
                    }
                    else
                        oSignalEvent.WaitOne();
                }
                catch (Exception e)
                {
                    LogWriter.Instance.WriteLog(2, e.Message + e.StackTrace);
                }
            }
            LogWriter.Instance.WriteLog(1, "SendDataToServer end");
        }
        public void CloseThreads()
        {
            try
            {
                isStop = true;
                oSignalEvent.Set();
                threadSendData.Abort();
                Stop_Timer();
            }
            catch { }
        }
        public virtual void Close()
        {
            CloseThreads();            
        }

        public virtual void ConnectCamera()
        {

        }
        public virtual void DisconnectCamera()
        {

        }
        public virtual void CheckForReconnnectCamera()
        {

        }
        public virtual void MoveTopOnce(int _panSpeed, int _tiltSpeed)
        {

        }
        public virtual void MoveLeftOnce(int _panSpeed, int _tiltSpeed)
        {
            
        }
        public virtual void MoveBottomOnce(int _panSpeed, int _tiltSpeed)
        {
            
        }
        public virtual void MoveRightOnce(int _panSpeed, int _tiltSpeed)
        {
            
        }
        public virtual void MoveHome()
        {
            
        }
        public virtual void StopMoving(int _panSpeed, int _tiltSpeed)
        {
            
        }
        public virtual void StopZooming()
        {
            
        }
        public virtual void ZoomInOnce(int _zoomSpeed)
        {
            
        }
        public virtual void ZoomOutOnce(int _zoomSpeed)
        {
           
        }
        public virtual void FocusInOnce(int _focusSpeed)
        {
            
        }
        public virtual void FocusOutOnce(int _focusSpeed)
        {
            
        }
        public virtual void AutoFocusChange(bool isChecked)
        {  
            
        }
        public virtual void StopFocusing()
        {
            
        }
        public virtual void SetPreset(int _index)
        {
            
        }
        public virtual void RecallPreset(int _index)
        {
            
        }
        public virtual void OSDToggle()
        {
            
        }
        public virtual void OSDEnter()
        {
            
        }
        public virtual void OSDBack()
        {
            
        }

        /*camera query functions begin*/
        public virtual void getZoomInfo()
        {

        }
        public virtual void getFocusInfo()
        {

        }
        public virtual void getPanTiltInfo()
        {

        }
        /*camera query functions end*/
    }
}
