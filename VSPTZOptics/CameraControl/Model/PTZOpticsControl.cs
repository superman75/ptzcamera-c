﻿using AsyncSocket;
using AsyncSocket.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraControl.Model
{
    public class PTZOpticsControl : CamControl
    {
        public SocketCommunication socket;
        public PTZOpticsControl()
        {

        }
        public PTZOpticsControl(Camera _cameraInfo) : base(_cameraInfo)
        {
            if (Cam != null)
            {
                if (!string.IsNullOrEmpty(Cam.Ip_Domain))
                {
                    socket = new SocketCommunication(Cam.Ip_Domain, Cam.SocketPort);
                    socket.Client.SocketEvent += Client_SocketEvent;
                    if (Cam.IsConnect && Cam.IsIPCamera)
                        socket.Start();
                }
            }
        }

        private void Client_SocketEvent(object sender, SocketEventArgs e)
        {
            if (e.EventType == EVENT_TYPE_ENUM.SOCKET_STATUS && Cam.IsIPCamera)
                Cam.ControlStatus = e.SocketStatus;
        }

        public override void ConnectCamera()
        {
            if (socket == null)
            {
                if (!string.IsNullOrEmpty(Cam.Ip_Domain))
                {
                    socket = new SocketCommunication(Cam.Ip_Domain, Cam.SocketPort);
                    socket.Client.SocketEvent += Client_SocketEvent;
                }
            }
            if (socket != null)
            {
                if (Cam.Ip_Domain != socket.Client.ServerAddress || Cam.SocketPort != socket.Client.Port)
                {
                    socket.Client.ServerAddress = Cam.Ip_Domain;
                    socket.Client.Port = Cam.SocketPort;
                    socket.Stop();
                }
                socket.Start();
            }
        }
        public override void Close()
        {
            base.Close();
            if (socket != null)
                socket.Close();
        }
        public override void DisconnectCamera()
        {
            if (socket != null)
                socket.Stop();
        }
        public override void SendData(object _message)
        {
            if (socket != null)
                socket.Send((ControlMessage)_message);
        }
        public override void MoveTopOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[9];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x01;
            command[4] = (byte)(_panSpeed % 255);
            command[5] = (byte)(_tiltSpeed % 255);
            command[6] = 0x03;
            command[7] = 0x01;
            command[8] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void MoveLeftOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[9];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x01;
            command[4] = (byte)(_panSpeed % 255);
            command[5] = (byte)(_tiltSpeed % 255);
            command[6] = 0x01;
            command[7] = 0x03;
            command[8] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void MoveBottomOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[9];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x01;
            command[4] = (byte)(_panSpeed % 255);
            command[5] = (byte)(_tiltSpeed % 255);
            command[6] = 0x03;
            command[7] = 0x02;
            command[8] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void MoveRightOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[9];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x01;
            command[4] = (byte)(_panSpeed % 255);
            command[5] = (byte)(_tiltSpeed % 255);
            command[6] = 0x02;
            command[7] = 0x03;
            command[8] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void MoveHome()
        {
            byte[] command = new byte[5];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x04;
            command[4] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void StopMoving(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[9];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x01;
            command[4] = (byte)(_panSpeed % 255);
            command[5] = (byte)(_tiltSpeed % 255);
            command[6] = 0x03;
            command[7] = 0x03;
            command[8] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void StopZooming()
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x07;
            command[4] = 0x00;
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void ZoomInOnce(int _zoomSpeed)
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x07;
            command[4] = (byte)(0x20 + _zoomSpeed);
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void ZoomOutOnce(int _zoomSpeed)
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x07;
            command[4] = (byte)(0x30 + _zoomSpeed);
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void FocusInOnce(int _focusSpeed)
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x08;
            command[4] = (byte)(0x20 + _focusSpeed);
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void FocusOutOnce(int _focusSpeed)
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x08;
            command[4] = (byte)(0x30 + _focusSpeed);
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void AutoFocusChange(bool isChecked)
        {  
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x38;
            if (isChecked)
                command[4] = 0x02;
            else
                command[4] = 0x03;
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void StopFocusing()
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x08;
            command[4] = 0x00;
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void SetPreset(int _index)
        {
            byte[] command = new byte[7];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x3f;
            command[4] = 0x01;
            command[5] = (byte)(_index % 255);
            command[6] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void RecallPreset(int _index)
        {
            byte[] command = new byte[7];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x3f;
            command[4] = 0x02;
            command[5] = (byte)(_index % 255);
            command[6] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.COMMAND, command);
            base.Enqueue(message);
        }
        public override void OSDToggle()
        {
            byte[] command = new byte[7];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x04;
            command[3] = 0x3f;
            command[4] = 0x02;
            command[5] = 0x5f;
            command[6] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.OSD, command);
            base.Enqueue(message);
        }
        public override void OSDEnter()
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x06;
            command[4] = 0x05;
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.OSD, command);
            base.Enqueue(message);
        }
        public override void OSDBack()
        {
            byte[] command = new byte[6];
            command[0] = 0x81;
            command[1] = 0x01;
            command[2] = 0x06;
            command[3] = 0x06;
            command[4] = 0x04;
            command[5] = 0xff;
            ControlMessage message = new ControlMessage((int)PTZ_COMMAND_TYPE_ENUM.OSD, command);
            base.Enqueue(message);
        }

        /*camera query functions begin*/
        public override void getZoomInfo()
        {

        }
        public override void getFocusInfo()
        {

        }
        public override void getPanTiltInfo()
        {

        }
        /*camera query functions end*/
    }
}
