﻿using AsyncSocket.Base;
using NDIStream;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CameraControl.Model
{
    public class NDIControl : CamControl
    {
        NDIStreamRef ndiStream;
        private object lockThread = new object();
        public NDIControl()
        {

        }
        public NDIControl(Camera _cameraInfo) : base(_cameraInfo)
        {
            ndiStream = new NDIStreamRef();
            if (Cam.IsConnect && Cam.IsNDICamera)
                ConnectCamera();
            
        }

        public override void CheckForReconnnectCamera()
        {
            if (Cam.IsConnect && Cam.IsNDICamera){
                ConnectCamera();
            }
        }

        public override void Close()
        {
            base.Close();
            ndiStream.Dispose();
            ndiStream = null;
        }

        public override void ConnectCamera()
        {
            if (Cam.IsConnect){
                new Thread(delegate (){
                    try{
                        lock (lockThread){
                            if (Cam.IsNDICamera){
                                if(Cam.ControlStatus != ShareLibrary.Base.STATUS_ENUM.CONNECTED)
                                {
                                    if (!string.IsNullOrEmpty(Cam.NDICameraName) && ndiStream.NDIPTZConnect(Cam.NDICameraName))
                                    {
                                        Cam.ControlStatus = ShareLibrary.Base.STATUS_ENUM.CONNECTED;
                                    }
                                    else
                                        Cam.ControlStatus = ShareLibrary.Base.STATUS_ENUM.DISCONNECTED;
                                }
                                Cam.ControlStatus = Cam.ControlStatus; //trigger binding for icon buttons
                            }
                           
                        }
                    }
                    catch { }
                }).Start();
            }
            else
            {
                if (Cam.IsNDICamera)
                    Cam.ControlStatus = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            }

        }
        public override void DisconnectCamera()
        {
            ndiStream.NDIDisConnect();
        }
        public override void SendData(object _message)
        {
            ControlMessage message = _message as ControlMessage;
            ndiStream.NDIControlReq(message.MessageType, (int)message.Data[0], (int)message.Data[1]);
        }
        public override void MoveTopOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_panSpeed % 255);
            command[1] = (byte)(_tiltSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.MOVE_TOP, command);
            base.Enqueue(message);
        }
        public override void MoveLeftOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_panSpeed % 255);
            command[1] = (byte)(_tiltSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.MOVE_LEFT, command);
            base.Enqueue(message);
        }
        public override void MoveBottomOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_panSpeed % 255);
            command[1] = (byte)(_tiltSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.MOVE_BOTTOM, command);
            base.Enqueue(message);
        }
        public override void MoveRightOnce(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_panSpeed % 255);
            command[1] = (byte)(_tiltSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.MOVE_RIGHT, command);
            base.Enqueue(message);
        }
        public override void MoveHome()
        {
            byte[] command = new byte[2];
            command[0] = 0;
            command[1] = 0;
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.MOVE_HOME, command);
            base.Enqueue(message);
        }
        public override void StopMoving(int _panSpeed, int _tiltSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_panSpeed % 255);
            command[1] = (byte)(_tiltSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.STOP_MOVING, command);
            base.Enqueue(message);
        }
        public override void StopZooming()
        {
            byte[] command = new byte[2];
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.STOP_ZOOMING, command);
            base.Enqueue(message);
        }
        public override void ZoomInOnce(int _zoomSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_zoomSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.ZOOM_IN, command);
            base.Enqueue(message);
        }
        public override void ZoomOutOnce(int _zoomSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_zoomSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.ZOOM_OUT, command);
            base.Enqueue(message);
        }
        public override void FocusInOnce(int _focusSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_focusSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.FOCUS_IN, command);
            base.Enqueue(message);
        }
        public override void FocusOutOnce(int _focusSpeed)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_focusSpeed % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.FOCUS_OUT, command);
            base.Enqueue(message);
        }
        public override void AutoFocusChange(bool _isChecked)
        {
            if(_isChecked)
            {
                byte[] command = new byte[2];
                ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.AUTO_FOCUS, command);
                base.Enqueue(message);
            }
        }
        public override void StopFocusing()
        {
            byte[] command = new byte[2];
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.STOP_FOCUSING, command);
            base.Enqueue(message);
        }
        public override void SetPreset(int _index)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_index % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.SET_PRESET, command);
            base.Enqueue(message);
        }
        public override void RecallPreset(int _index)
        {
            byte[] command = new byte[2];
            command[0] = (byte)(_index % 255);
            ControlMessage message = new ControlMessage((int)PTZ_CONTROL_ENUM.RECAL_PRESET, command);
            base.Enqueue(message);
        }
        public override void OSDToggle()
        {

        }
        public override void OSDEnter()
        {

        }
        public override void OSDBack()
        {

        }

        /*camera query functions begin*/
        public override void getZoomInfo()
        {

        }
        public override void getFocusInfo()
        {

        }
        public override void getPanTiltInfo()
        {

        }
        /*camera query functions end*/
    }
}
