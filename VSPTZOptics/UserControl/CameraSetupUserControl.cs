﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShareLibrary.Model;
using CameraView.ViewModel;
using CameraUserControl.Model;
using ShareLibrary.Base;
using CameraUserControl.Base;
using CameraControl.Model;
using CameraView.Model;
using Settings.ViewModel;
using CameraUserControl.ViewModel;

namespace CameraUserControl
{
    public partial class CameraSetupUserControl : UserControl
    {
        private int cameraInd; //Current setup for specific camera index (start from 0 - 8); -1 for all cameras
        private List<Camera> unsavedCameraList = new List<Camera>();
        public CameraSetupUserControl()
        {
            InitializeComponent();
            //InitilizeBindingButtons();
        }
        private void InitilizeBindingButtons() //Binding connect icon with camera sub stream status
        {
            for (int rowInd = 1; rowInd <= 8; rowInd++)
            {
                Camera cam = CamViewModel.Instance.CamViewList[rowInd - 1].Cam;
                var control = cameraInfoTableLayoutPanel.GetControlFromPosition(4, rowInd); //Connect Button
                control.DataBindings.Add("Status", cam, "SocketStatus", true, DataSourceUpdateMode.OnPropertyChanged);
                control.DataBindings.Add("IsConnect", cam, "IsConnect", true, DataSourceUpdateMode.OnPropertyChanged);
                control = cameraInfoTableLayoutPanel.GetControlFromPosition(2, rowInd); //IP text box
                control.DataBindings.Add("Enabled", cam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);
                //control = cameraInfoTableLayoutPanel.GetControlFromPosition(3, rowInd); //NDI combobox
                //control.DataBindings.Add("Enabled", cam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);
                /*((ComboBox)control).DataSource = new BindingSource(NDISourceViewModel.Instance.NDISourceList, null);
                ((ComboBox)control).DisplayMember = "Value";
                ((ComboBox)control).ValueMember = "Key";
                if(string.IsNullOrEmpty(cam.NDICameraName))
                    ((ComboBox)control).SelectedValue = 0;
                else
                {
                    ((ComboBox)control).SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(cam.NDICameraName);
                }*/

                control = cameraInfoTableLayoutPanel.GetControlFromPosition(1, rowInd); //Camera Name TextBox
                control.DataBindings.Add("Text", cam, "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            }
            Camera stageCam = CamViewModel.Instance.CamViewList[8].Cam;
            liveStageConnectButton.DataBindings.Add("Status", stageCam, "MainStreamStatus", true, DataSourceUpdateMode.OnPropertyChanged);
            liveStageConnectButton.DataBindings.Add("IsConnect", stageCam, "IsConnect", true, DataSourceUpdateMode.OnPropertyChanged);
            liveTextBox.DataBindings.Add("Enabled", stageCam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);
            /*liveNDINameComboBox.DataSource = new BindingSource(NDISourceViewModel.Instance.NDISourceList, null);
            liveNDINameComboBox.DisplayMember = "Value";
            liveNDINameComboBox.ValueMember = "Key";
            if (string.IsNullOrEmpty(stageCam.NDICameraName))
                liveNDINameComboBox.SelectedValue = 0;
            else
            {
                liveNDINameComboBox.SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(stageCam.NDICameraName);
            }*/

            Camera stageImage = CamViewModel.Instance.CamViewList[9].Cam;
        }
        public void LoadCameraSetup(int _cameraInd)
        {
            NDISourceViewModel.Instance.GetNDISources();
            cameraInd = _cameraInd;
            if(cameraInd == -1)//Load all camera setup
            {
                this.cameraSetupTableLayoutPanel.RowStyles[1].Height = 339F;// PTZ camera
                this.cameraSetupTableLayoutPanel.RowStyles[2].Height = 120F;// Stage Source
                this.ptzCameraGroupBox.Visible = true;
                this.stageSourceGroupBox.Visible = true;
                for (int rowInd = 1; rowInd <= 8; rowInd++){
                    cameraInfoTableLayoutPanel.RowStyles[rowInd].Height = 37F;
                    for (int colInd = 0; colInd < cameraInfoTableLayoutPanel.ColumnCount; colInd++)
                    {
                        var control = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd);
                        if (control != null)
                            control.Visible = true;
                    }
                }
            }
            else
            {
                if(cameraInd == 8) //Load stage source ZCAM
                {
                    this.cameraSetupTableLayoutPanel.RowStyles[1].Height = 0F;// PTZ area
                    this.cameraSetupTableLayoutPanel.RowStyles[2].Height = 120F;// Stage area
                    this.ptzCameraGroupBox.Visible = false;
                    this.stageSourceGroupBox.Visible = true;
                }
                else //Load setup for only specific OPTICS camera
                {
                    this.cameraSetupTableLayoutPanel.RowStyles[1].Height = 80F;// PTZ area
                    this.cameraSetupTableLayoutPanel.RowStyles[2].Height = 0F;// Stage area
                    this.ptzCameraGroupBox.Visible = true;
                    this.stageSourceGroupBox.Visible = false;                    
                    for (int rowInd = 1; rowInd <= 8; rowInd++)
                    {
                        for (int colInd = 0; colInd < cameraInfoTableLayoutPanel.ColumnCount; colInd++)
                        {
                            var control = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd);
                            if (rowInd == cameraInd + 1){
                                cameraInfoTableLayoutPanel.RowStyles[rowInd].Height = 37F;                                
                                if (control != null)
                                    control.Visible = true;
                            }
                            else{
                                cameraInfoTableLayoutPanel.RowStyles[rowInd].Height = 0F;
                                if (control != null)
                                    control.Visible = false;
                            }                            
                        }
                                                   
                    }
                }
            }
            LoadCameraInfo(cameraInd);
        }

        private void LoadStageCameraInfo()
        {
            Camera stageCam = CamViewModel.Instance.CamViewList[8].Cam;
            liveTextBox.Text = stageCam.Ip_Domain;
            liveStageNDICheckbox.Checked = stageCam.IsNDIOutput;
            liveStageNDIInputCheckbox.Checked = stageCam.IsNDICamera;
            liveNameTextBox.Text = stageCam.Name;
            if (string.IsNullOrEmpty(stageCam.NDICameraName))
                liveNDINameComboBox.SelectedValue = 0;
            else
            {
                liveNDINameComboBox.SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(stageCam.NDICameraName);
            }
            unsavedCameraList.Add(stageCam.Clone());
            Camera imageCam = CamViewModel.Instance.CamViewList[9].Cam;
            staticTextBox.Text = imageCam.Ip_Domain;
            imageStageNDICheckBox.Checked = imageCam.IsNDIOutput;
            imageNameTextBox.Text = imageCam.Name;

            liveRadioButton.Checked = ProfileViewModel.Instance.SelectedProfile.IsStageLiveView;
            staticRadioButton.Checked = !liveRadioButton.Checked;
            unsavedCameraList.Add(imageCam.Clone());
        }

        private void LoadPTZCameraInfo(int rowInd)
        {
            var control = cameraInfoTableLayoutPanel.GetControlFromPosition(2, rowInd); //IP Textblox
            Camera cam = CamViewModel.Instance.CamViewList[rowInd - 1].Cam;
            control.Text = cam.Ip_Domain;
            /*control = cameraInfoTableLayoutPanel.GetControlFromPosition(5, rowInd); //NDI input
            ((CheckBox)control).Checked = cam.IsNDICamera;*/
            control = cameraInfoTableLayoutPanel.GetControlFromPosition(6, rowInd); //NDI output
            ((CheckBox)control).Checked = cam.IsNDIOutput;
            control = cameraInfoTableLayoutPanel.GetControlFromPosition(1, rowInd); //Camera Name
            ((TextBox)control).Text = cam.Name;
            /*control = cameraInfoTableLayoutPanel.GetControlFromPosition(3, rowInd); //NDI combobox
            if (string.IsNullOrEmpty(cam.NDICameraName))
                ((ComboBox)control).SelectedValue = 0;
            else
            {
                ((ComboBox)control).SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(cam.NDICameraName);
            }*/
            unsavedCameraList.Add(cam.Clone());
        }

        private void LoadCameraInfo(int _cameraInd) //Load Info to UI
        {
            unsavedCameraList.Clear();
            if(_cameraInd == -1)
            {
                for (int rowInd = 1; rowInd <= 8; rowInd++)
                {
                    LoadPTZCameraInfo(rowInd);
                }
                LoadStageCameraInfo();
            }
            else if(_cameraInd == 8) //Stage setup
            {
                LoadStageCameraInfo();
            }
            else
            {
                LoadPTZCameraInfo(cameraInd + 1);
            }
        }

        private void SaveStageCameraInfo()
        {
            Camera stageCam = CamViewModel.Instance.CamViewList[8].Cam;
            stageCam.Ip_Domain = liveTextBox.Text;
            stageCam.IsNDIOutput = liveStageNDICheckbox.Checked;
            stageCam.IsNDICamera = liveStageNDIInputCheckbox.Checked;
            stageCam.Name = ((TextBox)liveNameTextBox).Text;
            stageCam.NDICameraName = liveNDINameComboBox.Text;

            Camera imageCam = CamViewModel.Instance.CamViewList[9].Cam;
            imageCam.Ip_Domain = staticTextBox.Text;
            imageCam.IsNDIOutput = imageStageNDICheckBox.Checked;
            imageCam.Name = ((TextBox)imageNameTextBox).Text;

            ProfileViewModel.Instance.SelectedProfile.IsStageLiveView = liveRadioButton.Checked;
            if (ProfileViewModel.Instance.SelectedProfile.IsStageLiveView)
                imageCam.IsConnect = false;
            else
                imageCam.IsConnect = true;
        }

        private void SavePTZCameraInfo(int rowInd)
        {
            var control = cameraInfoTableLayoutPanel.GetControlFromPosition(2, rowInd); //IP Textblox
            Camera cam = CamViewModel.Instance.CamViewList[rowInd - 1].Cam;
            cam.Ip_Domain = control.Text;
           /* control = cameraInfoTableLayoutPanel.GetControlFromPosition(5, rowInd); //NDI input
            cam.IsNDICamera = ((CheckBox)control).Checked;*/
            control = cameraInfoTableLayoutPanel.GetControlFromPosition(6, rowInd); //NDI output
            cam.IsNDIOutput = ((CheckBox)control).Checked;
            control = cameraInfoTableLayoutPanel.GetControlFromPosition(1, rowInd); //Camera Name
            cam.Name = ((TextBox)control).Text;
            /*control = cameraInfoTableLayoutPanel.GetControlFromPosition(3, rowInd); //NDI Name combobox
            cam.NDICameraName = ((ComboBox)control).Text;*/
        }

        private void SaveCameraInfo() //Save UI
        {
            ProfileViewModel.Instance.SelectedProfile.IsChange = true;
            if (cameraInd == -1)
            {
                for (int rowInd = 1; rowInd <= 8; rowInd++)
                {
                    SavePTZCameraInfo(rowInd);
                }
                SaveStageCameraInfo();
            }
            else if (cameraInd == 8) //Stage setup
            {
                SaveStageCameraInfo();
            }
            else
            {
                SavePTZCameraInfo(cameraInd + 1);
            }
        }
        private void camera1NDICheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }
        private void cameraNDIInputCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            for (int rowInd = 1; rowInd <= 8; rowInd++)
            {
                var control = cameraInfoTableLayoutPanel.GetControlFromPosition(4, rowInd); //NDI input
                if(control == sender)
                {
                    Camera cam = CamViewModel.Instance.CamViewList[rowInd - 1].Cam;
                }                
            }
        }
        private void cancelButton_Click(object sender, EventArgs e) //Cancel Setup
        {
            foreach(Camera unsavedCam in unsavedCameraList)
            {
                CamView camView = CamViewModel.Instance.CamViewList.Find(x => x.Cam.Index == unsavedCam.Index);
                if (camView != null)
                {
                    if(camView.Cam.IsConnect != unsavedCam.IsConnect)
                    {
                        camView.Cam.Copy(unsavedCam);
                        if(camView.Cam.IsConnect)
                            camView.CamControl.ConnectCamera();
                        else
                        {
                            camView.CamControl.DisconnectCamera();
                            camView.MainStream.Stop();
                            camView.SubStream.Stop();
                        }
                    }
                    else
                        camView.Cam.Copy(unsavedCam);

                }

            }
            Form parentForm = this.Parent as Form;
            if (parentForm != null)
                parentForm.Close();
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveCameraInfo();
            Form parentForm = this.Parent as Form;
            if (parentForm != null)
            {
                parentForm.DialogResult = DialogResult.OK;
                parentForm.Close();
            }
        }

        public event EventHandler<UserControlEventArgs> UserControlEvent;
        protected virtual void OnUserControlEvent(UserControlEventArgs e)
        {
            EventHandler<UserControlEventArgs> handler = UserControlEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void cameraConnectButton_Click(object sender, EventArgs e)
        {
            for (int rowInd = 1; rowInd <= 8; rowInd++)
            {
                CamView camView = CamViewModel.Instance.CamViewList[rowInd - 1];
                var button = cameraInfoTableLayoutPanel.GetControlFromPosition(4, rowInd); //Connect Button
                if(button == sender)
                {
                    if(!camView.Cam.IsConnect) //not connected yet
                    {
                        camView.Cam.IsConnect = true;
                        if(!camView.Cam.IsNDICamera)
                        {
                            var textBox = cameraInfoTableLayoutPanel.GetControlFromPosition(2, rowInd); //IP Textblox
                            camView.Cam.Ip_Domain = textBox.Text;
                        }
                        else
                        {
                            var comboBox = cameraInfoTableLayoutPanel.GetControlFromPosition(3, rowInd); //IP Textblox
                            camView.Cam.NDICameraName = comboBox.Text;
                        }
                        camView.CamControl.ConnectCamera();
                    }
                    else
                    {

                        camView.Cam.IsConnect = false;
                        camView.CamControl.DisconnectCamera();
                        camView.MainStream.Stop();
                        camView.SubStream.Stop();
                    }
                }
            }
        }

        private void liveStageConnectButton_Click(object sender, EventArgs e)
        {
            CamView camView = CamViewModel.Instance.CamViewList[8];
            MyToggleButton button = sender as MyToggleButton;
            if (!camView.Cam.IsConnect) //not connected yet
            {                
                camView.Cam.IsConnect = true;
                button.Status = STATUS_ENUM.DISCONNECTED;
                if (!camView.Cam.IsNDICamera)
                {
                    camView.Cam.Ip_Domain = liveTextBox.Text;
                }
                else
                {
                    camView.Cam.NDICameraName = liveNDINameComboBox.Text;
                }
            }
            else
            {
                camView.MainStream.Stop();
                camView.SubStream.Stop();
                camView.Cam.IsConnect = false;
                button.Status = STATUS_ENUM.UNKNOW;
            }
        }

        private void stageBrowseButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "xml files (*.jpg)|*.jpeg|All files (*.*)|*.*";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    staticTextBox.Text = dialog.FileName;
                }
            }
        }

        private void liveRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if(liveRadioButton.Checked)
            {
                CamView camView = CamViewModel.Instance.CamViewList[8];
                if (camView.Cam.IsConnect) //not connected yet
                {
                    liveStageConnectButton.Status = STATUS_ENUM.DISCONNECTED;
                }
                else
                {
                    liveStageConnectButton.Status = STATUS_ENUM.UNKNOW;
                }
            }
        }
    }
}
