﻿using CameraUserControl.Model;
using System.Drawing;

namespace CameraUserControl
{
    partial class PTZUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ptzControlTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new CameraUserControl.Model.MyLabel();
            this.label3 = new CameraUserControl.Model.MyLabel();
            this.label2 = new CameraUserControl.Model.MyLabel();
            this.label1 = new CameraUserControl.Model.MyLabel();
            this.panSpeedComboBox = new CameraUserControl.Model.MyComboBox();
            this.tiltSpeedComboBox = new CameraUserControl.Model.MyComboBox();
            this.zoomSpeedComboBox = new CameraUserControl.Model.MyComboBox();
            this.focusSpeedComboBox = new CameraUserControl.Model.MyComboBox();
            this.xboxCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.setPresetTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ptzReCallButton = new CameraUserControl.Model.RoundedButton();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.manFocusRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.autoFocusRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new CameraUserControl.Model.MyLabel();
            this.reCallPresetComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new CameraUserControl.Model.MyLabel();
            this.setPresetComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new CameraUserControl.Model.MyLabel();
            this.presetNameTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.setPresetButton = new CameraUserControl.Model.RoundedButton();
            this.leftpanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.osdBackButton = new CameraUserControl.Model.RoundedButton();
            this.osdButton = new CameraUserControl.Model.RoundedButton();
            this.osdEnterButton = new CameraUserControl.Model.RoundedButton();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.mainPtzControlTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.ptzHomeButton = new CameraUserControl.Model.MyButton();
            this.ptzUpButton = new CameraUserControl.Model.MyButton();
            this.ptzRightButton = new CameraUserControl.Model.MyButton();
            this.ptzDownButton = new CameraUserControl.Model.MyButton();
            this.ptzLeftButton = new CameraUserControl.Model.MyButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new CameraUserControl.Model.MyLabel();
            this.label6 = new CameraUserControl.Model.MyLabel();
            this.focusOutButton = new CameraUserControl.Model.MyButton();
            this.zoomOutButton = new CameraUserControl.Model.MyButton();
            this.focusInButton = new CameraUserControl.Model.MyButton();
            this.zoomInButton = new CameraUserControl.Model.MyButton();
            this.ptzControlTableLayoutPanel.SuspendLayout();
            this.rightPanel.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.setPresetTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.leftpanel.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.mainPtzControlTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ptzControlTableLayoutPanel
            // 
            this.ptzControlTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.ptzControlTableLayoutPanel.ColumnCount = 4;
            this.ptzControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.ptzControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 275F));
            this.ptzControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.ptzControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ptzControlTableLayoutPanel.Controls.Add(this.rightPanel, 3, 1);
            this.ptzControlTableLayoutPanel.Controls.Add(this.setPresetTableLayoutPanel, 2, 1);
            this.ptzControlTableLayoutPanel.Controls.Add(this.leftpanel, 0, 1);
            this.ptzControlTableLayoutPanel.Controls.Add(this.tableLayoutPanel16, 1, 1);
            this.ptzControlTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzControlTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.ptzControlTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ptzControlTableLayoutPanel.Name = "ptzControlTableLayoutPanel";
            this.ptzControlTableLayoutPanel.RowCount = 2;
            this.ptzControlTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.ptzControlTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ptzControlTableLayoutPanel.Size = new System.Drawing.Size(755, 268);
            this.ptzControlTableLayoutPanel.TabIndex = 0;
            // 
            // rightPanel
            // 
            this.rightPanel.Controls.Add(this.tableLayoutPanel15);
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightPanel.Location = new System.Drawing.Point(543, 3);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(209, 262);
            this.rightPanel.TabIndex = 2;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.tableLayoutPanel7, 1, 2);
            this.tableLayoutPanel15.Controls.Add(this.xboxCheckBox, 1, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 4;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(209, 262);
            this.tableLayoutPanel15.TabIndex = 4;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 329F));
            this.tableLayoutPanel7.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.panSpeedComboBox, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.tiltSpeedComboBox, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.zoomSpeedComboBox, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.focusSpeedComboBox, 1, 3);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(5, 83);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 5;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.73899F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.73899F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.26101F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.26101F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(204, 125);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 28);
            this.label4.TabIndex = 2;
            this.label4.Text = "Focus Speed";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Zoom Speed";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 27);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tilt Speed";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pan Speed";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panSpeedComboBox
            // 
            this.panSpeedComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panSpeedComboBox.BackColor = System.Drawing.Color.Black;
            this.panSpeedComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.panSpeedComboBox.ForeColor = System.Drawing.Color.White;
            this.panSpeedComboBox.FormattingEnabled = true;
            this.panSpeedComboBox.Location = new System.Drawing.Point(87, 3);
            this.panSpeedComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.panSpeedComboBox.Name = "panSpeedComboBox";
            this.panSpeedComboBox.Size = new System.Drawing.Size(80, 21);
            this.panSpeedComboBox.TabIndex = 2;
            // 
            // tiltSpeedComboBox
            // 
            this.tiltSpeedComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tiltSpeedComboBox.BackColor = System.Drawing.Color.Black;
            this.tiltSpeedComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.tiltSpeedComboBox.ForeColor = System.Drawing.Color.White;
            this.tiltSpeedComboBox.FormattingEnabled = true;
            this.tiltSpeedComboBox.Location = new System.Drawing.Point(87, 30);
            this.tiltSpeedComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.tiltSpeedComboBox.Name = "tiltSpeedComboBox";
            this.tiltSpeedComboBox.Size = new System.Drawing.Size(80, 21);
            this.tiltSpeedComboBox.TabIndex = 2;
            // 
            // zoomSpeedComboBox
            // 
            this.zoomSpeedComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomSpeedComboBox.BackColor = System.Drawing.Color.Black;
            this.zoomSpeedComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.zoomSpeedComboBox.ForeColor = System.Drawing.Color.White;
            this.zoomSpeedComboBox.FormattingEnabled = true;
            this.zoomSpeedComboBox.Location = new System.Drawing.Point(87, 57);
            this.zoomSpeedComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.zoomSpeedComboBox.Name = "zoomSpeedComboBox";
            this.zoomSpeedComboBox.Size = new System.Drawing.Size(80, 21);
            this.zoomSpeedComboBox.TabIndex = 2;
            // 
            // focusSpeedComboBox
            // 
            this.focusSpeedComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.focusSpeedComboBox.BackColor = System.Drawing.Color.Black;
            this.focusSpeedComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.focusSpeedComboBox.ForeColor = System.Drawing.Color.White;
            this.focusSpeedComboBox.FormattingEnabled = true;
            this.focusSpeedComboBox.Location = new System.Drawing.Point(87, 85);
            this.focusSpeedComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.focusSpeedComboBox.Name = "focusSpeedComboBox";
            this.focusSpeedComboBox.Size = new System.Drawing.Size(80, 21);
            this.focusSpeedComboBox.TabIndex = 2;
            // 
            // xboxCheckBox
            // 
            this.xboxCheckBox.AutoSize = true;
            this.xboxCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xboxCheckBox.ForeColor = System.Drawing.Color.White;
            this.xboxCheckBox.Location = new System.Drawing.Point(8, 56);
            this.xboxCheckBox.Name = "xboxCheckBox";
            this.xboxCheckBox.Size = new System.Drawing.Size(198, 24);
            this.xboxCheckBox.TabIndex = 0;
            this.xboxCheckBox.Text = "Enable XBOX Controller";
            this.xboxCheckBox.UseVisualStyleBackColor = true;
            this.xboxCheckBox.CheckedChanged += new System.EventHandler(this.xboxCheckBox_CheckedChanged);
            // 
            // setPresetTableLayoutPanel
            // 
            this.setPresetTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.setPresetTableLayoutPanel.ColumnCount = 2;
            this.setPresetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.setPresetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.setPresetTableLayoutPanel.Controls.Add(this.ptzReCallButton, 0, 5);
            this.setPresetTableLayoutPanel.Controls.Add(this.tableLayoutPanel14, 0, 6);
            this.setPresetTableLayoutPanel.Controls.Add(this.tableLayoutPanel5, 0, 4);
            this.setPresetTableLayoutPanel.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.setPresetTableLayoutPanel.Controls.Add(this.tableLayoutPanel9, 0, 2);
            this.setPresetTableLayoutPanel.Controls.Add(this.setPresetButton, 0, 3);
            this.setPresetTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setPresetTableLayoutPanel.Location = new System.Drawing.Point(365, 0);
            this.setPresetTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.setPresetTableLayoutPanel.Name = "setPresetTableLayoutPanel";
            this.setPresetTableLayoutPanel.RowCount = 8;
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.setPresetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.setPresetTableLayoutPanel.Size = new System.Drawing.Size(175, 268);
            this.setPresetTableLayoutPanel.TabIndex = 3;
            // 
            // ptzReCallButton
            // 
            this.ptzReCallButton.BackColor = System.Drawing.Color.Gray;
            this.ptzReCallButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzReCallButton.FlatAppearance.BorderSize = 0;
            this.ptzReCallButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.ptzReCallButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.ptzReCallButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ptzReCallButton.ForeColor = System.Drawing.Color.White;
            this.ptzReCallButton.Location = new System.Drawing.Point(0, 164);
            this.ptzReCallButton.Margin = new System.Windows.Forms.Padding(0);
            this.ptzReCallButton.Name = "ptzReCallButton";
            this.ptzReCallButton.Size = new System.Drawing.Size(175, 30);
            this.ptzReCallButton.TabIndex = 0;
            this.ptzReCallButton.TabStop = false;
            this.ptzReCallButton.Text = "ReCall Preset";
            this.ptzReCallButton.UseVisualStyleBackColor = false;
            this.ptzReCallButton.Click += new System.EventHandler(this.ptzReCallButton_Click);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.manFocusRadioButton, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.autoFocusRadioButton, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 194);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(175, 35);
            this.tableLayoutPanel14.TabIndex = 3;
            // 
            // manFocusRadioButton
            // 
            this.manFocusRadioButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.manFocusRadioButton.AutoSize = true;
            this.manFocusRadioButton.ForeColor = System.Drawing.Color.White;
            this.manFocusRadioButton.Location = new System.Drawing.Point(103, 9);
            this.manFocusRadioButton.Name = "manFocusRadioButton";
            this.manFocusRadioButton.Size = new System.Drawing.Size(55, 17);
            this.manFocusRadioButton.TabIndex = 3;
            this.manFocusRadioButton.Text = "Man F";
            this.manFocusRadioButton.UseVisualStyleBackColor = true;
            // 
            // autoFocusRadioButton
            // 
            this.autoFocusRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.autoFocusRadioButton.AutoSize = true;
            this.autoFocusRadioButton.Checked = true;
            this.autoFocusRadioButton.ForeColor = System.Drawing.Color.White;
            this.autoFocusRadioButton.Location = new System.Drawing.Point(28, 9);
            this.autoFocusRadioButton.Name = "autoFocusRadioButton";
            this.autoFocusRadioButton.Size = new System.Drawing.Size(56, 17);
            this.autoFocusRadioButton.TabIndex = 2;
            this.autoFocusRadioButton.TabStop = true;
            this.autoFocusRadioButton.Text = "Auto F";
            this.autoFocusRadioButton.UseVisualStyleBackColor = true;
            this.autoFocusRadioButton.CheckedChanged += new System.EventHandler(this.autoFocusRadioButton_CheckedChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.reCallPresetComboBox, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 129);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(175, 35);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(3, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Preset Number";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // reCallPresetComboBox
            // 
            this.reCallPresetComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.reCallPresetComboBox.BackColor = System.Drawing.Color.Black;
            this.reCallPresetComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.reCallPresetComboBox.ForeColor = System.Drawing.Color.White;
            this.reCallPresetComboBox.FormattingEnabled = true;
            this.reCallPresetComboBox.Location = new System.Drawing.Point(87, 7);
            this.reCallPresetComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.reCallPresetComboBox.Name = "reCallPresetComboBox";
            this.reCallPresetComboBox.Size = new System.Drawing.Size(88, 21);
            this.reCallPresetComboBox.TabIndex = 2;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.setPresetComboBox, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(175, 30);
            this.tableLayoutPanel8.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Preset Number";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // setPresetComboBox
            // 
            this.setPresetComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.setPresetComboBox.BackColor = System.Drawing.Color.Black;
            this.setPresetComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.setPresetComboBox.ForeColor = System.Drawing.Color.White;
            this.setPresetComboBox.FormattingEnabled = true;
            this.setPresetComboBox.Location = new System.Drawing.Point(87, 4);
            this.setPresetComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.setPresetComboBox.Name = "setPresetComboBox";
            this.setPresetComboBox.Size = new System.Drawing.Size(88, 21);
            this.setPresetComboBox.TabIndex = 2;
            this.setPresetComboBox.SelectedIndexChanged += new System.EventHandler(this.setPresetComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.presetNameTextBox, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 69);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(175, 30);
            this.tableLayoutPanel9.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(3, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Preset Name";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // presetNameTextBox
            // 
            this.presetNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.presetNameTextBox.BackColor = System.Drawing.Color.Black;
            this.presetNameTextBox.ForeColor = System.Drawing.Color.White;
            this.presetNameTextBox.Location = new System.Drawing.Point(87, 5);
            this.presetNameTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.presetNameTextBox.Name = "presetNameTextBox";
            this.presetNameTextBox.Size = new System.Drawing.Size(88, 20);
            this.presetNameTextBox.TabIndex = 1;
            // 
            // setPresetButton
            // 
            this.setPresetButton.BackColor = System.Drawing.Color.Gray;
            this.setPresetButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setPresetButton.FlatAppearance.BorderSize = 0;
            this.setPresetButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.setPresetButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.setPresetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.setPresetButton.ForeColor = System.Drawing.Color.White;
            this.setPresetButton.Location = new System.Drawing.Point(0, 99);
            this.setPresetButton.Margin = new System.Windows.Forms.Padding(0);
            this.setPresetButton.Name = "setPresetButton";
            this.setPresetButton.Size = new System.Drawing.Size(175, 30);
            this.setPresetButton.TabIndex = 0;
            this.setPresetButton.TabStop = false;
            this.setPresetButton.Text = "Set Preset";
            this.setPresetButton.UseVisualStyleBackColor = false;
            this.setPresetButton.Click += new System.EventHandler(this.setPresetButton_Click);
            // 
            // leftpanel
            // 
            this.leftpanel.Controls.Add(this.tableLayoutPanel4);
            this.leftpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftpanel.Location = new System.Drawing.Point(3, 3);
            this.leftpanel.Name = "leftpanel";
            this.leftpanel.Size = new System.Drawing.Size(84, 262);
            this.leftpanel.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel4.Controls.Add(this.osdBackButton, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.osdButton, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.osdEnterButton, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(84, 262);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // osdBackButton
            // 
            this.osdBackButton.BackColor = System.Drawing.Color.Gray;
            this.osdBackButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.osdBackButton.FlatAppearance.BorderSize = 0;
            this.osdBackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.osdBackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.osdBackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.osdBackButton.ForeColor = System.Drawing.Color.White;
            this.osdBackButton.Location = new System.Drawing.Point(3, 151);
            this.osdBackButton.Name = "osdBackButton";
            this.osdBackButton.Size = new System.Drawing.Size(78, 29);
            this.osdBackButton.TabIndex = 0;
            this.osdBackButton.Text = "OSD Back";
            this.osdBackButton.UseVisualStyleBackColor = false;
            this.osdBackButton.Click += new System.EventHandler(this.osdBackButton_Click);
            // 
            // osdButton
            // 
            this.osdButton.BackColor = System.Drawing.Color.Gray;
            this.osdButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.osdButton.FlatAppearance.BorderSize = 0;
            this.osdButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.osdButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.osdButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.osdButton.ForeColor = System.Drawing.Color.White;
            this.osdButton.Location = new System.Drawing.Point(3, 81);
            this.osdButton.Name = "osdButton";
            this.osdButton.Size = new System.Drawing.Size(78, 29);
            this.osdButton.TabIndex = 0;
            this.osdButton.Text = "OSD";
            this.osdButton.UseVisualStyleBackColor = false;
            this.osdButton.Click += new System.EventHandler(this.osdButton_Click);
            // 
            // osdEnterButton
            // 
            this.osdEnterButton.BackColor = System.Drawing.Color.Gray;
            this.osdEnterButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.osdEnterButton.FlatAppearance.BorderSize = 0;
            this.osdEnterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.osdEnterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.osdEnterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.osdEnterButton.ForeColor = System.Drawing.Color.White;
            this.osdEnterButton.Location = new System.Drawing.Point(3, 116);
            this.osdEnterButton.Name = "osdEnterButton";
            this.osdEnterButton.Size = new System.Drawing.Size(78, 29);
            this.osdEnterButton.TabIndex = 0;
            this.osdEnterButton.Text = "OSD Enter";
            this.osdEnterButton.UseVisualStyleBackColor = false;
            this.osdEnterButton.Click += new System.EventHandler(this.osdEnterButton_Click);
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 273F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.Controls.Add(this.mainPtzControlTableLayoutPanel, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(90, 0);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 3;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(275, 268);
            this.tableLayoutPanel16.TabIndex = 3;
            // 
            // mainPtzControlTableLayoutPanel
            // 
            this.mainPtzControlTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainPtzControlTableLayoutPanel.ColumnCount = 2;
            this.mainPtzControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.mainPtzControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.mainPtzControlTableLayoutPanel.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.mainPtzControlTableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.mainPtzControlTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPtzControlTableLayoutPanel.Location = new System.Drawing.Point(3, 57);
            this.mainPtzControlTableLayoutPanel.Name = "mainPtzControlTableLayoutPanel";
            this.mainPtzControlTableLayoutPanel.RowCount = 1;
            this.mainPtzControlTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainPtzControlTableLayoutPanel.Size = new System.Drawing.Size(269, 154);
            this.mainPtzControlTableLayoutPanel.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(150, 154);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Controls.Add(this.ptzHomeButton, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.ptzUpButton, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.ptzRightButton, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.ptzDownButton, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.ptzLeftButton, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 3);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(150, 148);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // ptzHomeButton
            // 
            this.ptzHomeButton.AutoSize = true;
            this.ptzHomeButton.BackColor = System.Drawing.Color.Transparent;
            this.ptzHomeButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_home_arrow;
            this.ptzHomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptzHomeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzHomeButton.FlatAppearance.BorderSize = 0;
            this.ptzHomeButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ptzHomeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ptzHomeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ptzHomeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ptzHomeButton.Location = new System.Drawing.Point(52, 51);
            this.ptzHomeButton.Margin = new System.Windows.Forms.Padding(2);
            this.ptzHomeButton.Name = "ptzHomeButton";
            this.ptzHomeButton.Size = new System.Drawing.Size(46, 45);
            this.ptzHomeButton.TabIndex = 0;
            this.ptzHomeButton.UseVisualStyleBackColor = false;
            this.ptzHomeButton.Click += new System.EventHandler(this.ptzHomeButton_Click);
            // 
            // ptzUpButton
            // 
            this.ptzUpButton.AutoSize = true;
            this.ptzUpButton.BackColor = System.Drawing.Color.Transparent;
            this.ptzUpButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_up_arrow;
            this.ptzUpButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptzUpButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzUpButton.FlatAppearance.BorderSize = 0;
            this.ptzUpButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ptzUpButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ptzUpButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ptzUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ptzUpButton.Location = new System.Drawing.Point(52, 2);
            this.ptzUpButton.Margin = new System.Windows.Forms.Padding(2);
            this.ptzUpButton.Name = "ptzUpButton";
            this.ptzUpButton.Size = new System.Drawing.Size(46, 45);
            this.ptzUpButton.TabIndex = 0;
            this.ptzUpButton.UseVisualStyleBackColor = false;
            this.ptzUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ptzUpButton_MouseDown);
            this.ptzUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ptzButton_MouseUp);
            // 
            // ptzRightButton
            // 
            this.ptzRightButton.AutoSize = true;
            this.ptzRightButton.BackColor = System.Drawing.Color.Transparent;
            this.ptzRightButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_right_arrow;
            this.ptzRightButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptzRightButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzRightButton.FlatAppearance.BorderSize = 0;
            this.ptzRightButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ptzRightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ptzRightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ptzRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ptzRightButton.Location = new System.Drawing.Point(102, 51);
            this.ptzRightButton.Margin = new System.Windows.Forms.Padding(2);
            this.ptzRightButton.Name = "ptzRightButton";
            this.ptzRightButton.Size = new System.Drawing.Size(46, 45);
            this.ptzRightButton.TabIndex = 0;
            this.ptzRightButton.UseVisualStyleBackColor = false;
            this.ptzRightButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ptzRightButton_MouseDown);
            this.ptzRightButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ptzButton_MouseUp);
            // 
            // ptzDownButton
            // 
            this.ptzDownButton.AutoSize = true;
            this.ptzDownButton.BackColor = System.Drawing.Color.Transparent;
            this.ptzDownButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_down_arrow;
            this.ptzDownButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptzDownButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzDownButton.FlatAppearance.BorderSize = 0;
            this.ptzDownButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ptzDownButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ptzDownButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ptzDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ptzDownButton.Location = new System.Drawing.Point(52, 100);
            this.ptzDownButton.Margin = new System.Windows.Forms.Padding(2);
            this.ptzDownButton.Name = "ptzDownButton";
            this.ptzDownButton.Size = new System.Drawing.Size(46, 46);
            this.ptzDownButton.TabIndex = 0;
            this.ptzDownButton.UseVisualStyleBackColor = false;
            this.ptzDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ptzDownButton_MouseDown);
            this.ptzDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ptzButton_MouseUp);
            // 
            // ptzLeftButton
            // 
            this.ptzLeftButton.AutoSize = true;
            this.ptzLeftButton.BackColor = System.Drawing.Color.Transparent;
            this.ptzLeftButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_left_arrow;
            this.ptzLeftButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptzLeftButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzLeftButton.FlatAppearance.BorderSize = 0;
            this.ptzLeftButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ptzLeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ptzLeftButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ptzLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ptzLeftButton.Location = new System.Drawing.Point(2, 51);
            this.ptzLeftButton.Margin = new System.Windows.Forms.Padding(2);
            this.ptzLeftButton.Name = "ptzLeftButton";
            this.ptzLeftButton.Size = new System.Drawing.Size(46, 45);
            this.ptzLeftButton.TabIndex = 0;
            this.ptzLeftButton.UseVisualStyleBackColor = false;
            this.ptzLeftButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ptzLeftButton_MouseDown);
            this.ptzLeftButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ptzButton_MouseUp);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.focusOutButton, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.zoomOutButton, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.focusInButton, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.zoomInButton, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(155, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(112, 154);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(56, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Focus";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(0, 70);
            this.label6.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Zoom";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // focusOutButton
            // 
            this.focusOutButton.BackColor = System.Drawing.Color.Transparent;
            this.focusOutButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_focus_out;
            this.focusOutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.focusOutButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.focusOutButton.FlatAppearance.BorderSize = 0;
            this.focusOutButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.focusOutButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.focusOutButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.focusOutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.focusOutButton.Location = new System.Drawing.Point(56, 92);
            this.focusOutButton.Margin = new System.Windows.Forms.Padding(0);
            this.focusOutButton.Name = "focusOutButton";
            this.focusOutButton.Size = new System.Drawing.Size(56, 46);
            this.focusOutButton.TabIndex = 0;
            this.focusOutButton.UseVisualStyleBackColor = false;
            this.focusOutButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.focusOutButton_MouseDown);
            this.focusOutButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.focusButton_MouseUp);
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.BackColor = System.Drawing.Color.Transparent;
            this.zoomOutButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_zoom_out;
            this.zoomOutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.zoomOutButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zoomOutButton.FlatAppearance.BorderSize = 0;
            this.zoomOutButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.zoomOutButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.zoomOutButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.zoomOutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zoomOutButton.Location = new System.Drawing.Point(0, 92);
            this.zoomOutButton.Margin = new System.Windows.Forms.Padding(0);
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.Size = new System.Drawing.Size(56, 46);
            this.zoomOutButton.TabIndex = 0;
            this.zoomOutButton.UseVisualStyleBackColor = false;
            this.zoomOutButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.zoomOutButton_MouseDown);
            this.zoomOutButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.zoomButton_MouseUp);
            // 
            // focusInButton
            // 
            this.focusInButton.BackColor = System.Drawing.Color.Transparent;
            this.focusInButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_focus_in;
            this.focusInButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.focusInButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.focusInButton.FlatAppearance.BorderSize = 0;
            this.focusInButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.focusInButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.focusInButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.focusInButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.focusInButton.Location = new System.Drawing.Point(56, 15);
            this.focusInButton.Margin = new System.Windows.Forms.Padding(0);
            this.focusInButton.Name = "focusInButton";
            this.focusInButton.Size = new System.Drawing.Size(56, 46);
            this.focusInButton.TabIndex = 0;
            this.focusInButton.UseVisualStyleBackColor = false;
            this.focusInButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.focusInButton_MouseDown);
            this.focusInButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.focusButton_MouseUp);
            // 
            // zoomInButton
            // 
            this.zoomInButton.BackColor = System.Drawing.Color.Transparent;
            this.zoomInButton.BackgroundImage = global::CameraUserControl.Properties.Resources.normal_zoom_in;
            this.zoomInButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.zoomInButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zoomInButton.FlatAppearance.BorderSize = 0;
            this.zoomInButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.zoomInButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.zoomInButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.zoomInButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zoomInButton.Location = new System.Drawing.Point(0, 15);
            this.zoomInButton.Margin = new System.Windows.Forms.Padding(0);
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.Size = new System.Drawing.Size(56, 46);
            this.zoomInButton.TabIndex = 0;
            this.zoomInButton.UseVisualStyleBackColor = false;
            this.zoomInButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.zoomInButton_MouseDown);
            this.zoomInButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.zoomButton_MouseUp);
            // 
            // PTZUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.ptzControlTableLayoutPanel);
            this.Name = "PTZUserControl";
            this.Size = new System.Drawing.Size(755, 268);
            this.ptzControlTableLayoutPanel.ResumeLayout(false);
            this.rightPanel.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.setPresetTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.leftpanel.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.mainPtzControlTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ptzControlTableLayoutPanel;
        private System.Windows.Forms.Panel rightPanel;
        private MyLabel label4;
        private MyLabel label3;
        private MyLabel label2;
        private MyLabel label1;
        private MyCheckBox xboxCheckBox;
        private System.Windows.Forms.Panel leftpanel;
        private MyRadioButton manFocusRadioButton;
        private MyRadioButton autoFocusRadioButton;
        private MyComboBox reCallPresetComboBox;
        private RoundedButton ptzReCallButton;
        private RoundedButton osdBackButton;
        private RoundedButton osdEnterButton;
        private RoundedButton osdButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel setPresetTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MyComboBox panSpeedComboBox;
        private MyComboBox tiltSpeedComboBox;
        private MyComboBox zoomSpeedComboBox;
        private MyComboBox focusSpeedComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MyLabel label8;
        private System.Windows.Forms.TableLayoutPanel mainPtzControlTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MyButton ptzHomeButton;
        private MyButton ptzUpButton;
        private MyButton ptzRightButton;
        private MyButton ptzDownButton;
        private MyButton ptzLeftButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MyLabel label7;
        private MyLabel label6;
        private MyButton focusOutButton;
        private MyButton zoomOutButton;
        private MyButton focusInButton;
        private MyButton zoomInButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private MyLabel label5;
        private MyComboBox setPresetComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MyLabel label9;
        private RoundedButton setPresetButton;
        private RoundedTextBox presetNameTextBox;
    }
}
