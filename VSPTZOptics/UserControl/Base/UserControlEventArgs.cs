﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.Base
{
    public class UserControlEventArgs
    {
        public EVENT_TYPE_ENUM EventType { get; set; }
        public int CameraInd { get; set; }
    }
}
