﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.Base
{
    public enum EVENT_TYPE_ENUM
    {
        ADD_CAMERA,
        EDIT_CAMERA,
        EDIT_STAGE_SOURCE,
        EDIT_CAMERA_PRESETS,
        EDIT_STAGE_PRESETS,
        SWITCH_LEFT_CAMERA,
        SWITCH_RIGHT_CAMERA
    }
    public enum RESIZE_TYPE_ENUM
    {
        NONE,
        LEFT,
        RIGHT,
        TOP,
        BOTTOM
    }
}
