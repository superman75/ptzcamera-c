﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.ViewModel
{
    public  class PresetNumberViewModel
    {
        private static PresetNumberViewModel instance = null;
        private Dictionary<int, string> presetNumberList = null;

        public PresetNumberViewModel()
        {
            presetNumberList = new Dictionary<int, string>();
            presetNumberList.Add(1, "Preset 1");
            presetNumberList.Add(2, "Preset 2");
            presetNumberList.Add(3, "Preset 3");
            presetNumberList.Add(4, "Preset 4");
            presetNumberList.Add(5, "Preset 5");
            presetNumberList.Add(6, "Preset 6");
            presetNumberList.Add(7, "Preset 7");
            presetNumberList.Add(8, "Preset 8");
            presetNumberList.Add(9, "Preset 9");
        }

        public static PresetNumberViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PresetNumberViewModel();
                }
                return instance;
            }
        }

        public Dictionary<int, string> PresetNumberList
        {
            get
            {
                return presetNumberList;
            }
            set
            {
                presetNumberList = value;
            }
        }
    }
}
