﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.ViewModel
{
    public  class ZoomSpeedViewModel
    {
        private static ZoomSpeedViewModel instance = null;
        private Dictionary<int, string> zoomSpeedList = null;

        public ZoomSpeedViewModel()
        {
            zoomSpeedList = new Dictionary<int, string>();
            zoomSpeedList.Add(0, "Speed 0");
            zoomSpeedList.Add(1, "Speed 1");
            zoomSpeedList.Add(2, "Speed 2");
            zoomSpeedList.Add(3, "Speed 3");
            zoomSpeedList.Add(4, "Speed 4");
            zoomSpeedList.Add(5, "Speed 5");
            zoomSpeedList.Add(6, "Speed 6");
            zoomSpeedList.Add(7, "Speed 7");
        }

        public static ZoomSpeedViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ZoomSpeedViewModel();
                }
                return instance;
            }
        }

        public Dictionary<int, string> ZoomSpeedList
        {
            get
            {
                return zoomSpeedList;
            }
            set
            {
                zoomSpeedList = value;
            }
        }
    }
}
