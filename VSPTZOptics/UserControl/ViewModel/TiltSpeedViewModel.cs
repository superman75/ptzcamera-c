﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.ViewModel
{
    public  class TiltSpeedViewModel
    {
        private static TiltSpeedViewModel instance = null;
        private Dictionary<int, string> tiltSpeedList = null;

        public TiltSpeedViewModel()
        {
            tiltSpeedList = new Dictionary<int, string>();
            tiltSpeedList.Add(1, "Speed 1");
            tiltSpeedList.Add(2, "Speed 2");
            tiltSpeedList.Add(3, "Speed 3");
            tiltSpeedList.Add(4, "Speed 4");
            tiltSpeedList.Add(5, "Speed 5");
            tiltSpeedList.Add(6, "Speed 6");
            tiltSpeedList.Add(7, "Speed 7");
            tiltSpeedList.Add(8, "Speed 8");
            tiltSpeedList.Add(9, "Speed 9");
            tiltSpeedList.Add(10, "Speed 10");
            tiltSpeedList.Add(11, "Speed 11");
            tiltSpeedList.Add(12, "Speed 12");
            tiltSpeedList.Add(13, "Speed 13");
            tiltSpeedList.Add(14, "Speed 14");
            tiltSpeedList.Add(15, "Speed 15");
            tiltSpeedList.Add(16, "Speed 16");
            tiltSpeedList.Add(17, "Speed 17");
            tiltSpeedList.Add(18, "Speed 18");
            tiltSpeedList.Add(19, "Speed 19");
            tiltSpeedList.Add(20, "Speed 20");
        }

        public static TiltSpeedViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TiltSpeedViewModel();
                }
                return instance;
            }
        }

        public Dictionary<int, string> TiltSpeedList
        {
            get
            {
                return tiltSpeedList;
            }
            set
            {
                tiltSpeedList = value;
            }
        }
    }
}
