﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.ViewModel
{
    public  class PanSpeedViewModel
    {
        private static PanSpeedViewModel instance = null;
        private Dictionary<int, string> panSpeedList = null;

        public PanSpeedViewModel()
        {
            panSpeedList = new Dictionary<int, string>();
            panSpeedList.Add(1, "Speed 1");
            panSpeedList.Add(2, "Speed 2");
            panSpeedList.Add(3, "Speed 3");
            panSpeedList.Add(4, "Speed 4");
            panSpeedList.Add(5, "Speed 5");
            panSpeedList.Add(6, "Speed 6");
            panSpeedList.Add(7, "Speed 7");
            panSpeedList.Add(8, "Speed 8");
            panSpeedList.Add(9, "Speed 9");
            panSpeedList.Add(10, "Speed 10");
            panSpeedList.Add(11, "Speed 11");
            panSpeedList.Add(12, "Speed 12");
            panSpeedList.Add(13, "Speed 13");
            panSpeedList.Add(14, "Speed 14");
            panSpeedList.Add(15, "Speed 15");
            panSpeedList.Add(16, "Speed 16");
            panSpeedList.Add(17, "Speed 17");
            panSpeedList.Add(18, "Speed 18");
            panSpeedList.Add(19, "Speed 19");
            panSpeedList.Add(20, "Speed 20");
            panSpeedList.Add(21, "Speed 21");
            panSpeedList.Add(22, "Speed 22");
            panSpeedList.Add(23, "Speed 23");
            panSpeedList.Add(24, "Speed 24");
        }

        public static PanSpeedViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PanSpeedViewModel();
                }
                return instance;
            }
        }

        public Dictionary<int, string> PanSpeedList
        {
            get
            {
                return panSpeedList;
            }
            set
            {
                panSpeedList = value;
            }
        }
    }
}
