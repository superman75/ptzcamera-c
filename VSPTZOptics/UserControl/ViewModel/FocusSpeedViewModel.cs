﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraUserControl.ViewModel
{
    public  class FocusSpeedViewModel
    {
        private static FocusSpeedViewModel instance = null;
        private Dictionary<int, string> focusSpeedList = null;

        public FocusSpeedViewModel()
        {
            focusSpeedList = new Dictionary<int, string>();
            focusSpeedList.Add(0, "Speed 0");
            focusSpeedList.Add(1, "Speed 1");
            focusSpeedList.Add(2, "Speed 2");
            focusSpeedList.Add(3, "Speed 3");
            focusSpeedList.Add(4, "Speed 4");
            focusSpeedList.Add(5, "Speed 5");
            focusSpeedList.Add(6, "Speed 6");
            focusSpeedList.Add(7, "Speed 7");
        }

        public static FocusSpeedViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FocusSpeedViewModel();
                }
                return instance;
            }
        }

        public Dictionary<int, string> FocusSpeedList
        {
            get
            {
                return focusSpeedList;
            }
            set
            {
                focusSpeedList = value;
            }
        }
    }
}
