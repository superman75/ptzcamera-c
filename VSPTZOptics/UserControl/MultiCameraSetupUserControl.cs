﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShareLibrary.Model;
using CameraView.ViewModel;
using CameraUserControl.Model;
using ShareLibrary.Base;
using CameraUserControl.Base;
using CameraControl.Model;
using CameraView.Model;
using Settings.ViewModel;
using CameraUserControl.ViewModel;
using System.Threading;

namespace CameraUserControl
{
    public partial class MultiCameraSetupUserControl : UserControl
    {
        private int cameraInd; //Current setup for specific camera index (start from 0 - 8); -1 for all cameras
        private List<Camera> unsavedCameraList = new List<Camera>();
        private System.Timers.Timer timer = null;
        private object lockTimer = null;
        private bool isStop;
        public MultiCameraSetupUserControl()
        {
            InitializeComponent();
            InitilizeBindingButtons();
            InitTimer();
            this.Load += MultiCameraSetupUserControl_Load;
        }

        private void MultiCameraSetupUserControl_Load(object sender, EventArgs e)
        {
            Timer_Elapsed(null, null);
        }

        public void InitTimer()
        {
            timer = new System.Timers.Timer(); // fire every 1 second
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 5000;
            timer.Enabled = true;
            lockTimer = new object();
            isStop = false;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock(lockTimer)
                {
                    if(!isStop)
                        refreshButton_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteLog(2, ex.Message + ex.StackTrace);
            }
        }

        private void Stop_Timer()
        {
            isStop = true;
            timer.Stop();
        }
        public void Close()
        {
            Stop_Timer();
        }

        private void InitilizeBindingButtons() //Binding connect icon with camera sub stream status
        {
            for (int rowInd = 0; rowInd < 4; rowInd++)
            {
                for (int colInd = 0; colInd < 2; colInd++)
                {
                    var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
                    var cameraTbPanelControl = gBcontrol.Controls[0];
                    Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[colInd * 4 + rowInd];

                    var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 0); //Camera Name TextBox
                    control.DataBindings.Add("Text", cam, "Name", true, DataSourceUpdateMode.OnPropertyChanged);

                    control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 1); //IP Camera text box
                    control.DataBindings.Add("Enabled", cam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);

                    control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 2); //NDI Camera combobox
                    control.DataBindings.Add("Enabled", cam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);

                    control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 3); //USB Camera combobox
                    control.DataBindings.Add("Enabled", cam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);

                    control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 4); //Connect Button
                    control.DataBindings.Add("Status", cam, "ControlStatus", true, DataSourceUpdateMode.OnPropertyChanged);
                    control.DataBindings.Add("IsConnect", cam, "IsConnect", true, DataSourceUpdateMode.OnPropertyChanged);
                }
            }

            Camera stageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[8];
            liveStageConnectButton.DataBindings.Add("Status", stageCam, "MainStreamStatus", true, DataSourceUpdateMode.OnPropertyChanged);
            liveStageConnectButton.DataBindings.Add("IsConnect", stageCam, "IsConnect", true, DataSourceUpdateMode.OnPropertyChanged);
            liveIPCameraTextBox.DataBindings.Add("Enabled", stageCam, "IsDisConnect", true, DataSourceUpdateMode.OnPropertyChanged);

            Camera stageImage = ProfileViewModel.Instance.SelectedProfile.CameraList[9];
        }
        public void LoadCameraSetup(int _cameraInd)
        {
            cameraInd = _cameraInd;
            if (cameraInd == -1)//Load all camera setup
            {
                this.cameraSetupTableLayoutPanel.RowStyles[1].Height = 700F;// PTZ camera
                this.cameraSetupTableLayoutPanel.RowStyles[2].Height = 210F;// Stage Source
                this.ptzCameraGroupBox.Visible = true;
                this.stageSourceGroupBox.Visible = true;

                for (int rowInd = 0; rowInd < 4; rowInd++)
                {
                    cameraInfoTableLayoutPanel.RowStyles[rowInd].Height = 25F;
                    for (int colInd = 0; colInd < 2; colInd++)
                    {
                        cameraInfoTableLayoutPanel.ColumnStyles[colInd].Width = 50F;
                        var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
                        var cameraTbPanelControl = gBcontrol.Controls[0];
                        gBcontrol.Visible = true;
                        for (int rowInd1 = 0; rowInd1 < 4; rowInd1++)
                        {
                            for (int colInd1 = 0; colInd1 < 2; colInd1++)
                            {
                                var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(colInd, rowInd);
                                if (control != null)
                                    control.Visible = true;
                            }
                        }
                    }
                }
            }
            else
            {
                if (cameraInd == 8) //Load stage source ZCAM
                {
                    this.cameraSetupTableLayoutPanel.RowStyles[1].Height = 0F;// PTZ area
                    this.cameraSetupTableLayoutPanel.RowStyles[2].Height = 210F;// Stage area
                    this.ptzCameraGroupBox.Visible = false;
                    this.stageSourceGroupBox.Visible = true;
                }
                else //Load setup for only specific OPTICS camera
                {
                    this.cameraSetupTableLayoutPanel.RowStyles[1].Height = 205F;// PTZ area
                    this.cameraSetupTableLayoutPanel.RowStyles[2].Height = 0F;// Stage area
                    this.ptzCameraGroupBox.Visible = true;
                    this.stageSourceGroupBox.Visible = false;

                    int selectedRowInd = 0;
                    int selectedColInd = 0;
                    for (int rowInd = 0; rowInd < 4; rowInd++)
                    {
                        cameraInfoTableLayoutPanel.RowStyles[rowInd].Height = 0F;
                        for (int colInd = 0; colInd < 2; colInd++)
                        {
                            cameraInfoTableLayoutPanel.ColumnStyles[colInd].Width = 0F;
                            var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
                            var cameraTbPanelControl = gBcontrol.Controls[0];
                            if (cameraInd == colInd * 4 + rowInd)
                            {
                                gBcontrol.Visible = true;
                                selectedRowInd = rowInd;
                                selectedColInd = colInd;
                                for (int rowInd1 = 0; rowInd1 < 4; rowInd1++)
                                {
                                    for (int colInd1 = 0; colInd1 < 2; colInd1++)
                                    {
                                        var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(colInd, rowInd);
                                        if (control != null)
                                            control.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                gBcontrol.Visible = false;
                                for (int rowInd1 = 0; rowInd1 < 4; rowInd1++)
                                {
                                    for (int colInd1 = 0; colInd1 < 2; colInd1++)
                                    {
                                        var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(colInd, rowInd);
                                        if (control != null)
                                            control.Visible = false;
                                    }
                                }
                            }
                        }
                    }

                    cameraInfoTableLayoutPanel.RowStyles[selectedRowInd].Height = 100F;
                    cameraInfoTableLayoutPanel.ColumnStyles[selectedColInd].Width = 100F;
                }
            }
            LoadCameraInfo(cameraInd);
        }

        private void LoadStageCameraInfo()
        {
            Camera stageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[8];
            liveIPCameraTextBox.Text = stageCam.Ip_Domain;
            liveStageNDICheckbox.Checked = stageCam.IsNDIOutput;
            liveNDICameraRadioButton.Checked = stageCam.IsNDICamera;
            liveIPCameraRadioButton.Checked = stageCam.IsIPCamera;
            liveUSBCameraRadioButton.Checked = stageCam.IsUSBCamera;
            liveNameTextBox.Text = stageCam.Name;
            if (string.IsNullOrEmpty(stageCam.NDICameraName))
                liveNDICameraComboBox.SelectedValue = 0;
            else
            {
                liveNDICameraComboBox.SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(stageCam.NDICameraName);
            }
            unsavedCameraList.Add(stageCam.Clone());

            Camera imageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[9];
            staticTextBox.Text = imageCam.ImageName;
            imageStageNDICheckBox.Checked = imageCam.IsNDIOutput;
            imageNameTextBox.Text = imageCam.Name;

            liveRadioButton.Checked = ProfileViewModel.Instance.SelectedProfile.IsStageLiveView;
            staticRadioButton.Checked = !liveRadioButton.Checked;
            unsavedCameraList.Add(imageCam.Clone());
        }

        private void LoadPTZCameraInfo(int _cameraInd)
        {
            int rowInd = _cameraInd % 4;
            int colInd = _cameraInd / 4;
            var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
            var cameraTbPanelControl = gBcontrol.Controls[0];
            Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[_cameraInd];

            var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 0); //Camera Name TextBox
            ((TextBox)control).Text = cam.Name;

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 1); //IP Camera check box
            ((MyRadioButton)control).Checked = cam.IsIPCamera;
            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 1); //IP Camera text box
            ((TextBox)control).Text = cam.Ip_Domain;

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 2); //NDI Camera check box
            ((MyRadioButton)control).Checked = cam.IsNDICamera;
            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 2); //NDI Camera combobox
            if (string.IsNullOrEmpty(cam.NDICameraName))
                ((ComboBox)control).SelectedValue = 0;
            else
            {
                ((ComboBox)control).SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(cam.NDICameraName);
            }

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 3); //USB Camera check box
            ((MyRadioButton)control).Checked = cam.IsUSBCamera;
            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 3); //USB Camera combobox

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 4); //NDI Output
            ((CheckBox)control).Checked = cam.IsNDIOutput;

            unsavedCameraList.Add(cam.Clone());
        }

        private void LoadCameraInfo(int _cameraInd) //Load Info to UI
        {
            unsavedCameraList.Clear();
            if (_cameraInd == -1)
            {
                for (int rowInd = 0; rowInd < 4; rowInd++)
                {
                    for (int colInd = 0; colInd < 2; colInd++)
                    {
                        LoadPTZCameraInfo(colInd * 4 + rowInd);
                    }
                }
                LoadStageCameraInfo();
            }
            else if (_cameraInd == 8) //Stage setup
            {
                LoadStageCameraInfo();
            }
            else
            {
                LoadPTZCameraInfo(cameraInd);
            }
        }

        private void SaveStageCameraInfo()
        {
            Camera stageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[8];
            stageCam.Ip_Domain = liveIPCameraTextBox.Text;
            stageCam.IsNDIOutput = liveStageNDICheckbox.Checked;
            stageCam.IsIPCamera = liveIPCameraRadioButton.Checked;
            stageCam.IsNDICamera = liveNDICameraRadioButton.Checked;
            stageCam.IsUSBCamera = liveUSBCameraRadioButton.Checked;
            stageCam.Name = ((TextBox)liveNameTextBox).Text;
            stageCam.Ip_Domain = liveIPCameraTextBox.Text;
            stageCam.NDICameraName = liveNDICameraComboBox.Text;
            stageCam.USBCameraName = liveUSBCameraComboBox.Text;

            CamViewModel.Instance.UpdateActiveCamView(stageCam.Index);

            Camera imageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[9];
            imageCam.ImageName = staticTextBox.Text;
            imageCam.IsNDIOutput = imageStageNDICheckBox.Checked;
            if (!string.IsNullOrEmpty(((TextBox)imageNameTextBox).Text))
                imageCam.Name = ((TextBox)imageNameTextBox).Text;

            ProfileViewModel.Instance.SelectedProfile.IsStageLiveView = liveRadioButton.Checked;
            if (ProfileViewModel.Instance.SelectedProfile.IsStageLiveView)
                imageCam.IsConnect = false;
            else
                imageCam.IsConnect = true;
        }

        private void SavePTZCameraInfo(int _cameraInd)
        {
            int rowInd = _cameraInd % 4;
            int colInd = _cameraInd / 4;
            var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
            var cameraTbPanelControl = gBcontrol.Controls[0];
            Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[_cameraInd];

            var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 0); //Camera Name
            cam.Name = ((TextBox)control).Text;

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 1); //IP Camera check box
            cam.IsIPCamera = ((MyRadioButton)control).Checked;
            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 1); //IP Camera text box
            cam.Ip_Domain = ((TextBox)control).Text;

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 2); //NDI Camera check box
            cam.IsNDICamera = ((MyRadioButton)control).Checked;
            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 2); //NDI Camera combobox
            if (!string.IsNullOrEmpty(((ComboBox)control).Text))
                cam.NDICameraName = ((ComboBox)control).Text;

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 3); //USB Camera check box
            cam.IsUSBCamera = ((MyRadioButton)control).Checked;
            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 3); //USB Camera combobox
            if (!string.IsNullOrEmpty(((ComboBox)control).Text))
                cam.USBCameraName = ((ComboBox)control).Text;

            control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 4); //NDI Output
            cam.IsNDIOutput = ((CheckBox)control).Checked;

            CamViewModel.Instance.UpdateActiveCamView(cam.Index);

        }

        private void SaveCameraInfo() //Save UI
        {
            ProfileViewModel.Instance.SelectedProfile.IsChange = true;
            if (cameraInd == -1)
            {
                for (int rowInd = 0; rowInd < 4; rowInd++)
                {
                    for (int colInd = 0; colInd < 2; colInd++)
                    {
                        SavePTZCameraInfo(colInd * 4 + rowInd);
                    }
                }
                SaveStageCameraInfo();
            }
            else if (cameraInd == 8) //Stage setup
            {
                SaveStageCameraInfo();
            }
            else
            {
                SavePTZCameraInfo(cameraInd);
            }
        }
        private void camera1NDICheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }
        public void CancelUpdateCameraInfo()
        {
            foreach (Camera unsavedCam in unsavedCameraList)
            {
                Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList.Find(x => x.Index == unsavedCam.Index);
                if (cam != null)
                {
                    cam.Copy(unsavedCam);
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e) //Cancel Setup
        {
            Form parentForm = this.Parent as Form;
            if (parentForm != null)
            {
                parentForm.DialogResult = DialogResult.Cancel;
                parentForm.Close();
            }
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveCameraInfo();
            Form parentForm = this.Parent as Form;
            if (parentForm != null)
            {
                parentForm.DialogResult = DialogResult.OK;
                parentForm.Close();
            }
        }

        public event EventHandler<UserControlEventArgs> UserControlEvent;
        protected virtual void OnUserControlEvent(UserControlEventArgs e)
        {
            EventHandler<UserControlEventArgs> handler = UserControlEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void cameraConnectButton_Click(object sender, EventArgs e)
        {
            for (int rowInd = 0; rowInd < 4; rowInd++)
            {
                for (int colInd = 0; colInd < 2; colInd++)
                {
                    var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
                    var cameraTbPanelControl = gBcontrol.Controls[0];
                    Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[colInd * 4 + rowInd];
                    var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(0, 4); //Connect Button
                    if (sender == control)
                    {
                        if (!cam.IsConnect) //not connected yet
                        {
                            cam.IsConnect = true;
                            ((MyToggleButton)control).Status = STATUS_ENUM.DISCONNECTED;
                        }
                        else
                        {

                            cam.IsConnect = false;
                            ((MyToggleButton)control).Status = STATUS_ENUM.UNKNOW;
                        }
                        return;
                    }
                }
            }
        }

        private void liveStageConnectButton_Click(object sender, EventArgs e)
        {
            Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[8];
            MyToggleButton button = sender as MyToggleButton;
            if (!cam.IsConnect) //not connected yet
            {
                cam.IsConnect = true;
                button.Status = STATUS_ENUM.DISCONNECTED;
            }
            else
            {
                cam.IsConnect = false;
                button.Status = STATUS_ENUM.UNKNOW;
            }
        }

        private void stageBrowseButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "xml files (*.jpg)|*.jpeg|All files (*.*)|*.*";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    staticTextBox.Text = dialog.FileName;
                }
            }
        }

        private void liveRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (liveRadioButton.Checked)
            {
                CamView camView = CamViewModel.Instance.FindActiveCamView(8);
                if (camView != null)
                {
                    if (camView.Cam.IsConnect) //not connected yet
                    {
                        liveStageConnectButton.Status = STATUS_ENUM.DISCONNECTED;
                    }
                    else
                    {
                        liveStageConnectButton.Status = STATUS_ENUM.UNKNOW;
                    }
                }
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            new Thread(delegate ()
            {
                List<string> ndiSources = NDISourceViewModel.Instance.GetNDISources();
                if (this.IsHandleCreated)
                {
                    this.BeginInvoke(new Action(delegate ()
                    {
                        try
                        {
                            if (NDISourceViewModel.Instance.CheckChangeNDISources(ndiSources))
                            {
                                NDISourceViewModel.Instance.UpdateNDISources(ndiSources);
                                for (int rowInd = 0; rowInd < 4; rowInd++)
                                {
                                    for (int colInd = 0; colInd < 2; colInd++)
                                    {
                                        var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
                                        var cameraTbPanelControl = gBcontrol.Controls[0];
                                        Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[colInd * 4 + rowInd];

                                        var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 2); //NDI Camera combobox
                                        ((ComboBox)control).DataSource = new BindingSource(NDISourceViewModel.Instance.NDISourceList, null);
                                        ((ComboBox)control).DisplayMember = "Value";
                                        ((ComboBox)control).ValueMember = "Key";
                                        if (string.IsNullOrEmpty(cam.NDICameraName))
                                            ((ComboBox)control).SelectedValue = 0;
                                        else
                                        {
                                            ((ComboBox)control).SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(cam.NDICameraName);
                                        }
                                        control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 3); //USB Camera combobox                                                                                                                    
                                    }
                                }
                                Camera stageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[8];
                                liveNDICameraComboBox.DataSource = new BindingSource(NDISourceViewModel.Instance.NDISourceList, null);
                                liveNDICameraComboBox.DisplayMember = "Value";
                                liveNDICameraComboBox.ValueMember = "Key";
                                if (string.IsNullOrEmpty(stageCam.NDICameraName))
                                    liveNDICameraComboBox.SelectedValue = 0;
                                else
                                {
                                    liveNDICameraComboBox.SelectedValue = NDISourceViewModel.Instance.FindKeyByValue(stageCam.NDICameraName);
                                }
                            }
                            else
                            {
                                //LogWriter.Instance.WriteLog(2, "refreshButton_Click nothing has changed");
                            }
                        }
                        catch { }
                    }));
                }
                else
                {
                    //LogWriter.Instance.WriteLog(2, "refreshButton_Click ignored due to handle is not created");
                }

            }).Start();
        }

        private void ndiCameraComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int rowInd = 0; rowInd < 4; rowInd++)
            {
                for (int colInd = 0; colInd < 2; colInd++)
                {
                    var gBcontrol = cameraInfoTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //GroupBox
                    var cameraTbPanelControl = gBcontrol.Controls[0];
                    Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[colInd * 4 + rowInd];

                    var control = ((TableLayoutPanel)cameraTbPanelControl).GetControlFromPosition(1, 2); //NDI Camera combobox
                    if(control == sender)
                    {
                        if(!((ComboBox)control).Text.Contains("No Input"))
                            cam.NDICameraName = ((ComboBox)control).Text;
                    }                                                                                                                 
                }
            }
        }

        private void liveNDICameraComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Camera stageCam = ProfileViewModel.Instance.SelectedProfile.CameraList[8];
            if (!((ComboBox)liveNDICameraComboBox).Text.Contains("No Input"))
                stageCam.NDICameraName = ((ComboBox)liveNDICameraComboBox).Text;
        }
    }
}
