﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CameraUserControl.ViewModel;
using CameraUserControl.Model;
using CameraControl.Model;
using CameraView.Model;
using CameraView.Base;
using CameraView.ViewModel;

namespace CameraUserControl
{
    public partial class CameraPresetSetupUserControl : UserControl
    {
        public CameraPresetSetupUserControl()
        {
            InitializeComponent();            
            this.Load += CameraPresetSetupUserControl_Load;
        }

        private void CameraPresetSetupUserControl_Load(object sender, EventArgs e)
        {
            InitializeTreeView();
        }

        public void InitializeTreeView()
        {
            CameraTreeNodeViewModel.Instance.InitTreeView(cameraTreeView);
            this.ActiveControl = cameraTreeView;
        }

        private void cameraTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNodeEx node = e.Node as TreeNodeEx;
            if(node != null)
            {
                CamView camView = node.Value as CamView;
                if(camView != null)
                {
                    PanelEx panelEx = null;
                    ptzUserControl.SetCameraControl(camView.CamControl);
                    foreach (var control in previewPanel.Controls)
                    {
                        panelEx = control as PanelEx;
                        if(panelEx != null)
                        {
                            panelEx.IsUsing = false;
                        }
                    }
                    previewPanel.Controls.Clear();
                    panelEx = camView.MainStream.GetChannelPanel(); //this panel is using for render
                    if (panelEx == null)
                    {
                        panelEx = new PanelEx(camView);
                        camView.MainStream.AddChannelPanel(panelEx);
                    }
                    else
                        camView.MainStream.Start();
                    previewPanel.Controls.Add(panelEx);
                    panelEx.IsUsing = true;
                }
            }
        }
        public void Close()
        {
            foreach (var control in previewPanel.Controls)
            {
                PanelEx panelEx = control as PanelEx;
                if (panelEx != null)
                {
                    panelEx.IsUsing = false;
                }
            }
            previewPanel.Controls.Clear();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Form parentForm = this.Parent as Form;
            if (parentForm != null)
            {
                parentForm.DialogResult = DialogResult.OK;
                parentForm.Close();
            }
        }

    }
}
