﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CameraControl.Model;
using CameraUserControl.ViewModel;
using Settings.ViewModel;
using System.IO;
using ShareLibrary.Base;
using CameraUserControl.Base;

namespace CameraUserControl
{
    public partial class PTZUserControl : UserControl
    {
        private CamControl camControl;
        private bool isLoaded = false;
        private bool isMainView = false;
        public bool IsAutoFocus
        {
            get { return autoFocusRadioButton.Checked; }
        }
        public PTZUserControl()
        {
            InitializeComponent();
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            InitializeBinding();
            PTZButtonUpdateImage();            
            this.Load += PTZUserControl_Load;
        }

        private void PTZUserControl_Load(object sender, EventArgs e)
        {
            if(!isLoaded)
            {
                if (JoystickControlViewModel.Instance != null && JoystickControlViewModel.Instance.Joystick != null)
                    JoystickControlViewModel.Instance.Joystick.JoystickEvent += Joystick_JoystickEvent;
            }
            isLoaded = true;
        }

        private void JoystickPOVChanged(JoystickPOVEvent _jsPovEvent)
        {
            switch (_jsPovEvent.Angle)
            {
                case 0:
                    if (camControl != null)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            camControl.MoveTopOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                        }));
                    }
                    break;
                case 45:
                    break;
                case 90:
                    if (camControl != null)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            LogWriter.Instance.WriteLog(2, "POV MoveRightOnce");
                            camControl.MoveRightOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                        }));
                    }                    
                    break;
                case 135:                    
                    break;
                case 180:
                    if (camControl != null)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            camControl.MoveBottomOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                        }));
                    }                    
                    break;
                case 225:
                    break;
                case 270:
                    if (camControl != null)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            LogWriter.Instance.WriteLog(2, "POV MoveLeftOnce");
                            camControl.MoveLeftOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                        }));
                    }                    
                    break;
                case 315:                    
                    break;
                case -1:
                    if (camControl != null)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            LogWriter.Instance.WriteLog(2, "Stop Moving");
                            camControl.StopMoving((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                        }));
                    }                    
                    break;
                default:
                    break;
            }
        }
        private void JoystickAxisChanged(JoystickAxisEvent _jsAxisEvent)
        {
            switch (_jsAxisEvent.Axis)
            {
                case 3://TR
                    if (_jsAxisEvent.Value > JOYSTICK.STICK_THRESHOLD)
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.MoveRightOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                            }));
                        }                            
                    }
                    else if (_jsAxisEvent.Value < -JOYSTICK.STICK_THRESHOLD)
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate ()
                            {
                                camControl.MoveLeftOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                            }));
                        }
                    }
                    else
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.StopMoving((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                            }));
                        }                        
                    }
                    break;

                case 4://TR
                    if (_jsAxisEvent.Value > JOYSTICK.STICK_THRESHOLD)
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.MoveBottomOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                            }));
                        }
                    }
                    else if (_jsAxisEvent.Value < -JOYSTICK.STICK_THRESHOLD)
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.MoveTopOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                            }));
                        }                        
                    }
                    else
                    {
                        if (camControl != null)
                        {
                            this.BeginInvoke(new Action(delegate ()
                            {
                                camControl.StopMoving((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
                            }));
                        }                        
                    }
                    break;

                case 1://TL
                    
                    break;

                case 0://TL
                    if (_jsAxisEvent.Value > JOYSTICK.STICK_THRESHOLD)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            UserControlEventArgs args = new UserControlEventArgs();
                            args.EventType = EVENT_TYPE_ENUM.SWITCH_RIGHT_CAMERA;
                            OnPTZControlEvent(args);
                        }));
                    }
                    else if (_jsAxisEvent.Value < -JOYSTICK.STICK_THRESHOLD)
                    {
                        this.BeginInvoke(new Action(delegate ()
                        {
                            UserControlEventArgs args = new UserControlEventArgs();
                            args.EventType = EVENT_TYPE_ENUM.SWITCH_LEFT_CAMERA;
                            OnPTZControlEvent(args);
                        }));
                    }
                    else
                    {
 
                    }
                    break;

                default:
                    break;
            }
        }
        private void JoystickButtonChanged(JoystickButtonEvent _jsButtonEvent)
        {
            switch ((JOYSTICK_BUTTON_ENUM)_jsButtonEvent.Button)
            {
                case JOYSTICK_BUTTON_ENUM.BTN_START:
                    if (!_jsButtonEvent.Pressed)
                    {
                        if (camControl != null)
                        {
                            this.BeginInvoke(new Action(delegate ()
                            {
                                camControl.OSDEnter();
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_BACK:
                    if (!_jsButtonEvent.Pressed)
                    {
                        if (camControl != null)
                        {
                            this.BeginInvoke(new Action(delegate ()
                            {
                                camControl.OSDBack();
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_LT:
                    if (_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.FocusInOnce((int)focusSpeedComboBox.SelectedValue);
                            }));
                        }
                    }
                    else
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.StopFocusing();
                            }));
                        }                        
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_RT:
                    if (_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.FocusOutOnce((int)focusSpeedComboBox.SelectedValue);
                            }));
                        }                        
                    }
                    else
                    {
                        if (camControl != null) {
                            this.BeginInvoke(new Action(delegate (){
                                camControl.StopFocusing();
                            }));
                        }                        
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_LB:
                    if (_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.ZoomInOnce((int)zoomSpeedComboBox.SelectedValue);
                            }));
                        }                        
                    }
                    else
                    {
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.StopZooming();
                            }));
                        }                        
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_RB:
                    if (_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.ZoomOutOnce((int)zoomSpeedComboBox.SelectedValue);
                            }));
                        }                        
                    }
                    else
                    {
                        if (camControl != null)
                        {
                            this.BeginInvoke(new Action(delegate ()
                            {
                                camControl.StopZooming();
                            }));
                        }                        
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_X:
                    if (!_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate ()
                            {
                                camControl.RecallPreset(2);
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_Y:
                    if (!_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.RecallPreset(3);
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_A:
                    if (!_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.RecallPreset(1);
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_B:
                    if (!_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate ()
                            {
                                autoFocusRadioButton.Checked = !autoFocusRadioButton.Checked; //change auto/manual focus
                                manFocusRadioButton.Checked = !autoFocusRadioButton.Checked; //change auto/manual focus
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_TL:
                    if (!_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.OSDToggle();
                            }));
                        }
                    }
                    break;
                case JOYSTICK_BUTTON_ENUM.BTN_TR:
                    if (!_jsButtonEvent.Pressed){
                        if (camControl != null){
                            this.BeginInvoke(new Action(delegate (){
                                camControl.MoveHome();
                            }));
                        }
                    }
                    break;
                default:
                    break;
            }
            
        }
        private void Joystick_JoystickEvent(object sender, ShareLibrary.Base.JoystickEventArgs e)
        {
            if(isMainView != ProfileViewModel.Instance.IsPresetSetup)
            {
                switch (e.EventType)
                {
                    case ShareLibrary.Base.JOYSTICK_EVENT_TYPE.POV_EVENT:
                        JoystickPOVChanged(e.JSPOVEvent);
                        break;
                    case ShareLibrary.Base.JOYSTICK_EVENT_TYPE.AXIS_EVENT:
                        JoystickAxisChanged(e.JSPAxisvent);
                        break;
                    case ShareLibrary.Base.JOYSTICK_EVENT_TYPE.BUTTON_EVENT:
                        JoystickButtonChanged(e.JSButtonEvent);
                        break;
                    default:
                        break;
                }
            }

        }
        private void InitializeBinding()
        {
            reCallPresetComboBox.DataSource = new BindingSource(PresetNumberViewModel.Instance.PresetNumberList, null);
            reCallPresetComboBox.DisplayMember = "Value";
            reCallPresetComboBox.ValueMember = "Key";
            reCallPresetComboBox.DataBindings.Add("SelectedValue", ProfileViewModel.Instance.SelectedProfile.PTZ, "ReCallPresetNumber", true, DataSourceUpdateMode.OnPropertyChanged);

            setPresetComboBox.DataSource = new BindingSource(PresetNumberViewModel.Instance.PresetNumberList, null);
            setPresetComboBox.DisplayMember = "Value";
            setPresetComboBox.ValueMember = "Key";
            setPresetComboBox.DataBindings.Add("SelectedValue", ProfileViewModel.Instance.SelectedProfile.PTZ, "SetPresetNumber", true, DataSourceUpdateMode.OnPropertyChanged);

            panSpeedComboBox.DataSource = new BindingSource(PanSpeedViewModel.Instance.PanSpeedList, null);
            panSpeedComboBox.DisplayMember = "Value";
            panSpeedComboBox.ValueMember = "Key";
            panSpeedComboBox.DataBindings.Add("SelectedValue", ProfileViewModel.Instance.SelectedProfile.PTZ, "PanSpeed", true, DataSourceUpdateMode.OnPropertyChanged);

            tiltSpeedComboBox.DataSource = new BindingSource(TiltSpeedViewModel.Instance.TiltSpeedList, null);
            tiltSpeedComboBox.DisplayMember = "Value";
            tiltSpeedComboBox.ValueMember = "Key";
            tiltSpeedComboBox.DataBindings.Add("SelectedValue", ProfileViewModel.Instance.SelectedProfile.PTZ, "TiltSpeed", true, DataSourceUpdateMode.OnPropertyChanged);

            zoomSpeedComboBox.DataSource = new BindingSource(ZoomSpeedViewModel.Instance.ZoomSpeedList, null);
            zoomSpeedComboBox.DisplayMember = "Value";
            zoomSpeedComboBox.ValueMember = "Key";
            zoomSpeedComboBox.DataBindings.Add("SelectedValue", ProfileViewModel.Instance.SelectedProfile.PTZ, "ZoomSpeed", true, DataSourceUpdateMode.OnPropertyChanged);

            focusSpeedComboBox.DataSource = new BindingSource(FocusSpeedViewModel.Instance.FocusSpeedList, null);
            focusSpeedComboBox.DisplayMember = "Value";
            focusSpeedComboBox.ValueMember = "Key";
            focusSpeedComboBox.DataBindings.Add("SelectedValue", ProfileViewModel.Instance.SelectedProfile.PTZ, "FocusSpeed", true, DataSourceUpdateMode.OnPropertyChanged);

            autoFocusRadioButton.Checked = ProfileViewModel.Instance.SelectedProfile.PTZ.IsAutoFocus;
            manFocusRadioButton.Checked = !autoFocusRadioButton.Checked;

            xboxCheckBox.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.PTZ, "IsXBOX", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        public void SetPTZModeForMainView()
        {
            isMainView = true;
            this.ptzControlTableLayoutPanel.ColumnStyles[0].Width = 0F; //Invisible OSD items
            this.setPresetTableLayoutPanel.RowStyles[1].Height = 0F; //Invisible Preset Number
            this.setPresetTableLayoutPanel.RowStyles[2].Height = 0F; //Invisible Preset Name
            this.setPresetTableLayoutPanel.RowStyles[3].Height = 0F; //Invisible Set Preset
        }
        private void PTZButtonUpdateImage() //Add normal, hover and active image for buttons
        {
            this.ptzLeftButton.UpdateImage(Properties.Resources.normal_left_arrow, Properties.Resources.hover_left_arrow, Properties.Resources.active_left_arrow);
            this.ptzRightButton.UpdateImage(Properties.Resources.normal_right_arrow, Properties.Resources.hover_right_arrow, Properties.Resources.active_right_arrow);
            this.ptzDownButton.UpdateImage(Properties.Resources.normal_down_arrow, Properties.Resources.hover_down_arrow, Properties.Resources.active_down_arrow);
            this.ptzUpButton.UpdateImage(Properties.Resources.normal_up_arrow, Properties.Resources.hover_up_arrow, Properties.Resources.active_up_arrow);
            this.ptzHomeButton.UpdateImage(Properties.Resources.normal_home_arrow, Properties.Resources.hover_home_arrow, Properties.Resources.active_home_arrow);

            this.zoomInButton.UpdateImage(Properties.Resources.normal_zoom_in, Properties.Resources.hover_zoom_in, Properties.Resources.active_zoom_in);
            this.zoomOutButton.UpdateImage(Properties.Resources.normal_zoom_out, Properties.Resources.hover_zoom_out, Properties.Resources.active_zoom_out);
            this.focusInButton.UpdateImage(Properties.Resources.normal_focus_in, Properties.Resources.hover_focus_in, Properties.Resources.active_focus_in);
            this.focusOutButton.UpdateImage(Properties.Resources.normal_focus_out, Properties.Resources.hover_focus_out, Properties.Resources.active_focus_out);
        } 
        public void SetCameraControl(CamControl _camControl)
        {
            camControl = _camControl;
            setPresetComboBox_SelectedIndexChanged(null, null);
        }
        //Pan Tilt control - begin
        private void ptzLeftButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.MoveLeftOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
            }
        }
        private void ptzUpButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.MoveTopOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
            }
        }
        private void ptzRightButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.MoveRightOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
            }
        }
        private void ptzDownButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.MoveBottomOnce((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
            }
        }
        private void ptzButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.StopMoving((int)panSpeedComboBox.SelectedValue, (int)tiltSpeedComboBox.SelectedValue);
            }
        }
        private void ptzHomeButton_Click(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                camControl.MoveHome();
            }
        }
        //Pan Tilt control - end
        //Zoom Focus control - begin
        private void zoomInButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.ZoomInOnce((int)zoomSpeedComboBox.SelectedValue);
            }
        }
        private void zoomOutButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.ZoomOutOnce((int)zoomSpeedComboBox.SelectedValue);
            }
        }
        private void zoomButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.StopZooming();
            }
        }
        private void focusInButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.FocusInOnce((int)focusSpeedComboBox.SelectedValue);
            }
        } 
        private void focusOutButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.FocusOutOnce((int)focusSpeedComboBox.SelectedValue);
            }
        }
        private void focusButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (camControl != null)
            {
                camControl.StopFocusing();
            }
        }
        //Zoom Focus control - end
        //Preset control - begin
        private void setPresetButton_Click(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                ProfileViewModel.Instance.SelectedProfile.IsChange = true;
                if (camControl.Cam.PTZ.PresetNameList.ContainsKey((int)setPresetComboBox.SelectedValue))
                    camControl.Cam.PTZ.PresetNameList[(int)setPresetComboBox.SelectedValue] = presetNameTextBox.Text;
                else
                    camControl.Cam.PTZ.PresetNameList.Add((int)setPresetComboBox.SelectedValue, presetNameTextBox.Text);
                camControl.SetPreset((int)setPresetComboBox.SelectedValue);
                camControl.Cam.ImagePath = ProfileViewModel.Instance.ImageDir + "Camera" + camControl.Cam.Index + "\\" + "Preset"+ (int)setPresetComboBox.SelectedValue+".jpg";
                string imageDir = Path.GetDirectoryName(camControl.Cam.ImagePath);
                if (!Directory.Exists(imageDir))
                    Directory.CreateDirectory(imageDir);
                camControl.Cam.IsSnapshot = true; 
            }
        }
        private void ptzReCallButton_Click(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                camControl.RecallPreset((int)reCallPresetComboBox.SelectedValue);
            }
        }
        private void osdButton_Click(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                camControl.OSDToggle();
            }
        }
        private void osdEnterButton_Click(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                camControl.OSDEnter();
            }
        }
        private void osdBackButton_Click(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                camControl.OSDBack();
            }
        }

        private void autoFocusRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            ProfileViewModel.Instance.SelectedProfile.IsChange = true;
            ProfileViewModel.Instance.SelectedProfile.PTZ.IsAutoFocus = autoFocusRadioButton.Checked;
            if (camControl != null)
            {
                camControl.AutoFocusChange(autoFocusRadioButton.Checked);
            }
        }

        private void setPresetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (camControl != null)
            {
                if (setPresetComboBox.SelectedValue == null)
                    setPresetComboBox.SelectedValue = 1; //init value
                if (camControl.Cam.PTZ.PresetNameList.ContainsKey((int)setPresetComboBox.SelectedValue))
                    presetNameTextBox.Text = camControl.Cam.PTZ.PresetNameList[(int)setPresetComboBox.SelectedValue];
                else
                    presetNameTextBox.Text = "Preset " + (int)setPresetComboBox.SelectedValue;
            }
        }

        private void xboxCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
                ProfileViewModel.Instance.SelectedProfile.IsChange = true;
            if (xboxCheckBox.Checked)
                JoystickControlViewModel.Instance.JoyStickControl.Start();
            else
                JoystickControlViewModel.Instance.JoyStickControl.Stop();
        }
        //Preset control - end  

        public event EventHandler<UserControlEventArgs> PTZControlEvent;
        protected virtual void OnPTZControlEvent(UserControlEventArgs e)
        {
            EventHandler<UserControlEventArgs> handler = PTZControlEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
