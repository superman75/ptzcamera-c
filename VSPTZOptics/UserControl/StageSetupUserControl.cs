﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Settings.ViewModel;
using CameraUserControl.Model;
using ShareLibrary.Model;
using CameraView.Model;
using CameraView.ViewModel;
using System.IO;
using HTTPControl;

namespace CameraUserControl
{
    public partial class StageSetupUserControl : UserControl
    {
        private Stage currentStage = null;
        public StageSetupUserControl()
        {
            InitializeComponent();
            InitializeBinding();
            LoadStageInfo();
            this.Load += StageSetupUserControl_Load;
        }
        private void InitializeBinding()
        {
            cameraName1Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[0], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName2Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[1], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName3Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[2], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName4Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[3], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName5Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[4], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName6Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[5], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName7Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[6], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            cameraName8Label.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[7], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        private void StageSetupUserControl_Load(object sender, EventArgs e)
        {
            InitilizeCombobox();
        }

        private void LoadStageInfo()
        {
            foreach(Stage stage in ProfileViewModel.Instance.SelectedProfile.StageList)
            {
                stage.StageEvent += Stage_StageEvent;
            }
        }

        private void Stage_StageEvent(object sender, ShareLibrary.Base.StageEventArgs e)
        {
            Stage stage = sender as Stage;
            if(stage != null)
            {
                for(int camInd = 0; camInd < 8; camInd++)
                {
                    CamView camView = CamViewModel.Instance.FindActiveCamView(camInd);
                    if(camView != null)
                    {
                        if (stage.PresetNumberList[camView.Cam.Index] != 0) //0 is No movement
                        {
                            camView.CamControl.RecallPreset(stage.PresetNumberList[camView.Cam.Index]);
                        }
                    }
                }                
            }
        }

        private void InitilizeCombobox()
        {
            currentStage = null;
            camera1ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[0].PTZ.PresetNameList, null);
            camera1ComboBox.DisplayMember = "Value";
            camera1ComboBox.ValueMember = "Key";
            camera1ComboBox.SelectedValue = 0;

            camera2ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[1].PTZ.PresetNameList, null);
            camera2ComboBox.DisplayMember = "Value";
            camera2ComboBox.ValueMember = "Key";
            camera2ComboBox.SelectedValue = 0;

            camera3ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[2].PTZ.PresetNameList, null);
            camera3ComboBox.DisplayMember = "Value";
            camera3ComboBox.ValueMember = "Key";
            camera3ComboBox.SelectedValue = 0;

            camera4ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[3].PTZ.PresetNameList, null);
            camera4ComboBox.DisplayMember = "Value";
            camera4ComboBox.ValueMember = "Key";
            camera4ComboBox.SelectedValue = 0;

            camera5ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[4].PTZ.PresetNameList, null);
            camera5ComboBox.DisplayMember = "Value";
            camera5ComboBox.ValueMember = "Key";
            camera5ComboBox.SelectedValue = 0;

            camera6ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[5].PTZ.PresetNameList, null);
            camera6ComboBox.DisplayMember = "Value";
            camera6ComboBox.ValueMember = "Key";
            camera6ComboBox.SelectedValue = 0;

            camera7ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[6].PTZ.PresetNameList, null);
            camera7ComboBox.DisplayMember = "Value";
            camera7ComboBox.ValueMember = "Key";
            camera7ComboBox.SelectedValue = 0;

            camera8ComboBox.DataSource = new BindingSource(ProfileViewModel.Instance.SelectedProfile.CameraList[7].PTZ.PresetNameList, null);
            camera8ComboBox.DisplayMember = "Value";
            camera8ComboBox.ValueMember = "Key";
            camera8ComboBox.SelectedValue = 0;

            var button = stageButtonTableLayoutPanel.GetControlFromPosition(1, 0); //Button 1
            stageButton_Click(button, null);//Select button 1;
        }
        private void UpdateComboBox(Stage _stage)
        {
            camera1ComboBox.SelectedValue = _stage.PresetNumberList[0];
            camera2ComboBox.SelectedValue = _stage.PresetNumberList[1];
            camera3ComboBox.SelectedValue = _stage.PresetNumberList[2];
            camera4ComboBox.SelectedValue = _stage.PresetNumberList[3];
            camera5ComboBox.SelectedValue = _stage.PresetNumberList[4];
            camera6ComboBox.SelectedValue = _stage.PresetNumberList[5];
            camera7ComboBox.SelectedValue = _stage.PresetNumberList[6];
            camera8ComboBox.SelectedValue = _stage.PresetNumberList[7];
        }
        private void UpdatePreviewImage(int _presetNumber, int _camInd)
        {
            int rowInd = _camInd % 4;
            int colInd = _camInd < 4 ? 1 : 3;
            Panel panel = (Panel)stageSetupTableLayoutPanel.GetControlFromPosition(colInd, rowInd);
            if (panel != null)
            {
                string imagePath = ProfileViewModel.Instance.ImageDir + "Camera" + _camInd + "\\" + "Preset" + _presetNumber + ".jpg";
                if (File.Exists(imagePath))
                {
                    using (FileStream stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
                    {
                        panel.BackgroundImage = Image.FromStream(stream);
                        panel.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                }
                else
                    panel.BackgroundImage = null;
            }

        }
        
        private void stageButton_Click(object sender, EventArgs e)
        {
            RoundedButton button = sender as RoundedButton;
            button.BackColor = System.Drawing.Color.FromArgb(79, 79, 79);
            int index = 0;
            for (int colInd = 1; colInd <= 8; colInd++)
            {
                var control = stageButtonTableLayoutPanel.GetControlFromPosition(colInd, 0); //Control Button
                if (control == button)
                {
                    Stage selectedStage = ProfileViewModel.Instance.SelectedProfile.StageList[index];
                    currentStage = selectedStage;
                    UpdateComboBox(currentStage);
                    httpTextBox.Text = selectedStage.HTTPTrigger;
                    showStageCheckBox.Checked = selectedStage.IsShowOnStageView;
                    spNameTextBox.Text = selectedStage.Name;
                }
                else
                {
                    control.BackColor = System.Drawing.Color.Gray;
                }
                index++;
            }
        }

        private void camera1ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 0);
            }
        }
        private void camera2ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 1);
            }
        }
        private void camera3ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 2);
            }
        }
        private void camera4ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 3);
            }
        }
        private void camera5ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 4);
            }
        }
        private void camera6ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 5);
            }
        }
        private void camera7ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 6);
            }
        }
        private void camera8ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                int presetNumber = ((KeyValuePair<int, string>)((ComboBox)sender).SelectedItem).Key;
                UpdatePreviewImage(presetNumber, 7);
            }
        }
        private void SaveStageInfo()
        {
            if(currentStage != null)
            {
                ProfileViewModel.Instance.SelectedProfile.IsChange = true;
                currentStage.IsShowOnStageView = showStageCheckBox.Checked;
                currentStage.HTTPTrigger = httpTextBox.Text;
                currentStage.Name = spNameTextBox.Text;
                currentStage.PresetNumberList[0] = (int)camera1ComboBox.SelectedValue;
                currentStage.PresetNumberList[1] = (int)camera2ComboBox.SelectedValue;
                currentStage.PresetNumberList[2] = (int)camera3ComboBox.SelectedValue;
                currentStage.PresetNumberList[3] = (int)camera4ComboBox.SelectedValue;
                currentStage.PresetNumberList[4] = (int)camera5ComboBox.SelectedValue;
                currentStage.PresetNumberList[5] = (int)camera6ComboBox.SelectedValue;
                currentStage.PresetNumberList[6] = (int)camera7ComboBox.SelectedValue;
                currentStage.PresetNumberList[7] = (int)camera8ComboBox.SelectedValue;
            }
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveStageInfo();
            /*Form parentForm = this.Parent as Form;
            if (parentForm != null)
            {
                parentForm.DialogResult = DialogResult.OK;
                parentForm.Close();
            }*/
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Form parentForm = this.Parent as Form;
            if (parentForm != null)
            {
                parentForm.DialogResult = DialogResult.Cancel;
                parentForm.Close();
            }
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(httpTextBox.Text))
            {
                string data = HTTPController.HTTPTigger(httpTextBox.Text);
                if (data != null)
                    MessageBox.Show(data);
            }
        }

        private void showStageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
