﻿using CameraUserControl.Model;

namespace CameraUserControl
{
    partial class CameraSetupUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraSetupUserControl));
            this.cameraSetupTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ptzCameraGroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraInfoTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName1TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName2TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName3TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName4TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName5TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName6TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName7TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.cameraName8TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera1ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera2ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera3ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera4ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera5ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera6ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera7ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera8ConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.camera1NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera2NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera3NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera4NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera5NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera6NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera7NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera8NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera1TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera2TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera3TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera4TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera5TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera6TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera7TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.camera8TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.stageSourceGroupBox = new CameraUserControl.Model.MyGroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.liveStageConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.stageBrowseButton = new CameraUserControl.Model.RoundedButton();
            this.liveStageNDICheckbox = new CameraUserControl.Model.MyCheckBox();
            this.imageStageNDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.liveTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.staticTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.liveRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.liveNameTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.staticRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.imageNameTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cancelButton = new CameraUserControl.Model.RoundedButton();
            this.saveButton = new CameraUserControl.Model.RoundedButton();
            this.camera1NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera2NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera3NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera4NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera5NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera6NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera7NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.camera8NDIInputCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.myComboBox1 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox2 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox3 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox4 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox5 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox6 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox7 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox8 = new CameraUserControl.Model.MyComboBox();
            this.liveStageNDIInputCheckbox = new CameraUserControl.Model.MyCheckBox();
            this.liveNDINameComboBox = new CameraUserControl.Model.MyComboBox();
            this.cameraSetupTableLayoutPanel.SuspendLayout();
            this.ptzCameraGroupBox.SuspendLayout();
            this.cameraInfoTableLayoutPanel.SuspendLayout();
            this.stageSourceGroupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cameraSetupTableLayoutPanel
            // 
            this.cameraSetupTableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.cameraSetupTableLayoutPanel.ColumnCount = 1;
            this.cameraSetupTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraSetupTableLayoutPanel.Controls.Add(this.ptzCameraGroupBox, 0, 1);
            this.cameraSetupTableLayoutPanel.Controls.Add(this.stageSourceGroupBox, 0, 2);
            this.cameraSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.cameraSetupTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraSetupTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.cameraSetupTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.cameraSetupTableLayoutPanel.Name = "cameraSetupTableLayoutPanel";
            this.cameraSetupTableLayoutPanel.RowCount = 5;
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 417F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraSetupTableLayoutPanel.Size = new System.Drawing.Size(826, 687);
            this.cameraSetupTableLayoutPanel.TabIndex = 0;
            // 
            // ptzCameraGroupBox
            // 
            this.ptzCameraGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ptzCameraGroupBox.Controls.Add(this.cameraInfoTableLayoutPanel);
            this.ptzCameraGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzCameraGroupBox.ForeColor = System.Drawing.Color.White;
            this.ptzCameraGroupBox.Location = new System.Drawing.Point(13, 12);
            this.ptzCameraGroupBox.Margin = new System.Windows.Forms.Padding(13, 12, 13, 0);
            this.ptzCameraGroupBox.Name = "ptzCameraGroupBox";
            this.ptzCameraGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.ptzCameraGroupBox.Size = new System.Drawing.Size(800, 405);
            this.ptzCameraGroupBox.TabIndex = 4;
            this.ptzCameraGroupBox.TabStop = false;
            this.ptzCameraGroupBox.Text = "PTZ Camera";
            // 
            // cameraInfoTableLayoutPanel
            // 
            this.cameraInfoTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.cameraInfoTableLayoutPanel.ColumnCount = 8;
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName1TextBox, 1, 1);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName2TextBox, 1, 2);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName3TextBox, 1, 3);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName4TextBox, 1, 4);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName5TextBox, 1, 5);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName6TextBox, 1, 6);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName7TextBox, 1, 7);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraName8TextBox, 1, 8);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera1ConnectButton, 4, 1);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera2ConnectButton, 4, 2);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera3ConnectButton, 4, 3);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera4ConnectButton, 4, 4);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera5ConnectButton, 4, 5);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera6ConnectButton, 4, 6);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera7ConnectButton, 4, 7);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera8ConnectButton, 4, 8);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera1NDICheckBox, 6, 1);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera2NDICheckBox, 6, 2);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera3NDICheckBox, 6, 3);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera4NDICheckBox, 6, 4);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera5NDICheckBox, 6, 5);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera6NDICheckBox, 6, 6);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera7NDICheckBox, 6, 7);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera8NDICheckBox, 6, 8);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera1TextBox, 2, 1);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera2TextBox, 2, 2);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera3TextBox, 2, 3);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera4TextBox, 2, 4);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera5TextBox, 2, 5);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera6TextBox, 2, 6);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera7TextBox, 2, 7);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera8TextBox, 2, 8);
            this.cameraInfoTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraInfoTableLayoutPanel.Location = new System.Drawing.Point(4, 17);
            this.cameraInfoTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.cameraInfoTableLayoutPanel.Name = "cameraInfoTableLayoutPanel";
            this.cameraInfoTableLayoutPanel.RowCount = 10;
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraInfoTableLayoutPanel.Size = new System.Drawing.Size(792, 384);
            this.cameraInfoTableLayoutPanel.TabIndex = 0;
            // 
            // cameraName1TextBox
            // 
            this.cameraName1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName1TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName1TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName1TextBox.Location = new System.Drawing.Point(4, 28);
            this.cameraName1TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName1TextBox.MaxLength = 9;
            this.cameraName1TextBox.Name = "cameraName1TextBox";
            this.cameraName1TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName1TextBox.TabIndex = 0;
            this.cameraName1TextBox.Text = "Camera #1";
            this.cameraName1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName2TextBox
            // 
            this.cameraName2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName2TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName2TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName2TextBox.Location = new System.Drawing.Point(4, 72);
            this.cameraName2TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName2TextBox.MaxLength = 9;
            this.cameraName2TextBox.Name = "cameraName2TextBox";
            this.cameraName2TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName2TextBox.TabIndex = 1;
            this.cameraName2TextBox.Text = "Camera #2";
            this.cameraName2TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName3TextBox
            // 
            this.cameraName3TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName3TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName3TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName3TextBox.Location = new System.Drawing.Point(4, 116);
            this.cameraName3TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName3TextBox.MaxLength = 9;
            this.cameraName3TextBox.Name = "cameraName3TextBox";
            this.cameraName3TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName3TextBox.TabIndex = 1;
            this.cameraName3TextBox.Text = "Camera #3";
            this.cameraName3TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName4TextBox
            // 
            this.cameraName4TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName4TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName4TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName4TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName4TextBox.Location = new System.Drawing.Point(4, 160);
            this.cameraName4TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName4TextBox.MaxLength = 9;
            this.cameraName4TextBox.Name = "cameraName4TextBox";
            this.cameraName4TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName4TextBox.TabIndex = 1;
            this.cameraName4TextBox.Text = "Camera #4";
            this.cameraName4TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName5TextBox
            // 
            this.cameraName5TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName5TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName5TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName5TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName5TextBox.Location = new System.Drawing.Point(4, 204);
            this.cameraName5TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName5TextBox.MaxLength = 9;
            this.cameraName5TextBox.Name = "cameraName5TextBox";
            this.cameraName5TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName5TextBox.TabIndex = 1;
            this.cameraName5TextBox.Text = "Camera #5";
            this.cameraName5TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName6TextBox
            // 
            this.cameraName6TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName6TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName6TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName6TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName6TextBox.Location = new System.Drawing.Point(4, 248);
            this.cameraName6TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName6TextBox.MaxLength = 9;
            this.cameraName6TextBox.Name = "cameraName6TextBox";
            this.cameraName6TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName6TextBox.TabIndex = 1;
            this.cameraName6TextBox.Text = "Camera #6";
            this.cameraName6TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName7TextBox
            // 
            this.cameraName7TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName7TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName7TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName7TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName7TextBox.Location = new System.Drawing.Point(4, 292);
            this.cameraName7TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName7TextBox.MaxLength = 9;
            this.cameraName7TextBox.Name = "cameraName7TextBox";
            this.cameraName7TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName7TextBox.TabIndex = 1;
            this.cameraName7TextBox.Text = "Camera #7";
            this.cameraName7TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cameraName8TextBox
            // 
            this.cameraName8TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraName8TextBox.BackColor = System.Drawing.Color.Black;
            this.cameraName8TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraName8TextBox.ForeColor = System.Drawing.Color.White;
            this.cameraName8TextBox.Location = new System.Drawing.Point(4, 336);
            this.cameraName8TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cameraName8TextBox.MaxLength = 9;
            this.cameraName8TextBox.Name = "cameraName8TextBox";
            this.cameraName8TextBox.Size = new System.Drawing.Size(96, 20);
            this.cameraName8TextBox.TabIndex = 1;
            this.cameraName8TextBox.Text = "Camera #8";
            this.cameraName8TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // camera1ConnectButton
            // 
            this.camera1ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera1ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera1ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera1ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera1ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera1ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera1ConnectButton.Image")));
            this.camera1ConnectButton.IsConnect = false;
            this.camera1ConnectButton.Location = new System.Drawing.Point(576, 20);
            this.camera1ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera1ConnectButton.Name = "camera1ConnectButton";
            this.camera1ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera1ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera1ConnectButton.TabIndex = 2;
            this.camera1ConnectButton.Text = " Connect";
            this.camera1ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera1ConnectButton.UseVisualStyleBackColor = false;
            this.camera1ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera2ConnectButton
            // 
            this.camera2ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera2ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera2ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera2ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera2ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera2ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera2ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera2ConnectButton.Image")));
            this.camera2ConnectButton.IsConnect = false;
            this.camera2ConnectButton.Location = new System.Drawing.Point(576, 64);
            this.camera2ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera2ConnectButton.Name = "camera2ConnectButton";
            this.camera2ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera2ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera2ConnectButton.TabIndex = 2;
            this.camera2ConnectButton.Text = " Connect";
            this.camera2ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera2ConnectButton.UseVisualStyleBackColor = false;
            this.camera2ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera3ConnectButton
            // 
            this.camera3ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera3ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera3ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera3ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera3ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera3ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera3ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera3ConnectButton.Image")));
            this.camera3ConnectButton.IsConnect = false;
            this.camera3ConnectButton.Location = new System.Drawing.Point(576, 108);
            this.camera3ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera3ConnectButton.Name = "camera3ConnectButton";
            this.camera3ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera3ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera3ConnectButton.TabIndex = 2;
            this.camera3ConnectButton.Text = " Connect";
            this.camera3ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera3ConnectButton.UseVisualStyleBackColor = false;
            this.camera3ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera4ConnectButton
            // 
            this.camera4ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera4ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera4ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera4ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera4ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera4ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera4ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera4ConnectButton.Image")));
            this.camera4ConnectButton.IsConnect = false;
            this.camera4ConnectButton.Location = new System.Drawing.Point(576, 152);
            this.camera4ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera4ConnectButton.Name = "camera4ConnectButton";
            this.camera4ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera4ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera4ConnectButton.TabIndex = 2;
            this.camera4ConnectButton.Text = " Connect";
            this.camera4ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera4ConnectButton.UseVisualStyleBackColor = false;
            this.camera4ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera5ConnectButton
            // 
            this.camera5ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera5ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera5ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera5ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera5ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera5ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera5ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera5ConnectButton.Image")));
            this.camera5ConnectButton.IsConnect = false;
            this.camera5ConnectButton.Location = new System.Drawing.Point(576, 196);
            this.camera5ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera5ConnectButton.Name = "camera5ConnectButton";
            this.camera5ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera5ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera5ConnectButton.TabIndex = 2;
            this.camera5ConnectButton.Text = " Connect";
            this.camera5ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera5ConnectButton.UseVisualStyleBackColor = false;
            this.camera5ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera6ConnectButton
            // 
            this.camera6ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera6ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera6ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera6ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera6ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera6ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera6ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera6ConnectButton.Image")));
            this.camera6ConnectButton.IsConnect = false;
            this.camera6ConnectButton.Location = new System.Drawing.Point(576, 240);
            this.camera6ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera6ConnectButton.Name = "camera6ConnectButton";
            this.camera6ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera6ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera6ConnectButton.TabIndex = 2;
            this.camera6ConnectButton.Text = " Connect";
            this.camera6ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera6ConnectButton.UseVisualStyleBackColor = false;
            this.camera6ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera7ConnectButton
            // 
            this.camera7ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera7ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera7ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera7ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera7ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera7ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera7ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera7ConnectButton.Image")));
            this.camera7ConnectButton.IsConnect = false;
            this.camera7ConnectButton.Location = new System.Drawing.Point(576, 284);
            this.camera7ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera7ConnectButton.Name = "camera7ConnectButton";
            this.camera7ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera7ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera7ConnectButton.TabIndex = 2;
            this.camera7ConnectButton.Text = " Connect";
            this.camera7ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera7ConnectButton.UseVisualStyleBackColor = false;
            this.camera7ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera8ConnectButton
            // 
            this.camera8ConnectButton.BackColor = System.Drawing.Color.Gray;
            this.camera8ConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera8ConnectButton.FlatAppearance.BorderSize = 0;
            this.camera8ConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera8ConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera8ConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera8ConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("camera8ConnectButton.Image")));
            this.camera8ConnectButton.IsConnect = false;
            this.camera8ConnectButton.Location = new System.Drawing.Point(576, 328);
            this.camera8ConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.camera8ConnectButton.Name = "camera8ConnectButton";
            this.camera8ConnectButton.Size = new System.Drawing.Size(112, 36);
            this.camera8ConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera8ConnectButton.TabIndex = 2;
            this.camera8ConnectButton.Text = " Connect";
            this.camera8ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera8ConnectButton.UseVisualStyleBackColor = false;
            this.camera8ConnectButton.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera1NDICheckBox
            // 
            this.camera1NDICheckBox.AutoSize = true;
            this.camera1NDICheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera1NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera1NDICheckBox.Location = new System.Drawing.Point(696, 20);
            this.camera1NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera1NDICheckBox.Name = "camera1NDICheckBox";
            this.camera1NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera1NDICheckBox.TabIndex = 3;
            this.camera1NDICheckBox.Text = "NDI Output";
            this.camera1NDICheckBox.UseVisualStyleBackColor = false;
            this.camera1NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera2NDICheckBox
            // 
            this.camera2NDICheckBox.AutoSize = true;
            this.camera2NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera2NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera2NDICheckBox.Location = new System.Drawing.Point(696, 64);
            this.camera2NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera2NDICheckBox.Name = "camera2NDICheckBox";
            this.camera2NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera2NDICheckBox.TabIndex = 3;
            this.camera2NDICheckBox.Text = "NDI Output";
            this.camera2NDICheckBox.UseVisualStyleBackColor = true;
            this.camera2NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera3NDICheckBox
            // 
            this.camera3NDICheckBox.AutoSize = true;
            this.camera3NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera3NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera3NDICheckBox.Location = new System.Drawing.Point(696, 108);
            this.camera3NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera3NDICheckBox.Name = "camera3NDICheckBox";
            this.camera3NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera3NDICheckBox.TabIndex = 3;
            this.camera3NDICheckBox.Text = "NDI Output";
            this.camera3NDICheckBox.UseVisualStyleBackColor = true;
            this.camera3NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera4NDICheckBox
            // 
            this.camera4NDICheckBox.AutoSize = true;
            this.camera4NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera4NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera4NDICheckBox.Location = new System.Drawing.Point(696, 152);
            this.camera4NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera4NDICheckBox.Name = "camera4NDICheckBox";
            this.camera4NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera4NDICheckBox.TabIndex = 3;
            this.camera4NDICheckBox.Text = "NDI Output";
            this.camera4NDICheckBox.UseVisualStyleBackColor = true;
            this.camera4NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera5NDICheckBox
            // 
            this.camera5NDICheckBox.AutoSize = true;
            this.camera5NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera5NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera5NDICheckBox.Location = new System.Drawing.Point(696, 196);
            this.camera5NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera5NDICheckBox.Name = "camera5NDICheckBox";
            this.camera5NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera5NDICheckBox.TabIndex = 3;
            this.camera5NDICheckBox.Text = "NDI Output";
            this.camera5NDICheckBox.UseVisualStyleBackColor = true;
            this.camera5NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera6NDICheckBox
            // 
            this.camera6NDICheckBox.AutoSize = true;
            this.camera6NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera6NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera6NDICheckBox.Location = new System.Drawing.Point(696, 240);
            this.camera6NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera6NDICheckBox.Name = "camera6NDICheckBox";
            this.camera6NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera6NDICheckBox.TabIndex = 3;
            this.camera6NDICheckBox.Text = "NDI Output";
            this.camera6NDICheckBox.UseVisualStyleBackColor = true;
            this.camera6NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera7NDICheckBox
            // 
            this.camera7NDICheckBox.AutoSize = true;
            this.camera7NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera7NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera7NDICheckBox.Location = new System.Drawing.Point(696, 284);
            this.camera7NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera7NDICheckBox.Name = "camera7NDICheckBox";
            this.camera7NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera7NDICheckBox.TabIndex = 3;
            this.camera7NDICheckBox.Text = "NDI Output";
            this.camera7NDICheckBox.UseVisualStyleBackColor = true;
            this.camera7NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera8NDICheckBox
            // 
            this.camera8NDICheckBox.AutoSize = true;
            this.camera8NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera8NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera8NDICheckBox.Location = new System.Drawing.Point(696, 328);
            this.camera8NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera8NDICheckBox.Name = "camera8NDICheckBox";
            this.camera8NDICheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera8NDICheckBox.TabIndex = 3;
            this.camera8NDICheckBox.Text = "NDI Output";
            this.camera8NDICheckBox.UseVisualStyleBackColor = true;
            this.camera8NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera1TextBox
            // 
            this.camera1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera1TextBox.BackColor = System.Drawing.Color.Black;
            this.camera1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera1TextBox.ForeColor = System.Drawing.Color.White;
            this.camera1TextBox.Location = new System.Drawing.Point(104, 28);
            this.camera1TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera1TextBox.Name = "camera1TextBox";
            this.camera1TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera1TextBox.TabIndex = 5;
            // 
            // camera2TextBox
            // 
            this.camera2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera2TextBox.BackColor = System.Drawing.Color.Black;
            this.camera2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera2TextBox.ForeColor = System.Drawing.Color.White;
            this.camera2TextBox.Location = new System.Drawing.Point(104, 72);
            this.camera2TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera2TextBox.Name = "camera2TextBox";
            this.camera2TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera2TextBox.TabIndex = 5;
            // 
            // camera3TextBox
            // 
            this.camera3TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera3TextBox.BackColor = System.Drawing.Color.Black;
            this.camera3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera3TextBox.ForeColor = System.Drawing.Color.White;
            this.camera3TextBox.Location = new System.Drawing.Point(104, 116);
            this.camera3TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera3TextBox.Name = "camera3TextBox";
            this.camera3TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera3TextBox.TabIndex = 5;
            // 
            // camera4TextBox
            // 
            this.camera4TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera4TextBox.BackColor = System.Drawing.Color.Black;
            this.camera4TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera4TextBox.ForeColor = System.Drawing.Color.White;
            this.camera4TextBox.Location = new System.Drawing.Point(104, 160);
            this.camera4TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera4TextBox.Name = "camera4TextBox";
            this.camera4TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera4TextBox.TabIndex = 5;
            // 
            // camera5TextBox
            // 
            this.camera5TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera5TextBox.BackColor = System.Drawing.Color.Black;
            this.camera5TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera5TextBox.ForeColor = System.Drawing.Color.White;
            this.camera5TextBox.Location = new System.Drawing.Point(104, 204);
            this.camera5TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera5TextBox.Name = "camera5TextBox";
            this.camera5TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera5TextBox.TabIndex = 5;
            // 
            // camera6TextBox
            // 
            this.camera6TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera6TextBox.BackColor = System.Drawing.Color.Black;
            this.camera6TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera6TextBox.ForeColor = System.Drawing.Color.White;
            this.camera6TextBox.Location = new System.Drawing.Point(104, 248);
            this.camera6TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera6TextBox.Name = "camera6TextBox";
            this.camera6TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera6TextBox.TabIndex = 5;
            // 
            // camera7TextBox
            // 
            this.camera7TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera7TextBox.BackColor = System.Drawing.Color.Black;
            this.camera7TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera7TextBox.ForeColor = System.Drawing.Color.White;
            this.camera7TextBox.Location = new System.Drawing.Point(104, 292);
            this.camera7TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera7TextBox.Name = "camera7TextBox";
            this.camera7TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera7TextBox.TabIndex = 5;
            // 
            // camera8TextBox
            // 
            this.camera8TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera8TextBox.BackColor = System.Drawing.Color.Black;
            this.camera8TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera8TextBox.ForeColor = System.Drawing.Color.White;
            this.camera8TextBox.Location = new System.Drawing.Point(104, 336);
            this.camera8TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera8TextBox.Name = "camera8TextBox";
            this.camera8TextBox.Size = new System.Drawing.Size(464, 20);
            this.camera8TextBox.TabIndex = 5;
            // 
            // stageSourceGroupBox
            // 
            this.stageSourceGroupBox.Controls.Add(this.tableLayoutPanel4);
            this.stageSourceGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageSourceGroupBox.ForeColor = System.Drawing.Color.White;
            this.stageSourceGroupBox.Location = new System.Drawing.Point(13, 429);
            this.stageSourceGroupBox.Margin = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.stageSourceGroupBox.Name = "stageSourceGroupBox";
            this.stageSourceGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.stageSourceGroupBox.Size = new System.Drawing.Size(800, 96);
            this.stageSourceGroupBox.TabIndex = 5;
            this.stageSourceGroupBox.TabStop = false;
            this.stageSourceGroupBox.Text = "Stage Source";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel4.ColumnCount = 9;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel4.Controls.Add(this.liveStageConnectButton, 5, 1);
            this.tableLayoutPanel4.Controls.Add(this.stageBrowseButton, 5, 2);
            this.tableLayoutPanel4.Controls.Add(this.liveStageNDICheckbox, 7, 1);
            this.tableLayoutPanel4.Controls.Add(this.imageStageNDICheckBox, 7, 2);
            this.tableLayoutPanel4.Controls.Add(this.liveTextBox, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.staticTextBox, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.liveRadioButton, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.liveNameTextBox, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.staticRadioButton, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.imageNameTextBox, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 17);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(792, 75);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // liveStageConnectButton
            // 
            this.liveStageConnectButton.BackColor = System.Drawing.Color.Gray;
            this.liveStageConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveStageConnectButton.FlatAppearance.BorderSize = 0;
            this.liveStageConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.liveStageConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.liveStageConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.liveStageConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("liveStageConnectButton.Image")));
            this.liveStageConnectButton.IsConnect = false;
            this.liveStageConnectButton.Location = new System.Drawing.Point(576, 5);
            this.liveStageConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.liveStageConnectButton.Name = "liveStageConnectButton";
            this.liveStageConnectButton.Size = new System.Drawing.Size(112, 28);
            this.liveStageConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.liveStageConnectButton.TabIndex = 2;
            this.liveStageConnectButton.Text = " Connect";
            this.liveStageConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.liveStageConnectButton.UseVisualStyleBackColor = false;
            this.liveStageConnectButton.Click += new System.EventHandler(this.liveStageConnectButton_Click);
            // 
            // stageBrowseButton
            // 
            this.stageBrowseButton.BackColor = System.Drawing.Color.Gray;
            this.stageBrowseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageBrowseButton.FlatAppearance.BorderSize = 0;
            this.stageBrowseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.stageBrowseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.stageBrowseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stageBrowseButton.Location = new System.Drawing.Point(576, 41);
            this.stageBrowseButton.Margin = new System.Windows.Forms.Padding(4);
            this.stageBrowseButton.Name = "stageBrowseButton";
            this.stageBrowseButton.Size = new System.Drawing.Size(112, 28);
            this.stageBrowseButton.TabIndex = 2;
            this.stageBrowseButton.Text = "Browse";
            this.stageBrowseButton.UseVisualStyleBackColor = false;
            this.stageBrowseButton.Click += new System.EventHandler(this.stageBrowseButton_Click);
            // 
            // liveStageNDICheckbox
            // 
            this.liveStageNDICheckbox.AutoSize = true;
            this.liveStageNDICheckbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveStageNDICheckbox.ForeColor = System.Drawing.Color.White;
            this.liveStageNDICheckbox.Location = new System.Drawing.Point(696, 5);
            this.liveStageNDICheckbox.Margin = new System.Windows.Forms.Padding(4);
            this.liveStageNDICheckbox.Name = "liveStageNDICheckbox";
            this.liveStageNDICheckbox.Size = new System.Drawing.Size(92, 28);
            this.liveStageNDICheckbox.TabIndex = 3;
            this.liveStageNDICheckbox.Text = "NDI Output";
            this.liveStageNDICheckbox.UseVisualStyleBackColor = true;
            // 
            // imageStageNDICheckBox
            // 
            this.imageStageNDICheckBox.AutoSize = true;
            this.imageStageNDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageStageNDICheckBox.ForeColor = System.Drawing.Color.White;
            this.imageStageNDICheckBox.Location = new System.Drawing.Point(696, 41);
            this.imageStageNDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.imageStageNDICheckBox.Name = "imageStageNDICheckBox";
            this.imageStageNDICheckBox.Size = new System.Drawing.Size(92, 28);
            this.imageStageNDICheckBox.TabIndex = 3;
            this.imageStageNDICheckBox.Text = "NDI Output";
            this.imageStageNDICheckBox.UseVisualStyleBackColor = true;
            // 
            // liveTextBox
            // 
            this.liveTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveTextBox.BackColor = System.Drawing.Color.Black;
            this.liveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.liveTextBox.ForeColor = System.Drawing.Color.White;
            this.liveTextBox.Location = new System.Drawing.Point(179, 9);
            this.liveTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.liveTextBox.Name = "liveTextBox";
            this.liveTextBox.Size = new System.Drawing.Size(389, 20);
            this.liveTextBox.TabIndex = 5;
            // 
            // staticTextBox
            // 
            this.staticTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.staticTextBox.BackColor = System.Drawing.Color.Black;
            this.staticTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel4.SetColumnSpan(this.staticTextBox, 2);
            this.staticTextBox.ForeColor = System.Drawing.Color.White;
            this.staticTextBox.Location = new System.Drawing.Point(179, 45);
            this.staticTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.staticTextBox.Name = "staticTextBox";
            this.staticTextBox.Size = new System.Drawing.Size(389, 20);
            this.staticTextBox.TabIndex = 5;
            // 
            // liveRadioButton
            // 
            this.liveRadioButton.AutoSize = true;
            this.liveRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveRadioButton.Location = new System.Drawing.Point(104, 5);
            this.liveRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.liveRadioButton.Name = "liveRadioButton";
            this.liveRadioButton.Size = new System.Drawing.Size(67, 28);
            this.liveRadioButton.TabIndex = 6;
            this.liveRadioButton.TabStop = true;
            this.liveRadioButton.Text = "Live";
            this.liveRadioButton.UseVisualStyleBackColor = true;
            this.liveRadioButton.CheckedChanged += new System.EventHandler(this.liveRadioButton_CheckedChanged);
            // 
            // liveNameTextBox
            // 
            this.liveNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveNameTextBox.BackColor = System.Drawing.Color.Black;
            this.liveNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.liveNameTextBox.ForeColor = System.Drawing.Color.White;
            this.liveNameTextBox.Location = new System.Drawing.Point(4, 9);
            this.liveNameTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.liveNameTextBox.Name = "liveNameTextBox";
            this.liveNameTextBox.Size = new System.Drawing.Size(96, 20);
            this.liveNameTextBox.TabIndex = 1;
            this.liveNameTextBox.Text = "Live Camera";
            this.liveNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // staticRadioButton
            // 
            this.staticRadioButton.AutoSize = true;
            this.staticRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.staticRadioButton.Location = new System.Drawing.Point(104, 41);
            this.staticRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.staticRadioButton.Name = "staticRadioButton";
            this.staticRadioButton.Size = new System.Drawing.Size(67, 28);
            this.staticRadioButton.TabIndex = 6;
            this.staticRadioButton.TabStop = true;
            this.staticRadioButton.Text = "Static";
            this.staticRadioButton.UseVisualStyleBackColor = true;
            // 
            // imageNameTextBox
            // 
            this.imageNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.imageNameTextBox.BackColor = System.Drawing.Color.Black;
            this.imageNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageNameTextBox.ForeColor = System.Drawing.Color.White;
            this.imageNameTextBox.Location = new System.Drawing.Point(4, 45);
            this.imageNameTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.imageNameTextBox.Name = "imageNameTextBox";
            this.imageNameTextBox.Size = new System.Drawing.Size(96, 20);
            this.imageNameTextBox.TabIndex = 1;
            this.imageNameTextBox.Text = "Static Image";
            this.imageNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.28342F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.71658F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.5508F));
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.saveButton, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 541);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(818, 41);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Gray;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(409, 4);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(85, 33);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.Gray;
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(260, 4);
            this.saveButton.Margin = new System.Windows.Forms.Padding(4);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(83, 33);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // camera1NDIInputCheckBox
            // 
            this.camera1NDIInputCheckBox.AutoSize = true;
            this.camera1NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera1NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1NDIInputCheckBox.Enabled = false;
            this.camera1NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera1NDIInputCheckBox.Location = new System.Drawing.Point(596, 20);
            this.camera1NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera1NDIInputCheckBox.Name = "camera1NDIInputCheckBox";
            this.camera1NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera1NDIInputCheckBox.TabIndex = 3;
            this.camera1NDIInputCheckBox.Text = "NDI Input";
            this.camera1NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera1NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.cameraNDIInputCheckBox_CheckedChanged);
            // 
            // camera2NDIInputCheckBox
            // 
            this.camera2NDIInputCheckBox.AutoSize = true;
            this.camera2NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera2NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera2NDIInputCheckBox.Enabled = false;
            this.camera2NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera2NDIInputCheckBox.Location = new System.Drawing.Point(596, 64);
            this.camera2NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera2NDIInputCheckBox.Name = "camera2NDIInputCheckBox";
            this.camera2NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera2NDIInputCheckBox.TabIndex = 3;
            this.camera2NDIInputCheckBox.Text = "NDI Input";
            this.camera2NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera2NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.cameraNDIInputCheckBox_CheckedChanged);
            // 
            // camera3NDIInputCheckBox
            // 
            this.camera3NDIInputCheckBox.AutoSize = true;
            this.camera3NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera3NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera3NDIInputCheckBox.Enabled = false;
            this.camera3NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera3NDIInputCheckBox.Location = new System.Drawing.Point(596, 108);
            this.camera3NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera3NDIInputCheckBox.Name = "camera3NDIInputCheckBox";
            this.camera3NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera3NDIInputCheckBox.TabIndex = 3;
            this.camera3NDIInputCheckBox.Text = "NDI Input";
            this.camera3NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera3NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.cameraNDIInputCheckBox_CheckedChanged);
            // 
            // camera4NDIInputCheckBox
            // 
            this.camera4NDIInputCheckBox.AutoSize = true;
            this.camera4NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera4NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera4NDIInputCheckBox.Enabled = false;
            this.camera4NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera4NDIInputCheckBox.Location = new System.Drawing.Point(596, 152);
            this.camera4NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera4NDIInputCheckBox.Name = "camera4NDIInputCheckBox";
            this.camera4NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera4NDIInputCheckBox.TabIndex = 3;
            this.camera4NDIInputCheckBox.Text = "NDI Input";
            this.camera4NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera4NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // camera5NDIInputCheckBox
            // 
            this.camera5NDIInputCheckBox.AutoSize = true;
            this.camera5NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera5NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera5NDIInputCheckBox.Enabled = false;
            this.camera5NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera5NDIInputCheckBox.Location = new System.Drawing.Point(596, 196);
            this.camera5NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera5NDIInputCheckBox.Name = "camera5NDIInputCheckBox";
            this.camera5NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera5NDIInputCheckBox.TabIndex = 3;
            this.camera5NDIInputCheckBox.Text = "NDI Input";
            this.camera5NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera5NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.cameraNDIInputCheckBox_CheckedChanged);
            // 
            // camera6NDIInputCheckBox
            // 
            this.camera6NDIInputCheckBox.AutoSize = true;
            this.camera6NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera6NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera6NDIInputCheckBox.Enabled = false;
            this.camera6NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera6NDIInputCheckBox.Location = new System.Drawing.Point(596, 240);
            this.camera6NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera6NDIInputCheckBox.Name = "camera6NDIInputCheckBox";
            this.camera6NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera6NDIInputCheckBox.TabIndex = 3;
            this.camera6NDIInputCheckBox.Text = "NDI Input";
            this.camera6NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera6NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.cameraNDIInputCheckBox_CheckedChanged);
            // 
            // camera7NDIInputCheckBox
            // 
            this.camera7NDIInputCheckBox.AutoSize = true;
            this.camera7NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera7NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera7NDIInputCheckBox.Enabled = false;
            this.camera7NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera7NDIInputCheckBox.Location = new System.Drawing.Point(596, 284);
            this.camera7NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera7NDIInputCheckBox.Name = "camera7NDIInputCheckBox";
            this.camera7NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera7NDIInputCheckBox.TabIndex = 3;
            this.camera7NDIInputCheckBox.Text = "NDI Input";
            this.camera7NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera7NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.cameraNDIInputCheckBox_CheckedChanged);
            // 
            // camera8NDIInputCheckBox
            // 
            this.camera8NDIInputCheckBox.AutoSize = true;
            this.camera8NDIInputCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera8NDIInputCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera8NDIInputCheckBox.Enabled = false;
            this.camera8NDIInputCheckBox.ForeColor = System.Drawing.Color.White;
            this.camera8NDIInputCheckBox.Location = new System.Drawing.Point(596, 328);
            this.camera8NDIInputCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera8NDIInputCheckBox.Name = "camera8NDIInputCheckBox";
            this.camera8NDIInputCheckBox.Size = new System.Drawing.Size(92, 36);
            this.camera8NDIInputCheckBox.TabIndex = 3;
            this.camera8NDIInputCheckBox.Text = "NDI Input";
            this.camera8NDIInputCheckBox.UseVisualStyleBackColor = false;
            this.camera8NDIInputCheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // myComboBox1
            // 
            this.myComboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox1.BackColor = System.Drawing.Color.Black;
            this.myComboBox1.Enabled = false;
            this.myComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox1.ForeColor = System.Drawing.Color.White;
            this.myComboBox1.FormattingEnabled = true;
            this.myComboBox1.Location = new System.Drawing.Point(289, 27);
            this.myComboBox1.Name = "myComboBox1";
            this.myComboBox1.Size = new System.Drawing.Size(180, 21);
            this.myComboBox1.TabIndex = 6;
            // 
            // myComboBox2
            // 
            this.myComboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox2.BackColor = System.Drawing.Color.Black;
            this.myComboBox2.Enabled = false;
            this.myComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox2.ForeColor = System.Drawing.Color.White;
            this.myComboBox2.FormattingEnabled = true;
            this.myComboBox2.Location = new System.Drawing.Point(289, 71);
            this.myComboBox2.Name = "myComboBox2";
            this.myComboBox2.Size = new System.Drawing.Size(180, 21);
            this.myComboBox2.TabIndex = 6;
            // 
            // myComboBox3
            // 
            this.myComboBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox3.BackColor = System.Drawing.Color.Black;
            this.myComboBox3.Enabled = false;
            this.myComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox3.ForeColor = System.Drawing.Color.White;
            this.myComboBox3.FormattingEnabled = true;
            this.myComboBox3.Location = new System.Drawing.Point(289, 115);
            this.myComboBox3.Name = "myComboBox3";
            this.myComboBox3.Size = new System.Drawing.Size(180, 21);
            this.myComboBox3.TabIndex = 6;
            // 
            // myComboBox4
            // 
            this.myComboBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox4.BackColor = System.Drawing.Color.Black;
            this.myComboBox4.Enabled = false;
            this.myComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox4.ForeColor = System.Drawing.Color.White;
            this.myComboBox4.FormattingEnabled = true;
            this.myComboBox4.Location = new System.Drawing.Point(289, 159);
            this.myComboBox4.Name = "myComboBox4";
            this.myComboBox4.Size = new System.Drawing.Size(180, 21);
            this.myComboBox4.TabIndex = 6;
            // 
            // myComboBox5
            // 
            this.myComboBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox5.BackColor = System.Drawing.Color.Black;
            this.myComboBox5.Enabled = false;
            this.myComboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox5.ForeColor = System.Drawing.Color.White;
            this.myComboBox5.FormattingEnabled = true;
            this.myComboBox5.Location = new System.Drawing.Point(289, 203);
            this.myComboBox5.Name = "myComboBox5";
            this.myComboBox5.Size = new System.Drawing.Size(180, 21);
            this.myComboBox5.TabIndex = 6;
            // 
            // myComboBox6
            // 
            this.myComboBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox6.BackColor = System.Drawing.Color.Black;
            this.myComboBox6.Enabled = false;
            this.myComboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox6.ForeColor = System.Drawing.Color.White;
            this.myComboBox6.FormattingEnabled = true;
            this.myComboBox6.Location = new System.Drawing.Point(289, 247);
            this.myComboBox6.Name = "myComboBox6";
            this.myComboBox6.Size = new System.Drawing.Size(180, 21);
            this.myComboBox6.TabIndex = 6;
            // 
            // myComboBox7
            // 
            this.myComboBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox7.BackColor = System.Drawing.Color.Black;
            this.myComboBox7.Enabled = false;
            this.myComboBox7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox7.ForeColor = System.Drawing.Color.White;
            this.myComboBox7.FormattingEnabled = true;
            this.myComboBox7.Location = new System.Drawing.Point(289, 291);
            this.myComboBox7.Name = "myComboBox7";
            this.myComboBox7.Size = new System.Drawing.Size(180, 21);
            this.myComboBox7.TabIndex = 6;
            // 
            // myComboBox8
            // 
            this.myComboBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox8.BackColor = System.Drawing.Color.Black;
            this.myComboBox8.Enabled = false;
            this.myComboBox8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox8.ForeColor = System.Drawing.Color.White;
            this.myComboBox8.FormattingEnabled = true;
            this.myComboBox8.Location = new System.Drawing.Point(289, 335);
            this.myComboBox8.Name = "myComboBox8";
            this.myComboBox8.Size = new System.Drawing.Size(180, 21);
            this.myComboBox8.TabIndex = 6;
            // 
            // liveStageNDIInputCheckbox
            // 
            this.liveStageNDIInputCheckbox.AutoSize = true;
            this.liveStageNDIInputCheckbox.BackColor = System.Drawing.Color.Transparent;
            this.liveStageNDIInputCheckbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveStageNDIInputCheckbox.Enabled = false;
            this.liveStageNDIInputCheckbox.ForeColor = System.Drawing.Color.White;
            this.liveStageNDIInputCheckbox.Location = new System.Drawing.Point(595, 5);
            this.liveStageNDIInputCheckbox.Margin = new System.Windows.Forms.Padding(4);
            this.liveStageNDIInputCheckbox.Name = "liveStageNDIInputCheckbox";
            this.liveStageNDIInputCheckbox.Size = new System.Drawing.Size(92, 28);
            this.liveStageNDIInputCheckbox.TabIndex = 3;
            this.liveStageNDIInputCheckbox.Text = "NDI Input";
            this.liveStageNDIInputCheckbox.UseVisualStyleBackColor = false;
            this.liveStageNDIInputCheckbox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // liveNDINameComboBox
            // 
            this.liveNDINameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveNDINameComboBox.BackColor = System.Drawing.Color.Black;
            this.liveNDINameComboBox.Enabled = false;
            this.liveNDINameComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.liveNDINameComboBox.ForeColor = System.Drawing.Color.White;
            this.liveNDINameComboBox.FormattingEnabled = true;
            this.liveNDINameComboBox.Location = new System.Drawing.Point(326, 8);
            this.liveNDINameComboBox.Name = "liveNDINameComboBox";
            this.liveNDINameComboBox.Size = new System.Drawing.Size(142, 21);
            this.liveNDINameComboBox.TabIndex = 6;
            // 
            // CameraSetupUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.cameraSetupTableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CameraSetupUserControl";
            this.Size = new System.Drawing.Size(826, 687);
            this.cameraSetupTableLayoutPanel.ResumeLayout(false);
            this.ptzCameraGroupBox.ResumeLayout(false);
            this.cameraInfoTableLayoutPanel.ResumeLayout(false);
            this.cameraInfoTableLayoutPanel.PerformLayout();
            this.stageSourceGroupBox.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel cameraSetupTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel cameraInfoTableLayoutPanel;
        private RoundedTextBox cameraName1TextBox;
        private RoundedTextBox cameraName2TextBox;
        private RoundedTextBox cameraName3TextBox;
        private RoundedTextBox cameraName4TextBox;
        private RoundedTextBox cameraName5TextBox;
        private RoundedTextBox cameraName6TextBox;
        private RoundedTextBox cameraName7TextBox;
        private RoundedTextBox cameraName8TextBox;
        private MyToggleButton camera1ConnectButton;
        private MyToggleButton camera2ConnectButton;
        private MyToggleButton camera3ConnectButton;
        private MyToggleButton camera4ConnectButton;
        private MyToggleButton camera5ConnectButton;
        private MyToggleButton camera6ConnectButton;
        private MyToggleButton camera7ConnectButton;
        private MyToggleButton camera8ConnectButton;
        private MyCheckBox camera1NDICheckBox;
        private MyCheckBox camera2NDICheckBox;
        private MyCheckBox camera3NDICheckBox;
        private MyCheckBox camera4NDICheckBox;
        private MyCheckBox camera5NDICheckBox;
        private MyCheckBox camera6NDICheckBox;
        private MyCheckBox camera7NDICheckBox;
        private MyCheckBox camera8NDICheckBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MyToggleButton liveStageConnectButton;
        private RoundedButton stageBrowseButton;
        private MyCheckBox liveStageNDICheckbox;
        private MyCheckBox imageStageNDICheckBox;
        private RoundedTextBox camera1TextBox;
        private RoundedTextBox camera2TextBox;
        private RoundedTextBox camera3TextBox;
        private RoundedTextBox camera4TextBox;
        private RoundedTextBox camera5TextBox;
        private RoundedTextBox camera6TextBox;
        private RoundedTextBox camera7TextBox;
        private RoundedTextBox camera8TextBox;
        private RoundedTextBox liveTextBox;
        private RoundedTextBox staticTextBox;
        private MyGroupBox ptzCameraGroupBox;
        private MyGroupBox stageSourceGroupBox;
        private MyRadioButton liveRadioButton;
        private MyRadioButton staticRadioButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private RoundedButton cancelButton;
        private RoundedButton saveButton;
        private RoundedTextBox liveNameTextBox;
        private RoundedTextBox imageNameTextBox;
        private MyCheckBox camera1NDIInputCheckBox;
        private MyCheckBox camera2NDIInputCheckBox;
        private MyCheckBox camera3NDIInputCheckBox;
        private MyCheckBox camera4NDIInputCheckBox;
        private MyCheckBox camera5NDIInputCheckBox;
        private MyCheckBox camera6NDIInputCheckBox;
        private MyCheckBox camera7NDIInputCheckBox;
        private MyCheckBox camera8NDIInputCheckBox;
        private MyCheckBox liveStageNDIInputCheckbox;
        private MyComboBox myComboBox1;
        private MyComboBox myComboBox2;
        private MyComboBox myComboBox3;
        private MyComboBox myComboBox4;
        private MyComboBox myComboBox5;
        private MyComboBox myComboBox6;
        private MyComboBox myComboBox7;
        private MyComboBox myComboBox8;
        private MyComboBox liveNDINameComboBox;
    }
}
