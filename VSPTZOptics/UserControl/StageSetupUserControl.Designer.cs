﻿using CameraUserControl.Model;

namespace CameraUserControl
{
    partial class StageSetupUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.stageButtonTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.roundedButton1 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton2 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton3 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton4 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton5 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton6 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton7 = new CameraUserControl.Model.RoundedButton();
            this.roundedButton8 = new CameraUserControl.Model.RoundedButton();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.closeButton = new CameraUserControl.Model.RoundedButton();
            this.stageSetupTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName1Label = new CameraUserControl.Model.MyLabel();
            this.label2 = new CameraUserControl.Model.MyLabel();
            this.camera1ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName5Label = new CameraUserControl.Model.MyLabel();
            this.label4 = new CameraUserControl.Model.MyLabel();
            this.camera5ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName2Label = new CameraUserControl.Model.MyLabel();
            this.label6 = new CameraUserControl.Model.MyLabel();
            this.camera2ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName6Label = new CameraUserControl.Model.MyLabel();
            this.label8 = new CameraUserControl.Model.MyLabel();
            this.camera6ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName3Label = new CameraUserControl.Model.MyLabel();
            this.label10 = new CameraUserControl.Model.MyLabel();
            this.camera3ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName7Label = new CameraUserControl.Model.MyLabel();
            this.label12 = new CameraUserControl.Model.MyLabel();
            this.camera7ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName4Label = new CameraUserControl.Model.MyLabel();
            this.label14 = new CameraUserControl.Model.MyLabel();
            this.camera4ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraName8Label = new CameraUserControl.Model.MyLabel();
            this.label16 = new CameraUserControl.Model.MyLabel();
            this.camera8ComboBox = new CameraUserControl.Model.MyComboBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.testButton = new CameraUserControl.Model.RoundedButton();
            this.saveButton = new CameraUserControl.Model.RoundedButton();
            this.label17 = new CameraUserControl.Model.MyLabel();
            this.httpTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.showStageCheckBox = new CameraUserControl.Model.MyCheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.myLabel1 = new CameraUserControl.Model.MyLabel();
            this.spNameTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.stageButtonTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.stageSetupTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.stageButtonTableLayoutPanel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.stageSetupTableLayoutPanel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel13, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 324F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(473, 484);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // stageButtonTableLayoutPanel
            // 
            this.stageButtonTableLayoutPanel.ColumnCount = 10;
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.stageButtonTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton1, 1, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton2, 2, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton3, 3, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton4, 4, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton5, 5, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton6, 6, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton7, 7, 0);
            this.stageButtonTableLayoutPanel.Controls.Add(this.roundedButton8, 8, 0);
            this.stageButtonTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageButtonTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.stageButtonTableLayoutPanel.Name = "stageButtonTableLayoutPanel";
            this.stageButtonTableLayoutPanel.RowCount = 1;
            this.stageButtonTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stageButtonTableLayoutPanel.Size = new System.Drawing.Size(467, 34);
            this.stageButtonTableLayoutPanel.TabIndex = 0;
            // 
            // roundedButton1
            // 
            this.roundedButton1.BackColor = System.Drawing.Color.Gray;
            this.roundedButton1.FlatAppearance.BorderSize = 0;
            this.roundedButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton1.ForeColor = System.Drawing.Color.White;
            this.roundedButton1.Location = new System.Drawing.Point(76, 3);
            this.roundedButton1.Name = "roundedButton1";
            this.roundedButton1.Size = new System.Drawing.Size(34, 28);
            this.roundedButton1.TabIndex = 6;
            this.roundedButton1.Text = "1";
            this.roundedButton1.UseVisualStyleBackColor = false;
            this.roundedButton1.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton2
            // 
            this.roundedButton2.BackColor = System.Drawing.Color.Gray;
            this.roundedButton2.FlatAppearance.BorderSize = 0;
            this.roundedButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton2.ForeColor = System.Drawing.Color.White;
            this.roundedButton2.Location = new System.Drawing.Point(116, 3);
            this.roundedButton2.Name = "roundedButton2";
            this.roundedButton2.Size = new System.Drawing.Size(34, 28);
            this.roundedButton2.TabIndex = 6;
            this.roundedButton2.Text = "2";
            this.roundedButton2.UseVisualStyleBackColor = false;
            this.roundedButton2.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton3
            // 
            this.roundedButton3.BackColor = System.Drawing.Color.Gray;
            this.roundedButton3.FlatAppearance.BorderSize = 0;
            this.roundedButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton3.ForeColor = System.Drawing.Color.White;
            this.roundedButton3.Location = new System.Drawing.Point(156, 3);
            this.roundedButton3.Name = "roundedButton3";
            this.roundedButton3.Size = new System.Drawing.Size(34, 28);
            this.roundedButton3.TabIndex = 6;
            this.roundedButton3.Text = "3";
            this.roundedButton3.UseVisualStyleBackColor = false;
            this.roundedButton3.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton4
            // 
            this.roundedButton4.BackColor = System.Drawing.Color.Gray;
            this.roundedButton4.FlatAppearance.BorderSize = 0;
            this.roundedButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton4.ForeColor = System.Drawing.Color.White;
            this.roundedButton4.Location = new System.Drawing.Point(196, 3);
            this.roundedButton4.Name = "roundedButton4";
            this.roundedButton4.Size = new System.Drawing.Size(34, 28);
            this.roundedButton4.TabIndex = 6;
            this.roundedButton4.Text = "4";
            this.roundedButton4.UseVisualStyleBackColor = false;
            this.roundedButton4.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton5
            // 
            this.roundedButton5.BackColor = System.Drawing.Color.Gray;
            this.roundedButton5.FlatAppearance.BorderSize = 0;
            this.roundedButton5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton5.ForeColor = System.Drawing.Color.White;
            this.roundedButton5.Location = new System.Drawing.Point(236, 3);
            this.roundedButton5.Name = "roundedButton5";
            this.roundedButton5.Size = new System.Drawing.Size(34, 28);
            this.roundedButton5.TabIndex = 6;
            this.roundedButton5.Text = "5";
            this.roundedButton5.UseVisualStyleBackColor = false;
            this.roundedButton5.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton6
            // 
            this.roundedButton6.BackColor = System.Drawing.Color.Gray;
            this.roundedButton6.FlatAppearance.BorderSize = 0;
            this.roundedButton6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton6.ForeColor = System.Drawing.Color.White;
            this.roundedButton6.Location = new System.Drawing.Point(276, 3);
            this.roundedButton6.Name = "roundedButton6";
            this.roundedButton6.Size = new System.Drawing.Size(34, 28);
            this.roundedButton6.TabIndex = 6;
            this.roundedButton6.Text = "6";
            this.roundedButton6.UseVisualStyleBackColor = false;
            this.roundedButton6.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton7
            // 
            this.roundedButton7.BackColor = System.Drawing.Color.Gray;
            this.roundedButton7.FlatAppearance.BorderSize = 0;
            this.roundedButton7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton7.ForeColor = System.Drawing.Color.White;
            this.roundedButton7.Location = new System.Drawing.Point(316, 3);
            this.roundedButton7.Name = "roundedButton7";
            this.roundedButton7.Size = new System.Drawing.Size(34, 28);
            this.roundedButton7.TabIndex = 6;
            this.roundedButton7.Text = "7";
            this.roundedButton7.UseVisualStyleBackColor = false;
            this.roundedButton7.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // roundedButton8
            // 
            this.roundedButton8.BackColor = System.Drawing.Color.Gray;
            this.roundedButton8.FlatAppearance.BorderSize = 0;
            this.roundedButton8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.roundedButton8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.roundedButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundedButton8.ForeColor = System.Drawing.Color.White;
            this.roundedButton8.Location = new System.Drawing.Point(356, 3);
            this.roundedButton8.Name = "roundedButton8";
            this.roundedButton8.Size = new System.Drawing.Size(34, 28);
            this.roundedButton8.TabIndex = 6;
            this.roundedButton8.Text = "8";
            this.roundedButton8.UseVisualStyleBackColor = false;
            this.roundedButton8.Click += new System.EventHandler(this.stageButton_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.closeButton, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 442);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(473, 35);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Gray;
            this.closeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.closeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.ForeColor = System.Drawing.Color.White;
            this.closeButton.Location = new System.Drawing.Point(204, 3);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(64, 29);
            this.closeButton.TabIndex = 6;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // stageSetupTableLayoutPanel
            // 
            this.stageSetupTableLayoutPanel.ColumnCount = 4;
            this.stageSetupTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.stageSetupTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.stageSetupTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.stageSetupTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel6, 3, 2);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel2, 3, 0);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel1, 1, 0);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel3, 1, 1);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel4, 3, 1);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel5, 1, 2);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel7, 1, 3);
            this.stageSetupTableLayoutPanel.Controls.Add(this.panel8, 3, 3);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel6, 2, 0);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel8, 2, 1);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel9, 0, 2);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel10, 2, 2);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel11, 0, 3);
            this.stageSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel12, 2, 3);
            this.stageSetupTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageSetupTableLayoutPanel.Location = new System.Drawing.Point(3, 83);
            this.stageSetupTableLayoutPanel.Name = "stageSetupTableLayoutPanel";
            this.stageSetupTableLayoutPanel.RowCount = 4;
            this.stageSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.stageSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.stageSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.stageSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.stageSetupTableLayoutPanel.Size = new System.Drawing.Size(467, 318);
            this.stageSetupTableLayoutPanel.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Black;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(349, 161);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(115, 73);
            this.panel6.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(349, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(115, 73);
            this.panel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(116, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(114, 73);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(116, 82);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(114, 73);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(349, 82);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(115, 73);
            this.panel4.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(116, 161);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(114, 73);
            this.panel5.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(116, 240);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(114, 75);
            this.panel7.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Black;
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(349, 240);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(115, 75);
            this.panel8.TabIndex = 6;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.cameraName1Label, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.camera1ComboBox, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(107, 73);
            this.tableLayoutPanel5.TabIndex = 7;
            // 
            // cameraName1Label
            // 
            this.cameraName1Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cameraName1Label.AutoSize = true;
            this.cameraName1Label.ForeColor = System.Drawing.Color.White;
            this.cameraName1Label.Location = new System.Drawing.Point(3, 5);
            this.cameraName1Label.Name = "cameraName1Label";
            this.cameraName1Label.Size = new System.Drawing.Size(59, 13);
            this.cameraName1Label.TabIndex = 0;
            this.cameraName1Label.Text = "Camera #1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "(PTZOptics Camera)";
            // 
            // camera1ComboBox
            // 
            this.camera1ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera1ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera1ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera1ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera1ComboBox.FormattingEnabled = true;
            this.camera1ComboBox.Location = new System.Drawing.Point(3, 51);
            this.camera1ComboBox.Name = "camera1ComboBox";
            this.camera1ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera1ComboBox.TabIndex = 1;
            this.camera1ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera1ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.cameraName5Label, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.camera5ComboBox, 0, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(236, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(107, 73);
            this.tableLayoutPanel6.TabIndex = 7;
            // 
            // cameraName5Label
            // 
            this.cameraName5Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cameraName5Label.AutoSize = true;
            this.cameraName5Label.ForeColor = System.Drawing.Color.White;
            this.cameraName5Label.Location = new System.Drawing.Point(3, 5);
            this.cameraName5Label.Name = "cameraName5Label";
            this.cameraName5Label.Size = new System.Drawing.Size(59, 13);
            this.cameraName5Label.TabIndex = 0;
            this.cameraName5Label.Text = "Camera #5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "(PTZOptics Camera)";
            // 
            // camera5ComboBox
            // 
            this.camera5ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera5ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera5ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera5ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera5ComboBox.FormattingEnabled = true;
            this.camera5ComboBox.Location = new System.Drawing.Point(3, 51);
            this.camera5ComboBox.Name = "camera5ComboBox";
            this.camera5ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera5ComboBox.TabIndex = 1;
            this.camera5ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera5ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.cameraName2Label, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.camera2ComboBox, 0, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 82);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(107, 73);
            this.tableLayoutPanel7.TabIndex = 7;
            // 
            // cameraName2Label
            // 
            this.cameraName2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cameraName2Label.AutoSize = true;
            this.cameraName2Label.ForeColor = System.Drawing.Color.White;
            this.cameraName2Label.Location = new System.Drawing.Point(3, 5);
            this.cameraName2Label.Name = "cameraName2Label";
            this.cameraName2Label.Size = new System.Drawing.Size(59, 13);
            this.cameraName2Label.TabIndex = 0;
            this.cameraName2Label.Text = "Camera #2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "(PTZOptics Camera)";
            // 
            // camera2ComboBox
            // 
            this.camera2ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera2ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera2ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera2ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera2ComboBox.FormattingEnabled = true;
            this.camera2ComboBox.Location = new System.Drawing.Point(3, 51);
            this.camera2ComboBox.Name = "camera2ComboBox";
            this.camera2ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera2ComboBox.TabIndex = 1;
            this.camera2ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera2ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.cameraName6Label, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.camera6ComboBox, 0, 2);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(236, 82);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(107, 73);
            this.tableLayoutPanel8.TabIndex = 7;
            // 
            // cameraName6Label
            // 
            this.cameraName6Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cameraName6Label.AutoSize = true;
            this.cameraName6Label.ForeColor = System.Drawing.Color.White;
            this.cameraName6Label.Location = new System.Drawing.Point(3, 5);
            this.cameraName6Label.Name = "cameraName6Label";
            this.cameraName6Label.Size = new System.Drawing.Size(59, 13);
            this.cameraName6Label.TabIndex = 0;
            this.cameraName6Label.Text = "Camera #6";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(3, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "(PTZOptics Camera)";
            // 
            // camera6ComboBox
            // 
            this.camera6ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera6ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera6ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera6ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera6ComboBox.FormattingEnabled = true;
            this.camera6ComboBox.Location = new System.Drawing.Point(3, 51);
            this.camera6ComboBox.Name = "camera6ComboBox";
            this.camera6ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera6ComboBox.TabIndex = 1;
            this.camera6ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera6ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Controls.Add(this.cameraName3Label, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.camera3ComboBox, 0, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 161);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(107, 73);
            this.tableLayoutPanel9.TabIndex = 7;
            // 
            // cameraName3Label
            // 
            this.cameraName3Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cameraName3Label.AutoSize = true;
            this.cameraName3Label.ForeColor = System.Drawing.Color.White;
            this.cameraName3Label.Location = new System.Drawing.Point(3, 5);
            this.cameraName3Label.Name = "cameraName3Label";
            this.cameraName3Label.Size = new System.Drawing.Size(59, 13);
            this.cameraName3Label.TabIndex = 0;
            this.cameraName3Label.Text = "Camera #3";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(3, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 24);
            this.label10.TabIndex = 0;
            this.label10.Text = "(PTZOptics Camera)";
            // 
            // camera3ComboBox
            // 
            this.camera3ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera3ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera3ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera3ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera3ComboBox.FormattingEnabled = true;
            this.camera3ComboBox.Location = new System.Drawing.Point(3, 51);
            this.camera3ComboBox.Name = "camera3ComboBox";
            this.camera3ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera3ComboBox.TabIndex = 1;
            this.camera3ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera3ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.cameraName7Label, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.camera7ComboBox, 0, 2);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(236, 161);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(107, 73);
            this.tableLayoutPanel10.TabIndex = 7;
            // 
            // cameraName7Label
            // 
            this.cameraName7Label.AutoSize = true;
            this.cameraName7Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraName7Label.ForeColor = System.Drawing.Color.White;
            this.cameraName7Label.Location = new System.Drawing.Point(3, 0);
            this.cameraName7Label.Name = "cameraName7Label";
            this.cameraName7Label.Size = new System.Drawing.Size(101, 24);
            this.cameraName7Label.TabIndex = 0;
            this.cameraName7Label.Text = "Camera #7";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(3, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "(PTZOptics Camera)";
            // 
            // camera7ComboBox
            // 
            this.camera7ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera7ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera7ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera7ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera7ComboBox.FormattingEnabled = true;
            this.camera7ComboBox.Location = new System.Drawing.Point(3, 51);
            this.camera7ComboBox.Name = "camera7ComboBox";
            this.camera7ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera7ComboBox.TabIndex = 1;
            this.camera7ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera7ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Controls.Add(this.cameraName4Label, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.camera4ComboBox, 0, 2);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 240);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(107, 75);
            this.tableLayoutPanel11.TabIndex = 7;
            // 
            // cameraName4Label
            // 
            this.cameraName4Label.AutoSize = true;
            this.cameraName4Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraName4Label.ForeColor = System.Drawing.Color.White;
            this.cameraName4Label.Location = new System.Drawing.Point(3, 0);
            this.cameraName4Label.Name = "cameraName4Label";
            this.cameraName4Label.Size = new System.Drawing.Size(101, 25);
            this.cameraName4Label.TabIndex = 0;
            this.cameraName4Label.Text = "Camera #4";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 25);
            this.label14.TabIndex = 0;
            this.label14.Text = "(PTZOptics Camera)";
            // 
            // camera4ComboBox
            // 
            this.camera4ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera4ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera4ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera4ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera4ComboBox.FormattingEnabled = true;
            this.camera4ComboBox.Location = new System.Drawing.Point(3, 53);
            this.camera4ComboBox.Name = "camera4ComboBox";
            this.camera4ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera4ComboBox.TabIndex = 1;
            this.camera4ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera4ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.Controls.Add(this.cameraName8Label, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.camera8ComboBox, 0, 2);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(236, 240);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 3;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(107, 75);
            this.tableLayoutPanel12.TabIndex = 7;
            // 
            // cameraName8Label
            // 
            this.cameraName8Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cameraName8Label.AutoSize = true;
            this.cameraName8Label.ForeColor = System.Drawing.Color.White;
            this.cameraName8Label.Location = new System.Drawing.Point(3, 6);
            this.cameraName8Label.Name = "cameraName8Label";
            this.cameraName8Label.Size = new System.Drawing.Size(59, 13);
            this.cameraName8Label.TabIndex = 0;
            this.cameraName8Label.Text = "Camera #8";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(3, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 25);
            this.label16.TabIndex = 0;
            this.label16.Text = "(PTZOptics Camera)";
            // 
            // camera8ComboBox
            // 
            this.camera8ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera8ComboBox.BackColor = System.Drawing.Color.Black;
            this.camera8ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera8ComboBox.ForeColor = System.Drawing.Color.White;
            this.camera8ComboBox.FormattingEnabled = true;
            this.camera8ComboBox.Location = new System.Drawing.Point(3, 53);
            this.camera8ComboBox.Name = "camera8ComboBox";
            this.camera8ComboBox.Size = new System.Drawing.Size(101, 21);
            this.camera8ComboBox.TabIndex = 1;
            this.camera8ComboBox.SelectedIndexChanged += new System.EventHandler(this.camera8ComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 5;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel13.Controls.Add(this.testButton, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.saveButton, 3, 0);
            this.tableLayoutPanel13.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.httpTextBox, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.showStageCheckBox, 4, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 407);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(473, 32);
            this.tableLayoutPanel13.TabIndex = 4;
            // 
            // testButton
            // 
            this.testButton.BackColor = System.Drawing.Color.Gray;
            this.testButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testButton.FlatAppearance.BorderSize = 0;
            this.testButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.testButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.testButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testButton.ForeColor = System.Drawing.Color.White;
            this.testButton.Location = new System.Drawing.Point(203, 3);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(44, 26);
            this.testButton.TabIndex = 7;
            this.testButton.Text = "Test";
            this.testButton.UseVisualStyleBackColor = false;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.Gray;
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(253, 3);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(64, 26);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(3, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "HTTP Trigger";
            // 
            // httpTextBox
            // 
            this.httpTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.httpTextBox.BackColor = System.Drawing.Color.Black;
            this.httpTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.httpTextBox.ForeColor = System.Drawing.Color.White;
            this.httpTextBox.Location = new System.Drawing.Point(83, 6);
            this.httpTextBox.Name = "httpTextBox";
            this.httpTextBox.Size = new System.Drawing.Size(114, 20);
            this.httpTextBox.TabIndex = 1;
            // 
            // showStageCheckBox
            // 
            this.showStageCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.showStageCheckBox.AutoSize = true;
            this.showStageCheckBox.ForeColor = System.Drawing.Color.White;
            this.showStageCheckBox.Location = new System.Drawing.Point(333, 7);
            this.showStageCheckBox.Name = "showStageCheckBox";
            this.showStageCheckBox.Size = new System.Drawing.Size(137, 17);
            this.showStageCheckBox.TabIndex = 8;
            this.showStageCheckBox.Text = "Show on stage view";
            this.showStageCheckBox.UseVisualStyleBackColor = true;
            this.showStageCheckBox.CheckedChanged += new System.EventHandler(this.showStageCheckBox_CheckedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.myLabel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.spNameTextBox, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(467, 34);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // myLabel1
            // 
            this.myLabel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.myLabel1.AutoSize = true;
            this.myLabel1.ForeColor = System.Drawing.Color.White;
            this.myLabel1.Location = new System.Drawing.Point(8, 10);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(99, 13);
            this.myLabel1.TabIndex = 0;
            this.myLabel1.Text = "Stage Preset Name";
            // 
            // spNameTextBox
            // 
            this.spNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.spNameTextBox.BackColor = System.Drawing.Color.Black;
            this.spNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spNameTextBox.ForeColor = System.Drawing.Color.White;
            this.spNameTextBox.Location = new System.Drawing.Point(118, 7);
            this.spNameTextBox.Name = "spNameTextBox";
            this.spNameTextBox.Size = new System.Drawing.Size(114, 20);
            this.spNameTextBox.TabIndex = 1;
            // 
            // StageSetupUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "StageSetupUserControl";
            this.Size = new System.Drawing.Size(473, 484);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.stageButtonTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.stageSetupTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel stageButtonTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Model.RoundedButton closeButton;
        private Model.RoundedButton saveButton;
        private Model.RoundedButton roundedButton1;
        private Model.RoundedButton roundedButton2;
        private Model.RoundedButton roundedButton3;
        private Model.RoundedButton roundedButton4;
        private Model.RoundedButton roundedButton5;
        private Model.RoundedButton roundedButton6;
        private Model.RoundedButton roundedButton7;
        private Model.RoundedButton roundedButton8;
        private System.Windows.Forms.TableLayoutPanel stageSetupTableLayoutPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MyLabel cameraName1Label;
        private MyLabel label2;
        private MyComboBox camera1ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private MyLabel cameraName5Label;
        private MyLabel label4;
        private MyComboBox camera5ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MyLabel cameraName2Label;
        private MyLabel label6;
        private MyComboBox camera2ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private MyLabel cameraName6Label;
        private MyLabel label8;
        private MyComboBox camera6ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MyLabel cameraName3Label;
        private MyLabel label10;
        private MyComboBox camera3ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private MyLabel cameraName7Label;
        private MyLabel label12;
        private MyComboBox camera7ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private MyLabel cameraName4Label;
        private MyLabel label14;
        private MyComboBox camera4ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private MyLabel cameraName8Label;
        private MyLabel label16;
        private MyComboBox camera8ComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private Model.RoundedButton testButton;
        private MyLabel label17;
        private Model.RoundedTextBox httpTextBox;
        private MyCheckBox showStageCheckBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MyLabel myLabel1;
        private RoundedTextBox spNameTextBox;
    }
}
