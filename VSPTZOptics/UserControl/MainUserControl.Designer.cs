﻿using CameraView.Base;
using CameraUserControl.Model;
using System.Windows.Media;

namespace CameraUserControl
{
    partial class MainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUserControl));
            this.ptzControlPanel = new System.Windows.Forms.Panel();
            this.ptzTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ptzLeftPanel = new System.Windows.Forms.Panel();
            this.cameraInfoTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cameraControlTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.camera3Button = new CameraUserControl.Model.MyToggleButton();
            this.camera2Button = new CameraUserControl.Model.MyToggleButton();
            this.camera1Button = new CameraUserControl.Model.MyToggleButton();
            this.camera4Button = new CameraUserControl.Model.MyToggleButton();
            this.camera5Button = new CameraUserControl.Model.MyToggleButton();
            this.camera6Button = new CameraUserControl.Model.MyToggleButton();
            this.camera7Button = new CameraUserControl.Model.MyToggleButton();
            this.camera8Button = new CameraUserControl.Model.MyToggleButton();
            this.ptzRightPanel = new System.Windows.Forms.Panel();
            this.previewPanel = new System.Windows.Forms.Panel();
            this.stagePreviewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editStageSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editCameraPresetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editStagePresetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unlockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.squarePanel = new System.Windows.Forms.Panel();
            this.livePreviewTableLayoutPanel = new CameraView.Base.DBTableLayoutPanel(this.components);
            this.plusButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.topPanel = new System.Windows.Forms.Panel();
            this.previewTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.previewBgrPanel = new System.Windows.Forms.Panel();
            this.ptzControlPanel.SuspendLayout();
            this.ptzTableLayoutPanel.SuspendLayout();
            this.ptzLeftPanel.SuspendLayout();
            this.cameraInfoTableLayoutPanel.SuspendLayout();
            this.cameraControlTableLayoutPanel.SuspendLayout();
            this.stagePreviewContextMenuStrip.SuspendLayout();
            this.squarePanel.SuspendLayout();
            this.livePreviewTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.previewTableLayoutPanel.SuspendLayout();
            this.previewBgrPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ptzControlPanel
            // 
            this.ptzControlPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.ptzControlPanel.Controls.Add(this.ptzTableLayoutPanel);
            this.ptzControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzControlPanel.Location = new System.Drawing.Point(10, 609);
            this.ptzControlPanel.Margin = new System.Windows.Forms.Padding(10, 0, 10, 20);
            this.ptzControlPanel.Name = "ptzControlPanel";
            this.ptzControlPanel.Size = new System.Drawing.Size(1236, 170);
            this.ptzControlPanel.TabIndex = 2;
            // 
            // ptzTableLayoutPanel
            // 
            this.ptzTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.ptzTableLayoutPanel.ColumnCount = 4;
            this.ptzTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99999F));
            this.ptzTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 455F));
            this.ptzTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 680F));
            this.ptzTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ptzTableLayoutPanel.Controls.Add(this.ptzLeftPanel, 1, 0);
            this.ptzTableLayoutPanel.Controls.Add(this.ptzRightPanel, 2, 0);
            this.ptzTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.ptzTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ptzTableLayoutPanel.Name = "ptzTableLayoutPanel";
            this.ptzTableLayoutPanel.RowCount = 1;
            this.ptzTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ptzTableLayoutPanel.Size = new System.Drawing.Size(1236, 170);
            this.ptzTableLayoutPanel.TabIndex = 0;
            // 
            // ptzLeftPanel
            // 
            this.ptzLeftPanel.BackColor = System.Drawing.Color.Transparent;
            this.ptzLeftPanel.Controls.Add(this.cameraInfoTableLayoutPanel);
            this.ptzLeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzLeftPanel.Location = new System.Drawing.Point(53, 3);
            this.ptzLeftPanel.Name = "ptzLeftPanel";
            this.ptzLeftPanel.Size = new System.Drawing.Size(449, 164);
            this.ptzLeftPanel.TabIndex = 0;
            // 
            // cameraInfoTableLayoutPanel
            // 
            this.cameraInfoTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.cameraInfoTableLayoutPanel.ColumnCount = 3;
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraInfoTableLayoutPanel.Controls.Add(this.cameraControlTableLayoutPanel, 1, 1);
            this.cameraInfoTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraInfoTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.cameraInfoTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.cameraInfoTableLayoutPanel.Name = "cameraInfoTableLayoutPanel";
            this.cameraInfoTableLayoutPanel.RowCount = 3;
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.cameraInfoTableLayoutPanel.Size = new System.Drawing.Size(449, 164);
            this.cameraInfoTableLayoutPanel.TabIndex = 1;
            // 
            // cameraControlTableLayoutPanel
            // 
            this.cameraControlTableLayoutPanel.ColumnCount = 4;
            this.cameraControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera3Button, 2, 0);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera2Button, 1, 0);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera1Button, 0, 0);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera4Button, 3, 0);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera5Button, 0, 1);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera6Button, 1, 1);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera7Button, 2, 1);
            this.cameraControlTableLayoutPanel.Controls.Add(this.camera8Button, 3, 1);
            this.cameraControlTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraControlTableLayoutPanel.Location = new System.Drawing.Point(52, 45);
            this.cameraControlTableLayoutPanel.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.cameraControlTableLayoutPanel.Name = "cameraControlTableLayoutPanel";
            this.cameraControlTableLayoutPanel.RowCount = 2;
            this.cameraControlTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraControlTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraControlTableLayoutPanel.Size = new System.Drawing.Size(397, 74);
            this.cameraControlTableLayoutPanel.TabIndex = 0;
            // 
            // camera3Button
            // 
            this.camera3Button.BackColor = System.Drawing.Color.Gray;
            this.camera3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera3Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera3Button.FlatAppearance.BorderSize = 0;
            this.camera3Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera3Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera3Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera3Button.ForeColor = System.Drawing.Color.White;
            this.camera3Button.Image = ((System.Drawing.Image)(resources.GetObject("camera3Button.Image")));
            this.camera3Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera3Button.IsConnect = false;
            this.camera3Button.Location = new System.Drawing.Point(201, 3);
            this.camera3Button.Name = "camera3Button";
            this.camera3Button.Size = new System.Drawing.Size(93, 31);
            this.camera3Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera3Button.TabIndex = 2;
            this.camera3Button.Text = " Camera #3";
            this.camera3Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera3Button.UseVisualStyleBackColor = false;
            this.camera3Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera2Button
            // 
            this.camera2Button.BackColor = System.Drawing.Color.Gray;
            this.camera2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera2Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera2Button.FlatAppearance.BorderSize = 0;
            this.camera2Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera2Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera2Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera2Button.ForeColor = System.Drawing.Color.White;
            this.camera2Button.Image = ((System.Drawing.Image)(resources.GetObject("camera2Button.Image")));
            this.camera2Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera2Button.IsConnect = false;
            this.camera2Button.Location = new System.Drawing.Point(102, 3);
            this.camera2Button.Name = "camera2Button";
            this.camera2Button.Size = new System.Drawing.Size(93, 31);
            this.camera2Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera2Button.TabIndex = 1;
            this.camera2Button.Text = " Camera #2";
            this.camera2Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera2Button.UseVisualStyleBackColor = false;
            this.camera2Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera1Button
            // 
            this.camera1Button.BackColor = System.Drawing.Color.Gray;
            this.camera1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera1Button.FlatAppearance.BorderSize = 0;
            this.camera1Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera1Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera1Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera1Button.ForeColor = System.Drawing.Color.White;
            this.camera1Button.Image = ((System.Drawing.Image)(resources.GetObject("camera1Button.Image")));
            this.camera1Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera1Button.IsConnect = false;
            this.camera1Button.Location = new System.Drawing.Point(3, 3);
            this.camera1Button.Name = "camera1Button";
            this.camera1Button.Size = new System.Drawing.Size(93, 31);
            this.camera1Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera1Button.TabIndex = 0;
            this.camera1Button.Text = " Camera #1";
            this.camera1Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera1Button.UseVisualStyleBackColor = false;
            this.camera1Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera4Button
            // 
            this.camera4Button.BackColor = System.Drawing.Color.Gray;
            this.camera4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera4Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera4Button.FlatAppearance.BorderSize = 0;
            this.camera4Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera4Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera4Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera4Button.ForeColor = System.Drawing.Color.White;
            this.camera4Button.Image = ((System.Drawing.Image)(resources.GetObject("camera4Button.Image")));
            this.camera4Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera4Button.IsConnect = false;
            this.camera4Button.Location = new System.Drawing.Point(300, 3);
            this.camera4Button.Name = "camera4Button";
            this.camera4Button.Size = new System.Drawing.Size(94, 31);
            this.camera4Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera4Button.TabIndex = 2;
            this.camera4Button.Text = " Camera #4";
            this.camera4Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera4Button.UseVisualStyleBackColor = false;
            this.camera4Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera5Button
            // 
            this.camera5Button.BackColor = System.Drawing.Color.Gray;
            this.camera5Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera5Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera5Button.FlatAppearance.BorderSize = 0;
            this.camera5Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera5Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera5Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera5Button.ForeColor = System.Drawing.Color.White;
            this.camera5Button.Image = ((System.Drawing.Image)(resources.GetObject("camera5Button.Image")));
            this.camera5Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera5Button.IsConnect = false;
            this.camera5Button.Location = new System.Drawing.Point(3, 40);
            this.camera5Button.Name = "camera5Button";
            this.camera5Button.Size = new System.Drawing.Size(93, 31);
            this.camera5Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera5Button.TabIndex = 2;
            this.camera5Button.Text = " Camera #5";
            this.camera5Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera5Button.UseVisualStyleBackColor = false;
            this.camera5Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera6Button
            // 
            this.camera6Button.BackColor = System.Drawing.Color.Gray;
            this.camera6Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera6Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera6Button.FlatAppearance.BorderSize = 0;
            this.camera6Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera6Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera6Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera6Button.ForeColor = System.Drawing.Color.White;
            this.camera6Button.Image = ((System.Drawing.Image)(resources.GetObject("camera6Button.Image")));
            this.camera6Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera6Button.IsConnect = false;
            this.camera6Button.Location = new System.Drawing.Point(102, 40);
            this.camera6Button.Name = "camera6Button";
            this.camera6Button.Size = new System.Drawing.Size(93, 31);
            this.camera6Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera6Button.TabIndex = 2;
            this.camera6Button.Text = " Camera #6";
            this.camera6Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera6Button.UseVisualStyleBackColor = false;
            this.camera6Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera7Button
            // 
            this.camera7Button.BackColor = System.Drawing.Color.Gray;
            this.camera7Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera7Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera7Button.FlatAppearance.BorderSize = 0;
            this.camera7Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera7Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera7Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera7Button.ForeColor = System.Drawing.Color.White;
            this.camera7Button.Image = ((System.Drawing.Image)(resources.GetObject("camera7Button.Image")));
            this.camera7Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera7Button.IsConnect = false;
            this.camera7Button.Location = new System.Drawing.Point(201, 40);
            this.camera7Button.Name = "camera7Button";
            this.camera7Button.Size = new System.Drawing.Size(93, 31);
            this.camera7Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera7Button.TabIndex = 2;
            this.camera7Button.Text = " Camera #7";
            this.camera7Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera7Button.UseVisualStyleBackColor = false;
            this.camera7Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // camera8Button
            // 
            this.camera8Button.BackColor = System.Drawing.Color.Gray;
            this.camera8Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera8Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.camera8Button.FlatAppearance.BorderSize = 0;
            this.camera8Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.camera8Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.camera8Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera8Button.ForeColor = System.Drawing.Color.White;
            this.camera8Button.Image = ((System.Drawing.Image)(resources.GetObject("camera8Button.Image")));
            this.camera8Button.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.camera8Button.IsConnect = false;
            this.camera8Button.Location = new System.Drawing.Point(300, 40);
            this.camera8Button.Name = "camera8Button";
            this.camera8Button.Size = new System.Drawing.Size(94, 31);
            this.camera8Button.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.camera8Button.TabIndex = 2;
            this.camera8Button.Text = " Camera #8";
            this.camera8Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.camera8Button.UseVisualStyleBackColor = false;
            this.camera8Button.Click += new System.EventHandler(this.cameraButton_Click);
            // 
            // ptzRightPanel
            // 
            this.ptzRightPanel.BackColor = System.Drawing.Color.Transparent;
            this.ptzRightPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzRightPanel.Location = new System.Drawing.Point(505, 0);
            this.ptzRightPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ptzRightPanel.Name = "ptzRightPanel";
            this.ptzRightPanel.Size = new System.Drawing.Size(680, 170);
            this.ptzRightPanel.TabIndex = 1;
            // 
            // previewPanel
            // 
            this.previewPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.previewPanel.BackColor = System.Drawing.Color.Black;
            this.previewPanel.ContextMenuStrip = this.stagePreviewContextMenuStrip;
            this.previewPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.previewPanel.Location = new System.Drawing.Point(9, 0);
            this.previewPanel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.previewPanel.Name = "previewPanel";
            this.previewPanel.Padding = new System.Windows.Forms.Padding(5);
            this.previewPanel.Size = new System.Drawing.Size(1216, 454);
            this.previewPanel.TabIndex = 1;
            // 
            // stagePreviewContextMenuStrip
            // 
            this.stagePreviewContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.stagePreviewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editStageSourceToolStripMenuItem,
            this.editCameraPresetsToolStripMenuItem,
            this.editStagePresetsToolStripMenuItem,
            this.lockToolStripMenuItem,
            this.unlockToolStripMenuItem});
            this.stagePreviewContextMenuStrip.Name = "stagePreviewContextMenuStrip";
            this.stagePreviewContextMenuStrip.Size = new System.Drawing.Size(181, 136);
            // 
            // editStageSourceToolStripMenuItem
            // 
            this.editStageSourceToolStripMenuItem.Name = "editStageSourceToolStripMenuItem";
            this.editStageSourceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.editStageSourceToolStripMenuItem.Text = "Edit Stage Source";
            this.editStageSourceToolStripMenuItem.Click += new System.EventHandler(this.editStageSourceToolStripMenuItem_Click);
            // 
            // editCameraPresetsToolStripMenuItem
            // 
            this.editCameraPresetsToolStripMenuItem.Name = "editCameraPresetsToolStripMenuItem";
            this.editCameraPresetsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.editCameraPresetsToolStripMenuItem.Text = "Edit Camera Presets";
            this.editCameraPresetsToolStripMenuItem.Click += new System.EventHandler(this.editCameraPresetsToolStripMenuItem_Click);
            // 
            // editStagePresetsToolStripMenuItem
            // 
            this.editStagePresetsToolStripMenuItem.Name = "editStagePresetsToolStripMenuItem";
            this.editStagePresetsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.editStagePresetsToolStripMenuItem.Text = "Edit Stage Presets";
            this.editStagePresetsToolStripMenuItem.Click += new System.EventHandler(this.editStagePresetsToolStripMenuItem_Click);
            // 
            // lockToolStripMenuItem
            // 
            this.lockToolStripMenuItem.Name = "lockToolStripMenuItem";
            this.lockToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lockToolStripMenuItem.Text = "Lock";
            this.lockToolStripMenuItem.Click += new System.EventHandler(this.lockToolStripMenuItem_Click);
            // 
            // unlockToolStripMenuItem
            // 
            this.unlockToolStripMenuItem.Name = "unlockToolStripMenuItem";
            this.unlockToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.unlockToolStripMenuItem.Text = "Unlock";
            this.unlockToolStripMenuItem.Click += new System.EventHandler(this.unlockToolStripMenuItem_Click);
            // 
            // squarePanel
            // 
            this.squarePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.squarePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.squarePanel.Controls.Add(this.livePreviewTableLayoutPanel);
            this.squarePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.squarePanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.squarePanel.Location = new System.Drawing.Point(0, 0);
            this.squarePanel.Margin = new System.Windows.Forms.Padding(0);
            this.squarePanel.Name = "squarePanel";
            this.squarePanel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.squarePanel.Size = new System.Drawing.Size(1236, 125);
            this.squarePanel.TabIndex = 0;
            // 
            // livePreviewTableLayoutPanel
            // 
            this.livePreviewTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.livePreviewTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.livePreviewTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.livePreviewTableLayoutPanel.ColumnCount = 1;
            this.livePreviewTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.livePreviewTableLayoutPanel.Controls.Add(this.plusButton, 0, 0);
            this.livePreviewTableLayoutPanel.Location = new System.Drawing.Point(545, 22);
            this.livePreviewTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.livePreviewTableLayoutPanel.Name = "livePreviewTableLayoutPanel";
            this.livePreviewTableLayoutPanel.RowCount = 1;
            this.livePreviewTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.livePreviewTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.livePreviewTableLayoutPanel.Size = new System.Drawing.Size(167, 92);
            this.livePreviewTableLayoutPanel.TabIndex = 0;
            // 
            // plusButton
            // 
            this.plusButton.BackColor = System.Drawing.Color.Black;
            this.plusButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plusButton.FlatAppearance.BorderSize = 0;
            this.plusButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plusButton.ForeColor = System.Drawing.Color.White;
            this.plusButton.Location = new System.Drawing.Point(1, 1);
            this.plusButton.Margin = new System.Windows.Forms.Padding(0);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(165, 90);
            this.plusButton.TabIndex = 0;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = false;
            this.plusButton.Click += new System.EventHandler(this.plusButton_Click);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tableLayoutPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.ptzControlPanel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.topPanel, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1256, 799);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.previewTableLayoutPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(10, 10);
            this.topPanel.Margin = new System.Windows.Forms.Padding(10);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1236, 589);
            this.topPanel.TabIndex = 4;
            // 
            // previewTableLayoutPanel
            // 
            this.previewTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.previewTableLayoutPanel.ColumnCount = 1;
            this.previewTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.previewTableLayoutPanel.Controls.Add(this.squarePanel, 0, 0);
            this.previewTableLayoutPanel.Controls.Add(this.previewBgrPanel, 0, 1);
            this.previewTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.previewTableLayoutPanel.Name = "previewTableLayoutPanel";
            this.previewTableLayoutPanel.RowCount = 2;
            this.previewTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.previewTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.previewTableLayoutPanel.Size = new System.Drawing.Size(1236, 589);
            this.previewTableLayoutPanel.TabIndex = 3;
            // 
            // previewBgrPanel
            // 
            this.previewBgrPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.previewBgrPanel.Controls.Add(this.previewPanel);
            this.previewBgrPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewBgrPanel.Location = new System.Drawing.Point(0, 135);
            this.previewBgrPanel.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.previewBgrPanel.Name = "previewBgrPanel";
            this.previewBgrPanel.Size = new System.Drawing.Size(1236, 454);
            this.previewBgrPanel.TabIndex = 1;
            // 
            // MainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "MainUserControl";
            this.Size = new System.Drawing.Size(1256, 799);
            this.ptzControlPanel.ResumeLayout(false);
            this.ptzTableLayoutPanel.ResumeLayout(false);
            this.ptzLeftPanel.ResumeLayout(false);
            this.cameraInfoTableLayoutPanel.ResumeLayout(false);
            this.cameraControlTableLayoutPanel.ResumeLayout(false);
            this.stagePreviewContextMenuStrip.ResumeLayout(false);
            this.squarePanel.ResumeLayout(false);
            this.livePreviewTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.previewTableLayoutPanel.ResumeLayout(false);
            this.previewBgrPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ptzControlPanel;
        private System.Windows.Forms.Panel previewPanel;
        private System.Windows.Forms.Panel squarePanel;
        private DBTableLayoutPanel livePreviewTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.TableLayoutPanel ptzTableLayoutPanel;
        private System.Windows.Forms.Panel ptzLeftPanel;
        private System.Windows.Forms.TableLayoutPanel cameraInfoTableLayoutPanel;
        private System.Windows.Forms.Panel ptzRightPanel;
        private System.Windows.Forms.TableLayoutPanel previewTableLayoutPanel;
        private System.Windows.Forms.Panel topPanel;
        private PTZUserControl ptzUserControl;
        private System.Windows.Forms.TableLayoutPanel cameraControlTableLayoutPanel;
        private Model.MyToggleButton camera1Button;
        private Model.MyToggleButton camera3Button;
        private Model.MyToggleButton camera2Button;
        private Model.MyToggleButton camera4Button;
        private Model.MyToggleButton camera5Button;
        private Model.MyToggleButton camera6Button;
        private Model.MyToggleButton camera7Button;
        private Model.MyToggleButton camera8Button;
        private System.Windows.Forms.ContextMenuStrip stagePreviewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editStageSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editCameraPresetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editStagePresetsToolStripMenuItem;
        private System.Windows.Forms.Panel previewBgrPanel;
        private System.Windows.Forms.ToolStripMenuItem lockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unlockToolStripMenuItem;
    }
}
