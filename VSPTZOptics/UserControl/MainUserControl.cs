﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShareLibrary.Base;
using CameraUserControl.Base;
using ShareLibrary.Model;
using CameraView.ViewModel;
using CameraView.Model;
using CameraView.Base;
using CameraUserControl.Model;
using CameraControl.Model;
using System.Runtime.InteropServices;
using Settings.ViewModel;
using System.Windows.Media;
using System.Windows.Interop;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Forms.Integration;
using JoystickControl;

namespace CameraUserControl
{
    public partial class MainUserControl : UserControl
    {
        private bool isLoad = false;
        private System.Drawing.Point clickPosition;
        private RESIZE_TYPE_ENUM resizeType = RESIZE_TYPE_ENUM.NONE;
        public MainUserControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            this.Load += MainUserControl_Load;
        }
        private void MainUserControl_Load(object sender, EventArgs e)
        {
            if(!isLoad)
            {
                isLoad = true;
                InitilizeBindingButtons();
                InitializePreviewPanelEvents();
                if (ptzUserControl == null)
                    ptzUserControl = new PTZUserControl();
                this.ptzRightPanel.Controls.Add(this.ptzUserControl);
                ptzUserControl.SetPTZModeForMainView();
                ptzUserControl.PTZControlEvent += PtzUserControl_PTZControlEvent;
                LoadPreviewCamera();
            }
        }

        private void PtzUserControl_PTZControlEvent(object sender, UserControlEventArgs e)
        {
            if (e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.SWITCH_LEFT_CAMERA)
            {
                Panel lastSelectedPanel = null;
                for(int colInd = 0; colInd < livePreviewTableLayoutPanel.ColumnCount; colInd++)
                {
                    Panel panel = livePreviewTableLayoutPanel.GetControlFromPosition(colInd, 0) as Panel;
                    if (panel != null)//Active camera
                    {
                        if(panel.BackColor == System.Drawing.Color.Yellow)
                        {
                            if (lastSelectedPanel != null)
                            {
                                break;
                            }
                        }
                        else
                            lastSelectedPanel = panel;
                    }
                    
                }
                if (lastSelectedPanel != null)
                    Panel_Click(lastSelectedPanel, null);
            }
            else if(e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.SWITCH_RIGHT_CAMERA)
            {
                Panel lastSelectedPanel = null;
                for (int colInd = livePreviewTableLayoutPanel.ColumnCount - 1; colInd >= 0; colInd--)
                {
                    Panel panel = livePreviewTableLayoutPanel.GetControlFromPosition(colInd, 0) as Panel;
                    if (panel != null)//Active camera
                    {
                        if (panel.BackColor == System.Drawing.Color.Yellow)
                        {
                            if (lastSelectedPanel != null)
                            {
                                break;
                            }
                        }
                        else
                            lastSelectedPanel = panel;
                    }
                }
                if (lastSelectedPanel != null)
                    Panel_Click(lastSelectedPanel, null);
            }
        }

        private void InitializePreviewPanelEvents()
        {
            this.previewPanel.MouseDown += PreviewPanel_MouseDown;
            this.previewPanel.MouseMove += PreviewPanel_MouseMove;
            this.previewPanel.MouseUp += PreviewPanel_MouseUp;
        }
        private void PreviewPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if(this.previewPanel.Cursor == Cursors.Default && e.Button == MouseButtons.Left)
            {
                Stage stage = ProfileViewModel.Instance.SelectedProfile.StageList.Find(x => x.IsSelected == true);
                if(stage!=null)
                {
                    stage.IsCallStagePreset = true;//Trigger Call Stage Preset Event
                }
            }
            this.previewPanel.Cursor = Cursors.Default;
        }
        private void PreviewPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if(this.previewPanel.Cursor == Cursors.SizeWE || this.previewPanel.Cursor == Cursors.SizeNS) //resize
                {
                    Stage stage = ProfileViewModel.Instance.SelectedProfile.StageList.Find(x => x.IsSelected == true);
                    if (stage != null && !stage.IsLock)
                    {
                        int newX = 0;
                        int newY = 0;
                        if (previewPanel.Controls.Count > 0)
                        {
                            PanelEx panelEx = previewPanel.Controls[0] as PanelEx;
                            if (panelEx != null)
                            {
                                newX = e.X - panelEx.Position.X;
                                newY = e.Y - panelEx.Position.Y;
                            }
                        }
                        switch (resizeType)
                        {
                            case RESIZE_TYPE_ENUM.NONE:
                                break;
                            case RESIZE_TYPE_ENUM.LEFT:
                                {
                                    if(stage.Rect.Width - (newX - stage.Rect.X) > 0)
                                    {
                                        Rectangle newRect = new Rectangle(newX, stage.Rect.Y, stage.Rect.Width - (newX - stage.Rect.X), stage.Rect.Height);
                                        stage.Rect = newRect;
                                    }
                                }
                                break;
                            case RESIZE_TYPE_ENUM.RIGHT:
                                {
                                    if(newX - stage.Rect.X > 0)
                                    {
                                        Rectangle newRect = new Rectangle(stage.Rect.X, stage.Rect.Y, newX - stage.Rect.X, stage.Rect.Height);
                                        stage.Rect = newRect;
                                    }
                                }
                                break;
                            case RESIZE_TYPE_ENUM.TOP:
                                {
                                    if(stage.Rect.Height - (newY - stage.Rect.Y) > 0)
                                    {
                                        Rectangle newRect = new Rectangle(stage.Rect.X, newY, stage.Rect.Width, stage.Rect.Height - (newY - stage.Rect.Y));
                                        stage.Rect = newRect;
                                    }
                                }
                                break;
                            case RESIZE_TYPE_ENUM.BOTTOM:
                                {
                                    if(newY - stage.Rect.Y > 0)
                                                                        {
                                        Rectangle newRect = new Rectangle(stage.Rect.X, stage.Rect.Y, stage.Rect.Width, newY - stage.Rect.Y);
                                        stage.Rect = newRect;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        ProfileViewModel.Instance.SelectedProfile.IsChange = true;
                    }
                }
                else //move
                {                    
                    Stage stage = ProfileViewModel.Instance.SelectedProfile.StageList.Find(x => x.IsSelected == true);
                    if (stage != null && !stage.IsLock)
                    {
                        this.previewPanel.Cursor = Cursors.NoMove2D;
                        if (previewPanel.Controls.Count > 0)
                        {
                            PanelEx panelEx = previewPanel.Controls[0] as PanelEx;
                            if (panelEx != null)
                            {
                                int newX = e.X - panelEx.Position.X;
                                int newY = e.Y - panelEx.Position.Y;
                                Rectangle rect = panelEx.GetDisplaySize();
                                int limitX = rect.Width - 30;
                                int limitY = rect.Height;
                                if ((newX < limitX && newX > 0) &&
                                    (newY < limitY && newY > 0))
                                {
                                    Rectangle newRect = new Rectangle(newX - stage.Rect.Width / 2, newY - stage.Rect.Height / 2, stage.Rect.Width, stage.Rect.Height);
                                    stage.Rect = newRect;
                                    ProfileViewModel.Instance.SelectedProfile.IsChange = true;
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                foreach (var control in previewPanel.Controls)
                {
                    PanelEx panelEx = control as PanelEx;
                    int delta = 5;
                    if (panelEx != null)
                    {
                        Rectangle rect = panelEx.GetDisplaySize();
                        panelEx.Position = new System.Drawing.Point((previewPanel.Width - rect.Width) / 2, (previewPanel.Height - rect.Height) / 2);
                        foreach (Stage stage in ProfileViewModel.Instance.SelectedProfile.StageList)
                        {
                            if (stage.IsShowOnStageView && stage.IsSelected && !stage.IsLock)
                            {
                                if ((stage.Rect.X + panelEx.Position.X > e.X - delta && stage.Rect.X + panelEx.Position.X < e.X + delta)
                                    || (stage.Rect.X + panelEx.Position.X + stage.Rect.Width > e.X - delta && stage.Rect.X + panelEx.Position.X + stage.Rect.Width < e.X + delta))
                                {
                                    this.previewPanel.Cursor = Cursors.SizeWE;
                                    if (stage.Rect.X + panelEx.Position.X > e.X - delta && stage.Rect.X + panelEx.Position.X < e.X + delta)
                                        resizeType = RESIZE_TYPE_ENUM.LEFT;
                                    else
                                        resizeType = RESIZE_TYPE_ENUM.RIGHT;
                                    break;
                                }
                                else if ((stage.Rect.Y + panelEx.Position.Y > e.Y - delta && stage.Rect.Y + panelEx.Position.Y < e.Y + delta)
                                    || (stage.Rect.Y + panelEx.Position.Y + stage.Rect.Height > e.Y - delta && stage.Rect.Y + panelEx.Position.Y + stage.Rect.Height < e.Y + delta))
                                {
                                    this.previewPanel.Cursor = Cursors.SizeNS;
                                    if (stage.Rect.Y + panelEx.Position.Y > e.Y - delta && stage.Rect.Y + panelEx.Position.Y < e.Y + delta)
                                        resizeType = RESIZE_TYPE_ENUM.TOP;
                                    else
                                        resizeType = RESIZE_TYPE_ENUM.BOTTOM;
                                    break;
                                }
                                else
                                    this.previewPanel.Cursor = Cursors.Default;
                            }
                        }
                    }
                }
            }
        }
        private void PreviewPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this.clickPosition.X = e.X;
            this.clickPosition.Y = e.Y;
            if (previewPanel.Cursor != Cursors.Default) //resizing or moving
                return;
            
            bool isSelected = false;
            foreach (Stage stage in ProfileViewModel.Instance.SelectedProfile.StageList)
            {
                if (stage.IsShowOnStageView)
                {
                    if (!isSelected)
                    {
                        if (previewPanel.Controls.Count > 0)
                        {
                            PanelEx panelEx = previewPanel.Controls[0] as PanelEx;
                            if (panelEx != null)
                            {
                                Rectangle rect = panelEx.GetDisplaySize();
                                panelEx.Position = new System.Drawing.Point((previewPanel.Width - rect.Width) / 2, (previewPanel.Height - rect.Height) / 2);
                                if ((stage.Rect.X + panelEx.Position.X < this.clickPosition.X && stage.Rect.X + panelEx.Position.X + stage.Rect.Width > this.clickPosition.X) &&
                                    (stage.Rect.Y + panelEx.Position.Y < this.clickPosition.Y && stage.Rect.Y + panelEx.Position.Y + stage.Rect.Height > this.clickPosition.Y))
                                {
                                    stage.IsSelected = true;
                                    isSelected = true;
                                }
                                else
                                    stage.IsSelected = false;
                            }
                            else
                                stage.IsSelected = false;
                        }
                        else
                            stage.IsSelected = false;
                    }
                    else
                        stage.IsSelected = false;
                }
                else
                    stage.IsSelected = false;
            }
            if (e.Button == MouseButtons.Right)
            {
                this.stagePreviewContextMenuStrip.Items.Clear();
                Stage stage = ProfileViewModel.Instance.SelectedProfile.StageList.Find(x => x.IsSelected == true);
                if (stage != null)
                {
                    if (stage.IsLock)
                    {
                        this.stagePreviewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        this.unlockToolStripMenuItem});
                    }
                    else
                    {
                        this.stagePreviewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                            this.lockToolStripMenuItem});
                    }
                }
                else
                {
                    this.stagePreviewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    this.editStageSourceToolStripMenuItem,
                    this.editCameraPresetsToolStripMenuItem,
                    this.editStagePresetsToolStripMenuItem,});
                }
            }
        }
        private void InitilizeBindingButtons() //Binding connect icon with camera sub stream status
        {
            int index = 0;
            for (int rowInd = 0; rowInd < cameraControlTableLayoutPanel.RowCount; rowInd++)
            {
                for(int colInd = 0; colInd < cameraControlTableLayoutPanel.ColumnCount; colInd++)
                {
                    Camera cam = ProfileViewModel.Instance.SelectedProfile.CameraList[index];
                    if(cam != null)
                    {
                        var control = cameraControlTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //Control Button
                        control.DataBindings.Add("Status", cam, "ControlStatus", true, DataSourceUpdateMode.OnPropertyChanged); //Binding control button status with camera socket status (PTZ control)
                        control.DataBindings.Add("IsConnect", cam, "IsConnect", true, DataSourceUpdateMode.OnPropertyChanged); //Binding control button status with camera socket status (PTZ control)
                        control.DataBindings.Add("Name", cam, "Name", true, DataSourceUpdateMode.OnPropertyChanged); //Binding control button status with camera socket status (PTZ control)
                    }
                    index++;
                }                
            }
        }
        private void ClearLivePreviewCamera()
        {
            for(int rowInd = 0; rowInd < livePreviewTableLayoutPanel.RowCount; rowInd++)
            {
                for (int colInd = 0; colInd < livePreviewTableLayoutPanel.ColumnCount; colInd++)
                {
                    Panel panelControl = livePreviewTableLayoutPanel.GetControlFromPosition(colInd, rowInd) as Panel;
                    if(panelControl != null && panelControl.Controls.Count > 0)
                    {
                        PanelEx panelEx = panelControl.Controls[0] as PanelEx;
                        if(panelEx != null)
                            panelEx.IsUsing = false;
                        panelControl.Controls.Clear();
                    }
                }
            }
            livePreviewTableLayoutPanel.Controls.Clear();
            livePreviewTableLayoutPanel.ColumnStyles.Clear();
            livePreviewTableLayoutPanel.RowStyles.Clear();
        }
        public void LoadPreviewCamera()
        {
            LoadLivePreviewCamera();
            LoadStagePreviewCamera();
        }
        public void LoadLivePreviewCamera() //Load live preview camera Hollywood
        {
            ClearLivePreviewCamera();
            int count = 0;
            foreach(Camera cam in ProfileViewModel.Instance.SelectedProfile.CameraList)
            {
                if(cam.Index < 8)
                {
                    if (cam.IsConnect &&
                        ((cam.IsIPCamera && !string.IsNullOrEmpty(cam.Ip_Domain)) ||
                         (cam.IsNDICamera && !string.IsNullOrEmpty(cam.NDICameraName)) ||
                         (cam.IsUSBCamera && !string.IsNullOrEmpty(cam.USBCameraName))))
                    {
                        count++;
                    }
                }
            }
            //Set row, column for table layout - begin
            livePreviewTableLayoutPanel.RowCount = 1;
            livePreviewTableLayoutPanel.ColumnCount = count < 8 ? count + 1: 8;
            int percent = 100 / livePreviewTableLayoutPanel.ColumnCount;
            for (int ind = 0; ind < livePreviewTableLayoutPanel.ColumnCount; ind++)
                livePreviewTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, percent));
            percent = 100 / livePreviewTableLayoutPanel.RowCount;
            for (int ind = 0; ind < livePreviewTableLayoutPanel.RowCount; ind++)
                livePreviewTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, percent));
            //Set row, column for table layout - end

            int columnInd = 0;
            foreach (Camera cam in ProfileViewModel.Instance.SelectedProfile.CameraList)
            {
                if (cam.Index < 8)
                {
                    if (cam.IsConnect &&
                        ((cam.IsIPCamera && !string.IsNullOrEmpty(cam.Ip_Domain)) ||
                         (cam.IsNDICamera && !string.IsNullOrEmpty(cam.NDICameraName)) ||
                         (cam.IsUSBCamera && !string.IsNullOrEmpty(cam.USBCameraName))))
                    {
                        CamView camView = CamViewModel.Instance.FindActiveCamView(cam.Index);
                        if(camView != null)
                        {
                            PanelEx panelEx = camView.SubStream.GetChannelPanel(); //this panel is using for render
                            if (panelEx == null)
                            {
                                panelEx = new PanelEx(camView);
                                camView.SubStream.AddChannelPanel(panelEx);
                            }
                            Panel panel = new Panel(); //this panel is a background to highlight when selected 
                            panel.Margin = new Padding(0, 0, 0, 0);
                            panel.Padding = new Padding(1, 1, 1, 1);
                            panel.Dock = DockStyle.Fill;
                            panel.BackColor = System.Drawing.Color.Black;
                            panel.Click += Panel_Click;
                            panel.DoubleClick += Panel_DoubleClick;
                            livePreviewTableLayoutPanel.Controls.Add(panel, columnInd, 0);
                            panel.Controls.Add(panelEx);
                            panelEx.IsUsing = true;
                            columnInd++;
                        }
                    }
                }
            }
            if(livePreviewTableLayoutPanel.ColumnCount > columnInd)
            {
                livePreviewTableLayoutPanel.Controls.Add(plusButton, livePreviewTableLayoutPanel.ColumnCount - 1, 0);
            }
            SetPreviewCameraSize();
        }
        private void Panel_DoubleClick(object sender, EventArgs e)
        {           
            PanelEx control = ((Panel)sender).Controls[0] as PanelEx;
            if(control != null)
            {
                UserControlEventArgs args = new UserControlEventArgs();
                args.EventType = EVENT_TYPE_ENUM.ADD_CAMERA;
                args.CameraInd = control.CView.Cam.Index;
                OnUserControlEvent(args); //raise event to main form
            }    
        }
        public void SetSuppendPreviewLayout(bool _isSuppend)
        {
            if(_isSuppend)
            {
                if (previewPanel.Controls.Count > 0)
                {
                    PanelEx panelEx = previewPanel.Controls[0] as PanelEx;
                    if (panelEx != null)
                    {
                        panelEx.IsUsing = false;
                    }
                }
                previewPanel.Controls.Clear();
            }
            else
            {
                LoadStagePreviewCamera();
            }
        }
        public void LoadStagePreviewCamera() //Load stage preview
        {
            foreach (var control in previewPanel.Controls)
            {
                PanelEx panelEx = control as PanelEx;
                if(panelEx != null)
                {
                    panelEx.IsUsing = false;
                }
            }
            previewPanel.Controls.Clear();
            SetStagePreviewCameraSize();
            CamView camView = null;
            DISPLAY_RATIO_ENUM displayRatio = DISPLAY_RATIO_ENUM.ORIGINAL_RATIO;
            if(ProfileViewModel.Instance.SelectedProfile.IsStageLiveView)
            {
                camView = CamViewModel.Instance.FindActiveCamView(8);
                ProfileViewModel.Instance.SelectedProfile.CameraList[9].StageList = null;
            }
            else
            {
                ProfileViewModel.Instance.SelectedProfile.CameraList[8].StageList = null;
                camView = CamViewModel.Instance.FindActiveCamView(9); ;
            }
            if(camView != null)
            {
                camView.Cam.StageList = ProfileViewModel.Instance.SelectedProfile.StageList;
                if (ProfileViewModel.Instance.SelectedProfile.IsStageLiveView)
                {
                    if (camView.Cam.IsConnect &&
                        ((camView.Cam.IsIPCamera && !string.IsNullOrEmpty(camView.Cam.Ip_Domain)) ||
                        (camView.Cam.IsNDICamera && !string.IsNullOrEmpty(camView.Cam.NDICameraName)) ||
                        (camView.Cam.IsUSBCamera && !string.IsNullOrEmpty(camView.Cam.USBCameraName))))
                    {
                        PanelEx panelEx = camView.MainStream.GetChannelPanel();
                        if (panelEx == null)
                        {
                            panelEx = new PanelEx(camView, displayRatio);
                            camView.MainStream.AddChannelPanel(panelEx);
                        }
                        previewPanel.Controls.Add(panelEx);
                        panelEx.IsUsing = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(camView.Cam.ImageName))
                    {
                        PanelEx panelEx = camView.MainStream.GetChannelPanel();
                        if (panelEx == null)
                        {
                            panelEx = new PanelEx(camView, displayRatio);
                            camView.MainStream.AddChannelPanel(panelEx);
                        }
                        previewPanel.Controls.Add(panelEx);
                        panelEx.IsUsing = true;
                    }
                }
            }
        }
        private void Panel_Click(object sender, EventArgs e)
        {
            PanelEx panelEx = ((Panel)sender).Controls[0] as PanelEx;
            if(panelEx != null)
            {
                if(panelEx.CView != null)
                {
                    int camInd = panelEx.CView.Cam.Index;
                    int rowInd = camInd < 4 ? 0 : 1;
                    int colInd = camInd % 4;
                    MyToggleButton cameraControlButton = (MyToggleButton)cameraControlTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //Control Button
                    cameraButton_Click(cameraControlButton, null);//call button click
                }
            }
        }
        private void SetPreviewCameraSize() //Set size and position of camera Hollywood
        {
            this.livePreviewTableLayoutPanel.Height = (int)(0.8 * this.squarePanel.Height);
            this.livePreviewTableLayoutPanel.Width = this.livePreviewTableLayoutPanel.ColumnCount * this.livePreviewTableLayoutPanel.Height * (int)ASPECT_RATIO.WIDTH / (int)ASPECT_RATIO.HEIGHT / this.livePreviewTableLayoutPanel.RowCount;
            this.livePreviewTableLayoutPanel.Left = (this.squarePanel.Width - this.livePreviewTableLayoutPanel.Width) / 2;
            this.livePreviewTableLayoutPanel.Top = (this.squarePanel.Height - this.livePreviewTableLayoutPanel.Height) / 2;
        }
        public void SetStagePreviewCameraSize() //Set size and position of camera Hollywood
        {
            this.previewPanel.Height = this.previewBgrPanel.Height - 20;
            this.previewPanel.Width = this.previewPanel.Height * (int)ASPECT_RATIO.WIDTH / (int)ASPECT_RATIO.HEIGHT;
            this.previewPanel.Left = (this.previewBgrPanel.Width - this.previewPanel.Width) / 2;
            this.previewPanel.Top = (this.previewBgrPanel.Height - this.previewPanel.Height) / 2;
        }
        private int FindUnaddedCamera()
        {
            int res = -1;
            foreach (Camera cam in ProfileViewModel.Instance.SelectedProfile.CameraList)
            {
                if (cam.Index < 8)
                {
                    if (!cam.IsConnect ||
                        ((cam.IsIPCamera && string.IsNullOrEmpty(cam.Ip_Domain)) ||
                         (cam.IsNDICamera && string.IsNullOrEmpty(cam.NDICameraName)) ||
                         (cam.IsUSBCamera && string.IsNullOrEmpty(cam.USBCameraName))))
                    {
                        return cam.Index;
                    }
                }
            }
            return res;
        }
        private void plusButton_Click(object sender, EventArgs e)
        {
            UserControlEventArgs args = new UserControlEventArgs();
            args.EventType = EVENT_TYPE_ENUM.ADD_CAMERA;
            args.CameraInd = FindUnaddedCamera();
            OnUserControlEvent(args); //raise event to main form
        }
        private void editStageSourceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserControlEventArgs args = new UserControlEventArgs();
            args.EventType = EVENT_TYPE_ENUM.EDIT_STAGE_SOURCE;
            args.CameraInd = 8; //Stage source camera
            OnUserControlEvent(args); //raise event to main form
        }
        private void editCameraPresetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserControlEventArgs args = new UserControlEventArgs();
            args.EventType = EVENT_TYPE_ENUM.EDIT_CAMERA_PRESETS;
            OnUserControlEvent(args); //raise event to main form
        }
        private void editStagePresetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserControlEventArgs args = new UserControlEventArgs();
            args.EventType = EVENT_TYPE_ENUM.EDIT_STAGE_PRESETS;
            OnUserControlEvent(args); //raise event to main form
        }
        private void ActiveLivePreviewCamera(int _cameraInd)
        {
            for (int row = 0; row < livePreviewTableLayoutPanel.RowCount; row++)
            {
                for (int column = 0; column < livePreviewTableLayoutPanel.ColumnCount; column++)
                {
                    Panel childPanel = livePreviewTableLayoutPanel.GetControlFromPosition(column, row) as Panel;
                    if(childPanel != null)
                    {
                        PanelEx panelEx = childPanel.Controls[0] as PanelEx;
                        if (panelEx != null)
                        {
                            if (panelEx.CView.Cam.Index == _cameraInd)
                                childPanel.BackColor = System.Drawing.Color.Yellow;
                            else
                                childPanel.BackColor = System.Drawing.Color.Black;
                        }
                    }                   
                        
                }
            }
        }
        private void cameraButton_Click(object sender, EventArgs e)
        {
            MyToggleButton button = sender as MyToggleButton;
            button.BackColor = System.Drawing.Color.FromArgb(79, 79, 79);
            int index = 0;
            for (int rowInd = 0; rowInd < cameraControlTableLayoutPanel.RowCount; rowInd++)
            {
                for (int colInd = 0; colInd < cameraControlTableLayoutPanel.ColumnCount; colInd++)
                {
                    CamView camView = CamViewModel.Instance.FindActiveCamView(index);
                    if(camView != null)
                    {
                        var control = cameraControlTableLayoutPanel.GetControlFromPosition(colInd, rowInd); //Control Button
                        if (control == button)
                        {
                            CamControl camControl = camView.CamControl;
                            ActiveLivePreviewCamera(index);
                            ptzUserControl.SetCameraControl(camControl);
                            camControl.AutoFocusChange(ptzUserControl.IsAutoFocus);
                        }
                        else
                        {
                            control.BackColor = System.Drawing.Color.Gray;
                        }
                    }
                    index++;
                }
            }
        }

        public event EventHandler<UserControlEventArgs> UserControlEvent;
        protected virtual void OnUserControlEvent(UserControlEventArgs e)
        {
            EventHandler<UserControlEventArgs> handler = UserControlEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void lockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stage stage = ProfileViewModel.Instance.SelectedProfile.StageList.Find(x => x.IsSelected == true);
            if (stage != null)
                stage.IsLock = true;
            ProfileViewModel.Instance.SelectedProfile.IsChange = true;
        }

        private void unlockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stage stage = ProfileViewModel.Instance.SelectedProfile.StageList.Find(x => x.IsSelected == true);
            if (stage != null)
                stage.IsLock = false;
            ProfileViewModel.Instance.SelectedProfile.IsChange = true;
        }
    }
}
