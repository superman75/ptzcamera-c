﻿using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    class MyToggleButton : RoundedButton
    {
        private int test = 0;
        public int Test
        {
            get { return test; }
            set
            {
                test = value;
            }
        }
        private STATUS_ENUM status = STATUS_ENUM.UNKNOW;
        public STATUS_ENUM Status
        {
            get { return status; }
            set
            {
                status = value;
                if (isConnect)
                {
                    if (status == STATUS_ENUM.CONNECTED)
                        this.Image = Properties.Resources.toggle_on;
                    else if (status == STATUS_ENUM.DISCONNECTED)
                        this.Image = Properties.Resources.toggle_off;
                    else
                        this.Image = Properties.Resources.toggle_off;
                }
                else
                    this.Image = Properties.Resources.toggle_mid;
            }
        }

        private bool isConnect = false;
        public bool IsConnect
        {
            get { return isConnect; }
            set
            {
                if (isConnect != value)
                {
                    isConnect = value;
                    if(this.Text.ToLower().Contains("connect"))
                    {
                        if (isConnect)
                            this.Text = " Disconnect";
                        else
                            this.Text = " Connect";
                    }
                    if(!isConnect)
                    {
                        Status = STATUS_ENUM.UNKNOW;
                    }
                    else
                        Status = STATUS_ENUM.DISCONNECTED;
                }
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                this.Text = " " + name;                
            }
        }
        public MyToggleButton()
        {
            this.Image = Properties.Resources.toggle_mid;
            this.Refresh();
        }
    }
}
