﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    public class MyPictureBox : PictureBox
    {
        private Point clickPosition;
        private Point currentPosition;
        private Point scrollPosition;
        private System.Timers.Timer timer = null;
        private Color borderColor = Color.Red;
        private string name;
        private bool isOnPainted = false;
        public MyPictureBox(Color _color, string _name)
        {
            borderColor = _color;
            name = _name;
            this.Size = new System.Drawing.Size(100, 100);
            this.MouseMove += MyPictureBox_MouseMove;
            this.MouseDown += MyPictureBox_MouseDown;
            this.MouseUp += MyPictureBox_MouseUp;
            this.ChangeFontSizeByDPI();
            InitTimer();
        }

        private void MyPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Default;
            this.Location = currentPosition;
            //Invalidate();
        }

        public void InitTimer()
        {
            timer = new System.Timers.Timer(); // fire every 1 second
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 10;
            timer.Enabled = true;
            //timer.Start();
        }
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //this.Invalidate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            this.SuspendLayout();
            base.OnPaint(e);
            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, borderColor, ButtonBorderStyle.Solid);
            e.Graphics.DrawString(name, this.Font, Brushes.White, new Point(2, 2));
            this.ResumeLayout(false);
        }
        private void MyPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            this.clickPosition.X = e.X;
            this.clickPosition.Y = e.Y;
            this.Cursor = Cursors.NoMove2D;
        }

        private void MyPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int newX = this.Location.X + (e.X - clickPosition.X);
                int newY = this.Location.Y + (e.Y - clickPosition.Y);
                currentPosition = new Point(newX, newY);
                this.Refresh();
            }
        }
    }
}
