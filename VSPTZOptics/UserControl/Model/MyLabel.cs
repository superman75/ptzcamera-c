﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    public class MyLabel : Label
    {
        public MyLabel()
        {
            this.ChangeFontSizeByDPI();
        }
    }
}
