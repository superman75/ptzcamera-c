﻿using Settings.ViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    public static class FontExtension
    {
        private static float deltaFont = 1.5F;
        private static object lockObject = new object();
        public static void ChangeFontSizeByDPI(this object _object, bool isWinForm = false)
        {
            try
            {
                if (ProfileViewModel.Instance.DpiY > 0)
                {
                    if (isWinForm)
                    {
                        //do nothing
                    }
                    else
                    {
                        switch(ProfileViewModel.Instance.DpiX)
                        {
                            case 96:
                                deltaFont = 1;
                                break;
                            case 120://125%
                                deltaFont = 1.24F;
                                break;
                            case 144://150%
                                deltaFont = 1.5F;
                                break;
                            case 168: //175%
                                deltaFont = 1.75F;
                                break;
                            case 192: //200%
                                deltaFont = 1.9F;
                                break;
                            case 240: //250%
                                deltaFont = 2.4F;
                                break;
                            case 288: //300%
                                deltaFont = 2.9F;
                                break;
                            case 312: //325%
                                deltaFont = 3.2F;
                                break;
                            default: break;
                        }
                        if (_object.GetType() == typeof(MyToggleButton) || _object.GetType() == typeof(RoundedButton)
                                                || _object.GetType() == typeof(MyButton))
                        {
                            float newSizeW = ((Button)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((Button)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((Button)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyLabel))
                        {
                            float newSizeW = ((Label)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((Label)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((Label)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyMenuStrip))
                        {
                            float newSizeW = ((MyMenuStrip)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyMenuStrip)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyMenuStrip)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyToolStripMenuItem))
                        {
                            deltaFont = ProfileViewModel.Instance.DpiX == 96 ? 1 : 1.15F;
                            float newSizeW = ((MyToolStripMenuItem)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyToolStripMenuItem)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyToolStripMenuItem)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyComboBox))
                        {
                            float newSizeW = ((MyComboBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyComboBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyComboBox)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyRadioButton))
                        {
                            float newSizeW = ((MyRadioButton)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyRadioButton)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyRadioButton)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyCheckBox))
                        {
                            float newSizeW = ((MyCheckBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyCheckBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyCheckBox)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyGroupBox))
                        {
                            float newSizeW = ((MyGroupBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyGroupBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyGroupBox)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(RoundedTextBox))
                        {
                            float newSizeW = ((RoundedTextBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((RoundedTextBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((RoundedTextBox)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                        else if (_object.GetType() == typeof(MyPictureBox))
                        {
                            float newSizeW = ((MyPictureBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiX * deltaFont;
                            float newSizeH = ((MyPictureBox)_object).Font.Size * 96 / ProfileViewModel.Instance.DpiY * deltaFont;
                            ((MyPictureBox)_object).Font = new Font("Microsoft Sans Serif", newSizeW > newSizeH ? newSizeH : newSizeW);
                        }
                    }
                }
            }
            catch{
            }            
        }
    }
}
