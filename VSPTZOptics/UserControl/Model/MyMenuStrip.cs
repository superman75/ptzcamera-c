﻿using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    public class MyMenuStrip : MenuStrip
    {
        public MyMenuStrip()
        {
            this.Renderer = new MyRenderer();
            this.ChangeFontSizeByDPI();
        }
        private class MyRenderer : ToolStripProfessionalRenderer
        {
            protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
            {
                if (!e.Item.Selected) base.OnRenderMenuItemBackground(e);
                else
                {
                    Rectangle rc = new Rectangle(Point.Empty, e.Item.Size);
                    Brush brush = new SolidBrush(System.Drawing.Color.FromArgb(51, 51, 51));
                    e.Graphics.FillRectangle(brush, rc);
                    Pen pen = new Pen(System.Drawing.Color.FromArgb(41, 41, 41));
                    e.Graphics.DrawRectangle(pen, 1, 0, rc.Width - 2, rc.Height - 1);
                }
            }
        }
    }
    public class MyToolStripMenuItem : ToolStripMenuItem, IBindableComponent
    {
        public MyToolStripMenuItem()
        {
            this.ChangeFontSizeByDPI();
        }
        #region IBindableComponent Members

        private BindingContext bindingContext;

        private ControlBindingsCollection dataBindings;

        [Browsable(false)]
        public BindingContext BindingContext
        {
            get
            {
                if (bindingContext == null)
                {
                    bindingContext = new BindingContext();
                }
                return bindingContext;
            }
            set
            {
                bindingContext = value;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ControlBindingsCollection DataBindings
        {
            get
            {
                if (dataBindings == null)
                {
                    dataBindings = new ControlBindingsCollection(this);
                }
                return dataBindings;
            }
        }
        #endregion
    }
}
