﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    public class MyComboBox: ComboBox
    {
        public MyComboBox()
        {
            this.BackColor = Color.Black;
            this.ForeColor = Color.White;
            this.FlatStyle = FlatStyle.Popup;
            this.ChangeFontSizeByDPI();
        }
    }
}
