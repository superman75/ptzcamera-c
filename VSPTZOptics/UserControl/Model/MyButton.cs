﻿using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraUserControl.Model
{
    public class MyButton : Button
    {
        private Image normalImage;
        private Image hoverImage;
        private Image activeImage;
        public MyButton()
        {
            normalImage = null;
            hoverImage = null;
            activeImage = null;
        }
        public MyButton(Image _normalImage, Image _hoverImage, Image _activeImage)
        {
            normalImage = _normalImage;
            hoverImage = _hoverImage;
            activeImage = _activeImage;
            this.BackgroundImage = normalImage;
        }
        public void UpdateImage(Image _normalImage, Image _hoverImage, Image _clickImage)
        {
            normalImage = _normalImage;
            hoverImage = _hoverImage;
            activeImage = _clickImage;
            this.BackgroundImage = normalImage;
        }
        protected override void OnMouseHover(EventArgs e)
        {
            this.BackgroundImage = hoverImage;
            base.OnMouseHover(e);
        }
        protected override void OnMouseLeave(EventArgs e)
        {
            this.BackgroundImage = normalImage;
            base.OnMouseLeave(e);
        }
        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            this.BackgroundImage = hoverImage;
            base.OnMouseUp(mevent);
        }
        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            this.BackgroundImage = activeImage;
            base.OnMouseDown(mevent);
        }
    }
}
