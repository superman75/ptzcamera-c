﻿using CameraUserControl.Model;
namespace CameraUserControl
{
    partial class MultiCameraSetupUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiCameraSetupUserControl));
            this.cameraSetupTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ptzCameraGroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraInfoTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.camera1GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup1TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.camera1NDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox1 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel1 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton1 = new CameraUserControl.Model.MyToggleButton();
            this.camera1TextBox = new CameraUserControl.Model.RoundedTextBox();
            this.ndiCameraComboBox = new CameraUserControl.Model.MyComboBox();
            this.myComboBox10 = new CameraUserControl.Model.MyComboBox();
            this.camera1IPCameraRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.camera1NDICameraRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.camera1USBCameraRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.camera5GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup5TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox4 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox8 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel5 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton5 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox9 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox17 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox18 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton10 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton11 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton12 = new CameraUserControl.Model.MyRadioButton();
            this.camera2GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup2TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox1 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox2 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel2 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton2 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox3 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox11 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox12 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton1 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton2 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton3 = new CameraUserControl.Model.MyRadioButton();
            this.camera6GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup6TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox5 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox10 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel6 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton6 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox11 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox19 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox20 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton13 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton14 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton15 = new CameraUserControl.Model.MyRadioButton();
            this.camera3GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup3TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox2 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox4 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel3 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton3 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox5 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox13 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox14 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton4 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton5 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton6 = new CameraUserControl.Model.MyRadioButton();
            this.camera7GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup7TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox6 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox12 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel7 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton7 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox13 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox21 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox22 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton16 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton17 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton18 = new CameraUserControl.Model.MyRadioButton();
            this.camera4GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup4TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox3 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox6 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel4 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton4 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox7 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox15 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox16 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton7 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton8 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton9 = new CameraUserControl.Model.MyRadioButton();
            this.camera8GroupBox = new CameraUserControl.Model.MyGroupBox();
            this.cameraGroup8TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.myCheckBox7 = new CameraUserControl.Model.MyCheckBox();
            this.roundedTextBox14 = new CameraUserControl.Model.RoundedTextBox();
            this.myLabel8 = new CameraUserControl.Model.MyLabel();
            this.myToggleButton8 = new CameraUserControl.Model.MyToggleButton();
            this.roundedTextBox15 = new CameraUserControl.Model.RoundedTextBox();
            this.myComboBox23 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox24 = new CameraUserControl.Model.MyComboBox();
            this.myRadioButton19 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton20 = new CameraUserControl.Model.MyRadioButton();
            this.myRadioButton21 = new CameraUserControl.Model.MyRadioButton();
            this.stageSourceGroupBox = new CameraUserControl.Model.MyGroupBox();
            this.stageSourceTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.myLabel11 = new CameraUserControl.Model.MyLabel();
            this.myLabel10 = new CameraUserControl.Model.MyLabel();
            this.imageNameTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.staticTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.stageBrowseButton = new CameraUserControl.Model.RoundedButton();
            this.imageStageNDICheckBox = new CameraUserControl.Model.MyCheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.liveIPCameraTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.liveStageNDICheckbox = new CameraUserControl.Model.MyCheckBox();
            this.liveStageConnectButton = new CameraUserControl.Model.MyToggleButton();
            this.myLabel9 = new CameraUserControl.Model.MyLabel();
            this.liveNameTextBox = new CameraUserControl.Model.RoundedTextBox();
            this.liveNDICameraComboBox = new CameraUserControl.Model.MyComboBox();
            this.liveUSBCameraComboBox = new CameraUserControl.Model.MyComboBox();
            this.liveIPCameraRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.liveNDICameraRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.liveUSBCameraRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.staticRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.liveRadioButton = new CameraUserControl.Model.MyRadioButton();
            this.cancelButton = new CameraUserControl.Model.RoundedButton();
            this.saveButton = new CameraUserControl.Model.RoundedButton();
            this.myComboBox1 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox2 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox3 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox4 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox5 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox6 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox7 = new CameraUserControl.Model.MyComboBox();
            this.myComboBox8 = new CameraUserControl.Model.MyComboBox();
            this.liveStageNDIInputCheckbox = new CameraUserControl.Model.MyCheckBox();
            this.cameraSetupTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.ptzCameraGroupBox.SuspendLayout();
            this.cameraInfoTableLayoutPanel.SuspendLayout();
            this.camera1GroupBox.SuspendLayout();
            this.cameraGroup1TableLayoutPanel.SuspendLayout();
            this.camera5GroupBox.SuspendLayout();
            this.cameraGroup5TableLayoutPanel.SuspendLayout();
            this.camera2GroupBox.SuspendLayout();
            this.cameraGroup2TableLayoutPanel.SuspendLayout();
            this.camera6GroupBox.SuspendLayout();
            this.cameraGroup6TableLayoutPanel.SuspendLayout();
            this.camera3GroupBox.SuspendLayout();
            this.cameraGroup3TableLayoutPanel.SuspendLayout();
            this.camera7GroupBox.SuspendLayout();
            this.cameraGroup7TableLayoutPanel.SuspendLayout();
            this.camera4GroupBox.SuspendLayout();
            this.cameraGroup4TableLayoutPanel.SuspendLayout();
            this.camera8GroupBox.SuspendLayout();
            this.cameraGroup8TableLayoutPanel.SuspendLayout();
            this.stageSourceGroupBox.SuspendLayout();
            this.stageSourceTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cameraSetupTableLayoutPanel
            // 
            this.cameraSetupTableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.cameraSetupTableLayoutPanel.ColumnCount = 1;
            this.cameraSetupTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraSetupTableLayoutPanel.Controls.Add(this.ptzCameraGroupBox, 0, 1);
            this.cameraSetupTableLayoutPanel.Controls.Add(this.stageSourceGroupBox, 0, 2);
            this.cameraSetupTableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.cameraSetupTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraSetupTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.cameraSetupTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.cameraSetupTableLayoutPanel.Name = "cameraSetupTableLayoutPanel";
            this.cameraSetupTableLayoutPanel.RowCount = 5;
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 700F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.cameraSetupTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraSetupTableLayoutPanel.Size = new System.Drawing.Size(664, 960);
            this.cameraSetupTableLayoutPanel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.saveButton, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 914);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(656, 41);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // ptzCameraGroupBox
            // 
            this.ptzCameraGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ptzCameraGroupBox.Controls.Add(this.cameraInfoTableLayoutPanel);
            this.ptzCameraGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptzCameraGroupBox.ForeColor = System.Drawing.Color.White;
            this.ptzCameraGroupBox.Location = new System.Drawing.Point(13, 12);
            this.ptzCameraGroupBox.Margin = new System.Windows.Forms.Padding(13, 12, 13, 0);
            this.ptzCameraGroupBox.Name = "ptzCameraGroupBox";
            this.ptzCameraGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.ptzCameraGroupBox.Size = new System.Drawing.Size(638, 688);
            this.ptzCameraGroupBox.TabIndex = 4;
            this.ptzCameraGroupBox.TabStop = false;
            this.ptzCameraGroupBox.Text = "PTZ Camera";
            // 
            // cameraInfoTableLayoutPanel
            // 
            this.cameraInfoTableLayoutPanel.ColumnCount = 2;
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera1GroupBox, 0, 0);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera5GroupBox, 1, 0);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera2GroupBox, 0, 1);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera6GroupBox, 1, 1);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera3GroupBox, 0, 2);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera7GroupBox, 1, 2);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera4GroupBox, 0, 3);
            this.cameraInfoTableLayoutPanel.Controls.Add(this.camera8GroupBox, 1, 3);
            this.cameraInfoTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraInfoTableLayoutPanel.Location = new System.Drawing.Point(4, 17);
            this.cameraInfoTableLayoutPanel.Name = "cameraInfoTableLayoutPanel";
            this.cameraInfoTableLayoutPanel.RowCount = 4;
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.cameraInfoTableLayoutPanel.Size = new System.Drawing.Size(630, 667);
            this.cameraInfoTableLayoutPanel.TabIndex = 7;
            // 
            // camera1GroupBox
            // 
            this.camera1GroupBox.Controls.Add(this.cameraGroup1TableLayoutPanel);
            this.camera1GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1GroupBox.Location = new System.Drawing.Point(3, 3);
            this.camera1GroupBox.Name = "camera1GroupBox";
            this.camera1GroupBox.Size = new System.Drawing.Size(309, 160);
            this.camera1GroupBox.TabIndex = 0;
            this.camera1GroupBox.TabStop = false;
            this.camera1GroupBox.Text = "Camera1";
            // 
            // cameraGroup1TableLayoutPanel
            // 
            this.cameraGroup1TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.camera1NDICheckBox, 1, 4);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.roundedTextBox1, 1, 0);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.myLabel1, 0, 0);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.myToggleButton1, 0, 4);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.camera1TextBox, 1, 1);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.ndiCameraComboBox, 1, 2);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.myComboBox10, 1, 3);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.camera1IPCameraRadioButton, 0, 1);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.camera1NDICameraRadioButton, 0, 2);
            this.cameraGroup1TableLayoutPanel.Controls.Add(this.camera1USBCameraRadioButton, 0, 3);
            this.cameraGroup1TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup1TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup1TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup1TableLayoutPanel.Name = "cameraGroup1TableLayoutPanel";
            this.cameraGroup1TableLayoutPanel.RowCount = 5;
            this.cameraGroup1TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup1TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup1TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup1TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup1TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup1TableLayoutPanel.Size = new System.Drawing.Size(303, 141);
            this.cameraGroup1TableLayoutPanel.TabIndex = 0;
            // 
            // camera1NDICheckBox
            // 
            this.camera1NDICheckBox.AutoSize = true;
            this.camera1NDICheckBox.BackColor = System.Drawing.Color.Transparent;
            this.camera1NDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1NDICheckBox.ForeColor = System.Drawing.Color.White;
            this.camera1NDICheckBox.Location = new System.Drawing.Point(124, 108);
            this.camera1NDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.camera1NDICheckBox.Name = "camera1NDICheckBox";
            this.camera1NDICheckBox.Size = new System.Drawing.Size(175, 29);
            this.camera1NDICheckBox.TabIndex = 3;
            this.camera1NDICheckBox.Text = "NDI Output";
            this.camera1NDICheckBox.UseVisualStyleBackColor = false;
            this.camera1NDICheckBox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox1
            // 
            this.roundedTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox1.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox1.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox1.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox1.MaxLength = 9;
            this.roundedTextBox1.Name = "roundedTextBox1";
            this.roundedTextBox1.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox1.TabIndex = 0;
            this.roundedTextBox1.Text = "Camera #1";
            // 
            // myLabel1
            // 
            this.myLabel1.AutoSize = true;
            this.myLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel1.ForeColor = System.Drawing.Color.White;
            this.myLabel1.Location = new System.Drawing.Point(3, 0);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(114, 26);
            this.myLabel1.TabIndex = 0;
            this.myLabel1.Text = "Camera Name";
            this.myLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton1
            // 
            this.myToggleButton1.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton1.FlatAppearance.BorderSize = 0;
            this.myToggleButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton1.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton1.Image")));
            this.myToggleButton1.IsConnect = false;
            this.myToggleButton1.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton1.Name = "myToggleButton1";
            this.myToggleButton1.Size = new System.Drawing.Size(112, 29);
            this.myToggleButton1.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton1.TabIndex = 2;
            this.myToggleButton1.Test = 0;
            this.myToggleButton1.Text = " Connect";
            this.myToggleButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton1.UseVisualStyleBackColor = false;
            this.myToggleButton1.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // camera1TextBox
            // 
            this.camera1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.camera1TextBox.BackColor = System.Drawing.Color.Black;
            this.camera1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.camera1TextBox.ForeColor = System.Drawing.Color.White;
            this.camera1TextBox.Location = new System.Drawing.Point(124, 29);
            this.camera1TextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.camera1TextBox.Name = "camera1TextBox";
            this.camera1TextBox.Size = new System.Drawing.Size(175, 20);
            this.camera1TextBox.TabIndex = 5;
            // 
            // ndiCameraComboBox
            // 
            this.ndiCameraComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ndiCameraComboBox.BackColor = System.Drawing.Color.Black;
            this.ndiCameraComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ndiCameraComboBox.ForeColor = System.Drawing.Color.White;
            this.ndiCameraComboBox.FormattingEnabled = true;
            this.ndiCameraComboBox.Location = new System.Drawing.Point(123, 55);
            this.ndiCameraComboBox.Name = "ndiCameraComboBox";
            this.ndiCameraComboBox.Size = new System.Drawing.Size(177, 21);
            this.ndiCameraComboBox.TabIndex = 6;
            this.ndiCameraComboBox.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox10
            // 
            this.myComboBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox10.BackColor = System.Drawing.Color.Black;
            this.myComboBox10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox10.ForeColor = System.Drawing.Color.White;
            this.myComboBox10.FormattingEnabled = true;
            this.myComboBox10.Location = new System.Drawing.Point(123, 81);
            this.myComboBox10.Name = "myComboBox10";
            this.myComboBox10.Size = new System.Drawing.Size(177, 21);
            this.myComboBox10.TabIndex = 6;
            // 
            // camera1IPCameraRadioButton
            // 
            this.camera1IPCameraRadioButton.AutoSize = true;
            this.camera1IPCameraRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1IPCameraRadioButton.ForeColor = System.Drawing.Color.White;
            this.camera1IPCameraRadioButton.Location = new System.Drawing.Point(3, 29);
            this.camera1IPCameraRadioButton.Name = "camera1IPCameraRadioButton";
            this.camera1IPCameraRadioButton.Size = new System.Drawing.Size(114, 20);
            this.camera1IPCameraRadioButton.TabIndex = 7;
            this.camera1IPCameraRadioButton.TabStop = true;
            this.camera1IPCameraRadioButton.Text = "IP Camera";
            this.camera1IPCameraRadioButton.UseVisualStyleBackColor = true;
            // 
            // camera1NDICameraRadioButton
            // 
            this.camera1NDICameraRadioButton.AutoSize = true;
            this.camera1NDICameraRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1NDICameraRadioButton.ForeColor = System.Drawing.Color.White;
            this.camera1NDICameraRadioButton.Location = new System.Drawing.Point(3, 55);
            this.camera1NDICameraRadioButton.Name = "camera1NDICameraRadioButton";
            this.camera1NDICameraRadioButton.Size = new System.Drawing.Size(114, 20);
            this.camera1NDICameraRadioButton.TabIndex = 7;
            this.camera1NDICameraRadioButton.TabStop = true;
            this.camera1NDICameraRadioButton.Text = "NDI Camera";
            this.camera1NDICameraRadioButton.UseVisualStyleBackColor = true;
            // 
            // camera1USBCameraRadioButton
            // 
            this.camera1USBCameraRadioButton.AutoSize = true;
            this.camera1USBCameraRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera1USBCameraRadioButton.ForeColor = System.Drawing.Color.White;
            this.camera1USBCameraRadioButton.Location = new System.Drawing.Point(3, 81);
            this.camera1USBCameraRadioButton.Name = "camera1USBCameraRadioButton";
            this.camera1USBCameraRadioButton.Size = new System.Drawing.Size(114, 20);
            this.camera1USBCameraRadioButton.TabIndex = 7;
            this.camera1USBCameraRadioButton.TabStop = true;
            this.camera1USBCameraRadioButton.Text = "USB Camera";
            this.camera1USBCameraRadioButton.UseVisualStyleBackColor = true;
            // 
            // camera5GroupBox
            // 
            this.camera5GroupBox.Controls.Add(this.cameraGroup5TableLayoutPanel);
            this.camera5GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera5GroupBox.Location = new System.Drawing.Point(318, 3);
            this.camera5GroupBox.Name = "camera5GroupBox";
            this.camera5GroupBox.Size = new System.Drawing.Size(309, 160);
            this.camera5GroupBox.TabIndex = 0;
            this.camera5GroupBox.TabStop = false;
            this.camera5GroupBox.Text = "Camera5";
            // 
            // cameraGroup5TableLayoutPanel
            // 
            this.cameraGroup5TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup5TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup5TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myCheckBox4, 1, 4);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.roundedTextBox8, 1, 0);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myLabel5, 0, 0);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myToggleButton5, 0, 4);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.roundedTextBox9, 1, 1);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myComboBox17, 1, 2);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myComboBox18, 1, 3);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myRadioButton10, 0, 1);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myRadioButton11, 0, 2);
            this.cameraGroup5TableLayoutPanel.Controls.Add(this.myRadioButton12, 0, 3);
            this.cameraGroup5TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup5TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup5TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup5TableLayoutPanel.Name = "cameraGroup5TableLayoutPanel";
            this.cameraGroup5TableLayoutPanel.RowCount = 5;
            this.cameraGroup5TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup5TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup5TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup5TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup5TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup5TableLayoutPanel.Size = new System.Drawing.Size(303, 141);
            this.cameraGroup5TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox4
            // 
            this.myCheckBox4.AutoSize = true;
            this.myCheckBox4.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox4.ForeColor = System.Drawing.Color.White;
            this.myCheckBox4.Location = new System.Drawing.Point(124, 108);
            this.myCheckBox4.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox4.Name = "myCheckBox4";
            this.myCheckBox4.Size = new System.Drawing.Size(175, 29);
            this.myCheckBox4.TabIndex = 3;
            this.myCheckBox4.Text = "NDI Output";
            this.myCheckBox4.UseVisualStyleBackColor = false;
            this.myCheckBox4.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox8
            // 
            this.roundedTextBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox8.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox8.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox8.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox8.MaxLength = 9;
            this.roundedTextBox8.Name = "roundedTextBox8";
            this.roundedTextBox8.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox8.TabIndex = 0;
            this.roundedTextBox8.Text = "Camera #1";
            // 
            // myLabel5
            // 
            this.myLabel5.AutoSize = true;
            this.myLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel5.ForeColor = System.Drawing.Color.White;
            this.myLabel5.Location = new System.Drawing.Point(3, 0);
            this.myLabel5.Name = "myLabel5";
            this.myLabel5.Size = new System.Drawing.Size(114, 26);
            this.myLabel5.TabIndex = 0;
            this.myLabel5.Text = "Camera Name";
            this.myLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton5
            // 
            this.myToggleButton5.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton5.FlatAppearance.BorderSize = 0;
            this.myToggleButton5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton5.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton5.Image")));
            this.myToggleButton5.IsConnect = false;
            this.myToggleButton5.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton5.Name = "myToggleButton5";
            this.myToggleButton5.Size = new System.Drawing.Size(112, 29);
            this.myToggleButton5.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton5.TabIndex = 2;
            this.myToggleButton5.Test = 0;
            this.myToggleButton5.Text = " Connect";
            this.myToggleButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton5.UseVisualStyleBackColor = false;
            this.myToggleButton5.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox9
            // 
            this.roundedTextBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox9.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox9.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox9.Location = new System.Drawing.Point(124, 29);
            this.roundedTextBox9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox9.Name = "roundedTextBox9";
            this.roundedTextBox9.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox9.TabIndex = 5;
            // 
            // myComboBox17
            // 
            this.myComboBox17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox17.BackColor = System.Drawing.Color.Black;
            this.myComboBox17.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox17.ForeColor = System.Drawing.Color.White;
            this.myComboBox17.FormattingEnabled = true;
            this.myComboBox17.Location = new System.Drawing.Point(123, 55);
            this.myComboBox17.Name = "myComboBox17";
            this.myComboBox17.Size = new System.Drawing.Size(177, 21);
            this.myComboBox17.TabIndex = 6;
            this.myComboBox17.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox18
            // 
            this.myComboBox18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox18.BackColor = System.Drawing.Color.Black;
            this.myComboBox18.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox18.ForeColor = System.Drawing.Color.White;
            this.myComboBox18.FormattingEnabled = true;
            this.myComboBox18.Location = new System.Drawing.Point(123, 81);
            this.myComboBox18.Name = "myComboBox18";
            this.myComboBox18.Size = new System.Drawing.Size(177, 21);
            this.myComboBox18.TabIndex = 6;
            // 
            // myRadioButton10
            // 
            this.myRadioButton10.AutoSize = true;
            this.myRadioButton10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton10.ForeColor = System.Drawing.Color.White;
            this.myRadioButton10.Location = new System.Drawing.Point(3, 29);
            this.myRadioButton10.Name = "myRadioButton10";
            this.myRadioButton10.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton10.TabIndex = 7;
            this.myRadioButton10.TabStop = true;
            this.myRadioButton10.Text = "IP Camera";
            this.myRadioButton10.UseVisualStyleBackColor = true;
            // 
            // myRadioButton11
            // 
            this.myRadioButton11.AutoSize = true;
            this.myRadioButton11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton11.ForeColor = System.Drawing.Color.White;
            this.myRadioButton11.Location = new System.Drawing.Point(3, 55);
            this.myRadioButton11.Name = "myRadioButton11";
            this.myRadioButton11.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton11.TabIndex = 7;
            this.myRadioButton11.TabStop = true;
            this.myRadioButton11.Text = "NDI Camera";
            this.myRadioButton11.UseVisualStyleBackColor = true;
            // 
            // myRadioButton12
            // 
            this.myRadioButton12.AutoSize = true;
            this.myRadioButton12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton12.ForeColor = System.Drawing.Color.White;
            this.myRadioButton12.Location = new System.Drawing.Point(3, 81);
            this.myRadioButton12.Name = "myRadioButton12";
            this.myRadioButton12.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton12.TabIndex = 7;
            this.myRadioButton12.TabStop = true;
            this.myRadioButton12.Text = "USB Camera";
            this.myRadioButton12.UseVisualStyleBackColor = true;
            // 
            // camera2GroupBox
            // 
            this.camera2GroupBox.Controls.Add(this.cameraGroup2TableLayoutPanel);
            this.camera2GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera2GroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.camera2GroupBox.Location = new System.Drawing.Point(3, 169);
            this.camera2GroupBox.Name = "camera2GroupBox";
            this.camera2GroupBox.Size = new System.Drawing.Size(309, 160);
            this.camera2GroupBox.TabIndex = 0;
            this.camera2GroupBox.TabStop = false;
            this.camera2GroupBox.Text = "Camera2";
            // 
            // cameraGroup2TableLayoutPanel
            // 
            this.cameraGroup2TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup2TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup2TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myCheckBox1, 1, 4);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.roundedTextBox2, 1, 0);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myLabel2, 0, 0);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myToggleButton2, 0, 4);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.roundedTextBox3, 1, 1);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myComboBox11, 1, 2);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myComboBox12, 1, 3);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myRadioButton1, 0, 1);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myRadioButton2, 0, 2);
            this.cameraGroup2TableLayoutPanel.Controls.Add(this.myRadioButton3, 0, 3);
            this.cameraGroup2TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup2TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup2TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup2TableLayoutPanel.Name = "cameraGroup2TableLayoutPanel";
            this.cameraGroup2TableLayoutPanel.RowCount = 5;
            this.cameraGroup2TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup2TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup2TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup2TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup2TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup2TableLayoutPanel.Size = new System.Drawing.Size(303, 141);
            this.cameraGroup2TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox1
            // 
            this.myCheckBox1.AutoSize = true;
            this.myCheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox1.ForeColor = System.Drawing.Color.White;
            this.myCheckBox1.Location = new System.Drawing.Point(124, 108);
            this.myCheckBox1.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox1.Name = "myCheckBox1";
            this.myCheckBox1.Size = new System.Drawing.Size(175, 29);
            this.myCheckBox1.TabIndex = 3;
            this.myCheckBox1.Text = "NDI Output";
            this.myCheckBox1.UseVisualStyleBackColor = false;
            this.myCheckBox1.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox2
            // 
            this.roundedTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox2.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox2.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox2.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox2.MaxLength = 9;
            this.roundedTextBox2.Name = "roundedTextBox2";
            this.roundedTextBox2.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox2.TabIndex = 0;
            this.roundedTextBox2.Text = "Camera #1";
            // 
            // myLabel2
            // 
            this.myLabel2.AutoSize = true;
            this.myLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel2.ForeColor = System.Drawing.Color.White;
            this.myLabel2.Location = new System.Drawing.Point(3, 0);
            this.myLabel2.Name = "myLabel2";
            this.myLabel2.Size = new System.Drawing.Size(114, 26);
            this.myLabel2.TabIndex = 0;
            this.myLabel2.Text = "Camera Name";
            this.myLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton2
            // 
            this.myToggleButton2.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton2.FlatAppearance.BorderSize = 0;
            this.myToggleButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton2.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton2.Image")));
            this.myToggleButton2.IsConnect = false;
            this.myToggleButton2.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton2.Name = "myToggleButton2";
            this.myToggleButton2.Size = new System.Drawing.Size(112, 29);
            this.myToggleButton2.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton2.TabIndex = 2;
            this.myToggleButton2.Test = 0;
            this.myToggleButton2.Text = " Connect";
            this.myToggleButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton2.UseVisualStyleBackColor = false;
            this.myToggleButton2.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox3
            // 
            this.roundedTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox3.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox3.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox3.Location = new System.Drawing.Point(124, 29);
            this.roundedTextBox3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox3.Name = "roundedTextBox3";
            this.roundedTextBox3.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox3.TabIndex = 5;
            // 
            // myComboBox11
            // 
            this.myComboBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox11.BackColor = System.Drawing.Color.Black;
            this.myComboBox11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox11.ForeColor = System.Drawing.Color.White;
            this.myComboBox11.FormattingEnabled = true;
            this.myComboBox11.Location = new System.Drawing.Point(123, 55);
            this.myComboBox11.Name = "myComboBox11";
            this.myComboBox11.Size = new System.Drawing.Size(177, 21);
            this.myComboBox11.TabIndex = 6;
            this.myComboBox11.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox12
            // 
            this.myComboBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox12.BackColor = System.Drawing.Color.Black;
            this.myComboBox12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox12.ForeColor = System.Drawing.Color.White;
            this.myComboBox12.FormattingEnabled = true;
            this.myComboBox12.Location = new System.Drawing.Point(123, 81);
            this.myComboBox12.Name = "myComboBox12";
            this.myComboBox12.Size = new System.Drawing.Size(177, 21);
            this.myComboBox12.TabIndex = 6;
            // 
            // myRadioButton1
            // 
            this.myRadioButton1.AutoSize = true;
            this.myRadioButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton1.ForeColor = System.Drawing.Color.White;
            this.myRadioButton1.Location = new System.Drawing.Point(3, 29);
            this.myRadioButton1.Name = "myRadioButton1";
            this.myRadioButton1.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton1.TabIndex = 7;
            this.myRadioButton1.TabStop = true;
            this.myRadioButton1.Text = "IP Camera";
            this.myRadioButton1.UseVisualStyleBackColor = true;
            // 
            // myRadioButton2
            // 
            this.myRadioButton2.AutoSize = true;
            this.myRadioButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton2.ForeColor = System.Drawing.Color.White;
            this.myRadioButton2.Location = new System.Drawing.Point(3, 55);
            this.myRadioButton2.Name = "myRadioButton2";
            this.myRadioButton2.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton2.TabIndex = 7;
            this.myRadioButton2.TabStop = true;
            this.myRadioButton2.Text = "NDI Camera";
            this.myRadioButton2.UseVisualStyleBackColor = true;
            // 
            // myRadioButton3
            // 
            this.myRadioButton3.AutoSize = true;
            this.myRadioButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton3.ForeColor = System.Drawing.Color.White;
            this.myRadioButton3.Location = new System.Drawing.Point(3, 81);
            this.myRadioButton3.Name = "myRadioButton3";
            this.myRadioButton3.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton3.TabIndex = 7;
            this.myRadioButton3.TabStop = true;
            this.myRadioButton3.Text = "USB Camera";
            this.myRadioButton3.UseVisualStyleBackColor = true;
            // 
            // camera6GroupBox
            // 
            this.camera6GroupBox.Controls.Add(this.cameraGroup6TableLayoutPanel);
            this.camera6GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera6GroupBox.Location = new System.Drawing.Point(318, 169);
            this.camera6GroupBox.Name = "camera6GroupBox";
            this.camera6GroupBox.Size = new System.Drawing.Size(309, 160);
            this.camera6GroupBox.TabIndex = 0;
            this.camera6GroupBox.TabStop = false;
            this.camera6GroupBox.Text = "Camera6";
            // 
            // cameraGroup6TableLayoutPanel
            // 
            this.cameraGroup6TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup6TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup6TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myCheckBox5, 1, 4);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.roundedTextBox10, 1, 0);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myLabel6, 0, 0);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myToggleButton6, 0, 4);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.roundedTextBox11, 1, 1);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myComboBox19, 1, 2);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myComboBox20, 1, 3);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myRadioButton13, 0, 1);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myRadioButton14, 0, 2);
            this.cameraGroup6TableLayoutPanel.Controls.Add(this.myRadioButton15, 0, 3);
            this.cameraGroup6TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup6TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup6TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup6TableLayoutPanel.Name = "cameraGroup6TableLayoutPanel";
            this.cameraGroup6TableLayoutPanel.RowCount = 5;
            this.cameraGroup6TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup6TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup6TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup6TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup6TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup6TableLayoutPanel.Size = new System.Drawing.Size(303, 141);
            this.cameraGroup6TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox5
            // 
            this.myCheckBox5.AutoSize = true;
            this.myCheckBox5.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox5.ForeColor = System.Drawing.Color.White;
            this.myCheckBox5.Location = new System.Drawing.Point(124, 108);
            this.myCheckBox5.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox5.Name = "myCheckBox5";
            this.myCheckBox5.Size = new System.Drawing.Size(175, 29);
            this.myCheckBox5.TabIndex = 3;
            this.myCheckBox5.Text = "NDI Output";
            this.myCheckBox5.UseVisualStyleBackColor = false;
            this.myCheckBox5.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox10
            // 
            this.roundedTextBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox10.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox10.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox10.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox10.MaxLength = 9;
            this.roundedTextBox10.Name = "roundedTextBox10";
            this.roundedTextBox10.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox10.TabIndex = 0;
            this.roundedTextBox10.Text = "Camera #1";
            // 
            // myLabel6
            // 
            this.myLabel6.AutoSize = true;
            this.myLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel6.ForeColor = System.Drawing.Color.White;
            this.myLabel6.Location = new System.Drawing.Point(3, 0);
            this.myLabel6.Name = "myLabel6";
            this.myLabel6.Size = new System.Drawing.Size(114, 26);
            this.myLabel6.TabIndex = 0;
            this.myLabel6.Text = "Camera Name";
            this.myLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton6
            // 
            this.myToggleButton6.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton6.FlatAppearance.BorderSize = 0;
            this.myToggleButton6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton6.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton6.Image")));
            this.myToggleButton6.IsConnect = false;
            this.myToggleButton6.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton6.Name = "myToggleButton6";
            this.myToggleButton6.Size = new System.Drawing.Size(112, 29);
            this.myToggleButton6.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton6.TabIndex = 2;
            this.myToggleButton6.Test = 0;
            this.myToggleButton6.Text = " Connect";
            this.myToggleButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton6.UseVisualStyleBackColor = false;
            this.myToggleButton6.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox11
            // 
            this.roundedTextBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox11.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox11.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox11.Location = new System.Drawing.Point(124, 29);
            this.roundedTextBox11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox11.Name = "roundedTextBox11";
            this.roundedTextBox11.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox11.TabIndex = 5;
            // 
            // myComboBox19
            // 
            this.myComboBox19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox19.BackColor = System.Drawing.Color.Black;
            this.myComboBox19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox19.ForeColor = System.Drawing.Color.White;
            this.myComboBox19.FormattingEnabled = true;
            this.myComboBox19.Location = new System.Drawing.Point(123, 55);
            this.myComboBox19.Name = "myComboBox19";
            this.myComboBox19.Size = new System.Drawing.Size(177, 21);
            this.myComboBox19.TabIndex = 6;
            this.myComboBox19.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox20
            // 
            this.myComboBox20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox20.BackColor = System.Drawing.Color.Black;
            this.myComboBox20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox20.ForeColor = System.Drawing.Color.White;
            this.myComboBox20.FormattingEnabled = true;
            this.myComboBox20.Location = new System.Drawing.Point(123, 81);
            this.myComboBox20.Name = "myComboBox20";
            this.myComboBox20.Size = new System.Drawing.Size(177, 21);
            this.myComboBox20.TabIndex = 6;
            // 
            // myRadioButton13
            // 
            this.myRadioButton13.AutoSize = true;
            this.myRadioButton13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton13.ForeColor = System.Drawing.Color.White;
            this.myRadioButton13.Location = new System.Drawing.Point(3, 29);
            this.myRadioButton13.Name = "myRadioButton13";
            this.myRadioButton13.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton13.TabIndex = 7;
            this.myRadioButton13.TabStop = true;
            this.myRadioButton13.Text = "IP Camera";
            this.myRadioButton13.UseVisualStyleBackColor = true;
            // 
            // myRadioButton14
            // 
            this.myRadioButton14.AutoSize = true;
            this.myRadioButton14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton14.ForeColor = System.Drawing.Color.White;
            this.myRadioButton14.Location = new System.Drawing.Point(3, 55);
            this.myRadioButton14.Name = "myRadioButton14";
            this.myRadioButton14.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton14.TabIndex = 7;
            this.myRadioButton14.TabStop = true;
            this.myRadioButton14.Text = "NDI Camera";
            this.myRadioButton14.UseVisualStyleBackColor = true;
            // 
            // myRadioButton15
            // 
            this.myRadioButton15.AutoSize = true;
            this.myRadioButton15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton15.ForeColor = System.Drawing.Color.White;
            this.myRadioButton15.Location = new System.Drawing.Point(3, 81);
            this.myRadioButton15.Name = "myRadioButton15";
            this.myRadioButton15.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton15.TabIndex = 7;
            this.myRadioButton15.TabStop = true;
            this.myRadioButton15.Text = "USB Camera";
            this.myRadioButton15.UseVisualStyleBackColor = true;
            // 
            // camera3GroupBox
            // 
            this.camera3GroupBox.Controls.Add(this.cameraGroup3TableLayoutPanel);
            this.camera3GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera3GroupBox.Location = new System.Drawing.Point(3, 335);
            this.camera3GroupBox.Name = "camera3GroupBox";
            this.camera3GroupBox.Size = new System.Drawing.Size(309, 160);
            this.camera3GroupBox.TabIndex = 0;
            this.camera3GroupBox.TabStop = false;
            this.camera3GroupBox.Text = "Camera3";
            // 
            // cameraGroup3TableLayoutPanel
            // 
            this.cameraGroup3TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myCheckBox2, 1, 4);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.roundedTextBox4, 1, 0);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myLabel3, 0, 0);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myToggleButton3, 0, 4);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.roundedTextBox5, 1, 1);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myComboBox13, 1, 2);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myComboBox14, 1, 3);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myRadioButton4, 0, 1);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myRadioButton5, 0, 2);
            this.cameraGroup3TableLayoutPanel.Controls.Add(this.myRadioButton6, 0, 3);
            this.cameraGroup3TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup3TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup3TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup3TableLayoutPanel.Name = "cameraGroup3TableLayoutPanel";
            this.cameraGroup3TableLayoutPanel.RowCount = 5;
            this.cameraGroup3TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup3TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup3TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup3TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup3TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup3TableLayoutPanel.Size = new System.Drawing.Size(303, 141);
            this.cameraGroup3TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox2
            // 
            this.myCheckBox2.AutoSize = true;
            this.myCheckBox2.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox2.ForeColor = System.Drawing.Color.White;
            this.myCheckBox2.Location = new System.Drawing.Point(124, 108);
            this.myCheckBox2.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox2.Name = "myCheckBox2";
            this.myCheckBox2.Size = new System.Drawing.Size(175, 29);
            this.myCheckBox2.TabIndex = 3;
            this.myCheckBox2.Text = "NDI Output";
            this.myCheckBox2.UseVisualStyleBackColor = false;
            this.myCheckBox2.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox4
            // 
            this.roundedTextBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox4.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox4.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox4.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox4.MaxLength = 9;
            this.roundedTextBox4.Name = "roundedTextBox4";
            this.roundedTextBox4.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox4.TabIndex = 0;
            this.roundedTextBox4.Text = "Camera #1";
            // 
            // myLabel3
            // 
            this.myLabel3.AutoSize = true;
            this.myLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel3.ForeColor = System.Drawing.Color.White;
            this.myLabel3.Location = new System.Drawing.Point(3, 0);
            this.myLabel3.Name = "myLabel3";
            this.myLabel3.Size = new System.Drawing.Size(114, 26);
            this.myLabel3.TabIndex = 0;
            this.myLabel3.Text = "Camera Name";
            this.myLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton3
            // 
            this.myToggleButton3.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton3.FlatAppearance.BorderSize = 0;
            this.myToggleButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton3.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton3.Image")));
            this.myToggleButton3.IsConnect = false;
            this.myToggleButton3.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton3.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton3.Name = "myToggleButton3";
            this.myToggleButton3.Size = new System.Drawing.Size(112, 29);
            this.myToggleButton3.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton3.TabIndex = 2;
            this.myToggleButton3.Test = 0;
            this.myToggleButton3.Text = " Connect";
            this.myToggleButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton3.UseVisualStyleBackColor = false;
            this.myToggleButton3.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox5
            // 
            this.roundedTextBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox5.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox5.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox5.Location = new System.Drawing.Point(124, 29);
            this.roundedTextBox5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox5.Name = "roundedTextBox5";
            this.roundedTextBox5.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox5.TabIndex = 5;
            // 
            // myComboBox13
            // 
            this.myComboBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox13.BackColor = System.Drawing.Color.Black;
            this.myComboBox13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox13.ForeColor = System.Drawing.Color.White;
            this.myComboBox13.FormattingEnabled = true;
            this.myComboBox13.Location = new System.Drawing.Point(123, 55);
            this.myComboBox13.Name = "myComboBox13";
            this.myComboBox13.Size = new System.Drawing.Size(177, 21);
            this.myComboBox13.TabIndex = 6;
            this.myComboBox13.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox14
            // 
            this.myComboBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox14.BackColor = System.Drawing.Color.Black;
            this.myComboBox14.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox14.ForeColor = System.Drawing.Color.White;
            this.myComboBox14.FormattingEnabled = true;
            this.myComboBox14.Location = new System.Drawing.Point(123, 81);
            this.myComboBox14.Name = "myComboBox14";
            this.myComboBox14.Size = new System.Drawing.Size(177, 21);
            this.myComboBox14.TabIndex = 6;
            // 
            // myRadioButton4
            // 
            this.myRadioButton4.AutoSize = true;
            this.myRadioButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton4.ForeColor = System.Drawing.Color.White;
            this.myRadioButton4.Location = new System.Drawing.Point(3, 29);
            this.myRadioButton4.Name = "myRadioButton4";
            this.myRadioButton4.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton4.TabIndex = 7;
            this.myRadioButton4.TabStop = true;
            this.myRadioButton4.Text = "IP Camera";
            this.myRadioButton4.UseVisualStyleBackColor = true;
            // 
            // myRadioButton5
            // 
            this.myRadioButton5.AutoSize = true;
            this.myRadioButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton5.ForeColor = System.Drawing.Color.White;
            this.myRadioButton5.Location = new System.Drawing.Point(3, 55);
            this.myRadioButton5.Name = "myRadioButton5";
            this.myRadioButton5.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton5.TabIndex = 7;
            this.myRadioButton5.TabStop = true;
            this.myRadioButton5.Text = "NDI Camera";
            this.myRadioButton5.UseVisualStyleBackColor = true;
            // 
            // myRadioButton6
            // 
            this.myRadioButton6.AutoSize = true;
            this.myRadioButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton6.ForeColor = System.Drawing.Color.White;
            this.myRadioButton6.Location = new System.Drawing.Point(3, 81);
            this.myRadioButton6.Name = "myRadioButton6";
            this.myRadioButton6.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton6.TabIndex = 7;
            this.myRadioButton6.TabStop = true;
            this.myRadioButton6.Text = "USB Camera";
            this.myRadioButton6.UseVisualStyleBackColor = true;
            // 
            // camera7GroupBox
            // 
            this.camera7GroupBox.Controls.Add(this.cameraGroup7TableLayoutPanel);
            this.camera7GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera7GroupBox.Location = new System.Drawing.Point(318, 335);
            this.camera7GroupBox.Name = "camera7GroupBox";
            this.camera7GroupBox.Size = new System.Drawing.Size(309, 160);
            this.camera7GroupBox.TabIndex = 0;
            this.camera7GroupBox.TabStop = false;
            this.camera7GroupBox.Text = "Camera7";
            // 
            // cameraGroup7TableLayoutPanel
            // 
            this.cameraGroup7TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup7TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup7TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myCheckBox6, 1, 4);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.roundedTextBox12, 1, 0);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myLabel7, 0, 0);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myToggleButton7, 0, 4);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.roundedTextBox13, 1, 1);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myComboBox21, 1, 2);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myComboBox22, 1, 3);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myRadioButton16, 0, 1);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myRadioButton17, 0, 2);
            this.cameraGroup7TableLayoutPanel.Controls.Add(this.myRadioButton18, 0, 3);
            this.cameraGroup7TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup7TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup7TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup7TableLayoutPanel.Name = "cameraGroup7TableLayoutPanel";
            this.cameraGroup7TableLayoutPanel.RowCount = 5;
            this.cameraGroup7TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup7TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup7TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup7TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup7TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup7TableLayoutPanel.Size = new System.Drawing.Size(303, 141);
            this.cameraGroup7TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox6
            // 
            this.myCheckBox6.AutoSize = true;
            this.myCheckBox6.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox6.ForeColor = System.Drawing.Color.White;
            this.myCheckBox6.Location = new System.Drawing.Point(124, 108);
            this.myCheckBox6.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox6.Name = "myCheckBox6";
            this.myCheckBox6.Size = new System.Drawing.Size(175, 29);
            this.myCheckBox6.TabIndex = 3;
            this.myCheckBox6.Text = "NDI Output";
            this.myCheckBox6.UseVisualStyleBackColor = false;
            this.myCheckBox6.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox12
            // 
            this.roundedTextBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox12.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox12.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox12.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox12.MaxLength = 9;
            this.roundedTextBox12.Name = "roundedTextBox12";
            this.roundedTextBox12.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox12.TabIndex = 0;
            this.roundedTextBox12.Text = "Camera #1";
            // 
            // myLabel7
            // 
            this.myLabel7.AutoSize = true;
            this.myLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel7.ForeColor = System.Drawing.Color.White;
            this.myLabel7.Location = new System.Drawing.Point(3, 0);
            this.myLabel7.Name = "myLabel7";
            this.myLabel7.Size = new System.Drawing.Size(114, 26);
            this.myLabel7.TabIndex = 0;
            this.myLabel7.Text = "Camera Name";
            this.myLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton7
            // 
            this.myToggleButton7.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton7.FlatAppearance.BorderSize = 0;
            this.myToggleButton7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton7.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton7.Image")));
            this.myToggleButton7.IsConnect = false;
            this.myToggleButton7.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton7.Name = "myToggleButton7";
            this.myToggleButton7.Size = new System.Drawing.Size(112, 29);
            this.myToggleButton7.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton7.TabIndex = 2;
            this.myToggleButton7.Test = 0;
            this.myToggleButton7.Text = " Connect";
            this.myToggleButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton7.UseVisualStyleBackColor = false;
            this.myToggleButton7.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox13
            // 
            this.roundedTextBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox13.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox13.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox13.Location = new System.Drawing.Point(124, 29);
            this.roundedTextBox13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox13.Name = "roundedTextBox13";
            this.roundedTextBox13.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox13.TabIndex = 5;
            // 
            // myComboBox21
            // 
            this.myComboBox21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox21.BackColor = System.Drawing.Color.Black;
            this.myComboBox21.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox21.ForeColor = System.Drawing.Color.White;
            this.myComboBox21.FormattingEnabled = true;
            this.myComboBox21.Location = new System.Drawing.Point(123, 55);
            this.myComboBox21.Name = "myComboBox21";
            this.myComboBox21.Size = new System.Drawing.Size(177, 21);
            this.myComboBox21.TabIndex = 6;
            this.myComboBox21.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox22
            // 
            this.myComboBox22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox22.BackColor = System.Drawing.Color.Black;
            this.myComboBox22.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox22.ForeColor = System.Drawing.Color.White;
            this.myComboBox22.FormattingEnabled = true;
            this.myComboBox22.Location = new System.Drawing.Point(123, 81);
            this.myComboBox22.Name = "myComboBox22";
            this.myComboBox22.Size = new System.Drawing.Size(177, 21);
            this.myComboBox22.TabIndex = 6;
            // 
            // myRadioButton16
            // 
            this.myRadioButton16.AutoSize = true;
            this.myRadioButton16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton16.ForeColor = System.Drawing.Color.White;
            this.myRadioButton16.Location = new System.Drawing.Point(3, 29);
            this.myRadioButton16.Name = "myRadioButton16";
            this.myRadioButton16.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton16.TabIndex = 7;
            this.myRadioButton16.TabStop = true;
            this.myRadioButton16.Text = "IP Camera";
            this.myRadioButton16.UseVisualStyleBackColor = true;
            // 
            // myRadioButton17
            // 
            this.myRadioButton17.AutoSize = true;
            this.myRadioButton17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton17.ForeColor = System.Drawing.Color.White;
            this.myRadioButton17.Location = new System.Drawing.Point(3, 55);
            this.myRadioButton17.Name = "myRadioButton17";
            this.myRadioButton17.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton17.TabIndex = 7;
            this.myRadioButton17.TabStop = true;
            this.myRadioButton17.Text = "NDI Camera";
            this.myRadioButton17.UseVisualStyleBackColor = true;
            // 
            // myRadioButton18
            // 
            this.myRadioButton18.AutoSize = true;
            this.myRadioButton18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton18.ForeColor = System.Drawing.Color.White;
            this.myRadioButton18.Location = new System.Drawing.Point(3, 81);
            this.myRadioButton18.Name = "myRadioButton18";
            this.myRadioButton18.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton18.TabIndex = 7;
            this.myRadioButton18.TabStop = true;
            this.myRadioButton18.Text = "USB Camera";
            this.myRadioButton18.UseVisualStyleBackColor = true;
            // 
            // camera4GroupBox
            // 
            this.camera4GroupBox.Controls.Add(this.cameraGroup4TableLayoutPanel);
            this.camera4GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.camera4GroupBox.Location = new System.Drawing.Point(3, 501);
            this.camera4GroupBox.Name = "camera4GroupBox";
            this.camera4GroupBox.Size = new System.Drawing.Size(309, 163);
            this.camera4GroupBox.TabIndex = 0;
            this.camera4GroupBox.TabStop = false;
            this.camera4GroupBox.Text = "Camera4";
            // 
            // cameraGroup4TableLayoutPanel
            // 
            this.cameraGroup4TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup4TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup4TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myCheckBox3, 1, 4);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.roundedTextBox6, 1, 0);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myLabel4, 0, 0);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myToggleButton4, 0, 4);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.roundedTextBox7, 1, 1);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myComboBox15, 1, 2);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myComboBox16, 1, 3);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myRadioButton7, 0, 1);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myRadioButton8, 0, 2);
            this.cameraGroup4TableLayoutPanel.Controls.Add(this.myRadioButton9, 0, 3);
            this.cameraGroup4TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup4TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup4TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup4TableLayoutPanel.Name = "cameraGroup4TableLayoutPanel";
            this.cameraGroup4TableLayoutPanel.RowCount = 5;
            this.cameraGroup4TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup4TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup4TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup4TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup4TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup4TableLayoutPanel.Size = new System.Drawing.Size(303, 144);
            this.cameraGroup4TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox3
            // 
            this.myCheckBox3.AutoSize = true;
            this.myCheckBox3.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox3.ForeColor = System.Drawing.Color.White;
            this.myCheckBox3.Location = new System.Drawing.Point(124, 112);
            this.myCheckBox3.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox3.Name = "myCheckBox3";
            this.myCheckBox3.Size = new System.Drawing.Size(175, 28);
            this.myCheckBox3.TabIndex = 3;
            this.myCheckBox3.Text = "NDI Output";
            this.myCheckBox3.UseVisualStyleBackColor = false;
            this.myCheckBox3.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox6
            // 
            this.roundedTextBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox6.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox6.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox6.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox6.MaxLength = 9;
            this.roundedTextBox6.Name = "roundedTextBox6";
            this.roundedTextBox6.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox6.TabIndex = 0;
            this.roundedTextBox6.Text = "Camera #1";
            // 
            // myLabel4
            // 
            this.myLabel4.AutoSize = true;
            this.myLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel4.ForeColor = System.Drawing.Color.White;
            this.myLabel4.Location = new System.Drawing.Point(3, 0);
            this.myLabel4.Name = "myLabel4";
            this.myLabel4.Size = new System.Drawing.Size(114, 27);
            this.myLabel4.TabIndex = 0;
            this.myLabel4.Text = "Camera Name";
            this.myLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton4
            // 
            this.myToggleButton4.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton4.FlatAppearance.BorderSize = 0;
            this.myToggleButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton4.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton4.Image")));
            this.myToggleButton4.IsConnect = false;
            this.myToggleButton4.Location = new System.Drawing.Point(4, 112);
            this.myToggleButton4.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton4.Name = "myToggleButton4";
            this.myToggleButton4.Size = new System.Drawing.Size(112, 28);
            this.myToggleButton4.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton4.TabIndex = 2;
            this.myToggleButton4.Test = 0;
            this.myToggleButton4.Text = " Connect";
            this.myToggleButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton4.UseVisualStyleBackColor = false;
            this.myToggleButton4.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox7
            // 
            this.roundedTextBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox7.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox7.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox7.Location = new System.Drawing.Point(124, 30);
            this.roundedTextBox7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox7.Name = "roundedTextBox7";
            this.roundedTextBox7.Size = new System.Drawing.Size(175, 20);
            this.roundedTextBox7.TabIndex = 5;
            // 
            // myComboBox15
            // 
            this.myComboBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox15.BackColor = System.Drawing.Color.Black;
            this.myComboBox15.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox15.ForeColor = System.Drawing.Color.White;
            this.myComboBox15.FormattingEnabled = true;
            this.myComboBox15.Location = new System.Drawing.Point(123, 57);
            this.myComboBox15.Name = "myComboBox15";
            this.myComboBox15.Size = new System.Drawing.Size(177, 21);
            this.myComboBox15.TabIndex = 6;
            this.myComboBox15.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox16
            // 
            this.myComboBox16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox16.BackColor = System.Drawing.Color.Black;
            this.myComboBox16.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox16.ForeColor = System.Drawing.Color.White;
            this.myComboBox16.FormattingEnabled = true;
            this.myComboBox16.Location = new System.Drawing.Point(123, 84);
            this.myComboBox16.Name = "myComboBox16";
            this.myComboBox16.Size = new System.Drawing.Size(177, 21);
            this.myComboBox16.TabIndex = 6;
            // 
            // myRadioButton7
            // 
            this.myRadioButton7.AutoSize = true;
            this.myRadioButton7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton7.ForeColor = System.Drawing.Color.White;
            this.myRadioButton7.Location = new System.Drawing.Point(3, 30);
            this.myRadioButton7.Name = "myRadioButton7";
            this.myRadioButton7.Size = new System.Drawing.Size(114, 21);
            this.myRadioButton7.TabIndex = 7;
            this.myRadioButton7.TabStop = true;
            this.myRadioButton7.Text = "IP Camera";
            this.myRadioButton7.UseVisualStyleBackColor = true;
            // 
            // myRadioButton8
            // 
            this.myRadioButton8.AutoSize = true;
            this.myRadioButton8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton8.ForeColor = System.Drawing.Color.White;
            this.myRadioButton8.Location = new System.Drawing.Point(3, 57);
            this.myRadioButton8.Name = "myRadioButton8";
            this.myRadioButton8.Size = new System.Drawing.Size(114, 21);
            this.myRadioButton8.TabIndex = 7;
            this.myRadioButton8.TabStop = true;
            this.myRadioButton8.Text = "NDI Camera";
            this.myRadioButton8.UseVisualStyleBackColor = true;
            // 
            // myRadioButton9
            // 
            this.myRadioButton9.AutoSize = true;
            this.myRadioButton9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton9.ForeColor = System.Drawing.Color.White;
            this.myRadioButton9.Location = new System.Drawing.Point(3, 84);
            this.myRadioButton9.Name = "myRadioButton9";
            this.myRadioButton9.Size = new System.Drawing.Size(114, 21);
            this.myRadioButton9.TabIndex = 7;
            this.myRadioButton9.TabStop = true;
            this.myRadioButton9.Text = "USB Camera";
            this.myRadioButton9.UseVisualStyleBackColor = true;
            // 
            // camera8GroupBox
            // 
            this.camera8GroupBox.Controls.Add(this.cameraGroup8TableLayoutPanel);
            this.camera8GroupBox.Location = new System.Drawing.Point(318, 501);
            this.camera8GroupBox.Name = "camera8GroupBox";
            this.camera8GroupBox.Size = new System.Drawing.Size(304, 159);
            this.camera8GroupBox.TabIndex = 0;
            this.camera8GroupBox.TabStop = false;
            this.camera8GroupBox.Text = "Camera8";
            // 
            // cameraGroup8TableLayoutPanel
            // 
            this.cameraGroup8TableLayoutPanel.ColumnCount = 2;
            this.cameraGroup8TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.cameraGroup8TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myCheckBox7, 1, 4);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.roundedTextBox14, 1, 0);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myLabel8, 0, 0);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myToggleButton8, 0, 4);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.roundedTextBox15, 1, 1);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myComboBox23, 1, 2);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myComboBox24, 1, 3);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myRadioButton19, 0, 1);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myRadioButton20, 0, 2);
            this.cameraGroup8TableLayoutPanel.Controls.Add(this.myRadioButton21, 0, 3);
            this.cameraGroup8TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraGroup8TableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cameraGroup8TableLayoutPanel.Margin = new System.Windows.Forms.Padding(5);
            this.cameraGroup8TableLayoutPanel.Name = "cameraGroup8TableLayoutPanel";
            this.cameraGroup8TableLayoutPanel.RowCount = 5;
            this.cameraGroup8TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup8TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup8TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup8TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.cameraGroup8TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.cameraGroup8TableLayoutPanel.Size = new System.Drawing.Size(298, 140);
            this.cameraGroup8TableLayoutPanel.TabIndex = 0;
            // 
            // myCheckBox7
            // 
            this.myCheckBox7.AutoSize = true;
            this.myCheckBox7.BackColor = System.Drawing.Color.Transparent;
            this.myCheckBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myCheckBox7.ForeColor = System.Drawing.Color.White;
            this.myCheckBox7.Location = new System.Drawing.Point(124, 108);
            this.myCheckBox7.Margin = new System.Windows.Forms.Padding(4);
            this.myCheckBox7.Name = "myCheckBox7";
            this.myCheckBox7.Size = new System.Drawing.Size(170, 28);
            this.myCheckBox7.TabIndex = 3;
            this.myCheckBox7.Text = "NDI Output";
            this.myCheckBox7.UseVisualStyleBackColor = false;
            this.myCheckBox7.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // roundedTextBox14
            // 
            this.roundedTextBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox14.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox14.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox14.Location = new System.Drawing.Point(124, 3);
            this.roundedTextBox14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox14.MaxLength = 9;
            this.roundedTextBox14.Name = "roundedTextBox14";
            this.roundedTextBox14.Size = new System.Drawing.Size(170, 20);
            this.roundedTextBox14.TabIndex = 0;
            this.roundedTextBox14.Text = "Camera #1";
            // 
            // myLabel8
            // 
            this.myLabel8.AutoSize = true;
            this.myLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel8.ForeColor = System.Drawing.Color.White;
            this.myLabel8.Location = new System.Drawing.Point(3, 0);
            this.myLabel8.Name = "myLabel8";
            this.myLabel8.Size = new System.Drawing.Size(114, 26);
            this.myLabel8.TabIndex = 0;
            this.myLabel8.Text = "Camera Name";
            this.myLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myToggleButton8
            // 
            this.myToggleButton8.BackColor = System.Drawing.Color.Gray;
            this.myToggleButton8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myToggleButton8.FlatAppearance.BorderSize = 0;
            this.myToggleButton8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.myToggleButton8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.myToggleButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myToggleButton8.Image = ((System.Drawing.Image)(resources.GetObject("myToggleButton8.Image")));
            this.myToggleButton8.IsConnect = false;
            this.myToggleButton8.Location = new System.Drawing.Point(4, 108);
            this.myToggleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.myToggleButton8.Name = "myToggleButton8";
            this.myToggleButton8.Size = new System.Drawing.Size(112, 28);
            this.myToggleButton8.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.myToggleButton8.TabIndex = 2;
            this.myToggleButton8.Test = 0;
            this.myToggleButton8.Text = " Connect";
            this.myToggleButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.myToggleButton8.UseVisualStyleBackColor = false;
            this.myToggleButton8.Click += new System.EventHandler(this.cameraConnectButton_Click);
            // 
            // roundedTextBox15
            // 
            this.roundedTextBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.roundedTextBox15.BackColor = System.Drawing.Color.Black;
            this.roundedTextBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.roundedTextBox15.ForeColor = System.Drawing.Color.White;
            this.roundedTextBox15.Location = new System.Drawing.Point(124, 29);
            this.roundedTextBox15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.roundedTextBox15.Name = "roundedTextBox15";
            this.roundedTextBox15.Size = new System.Drawing.Size(170, 20);
            this.roundedTextBox15.TabIndex = 5;
            // 
            // myComboBox23
            // 
            this.myComboBox23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox23.BackColor = System.Drawing.Color.Black;
            this.myComboBox23.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox23.ForeColor = System.Drawing.Color.White;
            this.myComboBox23.FormattingEnabled = true;
            this.myComboBox23.Location = new System.Drawing.Point(123, 55);
            this.myComboBox23.Name = "myComboBox23";
            this.myComboBox23.Size = new System.Drawing.Size(172, 21);
            this.myComboBox23.TabIndex = 6;
            this.myComboBox23.SelectedIndexChanged += new System.EventHandler(this.ndiCameraComboBox_SelectedIndexChanged);
            // 
            // myComboBox24
            // 
            this.myComboBox24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox24.BackColor = System.Drawing.Color.Black;
            this.myComboBox24.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox24.ForeColor = System.Drawing.Color.White;
            this.myComboBox24.FormattingEnabled = true;
            this.myComboBox24.Location = new System.Drawing.Point(123, 81);
            this.myComboBox24.Name = "myComboBox24";
            this.myComboBox24.Size = new System.Drawing.Size(172, 21);
            this.myComboBox24.TabIndex = 6;
            // 
            // myRadioButton19
            // 
            this.myRadioButton19.AutoSize = true;
            this.myRadioButton19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton19.ForeColor = System.Drawing.Color.White;
            this.myRadioButton19.Location = new System.Drawing.Point(3, 29);
            this.myRadioButton19.Name = "myRadioButton19";
            this.myRadioButton19.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton19.TabIndex = 7;
            this.myRadioButton19.TabStop = true;
            this.myRadioButton19.Text = "IP Camera";
            this.myRadioButton19.UseVisualStyleBackColor = true;
            // 
            // myRadioButton20
            // 
            this.myRadioButton20.AutoSize = true;
            this.myRadioButton20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton20.ForeColor = System.Drawing.Color.White;
            this.myRadioButton20.Location = new System.Drawing.Point(3, 55);
            this.myRadioButton20.Name = "myRadioButton20";
            this.myRadioButton20.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton20.TabIndex = 7;
            this.myRadioButton20.TabStop = true;
            this.myRadioButton20.Text = "NDI Camera";
            this.myRadioButton20.UseVisualStyleBackColor = true;
            // 
            // myRadioButton21
            // 
            this.myRadioButton21.AutoSize = true;
            this.myRadioButton21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRadioButton21.ForeColor = System.Drawing.Color.White;
            this.myRadioButton21.Location = new System.Drawing.Point(3, 81);
            this.myRadioButton21.Name = "myRadioButton21";
            this.myRadioButton21.Size = new System.Drawing.Size(114, 20);
            this.myRadioButton21.TabIndex = 7;
            this.myRadioButton21.TabStop = true;
            this.myRadioButton21.Text = "USB Camera";
            this.myRadioButton21.UseVisualStyleBackColor = true;
            // 
            // stageSourceGroupBox
            // 
            this.stageSourceGroupBox.Controls.Add(this.stageSourceTableLayoutPanel);
            this.stageSourceGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageSourceGroupBox.ForeColor = System.Drawing.Color.White;
            this.stageSourceGroupBox.Location = new System.Drawing.Point(13, 712);
            this.stageSourceGroupBox.Margin = new System.Windows.Forms.Padding(13, 12, 13, 0);
            this.stageSourceGroupBox.Name = "stageSourceGroupBox";
            this.stageSourceGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.stageSourceGroupBox.Size = new System.Drawing.Size(638, 198);
            this.stageSourceGroupBox.TabIndex = 5;
            this.stageSourceGroupBox.TabStop = false;
            this.stageSourceGroupBox.Text = "Stage Source";
            // 
            // stageSourceTableLayoutPanel
            // 
            this.stageSourceTableLayoutPanel.ColumnCount = 2;
            this.stageSourceTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.stageSourceTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.stageSourceTableLayoutPanel.Controls.Add(this.tableLayoutPanel5, 1, 1);
            this.stageSourceTableLayoutPanel.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.stageSourceTableLayoutPanel.Controls.Add(this.staticRadioButton, 1, 0);
            this.stageSourceTableLayoutPanel.Controls.Add(this.liveRadioButton, 0, 0);
            this.stageSourceTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageSourceTableLayoutPanel.Location = new System.Drawing.Point(4, 17);
            this.stageSourceTableLayoutPanel.Name = "stageSourceTableLayoutPanel";
            this.stageSourceTableLayoutPanel.RowCount = 2;
            this.stageSourceTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.stageSourceTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stageSourceTableLayoutPanel.Size = new System.Drawing.Size(630, 177);
            this.stageSourceTableLayoutPanel.TabIndex = 7;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.myLabel11, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.myLabel10, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.imageNameTextBox, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.staticTextBox, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.stageBrowseButton, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.imageStageNDICheckBox, 1, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(320, 35);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(305, 137);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // myLabel11
            // 
            this.myLabel11.AutoSize = true;
            this.myLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel11.ForeColor = System.Drawing.Color.White;
            this.myLabel11.Location = new System.Drawing.Point(3, 25);
            this.myLabel11.Name = "myLabel11";
            this.myLabel11.Size = new System.Drawing.Size(114, 25);
            this.myLabel11.TabIndex = 6;
            this.myLabel11.Text = "Image Path";
            this.myLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // myLabel10
            // 
            this.myLabel10.AutoSize = true;
            this.myLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel10.ForeColor = System.Drawing.Color.White;
            this.myLabel10.Location = new System.Drawing.Point(3, 0);
            this.myLabel10.Name = "myLabel10";
            this.myLabel10.Size = new System.Drawing.Size(114, 25);
            this.myLabel10.TabIndex = 0;
            this.myLabel10.Text = "Image Name";
            this.myLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // imageNameTextBox
            // 
            this.imageNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.imageNameTextBox.BackColor = System.Drawing.Color.Black;
            this.imageNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageNameTextBox.ForeColor = System.Drawing.Color.White;
            this.imageNameTextBox.Location = new System.Drawing.Point(124, 2);
            this.imageNameTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.imageNameTextBox.Name = "imageNameTextBox";
            this.imageNameTextBox.Size = new System.Drawing.Size(177, 20);
            this.imageNameTextBox.TabIndex = 1;
            this.imageNameTextBox.Text = "Static Image";
            // 
            // staticTextBox
            // 
            this.staticTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.staticTextBox.BackColor = System.Drawing.Color.Black;
            this.staticTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.staticTextBox.ForeColor = System.Drawing.Color.White;
            this.staticTextBox.Location = new System.Drawing.Point(124, 27);
            this.staticTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.staticTextBox.Name = "staticTextBox";
            this.staticTextBox.Size = new System.Drawing.Size(177, 20);
            this.staticTextBox.TabIndex = 5;
            // 
            // stageBrowseButton
            // 
            this.stageBrowseButton.BackColor = System.Drawing.Color.Gray;
            this.stageBrowseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageBrowseButton.FlatAppearance.BorderSize = 0;
            this.stageBrowseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.stageBrowseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.stageBrowseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stageBrowseButton.Location = new System.Drawing.Point(4, 54);
            this.stageBrowseButton.Margin = new System.Windows.Forms.Padding(4);
            this.stageBrowseButton.Name = "stageBrowseButton";
            this.stageBrowseButton.Size = new System.Drawing.Size(112, 27);
            this.stageBrowseButton.TabIndex = 2;
            this.stageBrowseButton.Text = "Browse";
            this.stageBrowseButton.UseVisualStyleBackColor = false;
            this.stageBrowseButton.Click += new System.EventHandler(this.stageBrowseButton_Click);
            // 
            // imageStageNDICheckBox
            // 
            this.imageStageNDICheckBox.AutoSize = true;
            this.imageStageNDICheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageStageNDICheckBox.ForeColor = System.Drawing.Color.White;
            this.imageStageNDICheckBox.Location = new System.Drawing.Point(124, 54);
            this.imageStageNDICheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.imageStageNDICheckBox.Name = "imageStageNDICheckBox";
            this.imageStageNDICheckBox.Size = new System.Drawing.Size(177, 27);
            this.imageStageNDICheckBox.TabIndex = 3;
            this.imageStageNDICheckBox.Text = "NDI Output";
            this.imageStageNDICheckBox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.liveIPCameraTextBox, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.liveStageNDICheckbox, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.liveStageConnectButton, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.myLabel9, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.liveNameTextBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.liveNDICameraComboBox, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.liveUSBCameraComboBox, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.liveIPCameraRadioButton, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.liveNDICameraRadioButton, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.liveUSBCameraRadioButton, 0, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 35);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(305, 137);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // liveIPCameraTextBox
            // 
            this.liveIPCameraTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveIPCameraTextBox.BackColor = System.Drawing.Color.Black;
            this.liveIPCameraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.liveIPCameraTextBox.ForeColor = System.Drawing.Color.White;
            this.liveIPCameraTextBox.Location = new System.Drawing.Point(124, 27);
            this.liveIPCameraTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.liveIPCameraTextBox.Name = "liveIPCameraTextBox";
            this.liveIPCameraTextBox.Size = new System.Drawing.Size(177, 20);
            this.liveIPCameraTextBox.TabIndex = 5;
            // 
            // liveStageNDICheckbox
            // 
            this.liveStageNDICheckbox.AutoSize = true;
            this.liveStageNDICheckbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveStageNDICheckbox.ForeColor = System.Drawing.Color.White;
            this.liveStageNDICheckbox.Location = new System.Drawing.Point(124, 104);
            this.liveStageNDICheckbox.Margin = new System.Windows.Forms.Padding(4);
            this.liveStageNDICheckbox.Name = "liveStageNDICheckbox";
            this.liveStageNDICheckbox.Size = new System.Drawing.Size(177, 29);
            this.liveStageNDICheckbox.TabIndex = 3;
            this.liveStageNDICheckbox.Text = "NDI Output";
            this.liveStageNDICheckbox.UseVisualStyleBackColor = true;
            // 
            // liveStageConnectButton
            // 
            this.liveStageConnectButton.BackColor = System.Drawing.Color.Gray;
            this.liveStageConnectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveStageConnectButton.FlatAppearance.BorderSize = 0;
            this.liveStageConnectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.liveStageConnectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.liveStageConnectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.liveStageConnectButton.Image = ((System.Drawing.Image)(resources.GetObject("liveStageConnectButton.Image")));
            this.liveStageConnectButton.IsConnect = false;
            this.liveStageConnectButton.Location = new System.Drawing.Point(4, 104);
            this.liveStageConnectButton.Margin = new System.Windows.Forms.Padding(4);
            this.liveStageConnectButton.Name = "liveStageConnectButton";
            this.liveStageConnectButton.Size = new System.Drawing.Size(112, 29);
            this.liveStageConnectButton.Status = ShareLibrary.Base.STATUS_ENUM.UNKNOW;
            this.liveStageConnectButton.TabIndex = 2;
            this.liveStageConnectButton.Test = 0;
            this.liveStageConnectButton.Text = " Connect";
            this.liveStageConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.liveStageConnectButton.UseVisualStyleBackColor = false;
            this.liveStageConnectButton.Click += new System.EventHandler(this.liveStageConnectButton_Click);
            // 
            // myLabel9
            // 
            this.myLabel9.AutoSize = true;
            this.myLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myLabel9.ForeColor = System.Drawing.Color.White;
            this.myLabel9.Location = new System.Drawing.Point(3, 0);
            this.myLabel9.Name = "myLabel9";
            this.myLabel9.Size = new System.Drawing.Size(114, 25);
            this.myLabel9.TabIndex = 0;
            this.myLabel9.Text = "Camera Name";
            this.myLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // liveNameTextBox
            // 
            this.liveNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveNameTextBox.BackColor = System.Drawing.Color.Black;
            this.liveNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.liveNameTextBox.ForeColor = System.Drawing.Color.White;
            this.liveNameTextBox.Location = new System.Drawing.Point(124, 2);
            this.liveNameTextBox.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.liveNameTextBox.Name = "liveNameTextBox";
            this.liveNameTextBox.Size = new System.Drawing.Size(177, 20);
            this.liveNameTextBox.TabIndex = 1;
            this.liveNameTextBox.Text = "Live Camera";
            // 
            // liveNDICameraComboBox
            // 
            this.liveNDICameraComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveNDICameraComboBox.BackColor = System.Drawing.Color.Black;
            this.liveNDICameraComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.liveNDICameraComboBox.ForeColor = System.Drawing.Color.White;
            this.liveNDICameraComboBox.FormattingEnabled = true;
            this.liveNDICameraComboBox.Location = new System.Drawing.Point(123, 53);
            this.liveNDICameraComboBox.Name = "liveNDICameraComboBox";
            this.liveNDICameraComboBox.Size = new System.Drawing.Size(179, 21);
            this.liveNDICameraComboBox.TabIndex = 6;
            this.liveNDICameraComboBox.SelectedIndexChanged += new System.EventHandler(this.liveNDICameraComboBox_SelectedIndexChanged);
            // 
            // liveUSBCameraComboBox
            // 
            this.liveUSBCameraComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.liveUSBCameraComboBox.BackColor = System.Drawing.Color.Black;
            this.liveUSBCameraComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.liveUSBCameraComboBox.ForeColor = System.Drawing.Color.White;
            this.liveUSBCameraComboBox.FormattingEnabled = true;
            this.liveUSBCameraComboBox.Location = new System.Drawing.Point(123, 78);
            this.liveUSBCameraComboBox.Name = "liveUSBCameraComboBox";
            this.liveUSBCameraComboBox.Size = new System.Drawing.Size(179, 21);
            this.liveUSBCameraComboBox.TabIndex = 6;
            // 
            // liveIPCameraRadioButton
            // 
            this.liveIPCameraRadioButton.AutoSize = true;
            this.liveIPCameraRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveIPCameraRadioButton.ForeColor = System.Drawing.Color.White;
            this.liveIPCameraRadioButton.Location = new System.Drawing.Point(3, 28);
            this.liveIPCameraRadioButton.Name = "liveIPCameraRadioButton";
            this.liveIPCameraRadioButton.Size = new System.Drawing.Size(114, 19);
            this.liveIPCameraRadioButton.TabIndex = 7;
            this.liveIPCameraRadioButton.TabStop = true;
            this.liveIPCameraRadioButton.Text = "IP Camera";
            this.liveIPCameraRadioButton.UseVisualStyleBackColor = true;
            // 
            // liveNDICameraRadioButton
            // 
            this.liveNDICameraRadioButton.AutoSize = true;
            this.liveNDICameraRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveNDICameraRadioButton.ForeColor = System.Drawing.Color.White;
            this.liveNDICameraRadioButton.Location = new System.Drawing.Point(3, 53);
            this.liveNDICameraRadioButton.Name = "liveNDICameraRadioButton";
            this.liveNDICameraRadioButton.Size = new System.Drawing.Size(114, 19);
            this.liveNDICameraRadioButton.TabIndex = 7;
            this.liveNDICameraRadioButton.TabStop = true;
            this.liveNDICameraRadioButton.Text = "NDI Camera";
            this.liveNDICameraRadioButton.UseVisualStyleBackColor = true;
            // 
            // liveUSBCameraRadioButton
            // 
            this.liveUSBCameraRadioButton.AutoSize = true;
            this.liveUSBCameraRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveUSBCameraRadioButton.ForeColor = System.Drawing.Color.White;
            this.liveUSBCameraRadioButton.Location = new System.Drawing.Point(3, 78);
            this.liveUSBCameraRadioButton.Name = "liveUSBCameraRadioButton";
            this.liveUSBCameraRadioButton.Size = new System.Drawing.Size(114, 19);
            this.liveUSBCameraRadioButton.TabIndex = 7;
            this.liveUSBCameraRadioButton.TabStop = true;
            this.liveUSBCameraRadioButton.Text = "USB Camera";
            this.liveUSBCameraRadioButton.UseVisualStyleBackColor = true;
            // 
            // staticRadioButton
            // 
            this.staticRadioButton.AutoSize = true;
            this.staticRadioButton.ForeColor = System.Drawing.Color.White;
            this.staticRadioButton.Location = new System.Drawing.Point(319, 4);
            this.staticRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.staticRadioButton.Name = "staticRadioButton";
            this.staticRadioButton.Size = new System.Drawing.Size(84, 17);
            this.staticRadioButton.TabIndex = 6;
            this.staticRadioButton.TabStop = true;
            this.staticRadioButton.Text = "Static Image";
            this.staticRadioButton.UseVisualStyleBackColor = true;
            // 
            // liveRadioButton
            // 
            this.liveRadioButton.AutoSize = true;
            this.liveRadioButton.ForeColor = System.Drawing.Color.White;
            this.liveRadioButton.Location = new System.Drawing.Point(4, 4);
            this.liveRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.liveRadioButton.Name = "liveRadioButton";
            this.liveRadioButton.Size = new System.Drawing.Size(45, 17);
            this.liveRadioButton.TabIndex = 6;
            this.liveRadioButton.TabStop = true;
            this.liveRadioButton.Text = "Live";
            this.liveRadioButton.UseVisualStyleBackColor = true;
            this.liveRadioButton.CheckedChanged += new System.EventHandler(this.liveRadioButton_CheckedChanged);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Gray;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(332, 4);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(82, 33);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.BackColor = System.Drawing.Color.Gray;
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(242, 4);
            this.saveButton.Margin = new System.Windows.Forms.Padding(4);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(82, 33);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // myComboBox1
            // 
            this.myComboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox1.BackColor = System.Drawing.Color.Black;
            this.myComboBox1.Enabled = false;
            this.myComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox1.ForeColor = System.Drawing.Color.White;
            this.myComboBox1.FormattingEnabled = true;
            this.myComboBox1.Location = new System.Drawing.Point(289, 27);
            this.myComboBox1.Name = "myComboBox1";
            this.myComboBox1.Size = new System.Drawing.Size(180, 21);
            this.myComboBox1.TabIndex = 6;
            // 
            // myComboBox2
            // 
            this.myComboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox2.BackColor = System.Drawing.Color.Black;
            this.myComboBox2.Enabled = false;
            this.myComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox2.ForeColor = System.Drawing.Color.White;
            this.myComboBox2.FormattingEnabled = true;
            this.myComboBox2.Location = new System.Drawing.Point(289, 71);
            this.myComboBox2.Name = "myComboBox2";
            this.myComboBox2.Size = new System.Drawing.Size(180, 21);
            this.myComboBox2.TabIndex = 6;
            // 
            // myComboBox3
            // 
            this.myComboBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox3.BackColor = System.Drawing.Color.Black;
            this.myComboBox3.Enabled = false;
            this.myComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox3.ForeColor = System.Drawing.Color.White;
            this.myComboBox3.FormattingEnabled = true;
            this.myComboBox3.Location = new System.Drawing.Point(289, 115);
            this.myComboBox3.Name = "myComboBox3";
            this.myComboBox3.Size = new System.Drawing.Size(180, 21);
            this.myComboBox3.TabIndex = 6;
            // 
            // myComboBox4
            // 
            this.myComboBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox4.BackColor = System.Drawing.Color.Black;
            this.myComboBox4.Enabled = false;
            this.myComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox4.ForeColor = System.Drawing.Color.White;
            this.myComboBox4.FormattingEnabled = true;
            this.myComboBox4.Location = new System.Drawing.Point(289, 159);
            this.myComboBox4.Name = "myComboBox4";
            this.myComboBox4.Size = new System.Drawing.Size(180, 21);
            this.myComboBox4.TabIndex = 6;
            // 
            // myComboBox5
            // 
            this.myComboBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox5.BackColor = System.Drawing.Color.Black;
            this.myComboBox5.Enabled = false;
            this.myComboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox5.ForeColor = System.Drawing.Color.White;
            this.myComboBox5.FormattingEnabled = true;
            this.myComboBox5.Location = new System.Drawing.Point(289, 203);
            this.myComboBox5.Name = "myComboBox5";
            this.myComboBox5.Size = new System.Drawing.Size(180, 21);
            this.myComboBox5.TabIndex = 6;
            // 
            // myComboBox6
            // 
            this.myComboBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox6.BackColor = System.Drawing.Color.Black;
            this.myComboBox6.Enabled = false;
            this.myComboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox6.ForeColor = System.Drawing.Color.White;
            this.myComboBox6.FormattingEnabled = true;
            this.myComboBox6.Location = new System.Drawing.Point(289, 247);
            this.myComboBox6.Name = "myComboBox6";
            this.myComboBox6.Size = new System.Drawing.Size(180, 21);
            this.myComboBox6.TabIndex = 6;
            // 
            // myComboBox7
            // 
            this.myComboBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox7.BackColor = System.Drawing.Color.Black;
            this.myComboBox7.Enabled = false;
            this.myComboBox7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox7.ForeColor = System.Drawing.Color.White;
            this.myComboBox7.FormattingEnabled = true;
            this.myComboBox7.Location = new System.Drawing.Point(289, 291);
            this.myComboBox7.Name = "myComboBox7";
            this.myComboBox7.Size = new System.Drawing.Size(180, 21);
            this.myComboBox7.TabIndex = 6;
            // 
            // myComboBox8
            // 
            this.myComboBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.myComboBox8.BackColor = System.Drawing.Color.Black;
            this.myComboBox8.Enabled = false;
            this.myComboBox8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox8.ForeColor = System.Drawing.Color.White;
            this.myComboBox8.FormattingEnabled = true;
            this.myComboBox8.Location = new System.Drawing.Point(289, 335);
            this.myComboBox8.Name = "myComboBox8";
            this.myComboBox8.Size = new System.Drawing.Size(180, 21);
            this.myComboBox8.TabIndex = 6;
            // 
            // liveStageNDIInputCheckbox
            // 
            this.liveStageNDIInputCheckbox.AutoSize = true;
            this.liveStageNDIInputCheckbox.BackColor = System.Drawing.Color.Transparent;
            this.liveStageNDIInputCheckbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveStageNDIInputCheckbox.Enabled = false;
            this.liveStageNDIInputCheckbox.ForeColor = System.Drawing.Color.White;
            this.liveStageNDIInputCheckbox.Location = new System.Drawing.Point(595, 5);
            this.liveStageNDIInputCheckbox.Margin = new System.Windows.Forms.Padding(4);
            this.liveStageNDIInputCheckbox.Name = "liveStageNDIInputCheckbox";
            this.liveStageNDIInputCheckbox.Size = new System.Drawing.Size(92, 28);
            this.liveStageNDIInputCheckbox.TabIndex = 3;
            this.liveStageNDIInputCheckbox.Text = "NDI Input";
            this.liveStageNDIInputCheckbox.UseVisualStyleBackColor = false;
            this.liveStageNDIInputCheckbox.CheckedChanged += new System.EventHandler(this.camera1NDICheckBox_CheckedChanged);
            // 
            // MultiCameraSetupUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 1);
            this.Controls.Add(this.cameraSetupTableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MultiCameraSetupUserControl";
            this.Size = new System.Drawing.Size(664, 960);
            this.cameraSetupTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ptzCameraGroupBox.ResumeLayout(false);
            this.cameraInfoTableLayoutPanel.ResumeLayout(false);
            this.camera1GroupBox.ResumeLayout(false);
            this.cameraGroup1TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup1TableLayoutPanel.PerformLayout();
            this.camera5GroupBox.ResumeLayout(false);
            this.cameraGroup5TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup5TableLayoutPanel.PerformLayout();
            this.camera2GroupBox.ResumeLayout(false);
            this.cameraGroup2TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup2TableLayoutPanel.PerformLayout();
            this.camera6GroupBox.ResumeLayout(false);
            this.cameraGroup6TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup6TableLayoutPanel.PerformLayout();
            this.camera3GroupBox.ResumeLayout(false);
            this.cameraGroup3TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup3TableLayoutPanel.PerformLayout();
            this.camera7GroupBox.ResumeLayout(false);
            this.cameraGroup7TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup7TableLayoutPanel.PerformLayout();
            this.camera4GroupBox.ResumeLayout(false);
            this.cameraGroup4TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup4TableLayoutPanel.PerformLayout();
            this.camera8GroupBox.ResumeLayout(false);
            this.cameraGroup8TableLayoutPanel.ResumeLayout(false);
            this.cameraGroup8TableLayoutPanel.PerformLayout();
            this.stageSourceGroupBox.ResumeLayout(false);
            this.stageSourceTableLayoutPanel.ResumeLayout(false);
            this.stageSourceTableLayoutPanel.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel cameraSetupTableLayoutPanel;
        private MyCheckBox camera1NDICheckBox;
        private MyToggleButton liveStageConnectButton;
        private RoundedButton stageBrowseButton;
        private MyCheckBox liveStageNDICheckbox;
        private MyCheckBox imageStageNDICheckBox;
        private RoundedTextBox camera1TextBox;
        private RoundedTextBox liveIPCameraTextBox;
        private RoundedTextBox staticTextBox;
        private MyGroupBox ptzCameraGroupBox;
        private MyGroupBox stageSourceGroupBox;
        private MyRadioButton liveRadioButton;
        private MyRadioButton staticRadioButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private RoundedButton cancelButton;
        private RoundedButton saveButton;
        private RoundedTextBox liveNameTextBox;
        private RoundedTextBox imageNameTextBox;
        private MyCheckBox liveStageNDIInputCheckbox;
        private MyComboBox myComboBox1;
        private MyComboBox myComboBox2;
        private MyComboBox myComboBox3;
        private MyComboBox myComboBox4;
        private MyComboBox myComboBox5;
        private MyComboBox myComboBox6;
        private MyComboBox myComboBox7;
        private MyComboBox myComboBox8;
        private System.Windows.Forms.TableLayoutPanel cameraInfoTableLayoutPanel;
        private MyGroupBox camera1GroupBox;
        private MyGroupBox camera5GroupBox;
        private MyGroupBox camera2GroupBox;
        private MyGroupBox camera6GroupBox;
        private MyGroupBox camera3GroupBox;
        private MyGroupBox camera7GroupBox;
        private MyGroupBox camera8GroupBox;
        private System.Windows.Forms.TableLayoutPanel cameraGroup1TableLayoutPanel;
        private RoundedTextBox roundedTextBox1;
        private MyLabel myLabel1;
        private MyToggleButton myToggleButton1;
        private MyComboBox ndiCameraComboBox;
        private MyComboBox myComboBox10;
        private MyRadioButton camera1IPCameraRadioButton;
        private MyRadioButton camera1NDICameraRadioButton;
        private MyRadioButton camera1USBCameraRadioButton;
        private System.Windows.Forms.TableLayoutPanel cameraGroup5TableLayoutPanel;
        private MyCheckBox myCheckBox4;
        private RoundedTextBox roundedTextBox8;
        private MyLabel myLabel5;
        private MyToggleButton myToggleButton5;
        private RoundedTextBox roundedTextBox9;
        private MyComboBox myComboBox17;
        private MyComboBox myComboBox18;
        private MyRadioButton myRadioButton10;
        private MyRadioButton myRadioButton11;
        private MyRadioButton myRadioButton12;
        private System.Windows.Forms.TableLayoutPanel cameraGroup2TableLayoutPanel;
        private MyCheckBox myCheckBox1;
        private RoundedTextBox roundedTextBox2;
        private MyLabel myLabel2;
        private MyToggleButton myToggleButton2;
        private RoundedTextBox roundedTextBox3;
        private MyComboBox myComboBox11;
        private MyComboBox myComboBox12;
        private MyRadioButton myRadioButton1;
        private MyRadioButton myRadioButton2;
        private MyRadioButton myRadioButton3;
        private System.Windows.Forms.TableLayoutPanel cameraGroup6TableLayoutPanel;
        private MyCheckBox myCheckBox5;
        private RoundedTextBox roundedTextBox10;
        private MyLabel myLabel6;
        private MyToggleButton myToggleButton6;
        private RoundedTextBox roundedTextBox11;
        private MyComboBox myComboBox19;
        private MyComboBox myComboBox20;
        private MyRadioButton myRadioButton13;
        private MyRadioButton myRadioButton14;
        private MyRadioButton myRadioButton15;
        private System.Windows.Forms.TableLayoutPanel cameraGroup3TableLayoutPanel;
        private MyCheckBox myCheckBox2;
        private RoundedTextBox roundedTextBox4;
        private MyLabel myLabel3;
        private MyToggleButton myToggleButton3;
        private RoundedTextBox roundedTextBox5;
        private MyComboBox myComboBox13;
        private MyComboBox myComboBox14;
        private MyRadioButton myRadioButton4;
        private MyRadioButton myRadioButton5;
        private MyRadioButton myRadioButton6;
        private System.Windows.Forms.TableLayoutPanel cameraGroup7TableLayoutPanel;
        private MyCheckBox myCheckBox6;
        private RoundedTextBox roundedTextBox12;
        private MyLabel myLabel7;
        private MyToggleButton myToggleButton7;
        private RoundedTextBox roundedTextBox13;
        private MyComboBox myComboBox21;
        private MyComboBox myComboBox22;
        private MyRadioButton myRadioButton16;
        private MyRadioButton myRadioButton17;
        private MyRadioButton myRadioButton18;
        private MyGroupBox camera4GroupBox;
        private System.Windows.Forms.TableLayoutPanel cameraGroup4TableLayoutPanel;
        private MyCheckBox myCheckBox3;
        private RoundedTextBox roundedTextBox6;
        private MyLabel myLabel4;
        private MyToggleButton myToggleButton4;
        private RoundedTextBox roundedTextBox7;
        private MyComboBox myComboBox15;
        private MyComboBox myComboBox16;
        private MyRadioButton myRadioButton7;
        private MyRadioButton myRadioButton8;
        private MyRadioButton myRadioButton9;
        private System.Windows.Forms.TableLayoutPanel cameraGroup8TableLayoutPanel;
        private MyCheckBox myCheckBox7;
        private RoundedTextBox roundedTextBox14;
        private MyLabel myLabel8;
        private MyToggleButton myToggleButton8;
        private RoundedTextBox roundedTextBox15;
        private MyComboBox myComboBox23;
        private MyComboBox myComboBox24;
        private MyRadioButton myRadioButton19;
        private MyRadioButton myRadioButton20;
        private MyRadioButton myRadioButton21;
        private System.Windows.Forms.TableLayoutPanel stageSourceTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MyLabel myLabel9;
        private MyComboBox liveNDICameraComboBox;
        private MyComboBox liveUSBCameraComboBox;
        private MyRadioButton liveIPCameraRadioButton;
        private MyRadioButton liveNDICameraRadioButton;
        private MyRadioButton liveUSBCameraRadioButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MyLabel myLabel10;
        private MyLabel myLabel11;
    }
}
