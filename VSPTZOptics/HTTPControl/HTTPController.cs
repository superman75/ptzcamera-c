﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace HTTPControl
{
    public class HTTPController
    {
        public HTTPController()
        {

        }

        public static string HTTPTigger(string _command)
        {
            try
            {
                string data = DoWebRequest(_command, "POST");
                return data;
            }
            catch (Exception ex){
                return ex.Message;
            }
        }
        private static string DoWebRequest(string address, string _method, string body = null)
        {
            var request = (HttpWebRequest)WebRequest.Create(address);
            request.Method = _method;
            //request.Credentials = new NetworkCredential(ioControl.Username, ioControl.Password);
            //request.PreAuthenticate = true;

            if (_method == "POST")
            {
                if (!string.IsNullOrEmpty(body))
                {
                    var requestBody = Encoding.UTF8.GetBytes(body);
                    request.ContentLength = requestBody.Length;
                    request.ContentType = "application/json";
                    using (var requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(requestBody, 0, requestBody.Length);
                    }
                }
                else
                {
                    request.ContentLength = 0;
                }
            }

            request.Timeout = 3000;
            request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);

            string output = string.Empty;
            try
            {
                using (var response = request.GetResponse())
                {
                    using (var stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(1252)))
                    {
                        output = stream.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    using (var stream = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        output = stream.ReadToEnd();
                    }
                }
                else if (ex.Status == WebExceptionStatus.Timeout)
                {
                    output = "Request timeout is expired.";
                }

                //LogWriter.Instance.WriteLog(output);
            }

            return output;
        }
    }
}
