﻿using Common;
using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTZOptics
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                CommonRef common = new CommonRef();
                common.InitLibs(); //Initilize cpp libs
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                Application.Run(new MainForm());
            }
            catch (FileNotFoundException ex)
            {
                LogWriter.Instance.WriteLog(2, ex.Message + ex.StackTrace);
                MessageBox.Show(ex.Message, "Unhandled Thread Exception");
            }
            
        }
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            LogWriter.Instance.WriteLog(2, e.Exception.Message + e.Exception.StackTrace);
            MessageBox.Show(e.Exception.Message + e.Exception.StackTrace, "Unhandled Thread Exception");
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogWriter.Instance.WriteLog(2, (e.ExceptionObject as Exception).Message + (e.ExceptionObject as Exception).StackTrace);
            MessageBox.Show((e.ExceptionObject as Exception).Message, "Unhandled UI Exception");
        }
    }
}
