﻿using CameraUserControl.Model;

namespace PTZOptics
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuPanel = new System.Windows.Forms.Panel();
            this.menuStrip = new CameraUserControl.Model.MyMenuStrip();
            this.fileToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.exitToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.nDIOutputToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera1ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera2ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera3ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera4ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera5ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera6ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera7ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera8ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera9ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.camera10ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.cameraSetupToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.presetSetupToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.stagePresetSetupToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.saveAsToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.loadToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.selectedProfileToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile2ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile3ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile4ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile5ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile6ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile7ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.profile8ToolStripMenuItem = new CameraUserControl.Model.MyToolStripMenuItem();
            this.menuPanel.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.menuPanel.Controls.Add(this.menuStrip);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(1084, 30);
            this.menuPanel.TabIndex = 2;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.menuStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip.ForeColor = System.Drawing.Color.White;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.cameraSetupToolStripMenuItem,
            this.presetSetupToolStripMenuItem,
            this.stagePresetSetupToolStripMenuItem,
            this.nDIOutputToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1084, 30);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 26);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // nDIOutputToolStripMenuItem
            // 
            this.nDIOutputToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.camera1ToolStripMenuItem,
            this.camera2ToolStripMenuItem,
            this.camera3ToolStripMenuItem,
            this.camera4ToolStripMenuItem,
            this.camera5ToolStripMenuItem,
            this.camera6ToolStripMenuItem,
            this.camera7ToolStripMenuItem,
            this.camera8ToolStripMenuItem,
            this.camera9ToolStripMenuItem,
            this.camera10ToolStripMenuItem});
            this.nDIOutputToolStripMenuItem.Name = "nDIOutputToolStripMenuItem";
            this.nDIOutputToolStripMenuItem.Size = new System.Drawing.Size(80, 26);
            this.nDIOutputToolStripMenuItem.Text = "NDI Output";
            // 
            // camera1ToolStripMenuItem
            // 
            this.camera1ToolStripMenuItem.CheckOnClick = true;
            this.camera1ToolStripMenuItem.Name = "camera1ToolStripMenuItem";
            this.camera1ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera1ToolStripMenuItem.Text = "Camera 1";
            // 
            // camera2ToolStripMenuItem
            // 
            this.camera2ToolStripMenuItem.CheckOnClick = true;
            this.camera2ToolStripMenuItem.Name = "camera2ToolStripMenuItem";
            this.camera2ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera2ToolStripMenuItem.Text = "Camera 2";
            // 
            // camera3ToolStripMenuItem
            // 
            this.camera3ToolStripMenuItem.CheckOnClick = true;
            this.camera3ToolStripMenuItem.Name = "camera3ToolStripMenuItem";
            this.camera3ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera3ToolStripMenuItem.Text = "Camera 3";
            // 
            // camera4ToolStripMenuItem
            // 
            this.camera4ToolStripMenuItem.CheckOnClick = true;
            this.camera4ToolStripMenuItem.Name = "camera4ToolStripMenuItem";
            this.camera4ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera4ToolStripMenuItem.Text = "Camera 4";
            // 
            // camera5ToolStripMenuItem
            // 
            this.camera5ToolStripMenuItem.CheckOnClick = true;
            this.camera5ToolStripMenuItem.Name = "camera5ToolStripMenuItem";
            this.camera5ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera5ToolStripMenuItem.Text = "Camera 5";
            // 
            // camera6ToolStripMenuItem
            // 
            this.camera6ToolStripMenuItem.CheckOnClick = true;
            this.camera6ToolStripMenuItem.Name = "camera6ToolStripMenuItem";
            this.camera6ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera6ToolStripMenuItem.Text = "Camera 6";
            // 
            // camera7ToolStripMenuItem
            // 
            this.camera7ToolStripMenuItem.CheckOnClick = true;
            this.camera7ToolStripMenuItem.Name = "camera7ToolStripMenuItem";
            this.camera7ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera7ToolStripMenuItem.Text = "Camera 7";
            // 
            // camera8ToolStripMenuItem
            // 
            this.camera8ToolStripMenuItem.CheckOnClick = true;
            this.camera8ToolStripMenuItem.Name = "camera8ToolStripMenuItem";
            this.camera8ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera8ToolStripMenuItem.Text = "Camera 8";
            // 
            // camera9ToolStripMenuItem
            // 
            this.camera9ToolStripMenuItem.CheckOnClick = true;
            this.camera9ToolStripMenuItem.Name = "camera9ToolStripMenuItem";
            this.camera9ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera9ToolStripMenuItem.Text = "Camera 9";
            // 
            // camera10ToolStripMenuItem
            // 
            this.camera10ToolStripMenuItem.CheckOnClick = true;
            this.camera10ToolStripMenuItem.Name = "camera10ToolStripMenuItem";
            this.camera10ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.camera10ToolStripMenuItem.Text = "Static Image";
            // 
            // cameraSetupToolStripMenuItem
            // 
            this.cameraSetupToolStripMenuItem.Name = "cameraSetupToolStripMenuItem";
            this.cameraSetupToolStripMenuItem.Size = new System.Drawing.Size(93, 26);
            this.cameraSetupToolStripMenuItem.Text = "Camera Setup";
            this.cameraSetupToolStripMenuItem.Click += new System.EventHandler(this.cameraSetupToolStripMenuItem_Click);
            // 
            // presetSetupToolStripMenuItem
            // 
            this.presetSetupToolStripMenuItem.Name = "presetSetupToolStripMenuItem";
            this.presetSetupToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.presetSetupToolStripMenuItem.Text = "Camera Preset Setup";
            this.presetSetupToolStripMenuItem.Click += new System.EventHandler(this.presetSetupToolStripMenuItem_Click);
            // 
            // stagePresetSetupToolStripMenuItem
            // 
            this.stagePresetSetupToolStripMenuItem.Name = "stagePresetSetupToolStripMenuItem";
            this.stagePresetSetupToolStripMenuItem.Size = new System.Drawing.Size(84, 26);
            this.stagePresetSetupToolStripMenuItem.Text = "Stage Preset Setup";
            this.stagePresetSetupToolStripMenuItem.Click += new System.EventHandler(this.stagePresetSetupToolStripMenuItem_Click);
            // 
            // contentPanel
            // 
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 30);
            this.contentPanel.Margin = new System.Windows.Forms.Padding(0);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(1084, 671);
            this.contentPanel.TabIndex = 3;
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // selectedProfileToolStripMenuItem
            // 
            this.selectedProfileToolStripMenuItem.Name = "selectedProfileToolStripMenuItem";
            this.selectedProfileToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.selectedProfileToolStripMenuItem.Text = "profile1";
            // 
            // profile2ToolStripMenuItem
            // 
            this.profile2ToolStripMenuItem.Name = "profile2ToolStripMenuItem";
            this.profile2ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile2ToolStripMenuItem.Text = "profile2";
            // 
            // profile3ToolStripMenuItem
            // 
            this.profile3ToolStripMenuItem.Name = "profile3ToolStripMenuItem";
            this.profile3ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile3ToolStripMenuItem.Text = "profile3";
            // 
            // profile4ToolStripMenuItem
            // 
            this.profile4ToolStripMenuItem.Name = "profile4ToolStripMenuItem";
            this.profile4ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile4ToolStripMenuItem.Text = "profile4";
            // 
            // profile5ToolStripMenuItem
            // 
            this.profile5ToolStripMenuItem.Name = "profile5ToolStripMenuItem";
            this.profile5ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile5ToolStripMenuItem.Text = "profile5";
            // 
            // profile6ToolStripMenuItem
            // 
            this.profile6ToolStripMenuItem.Name = "profile6ToolStripMenuItem";
            this.profile6ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile6ToolStripMenuItem.Text = "profile6";
            // 
            // profile7ToolStripMenuItem
            // 
            this.profile7ToolStripMenuItem.Name = "profile7ToolStripMenuItem";
            this.profile7ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile7ToolStripMenuItem.Text = "profile7";
            // 
            // profile8ToolStripMenuItem
            // 
            this.profile8ToolStripMenuItem.Name = "profile8ToolStripMenuItem";
            this.profile8ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.profile8ToolStripMenuItem.Text = "profile8";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(1084, 701);
            this.Controls.Add(this.contentPanel);
            this.Controls.Add(this.menuPanel);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 678);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PTZOptics AR Multicamera Controller";
            this.menuPanel.ResumeLayout(false);
            this.menuPanel.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MyMenuStrip menuStrip;
        private MyToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Panel menuPanel;
        private MyToolStripMenuItem nDIOutputToolStripMenuItem;
        private MyToolStripMenuItem exitToolStripMenuItem;
        private MyToolStripMenuItem saveToolStripMenuItem;
        private MyToolStripMenuItem saveAsToolStripMenuItem;
        private MyToolStripMenuItem loadToolStripMenuItem;
        private MyToolStripMenuItem selectedProfileToolStripMenuItem;
        private MyToolStripMenuItem profile2ToolStripMenuItem;
        private MyToolStripMenuItem profile3ToolStripMenuItem;
        private MyToolStripMenuItem profile4ToolStripMenuItem;
        private MyToolStripMenuItem profile5ToolStripMenuItem;
        private MyToolStripMenuItem profile6ToolStripMenuItem;
        private MyToolStripMenuItem profile7ToolStripMenuItem;
        private MyToolStripMenuItem profile8ToolStripMenuItem;

        private MyToolStripMenuItem camera1ToolStripMenuItem;
        private MyToolStripMenuItem camera2ToolStripMenuItem;
        private MyToolStripMenuItem camera3ToolStripMenuItem;
        private MyToolStripMenuItem camera4ToolStripMenuItem;
        private MyToolStripMenuItem camera5ToolStripMenuItem;
        private MyToolStripMenuItem camera6ToolStripMenuItem;
        private MyToolStripMenuItem camera7ToolStripMenuItem;
        private MyToolStripMenuItem camera8ToolStripMenuItem;
        private MyToolStripMenuItem camera9ToolStripMenuItem;
        private MyToolStripMenuItem camera10ToolStripMenuItem;
        private CameraUserControl.MainUserControl mainUserControl;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private MyToolStripMenuItem cameraSetupToolStripMenuItem;
        private MyToolStripMenuItem presetSetupToolStripMenuItem;
        private MyToolStripMenuItem stagePresetSetupToolStripMenuItem;
    }
}

