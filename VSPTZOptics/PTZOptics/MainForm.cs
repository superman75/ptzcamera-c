﻿using CameraView.Model;
using CameraView.ViewModel;
using CameraUserControl.Model;
using Settings.ViewModel;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CameraUserControl;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace PTZOptics
{
    public partial class MainForm : Form
    {
        [DllImport("User32.dll")]
        static extern bool EnableNonClientDpiScaling(
            IntPtr hwnd
        );
        [DllImport("user32.dll")]
        static extern IntPtr DefWindowProc(IntPtr hwnd, uint message, IntPtr wParam, IntPtr lParam);

        private SettingForm settingForm;
        private Graphics g;
        private bool isLoaded = false;
        private FormWindowState lastWindowState = FormWindowState.Minimized;
        public MainForm()
        {
            g = this.CreateGraphics();
            ProfileViewModel.Instance.DpiX = g.DpiX;
            ProfileViewModel.Instance.DpiY = g.DpiY;
            InitializeComponent();
            this.Load += MainForm_Load;
            this.FormClosing += MainForm_FormClosing;
            this.Resize += MainForm_Resize;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (WindowState != lastWindowState)
            {
                lastWindowState = WindowState;
                if (WindowState == FormWindowState.Maximized)
                {
                    mainUserControl.SetSuppendPreviewLayout(true);
                    mainUserControl.SetSuppendPreviewLayout(false);
                    // Maximized!
                }
                if (WindowState == FormWindowState.Normal)
                {
                    mainUserControl.SetSuppendPreviewLayout(true);
                    mainUserControl.SetSuppendPreviewLayout(false);
                    // Restored!
                }
            }
        }

        protected override void WndProc(ref Message m)
        {
            const int WM_NCCREATE = 0x0081;
            base.WndProc(ref m);
            switch (m.Msg)
            {
                case WM_NCCREATE:
                    {
                        if(EnableNonClientDpiScaling(this.Handle))
                            DefWindowProc(this.Handle, (uint)m.Msg, m.WParam, m.LParam);
                    }
                    break;
                default:
                    break;
            }

        }

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            mainUserControl.SetSuppendPreviewLayout(true);
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            mainUserControl.SetSuppendPreviewLayout(false);
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            if(!isLoaded)
            {
                isLoaded = true;
                settingForm = new SettingForm();
                InitializeUserControl();
                InitilizeBindingNDIOutput();
                LoadToolStripMenu();
            }
        }

        private void InitilizeBindingNDIOutput()
        {
            camera1ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[0], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera1ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[0], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera2ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[1], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera2ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[1], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera3ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[2], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera3ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[2], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera4ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[3], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera4ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[3], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera5ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[4], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera5ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[4], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera6ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[5], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera6ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[5], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera7ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[6], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera7ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[6], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera8ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[7], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera8ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[7], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera9ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[8], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera9ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[8], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            camera10ToolStripMenuItem.DataBindings.Add("Checked", ProfileViewModel.Instance.SelectedProfile.CameraList[9], "IsNDIOutput", true, DataSourceUpdateMode.OnPropertyChanged);
            camera10ToolStripMenuItem.DataBindings.Add("Text", ProfileViewModel.Instance.SelectedProfile.CameraList[9], "Name", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        private void LoadToolStripMenu()
        {
            this.fileToolStripMenuItem.DropDownItems.Clear();
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.loadToolStripMenuItem});            
            if(!string.IsNullOrEmpty(ProfileViewModel.Instance.SelectedProfile.FileNamePath))
            {
                selectedProfileToolStripMenuItem.Text = ProfileViewModel.Instance.SelectedProfile.FileNamePath + " (current)";
                this.fileToolStripMenuItem.DropDownItems.Add(selectedProfileToolStripMenuItem);
            }
            foreach (string profileName in ProfileViewModel.Instance.ProfileNameList)
            {
                if(profileName != ProfileViewModel.Instance.SelectedProfile.FileNamePath)
                {
                    MyToolStripMenuItem profileToolStripMenuItem = new MyToolStripMenuItem();
                    profileToolStripMenuItem.Click += ProfileToolStripMenuItem_Click;
                    profileToolStripMenuItem.Text = profileName;
                    this.fileToolStripMenuItem.DropDownItems.Add(profileToolStripMenuItem);
                }
            }

            this.fileToolStripMenuItem.DropDownItems.Add(this.exitToolStripMenuItem);
            mainUserControl.LoadPreviewCamera();
        }

        private void ProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProfileViewModel.Instance.SelectedProfile.ResetProfile();
            //Stop all connection - begin
            foreach (CamView camView in CamViewModel.Instance.CamViewList)
            {
                if (camView.Cam.Index < 8)
                    camView.DisconnectControl();
                camView.StopStream();
            }
            //Stop all connection - end
            var profileName = ((MyToolStripMenuItem)sender).Text ;
            ProfileViewModel.Instance.SelectedProfile.SetPath(profileName);
            ProfileViewModel.Instance.SelectedProfile.LoadProfile();
            ProfileViewModel.Instance.AddProfileName(profileName);

            CamViewModel.Instance.UpdateActiveCamView(-1);
               LoadToolStripMenu();
            ProfileViewModel.Instance.SelectedProfile.IsChange = false;
            ProfileViewModel.Instance.IsChangeSetup = true;
        }
        async Task ReleaseCamViewResource(CamView _camView)
        {
            if (_camView.Cam.Index < 8 && _camView.CamControl != null)
                _camView.CamControl.Close();
            _camView.Dispose();
        }
        public async Task ReleaseResource()
        {
            await Task.WhenAll(CamViewModel.Instance.CamViewList.Select(i => ReleaseCamViewResource(i)));
            CamViewModel.Instance.CamViewList.Clear();
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(ProfileViewModel.Instance.IsChangeSetup || ProfileViewModel.Instance.SelectedProfile.IsChange)
            {
                using (ConfirmForm confirmForm = new ConfirmForm("Do you want to save changes?"))
                {
                    confirmForm.ShowDialog();
                    if (confirmForm.DialogResult == DialogResult.OK)
                    {
                        if (string.IsNullOrEmpty(ProfileViewModel.Instance.SelectedProfile.FileNamePath))
                        {
                            DialogResult res = SaveAsToolStripMenuItem();
                            if (res == DialogResult.Cancel)
                            {
                                e.Cancel = true;
                                return;
                            }
                        }
                        else
                            ProfileViewModel.Instance.SelectedProfile.SaveProfile();

                        ProfileViewModel.Instance.SelectedProfile.SaveProfile();
                        ProfileViewModel.Instance.SaveConfig();
                    }
                    else if (confirmForm.DialogResult == DialogResult.Cancel)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
            Hide();
            this.settingForm.MultiCameraSetupUC.Close();
            var task = ReleaseResource();
            task.Wait();
        }

        private void InitializeUserControl()
        {
            if (this.mainUserControl == null)
                this.mainUserControl = new MainUserControl();
            this.contentPanel.Controls.Add(this.mainUserControl);
            this.mainUserControl.UserControlEvent += MainUserControl_UserControlEvent;
        }
        private void MainUserControl_UserControlEvent(object sender, CameraUserControl.Base.UserControlEventArgs e)
        {
            if(e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.ADD_CAMERA ||
                e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.EDIT_CAMERA)
            {
                settingForm.SetSetupMode(SETUP_MODE_ENUM.CAMERA_SETUP, e.CameraInd);
                settingForm.ShowDialog(); //Show camera setup dialog
                if(settingForm.DialogResult == DialogResult.OK)
                {
                    this.mainUserControl.LoadLivePreviewCamera();
                }
                else
                    this.settingForm.MultiCameraSetupUC.CancelUpdateCameraInfo();
            }
            else if (e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.EDIT_STAGE_SOURCE)
            {
                settingForm.SetSetupMode(SETUP_MODE_ENUM.CAMERA_SETUP, e.CameraInd);
                settingForm.ShowDialog(); //Show camera setup dialog
                if (settingForm.DialogResult == DialogResult.OK)
                {
                    this.mainUserControl.LoadStagePreviewCamera();
                }
                else
                    this.settingForm.MultiCameraSetupUC.CancelUpdateCameraInfo();
            }
            else if (e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.EDIT_CAMERA_PRESETS)
            {
                ProfileViewModel.Instance.IsPresetSetup = true;
                settingForm.SetSetupMode(SETUP_MODE_ENUM.PRESET_SETUP);
                settingForm.ShowDialog(); //Show camera setup dialog
                if (settingForm.DialogResult == DialogResult.OK)
                {
                    
                }
                ProfileViewModel.Instance.IsPresetSetup = false;
            }
            else if (e.EventType == CameraUserControl.Base.EVENT_TYPE_ENUM.EDIT_STAGE_PRESETS)
            {
                settingForm.SetSetupMode(SETUP_MODE_ENUM.STAGE_SETUP);
                settingForm.ShowDialog(); //Show camera setup dialog
                if (settingForm.DialogResult == DialogResult.OK)
                {
                    this.mainUserControl.LoadStagePreviewCamera();
                }
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close(); //Close Main Form
        }
        private void cameraSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingForm.SetSetupMode(SETUP_MODE_ENUM.CAMERA_SETUP);
            settingForm.ShowDialog(); //Show camera setup dialog
            if (settingForm.DialogResult == DialogResult.OK)
            {
                this.mainUserControl.LoadPreviewCamera();
            }
            else
                this.settingForm.MultiCameraSetupUC.CancelUpdateCameraInfo();
        }
        private void presetSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProfileViewModel.Instance.IsPresetSetup = true;
            settingForm.SetSetupMode(SETUP_MODE_ENUM.PRESET_SETUP);
            settingForm.ShowDialog(); //Show preset setup dialog
            ProfileViewModel.Instance.IsPresetSetup = false;
        }
        private void stagePresetSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingForm.SetSetupMode(SETUP_MODE_ENUM.STAGE_SETUP);
            settingForm.ShowDialog(); //Show stage setup dialog
            if (settingForm.DialogResult == DialogResult.OK)
            {
                this.mainUserControl.LoadStagePreviewCamera();
            }
        }
        
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ProfileViewModel.Instance.SelectedProfile.FileNamePath))
                saveAsToolStripMenuItem_Click(null, null);
            else
                ProfileViewModel.Instance.SelectedProfile.SaveProfile();
            ProfileViewModel.Instance.SelectedProfile.IsChange = false;
            ProfileViewModel.Instance.IsChangeSetup = false;
        }

        private DialogResult SaveAsToolStripMenuItem()
        {
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = ProfileViewModel.Instance.AppSettingsLocation + "Profile\\";
                DialogResult res = dialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    var profileName = Path.ChangeExtension(dialog.FileName, ".xml");
                    ProfileViewModel.Instance.SelectedProfile.SetPath(profileName);
                    ProfileViewModel.Instance.SelectedProfile.SaveProfile();
                    ProfileViewModel.Instance.AddProfileName(profileName);
                    LoadToolStripMenu();
                    ProfileViewModel.Instance.SelectedProfile.IsChange = false;
                    ProfileViewModel.Instance.IsChangeSetup = false;
                }
                return res;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAsToolStripMenuItem();
            return;
        }
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = ProfileViewModel.Instance.AppSettingsLocation + "Profile\\";

                if (dialog.ShowDialog() == DialogResult.OK)
                {                    
                    ProfileViewModel.Instance.SelectedProfile.ResetProfile();
                    //Stop all connection - begin
                    foreach (CamView camView in CamViewModel.Instance.CamViewList)
                    {
                        if(camView.Cam.Index < 8)
                            camView.DisconnectControl();
                        camView.StopStream();
                    }
                    //Stop all connection - end

                    var profileName = dialog.FileName;
                    ProfileViewModel.Instance.SelectedProfile.SetPath(profileName);
                    ProfileViewModel.Instance.SelectedProfile.LoadProfile();
                    ProfileViewModel.Instance.AddProfileName(profileName);

                    CamViewModel.Instance.UpdateActiveCamView(-1);

                    LoadToolStripMenu();
                    ProfileViewModel.Instance.SelectedProfile.IsChange = false;
                    ProfileViewModel.Instance.IsChangeSetup = true;
                }
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(ProfileViewModel.Instance.SelectedProfile.IsChange)
            {
                using (ConfirmForm confirmForm = new ConfirmForm("Do you want to save changes?"))
                {
                    confirmForm.ShowDialog();
                    if (confirmForm.DialogResult == DialogResult.OK)
                    {
                        if (string.IsNullOrEmpty(ProfileViewModel.Instance.SelectedProfile.FileNamePath))
                        {
                            DialogResult res = SaveAsToolStripMenuItem();
                            if (res == DialogResult.Cancel)
                            {
                                return;
                            }
                        }
                        else
                            ProfileViewModel.Instance.SelectedProfile.SaveProfile();
                        ProfileViewModel.Instance.SelectedProfile.ResetProfile();
                    }
                    else if (confirmForm.DialogResult == DialogResult.Abort)
                    {
                        ProfileViewModel.Instance.SelectedProfile.ResetProfile();
                    }
                    else
                        return;
                }
            }
            else
            {
                ProfileViewModel.Instance.SelectedProfile.ResetProfile();
            }
            //Stop all connection - begin
            foreach (CamView camView in CamViewModel.Instance.CamViewList)
            {
                if (camView.Cam.Index < 8)
                    camView.DisconnectControl();
                camView.StopStream();
            }
            //Stop all connection - end
            LoadToolStripMenu();
            ProfileViewModel.Instance.SelectedProfile.IsChange = false;
            ProfileViewModel.Instance.IsChangeSetup = true;
        }
    }
}
