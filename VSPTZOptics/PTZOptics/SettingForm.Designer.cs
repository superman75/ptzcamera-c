﻿namespace PTZOptics
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingForm));
            this.cameraSetupUserControl = new CameraUserControl.CameraSetupUserControl();
            this.cameraPresetSetupUserControl = new CameraUserControl.CameraPresetSetupUserControl();
            this.stageSetupUserControl = new CameraUserControl.StageSetupUserControl();
            this.multiCameraSetupUserControl = new CameraUserControl.MultiCameraSetupUserControl();
            this.SuspendLayout();
            // 
            // cameraSetupUserControl
            // 
            this.cameraSetupUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraSetupUserControl.Location = new System.Drawing.Point(0, 0);
            this.cameraSetupUserControl.MinimumSize = new System.Drawing.Size(392, 558);
            this.cameraSetupUserControl.Name = "cameraSetupUserControl";
            this.cameraSetupUserControl.Size = new System.Drawing.Size(398, 558);
            this.cameraSetupUserControl.TabIndex = 0;
            // 
            // multiCameraSetupUserControl
            // 
            this.multiCameraSetupUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multiCameraSetupUserControl.Location = new System.Drawing.Point(0, 0);
            this.multiCameraSetupUserControl.MinimumSize = new System.Drawing.Size(392, 558);
            this.multiCameraSetupUserControl.Name = "multiCameraSetupUserControl";
            this.multiCameraSetupUserControl.Size = new System.Drawing.Size(398, 558);
            this.multiCameraSetupUserControl.TabIndex = 0;
            // 
            // cameraPresetSetupUserControl
            // 
            this.cameraPresetSetupUserControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.cameraPresetSetupUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraPresetSetupUserControl.Location = new System.Drawing.Point(0, 0);
            this.cameraPresetSetupUserControl.Margin = new System.Windows.Forms.Padding(0);
            this.cameraPresetSetupUserControl.MinimumSize = new System.Drawing.Size(1016, 632);
            this.cameraPresetSetupUserControl.Name = "cameraPresetSetupUserControl";
            this.cameraPresetSetupUserControl.Size = new System.Drawing.Size(1067, 660);
            this.cameraPresetSetupUserControl.TabIndex = 0;
            // 
            // stageSetupUserControl
            // 
            this.stageSetupUserControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.stageSetupUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stageSetupUserControl.Location = new System.Drawing.Point(0, 0);
            this.stageSetupUserControl.Margin = new System.Windows.Forms.Padding(0);
            this.stageSetupUserControl.MinimumSize = new System.Drawing.Size(473, 478);
            this.stageSetupUserControl.Name = "stageSetupUserControl";
            this.stageSetupUserControl.Size = new System.Drawing.Size(473, 478);
            this.stageSetupUserControl.TabIndex = 0;
            // 
            // SettingForm
            // 
            this.AutoScrollMinSize = new System.Drawing.Size(0, 1);
            this.AutoScroll = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(1067, 660);
            this.Controls.Add(this.cameraPresetSetupUserControl);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(414, 550);
            this.Name = "SettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SettingForm";
            this.ResumeLayout(false);

        }

        #endregion

        private CameraUserControl.CameraSetupUserControl cameraSetupUserControl;
        private CameraUserControl.CameraPresetSetupUserControl cameraPresetSetupUserControl;
        private CameraUserControl.StageSetupUserControl stageSetupUserControl;
        private CameraUserControl.MultiCameraSetupUserControl multiCameraSetupUserControl;
    }
}