﻿using CameraUserControl;
using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTZOptics
{
    public partial class SettingForm : Form
    {
        private SETUP_MODE_ENUM setupMode;
        public MultiCameraSetupUserControl MultiCameraSetupUC
        {
            get { return multiCameraSetupUserControl; }
        }
        public SettingForm()
        {            
            InitializeComponent();
            this.FormClosing += SettingForm_FormClosing;
        }

        private void SettingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (setupMode == SETUP_MODE_ENUM.PRESET_SETUP)
            {
                cameraPresetSetupUserControl.Close();
            }
        }

        public void SetSetupMode(SETUP_MODE_ENUM _setupMode, int _cameraInd = -1)
        {
            setupMode = _setupMode;
            if (setupMode == SETUP_MODE_ENUM.CAMERA_SETUP)
            {
                switch (_cameraInd)
                {
                    case -1://Full setup for all camera will be shown
                        {
                            this.MinimumSize = new Size(670, 1010);
                            this.Size = new Size(670, 1010);
                        }
                        break;
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        {
                            this.MinimumSize = new Size(374, 300);
                            this.Size = new Size(394, 300);
                        }
                        break;
                    case 8: //Only stage setup will be shown
                        {
                            this.MinimumSize = new Size(674, 300);
                            this.Size = new Size(674, 300);
                        }
                        break;
                }
                this.Text = "Camera Setup";
                this.Controls.Clear();
                multiCameraSetupUserControl.LoadCameraSetup(_cameraInd);
                this.Controls.Add(multiCameraSetupUserControl);
            }
            else if (setupMode == SETUP_MODE_ENUM.PRESET_SETUP)
            {
                this.Text = "Preset Setup";
                this.MinimumSize = new Size(1016, 693);
                this.Size = new Size(1016, 693);
                this.Controls.Clear();
                this.Controls.Add(cameraPresetSetupUserControl);
                cameraPresetSetupUserControl.InitializeTreeView();
            }
            else if (setupMode == SETUP_MODE_ENUM.STAGE_SETUP)
            {
                this.Text = "Stage Setup";
                this.MinimumSize = new Size(493, 540);
                this.Size = new Size(493, 540);
                this.Controls.Clear();
                this.Controls.Add(stageSetupUserControl);
            }
        }
    }
}
