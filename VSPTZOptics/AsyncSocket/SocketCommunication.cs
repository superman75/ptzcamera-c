﻿using AsyncSocket.Base;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace AsyncSocket
{

    /// <summary>
    /// This is a console app to test the client and server.
    /// It does minimal error handling.
    /// To see the valid commands, start the app and type "help" at the command prompt
    /// </summary>
    /// 
    public class SocketCommunication
    {
        private OSClient client;
        public OSClient Client
        {
            get { return client; }
            set { client = value; }
        }

        public SocketCommunication()
        {

        }
        public SocketCommunication(string _serverAddress, int _port)
        {
            InitSocketClient(_serverAddress, _port);
        }
        private void InitSocketClient(string _serverAddress, int _port)
        {
            client = new OSClient(_serverAddress, _port);
        }
        public void Start()
        {
            if (client != null){
                client.IsStartConnect = true;
            }
        }
        public void Stop()
        {
            if(client != null){
                client.IsStartConnect = false;
            }
        }
        public void Enqueue(ControlMessage _message)
        {
            if (client != null)
            {
                client.Enqueue(_message);
            }
        }
        public void Send(ControlMessage _message)
        {
            if (client != null)
            {
                client.Send(_message.Data);
            }
        }
        public void Close()
        {
            client.Close();
        }
    }
}
