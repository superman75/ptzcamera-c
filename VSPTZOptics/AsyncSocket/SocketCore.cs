﻿using AsyncSocket.Base;
using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncSocket
{
    /// <summary>
    /// class OSCore
    /// This is a base class that is used by both clients and servers.
    /// It contains the plumbing to set up a socket connection.
    /// </summary>
    public class OSClientCore
    {
        // these are the defaults if the user does not provide any parameters
        protected const string DEFAULT_SERVER = "localhost";
        protected const int DEFAULT_PORT = 9000;

        //  We default to a 256 K Byte buffer size
        protected const int DEFAULT_BUFFER_SIZE = 10 * 1024 * 1024; //1M for buffer

        // This is the connection socket and endpoint information
        protected Socket connectionSocket;
        protected IPEndPoint connectionEndPoint;

        // Here is where we track the totalbytes read by the server
        public int totalBytesRead;

        private SocketAsyncEventArgs    socketAsyncEvent;
        public SocketAsyncEventArgs SocketAsyncEvent
        {
            get { return socketAsyncEvent; }
            set { socketAsyncEvent = value; }
        }
        // We only instantiate the utility class here.
        // We could probably make it static and avoid this.
        public OSClientCore()
        {
            connectionSocket = null;
            connectionEndPoint = null;
            totalBytesRead = 0;
            // Now we create read socket to service data from server
            // We also assign the event handler for IO Completed to each socket as we create it
            // and set up its buffer to the right size.
            socketAsyncEvent = new SocketAsyncEventArgs();
            socketAsyncEvent.Completed += new EventHandler<SocketAsyncEventArgs>(OnIOCompleted);
            socketAsyncEvent.SetBuffer(new Byte[DEFAULT_BUFFER_SIZE], 0, DEFAULT_BUFFER_SIZE);
        }

        // An IPEndPoint contains all of the information about a server or client
        // machine that a socket needs.  Here we create one from information
        // that we send in as parameters
        public IPEndPoint CreateIPEndPoint(string servername, int portnumber)
        {
            try
            {
                // We get the IP address and stuff from DNS (Domain Name Services)
                // I think you can also pass in an IP address, but I would not because
                // that would not be extensible to IPV6 later
                UriHostNameType type = Uri.CheckHostName(servername);
                if (type == UriHostNameType.Dns)
                {
                    IPHostEntry hostInfo = Dns.GetHostEntry(servername);
                    IPAddress serverAddr = hostInfo.AddressList[0];
                    return new IPEndPoint(serverAddr, portnumber);
                }
                else
                {
                    IPAddress serverAddr;
                    if (!IPAddress.TryParse(servername, out serverAddr))
                    {
                        return null;
                    }
                    else
                    {
                        LogWriter.Instance.WriteLog(1, serverAddr + " " + portnumber);
                        return new IPEndPoint(serverAddr, portnumber);
                    }
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteLog(2, ex.Message + "\r\n" + ex.StackTrace + " servername = " + servername);
                return null;
            }
        }
        // This method peels apart the command string to create either a client or server socket,
        // which is not great because it means the method has to know the semantics of the command
        // that is passed to it.  So this needs to be fixed.
        protected bool CreateSocket(string server = DEFAULT_SERVER, int port = DEFAULT_PORT)
        {
            connectionEndPoint = CreateIPEndPoint(server, port);

            // If we get here, we try to create the socket using the IPEndpoint information.
            // We are defaulting here to TCP Stream sockets, but you could change this with more parameters.
            try
            {
                if (connectionEndPoint != null)
                    connectionSocket = new Socket(connectionEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                else
                    return false;
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteLog(2, ex.Message + "\r\n" + ex.StackTrace);
                return false;
            }
            return true;
        }

        private void OnIOCompleted(object sender, SocketAsyncEventArgs e)
        {
            // Determine which type of operation just completed and call the associated handler.
            // We are only processing receives right now on this server.
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    this.ProcessReceive(e);
                    break;
                default:
                    string log = "The last operation completed on the socket was not a receive";
                    LogWriter.Instance.WriteLog(1, log);
                    throw new ArgumentException(log);
            }
        }

        // This method processes the read socket once it has a transaction
        private void ProcessReceive(SocketAsyncEventArgs readSocket)
        {
            try
            {
                //LogWriter.Instance.WriteLog(1, "ProcessReceive Enter");
                // if BytesTransferred is 0, then the remote end closed the connection
                if (readSocket.BytesTransferred > 0)
                {
                    //SocketError.Success indicates that the last operation on the underlying socket succeeded
                    if (readSocket.SocketError == SocketError.Success)
                    {
                        OSClientUserToken token = readSocket.UserToken as OSClientUserToken;
                        if (token.ReadSocketData(readSocket))
                        {
                            Socket readsocket = token.OwnerSocket;
                            //token.ProcessData(readSocket);

                            // Start another receive request and immediately check to see if the receive is already complete
                            // Otherwise OnIOCompleted will get called when the receive is complete
                            // We are basically calling this same method recursively until there is no more data
                            // on the read socket
                            bool IOPending = readsocket.ReceiveAsync(readSocket);
                            if (!IOPending)
                            {
                                ProcessReceive(readSocket);
                            }
                        }
                        else
                        {
                            LogWriter.Instance.WriteLog(2, "ReadSocketData fail");
                            CloseReadSocket(readSocket);
                        }

                    }
                    else
                    {
                        ProcessError(readSocket);
                    }
                }
                else
                {
                    LogWriter.Instance.WriteLog(1, "No BytesTransferred");
                    CloseReadSocket(readSocket);
                }
                //LogWriter.Instance.WriteLog(1, "ProcessReceive Exit");
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteLog(2, ex.Message + "\r\n" + ex.StackTrace);
            }
            
        }

        private void ProcessError(SocketAsyncEventArgs readSocket)
        {
            LogWriter.Instance.WriteLog(2, readSocket.SocketError.ToString());
            CloseReadSocket(readSocket);
        }

        // This overload of the close method doesn't require a token
        private void CloseReadSocket(SocketAsyncEventArgs readSocket)
        {
            OSClientUserToken token = readSocket.UserToken as OSClientUserToken;
            LogWriter.Instance.WriteLog(1, new StackFrame(1).GetMethod().Name + " socket close socketID = " + token.TokenID);
            CloseReadSocket(token, readSocket);
        }


        // This method closes the read socket and gets rid of our user token associated with it
        private void CloseReadSocket(OSClientUserToken token, SocketAsyncEventArgs readSocket)
        {
            LogWriter.Instance.WriteLog(1, "Close read socket id = " + token.TokenID);
            token.Dispose();
        }

        private void OSClientCore_socketEvent(object sender, SocketEventArgs e)
        {
            OnSocketEvent(e);
        }

        public event EventHandler<SocketEventArgs> socketEvent;
        public void OnSocketEvent(SocketEventArgs e)
        {
            EventHandler<SocketEventArgs> handler = socketEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
