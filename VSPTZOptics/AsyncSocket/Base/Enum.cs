﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncSocket.Base
{
     public enum DATA_TYPE_ENUM
    {
        USERNAME,
        PASSWORD,
        CLIENTID,
        CAMERAID,
        SOCKETMODE,
        SCREENINFO,
        DIVISION,
        PLAYBACK,
        PLAYBACK_UPDATE
    }
    public enum PTZ_COMMAND_TYPE_ENUM
    {
        COMMAND = 0,//ack, completion
        PRESET = 1,//ack, completion is optional//treated as Command
        OSD = 2,//no return value//treated as Command
        GET_ZOOM_INFO = 3,
        GET_FOCUS_INFO = 4,
        GET_AF_INFO = 5,
        GET_SHUTTER_INFO = 6,
        GET_IRIS_INFO = 7,
        GET_BRIGHT_INFO = 8,
        GET_PAN_TILT_INFO = 9,
        GET_LUMINANCE_INFO = 10,
        GET_CONTRAST_INFO = 11,
        GET_HUE_INFO = 12,
        GET_AE_MODE = 13,
        GET_WB_MODE = 14
    }
    public enum EVENT_TYPE_ENUM
    {
        SOCKET_STATUS
    }
}
