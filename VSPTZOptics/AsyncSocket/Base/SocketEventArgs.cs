﻿using ShareLibrary.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AsyncSocket.Base
{
    public class SocketEventArgs : EventArgs
    {
        public STATUS_ENUM SocketStatus;
        public EVENT_TYPE_ENUM EventType;
        public SocketEventArgs()
        {
            SocketStatus = STATUS_ENUM.UNKNOW;
        }
    }
}
