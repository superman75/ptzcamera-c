﻿using AsyncSocket.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using ShareLibrary.Base;
using AsyncSocketServer;
using ShareLibrary.Model;

namespace AsyncSocket
{
    /// <summary>
    /// class OSUserToken : IDisposable
    /// This class represents the instantiated read socket on the server side.
    /// It is instantiated when a server listener socket accepts a connection.
    /// </summary>
    sealed class OSClientUserToken : IDisposable
    {
        //This is the unique ID
        private string tokenID;

        // This is a ref copy of the socket that owns this token
        private Socket ownersocket;

        // this byte array is used to accumulate data off of the readsocket
        private byte[] dataArray;
        private Int32 totalByteCount;

        // The read socket that creates this object sends a copy of its "parent" accept socket in as a reference
        // We also take in a max buffer size for the data to be read off of the read socket
        private VIEW_MODE_ENUM viewMode;
        public OSClientUserToken(Socket _readSocket, Int32 _bufferSize, string _uuid)
        {
            ownersocket = _readSocket;
            dataArray = new byte[_bufferSize];
            totalByteCount = 0;
            tokenID = _uuid;
        }
        ~OSClientUserToken() //finalize/ Destructor
        {
            Dispose(false);            
        }

        // This allows us to refer to the socket that created this token's read socket
        public Socket OwnerSocket
        {
            get { return ownersocket; }
        }
        public string TokenID
        {
            get { return tokenID; }
        }
 
        public Int32 Totalbytecount
        {
            get { return totalByteCount; }
            set { totalByteCount = value; }
        }

        // This method gets the data out of the read socket and adds it to the accumulator byte array
        public bool ReadSocketData(SocketAsyncEventArgs readSocket)
        {
            Int32 bytecount = readSocket.BytesTransferred;

            return true;
        }

        // This is a standard IDisposable method
        // In this case, disposing of this token closes the accept socket

        private bool _disposed = false;
        void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Dispose any managed objects
                    // ...
                    LogWriter.Instance.WriteLog(1, "Socket dispose");
                    try{
                        ownersocket.Shutdown(SocketShutdown.Both);
                    }
                    catch{
                        //Nothing to do here, connection is closed already
                    }
                    finally{
                        ownersocket.Close();
                    }
                }
                // Now disposed of any unmanaged objects
                // ...
                _disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public event EventHandler<SocketEventArgs> SocketEvent;
        public void OnSocketEvent(SocketEventArgs e)
        {
            EventHandler<SocketEventArgs> handler = SocketEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
