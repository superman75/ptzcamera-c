﻿using AsyncSocket.Base;
using ShareLibrary.Base;
using ShareLibrary.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace AsyncSocket
{
    /// <summary>
    /// class OSClient : OSCore
    /// This is a naive client class that I added into this project just to test the server.
    /// It does very little error checking and is not suitable for anything but testing.
    /// </summary>
    public class OSClient : OSClientCore
    {
        private Thread reconnectThread;
        private object lockReconnectThread;
        private bool isStop;
        private bool isStartConect;
        private string serverAddress;
        private int port;
        private STATUS_ENUM socketStatus;

        private Queue<ControlMessage> sendMessageQueue; //list of data to be sent
        private object queueLock;
        private Thread threadSendData;
        private Stopwatch timer;
        ManualResetEvent oSignalEvent;
        public bool IsStop
        {
            get { return isStop; }
            set { isStop = value;
                               
            }
        }
        public bool IsStartConnect
        {
            get { return isStartConect; }
            set
            {
                isStartConect = value;
                if (!isStartConect)
                    DisConnect();
                else
                    LogWriter.Instance.WriteLog(1, "Start socket " + ServerAddress + " port " + port);
            }
        }
        public string ServerAddress
        {
            get { return serverAddress; }
            set { serverAddress = value; }
        }
        public int Port
        {
            get { return port; }
            set { port = value; }
        }       
        public STATUS_ENUM SocketStatus
        {
            get { return socketStatus; }
            set {
                socketStatus = value;
                SocketEventArgs args = new SocketEventArgs();
                args.SocketStatus = socketStatus;
                args.EventType = EVENT_TYPE_ENUM.SOCKET_STATUS;
                OnSocketEvent(args);
            }
        }
        public OSClient(string _server, int _port)
        {
            isStop = false;
            isStartConect = false;
            serverAddress = _server;
            port = _port;
            lockReconnectThread = new object();
            reconnectThread = new Thread(CheckConnection);
            reconnectThread.SetApartmentState(ApartmentState.MTA);
            reconnectThread.Start();

            sendMessageQueue = new Queue<ControlMessage>((int)LIMITATION_ENUM.MAX_CLIENT_MESSAGE_QUEUE);
            queueLock = new object();
            timer = new Stopwatch();
            oSignalEvent = new ManualResetEvent(false);
            threadSendData = new Thread(SendDataToServer);
            threadSendData.SetApartmentState(ApartmentState.MTA);
            //threadSendData.Start();
        }

        public void Close()
        {
            isStop = true;
            DisConnect();
            reconnectThread.Abort();
            oSignalEvent.Set();
            threadSendData.Abort();
        }

        ~OSClient()
        {
            Close();
        }

        public bool Enqueue(ControlMessage _message)
        {
            bool res = false;
            lock (queueLock)
            {
                if (sendMessageQueue.Count >= (int)LIMITATION_ENUM.MAX_CLIENT_MESSAGE_QUEUE)
                {
                    LogWriter.Instance.WriteLog(2, "Queue is full count = " + sendMessageQueue.Count);
                    res = false;
                }
                else
                {
                    sendMessageQueue.Enqueue(_message);
                    res = true;
                }
            }
            oSignalEvent.Set();
            oSignalEvent.Reset();//Once we call the Set() method on the ManualResetEvent object, its boolean remains true. To reset the value we can use Reset() method. Reset method change the boolean value to false.
            return res;
        }
        public ControlMessage Dequeue()
        {
            lock (queueLock)
            {
                if (sendMessageQueue.Count > 0){
                    return sendMessageQueue.Dequeue();
                }
                else{
                    return null;
                }
            }
        }
        public void ClearQueue()
        {
            lock (queueLock)
            {
                sendMessageQueue.Clear();
            }
        }
        private void SendDataToServer()
        {
            LogWriter.Instance.WriteLog(1, "SendDataToServer start");
            ControlMessage sendMessage;
            while (!isStop)
            {
                try
                {
                    lock (queueLock)
                    {
                        if (sendMessageQueue.Count > 0)
                        {
                            sendMessage = sendMessageQueue.Dequeue();
                        }
                        else
                            sendMessage = null;
                    }
                    if (sendMessage != null && sendMessage.Data.Length > 0){
                        int sentB = Send(sendMessage.Data);
                        if (sentB != sendMessage.Data.Length)
                        {
                            //LogWriter.Instance.WriteLog(2, "Unexpected sending data sentB = " + sentB + " dataLength = " + sendMessage.Data.Length);
                        }
                        sendMessage.Status = MESSAGE_STATUS_ENUM.SENT;                    

                    }
                    else
                        oSignalEvent.WaitOne();
                }
                catch (Exception e){
                    //LogWriter.Instance.WriteLog(2, e.Message + e.StackTrace);
                }
            }
            LogWriter.Instance.WriteLog(1, "SendDataToServer end");
        }
        private void CheckConnection()
        {
            lock(lockReconnectThread)
            {
                while (!isStop)
                {
                    try
                    {
                        if (isStartConect)
                        {
                            if (connectionSocket == null || !connectionSocket.Connected) //Socket is not connected then try to reconnect
                            {
                                LogWriter.Instance.WriteLog(1, "Try to reconnect socket");
                                DisConnect();
                                if (!Connect())
                                    LogWriter.Instance.WriteLog(2, "Failed to connect server camera port = " + port);
                                else
                                    SocketStatus = STATUS_ENUM.CONNECTED;
                            }
                            else
                                SocketStatus = STATUS_ENUM.CONNECTED;
                            int count = 0;
                            while(!isStop && count < 10)
                            {
                                Thread.Sleep(500);
                                count++;
                            }                            
                        }
                        else
                        {
                            DisConnect();
                            int count = 0;
                            while (!isStop && count < 3)
                            {
                                Thread.Sleep(500);
                                count++;
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        LogWriter.Instance.WriteLog(2, e.Message + e.StackTrace);
                    }
                }
            }
        }
        public int Send(byte[] sendData)
        {
            // We need a connection to the server to send a message
            if (connectionSocket != null && connectionSocket.Connected)
            {
                return connectionSocket.Send(sendData);
            }
            else
            {
                return -1;
            }
        }

        // This method connects us to the server.
        // Winsock is very optimistic about connecting to the server.
        // It will not tell you, for instance, if the server actually accepted the connection.  It assumes that it did.
        public bool Connect()
        {
            if (CreateSocket(serverAddress, port)){
                try{
                    // This will succeed as long as server is listening on this IP and port
                    var connectendpoint = CreateIPEndPoint(serverAddress, port);
                    connectionSocket.Connect(connectionEndPoint);
                    if (SocketAsyncEvent != null){
                        SocketAsyncEvent.UserToken = new OSClientUserToken(connectionSocket, DEFAULT_BUFFER_SIZE, Guid.NewGuid().ToString());
                        connectionSocket.ReceiveAsync(SocketAsyncEvent); 
                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex){
                    LogWriter.Instance.WriteLog(2, "server = " + serverAddress + " port = " + port + " " + ex.Message + "\r\n" + ex.StackTrace);
                    return false;
                }
            }
            else{
                return false;
            }
        } 

        public event EventHandler<SocketEventArgs> SocketEvent;
        public void OnSocketEvent(SocketEventArgs e)
        {
            EventHandler<SocketEventArgs> handler = SocketEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        // This method disconnects us from the server
        public void DisConnect()
        {
            try
            {
                if(connectionSocket != null && connectionSocket.Connected)
                {
                    connectionSocket.Close();
                    connectionSocket = null;
                }
            }
            catch
            {
                //nothing to do since connection is already closed
            }
            SocketStatus = STATUS_ENUM.DISCONNECTED;
        }
        static bool IsSocketConnected(Socket s)
        {
            return !((s.Poll(1000, SelectMode.SelectRead) && (s.Available == 0)) || !s.Connected);

            /* The long, but simpler-to-understand version:

                    bool part1 = s.Poll(1000, SelectMode.SelectRead);
                    bool part2 = (s.Available == 0);
                    if ((part1 && part2 ) || !s.Connected)
                        return false;
                    else
                        return true;

            */
        }
    }
}
