#include "RenderInfParams.h"

RenderInfParams::RenderInfParams()
{
	isRender = false;
	//Display params - begin
	tDisplay.QuadPart = 0;
	tDisplayTick = 0;
	frequencyDisplay.QuadPart = 0;
	fpsDisplay = 0;
	nFramesDisplay = 0;
	//Display params - end

	//Render params - begin
	timeStart.QuadPart = 0;
	frequency.QuadPart = 0;
	avgFrameRate = 0;
	maxFps = 0;
	fps = 0;
	nFrames = 0;
	numOfCheckIncreaseAvgFrameRate = 0;
	numOfCheckDecreaseAvgFrameRate = 0;

	cameraInd = -1;

	tAvgFrameRate.QuadPart = 0;
	currentBufferSize = 0;
	lastBufferSize = 0;
	numAdjustFrameRate = 0;
	delay_buffer = -1;
	timeNeedToSleep = 0;

	isEnableWriteLog = false;
	timeStartWriteLog = 0;
}

RenderInfParams::~RenderInfParams()
{

}

void RenderInfParams::ResetFrameRate()
{
	nFrames = 0;
	//fps = 0;
	timeStart.QuadPart = 0;
	nFramesDisplay = 0;
	//fpsDisplay = 0;
	tDisplay.QuadPart = 0;
	tDisplayTick = 0;
	timeStartTick = 0;
	//timeStartForFrameTick = 0;
}
