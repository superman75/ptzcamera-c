#pragma once
#include "stdafx.h"
#include <winsock2.h>
#include <pthread.h>
#include <vector>
#include "msclr/lock.h"

#define MIN_BUFFER_SIZE				10
#define MAX_BUFFER_SIZE				20
#define MAX_BUFFER_SIZE_APPEND		20
#define MIN_TIME_THRESHOLD	1000 //milliseconds
#define MIN_TIME_THRESHOLD2	3000 //milliseconds

using namespace ShareLibrary::Model;

enum ENUM_FRAME_RATE_MODE
{
	NONE_MODE = 0,
	AUTO_MODE
};

enum ENUM_RENDER_MODE
{
	SINGLE_MODE = 0,
	MULTI_MODE
};

enum ENUM_PROCESS_MODE
{
	SINGLE_PROCES_MODE = 0,
	DUAL_PROCESS_MODE
};

class RenderInfParams
{
private:
public:
	bool isRender;
	//Display params - begin
	LARGE_INTEGER tDisplay;
	LARGE_INTEGER frequencyDisplay;
	DWORD tDisplayTick;

	double fpsDisplay;
	unsigned long long nFramesDisplay;
	//Display params - end

	//Render params - begin
	LARGE_INTEGER timeStart;
	LARGE_INTEGER timeStartForFrame;
	DWORD timeStartTick;
	DWORD timeStartForFrameTick;
	LARGE_INTEGER frequency;
	double avgFrameRate = 0;
	double maxFps;
	double fps;
	unsigned long long nFrames;
	int numOfCheckIncreaseAvgFrameRate;
	int numOfCheckDecreaseAvgFrameRate;

	int cameraInd;

	LARGE_INTEGER tAvgFrameRate;
	DWORD tAvgFrameRateTick;

	int currentBufferSize;
	int lastBufferSize;
	int numAdjustFrameRate;
	bool isEnableWriteLog;
	int delay_buffer;
	int timeNeedToSleep;

	//For write log
	DWORD timeStartWriteLog;

	RenderInfParams();
	~RenderInfParams();
	VOID ResetFrameRate();
};
