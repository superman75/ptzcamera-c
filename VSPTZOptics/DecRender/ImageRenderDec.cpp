#include "stdafx.h"
#include "ImageRenderDec.h"

ImageRenderDec::ImageRenderDec()
	: RenderDecCore()
{

}

ImageRenderDec::ImageRenderDec(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type)
	: RenderDecCore(_pCamera, _stream, _type)
{

}

UINT32 ImageRenderDec::ReadProcess(Camera^ _camera)
{
	char url[STR_LENGTH_256];
	String^ lastUrlStr = nullptr;
	char cameraName[STR_LENGTH_256];
	String^ lastCameraNameStr = nullptr;
	while (readThreadCtr.GetIsThreadRunning()) {
		try {
			String^ urlStr = nullptr;
			urlStr = _camera->ImageName;
			if (urlStr != lastUrlStr)
			{
				IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(urlStr);
				char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
				strncpy(url, nativeString, urlStr->Length);
				url[urlStr->Length] = STR_NULL;
				lastUrlStr = urlStr;
			}
			IMAGE_TYPE_ENUM imgType;
			if (urlStr->ToLower()->Contains(".jpg"))
				imgType = IMAGE_TYPE_ENUM::JPEG;
			else if (urlStr->ToLower()->Contains(".png"))
				imgType = IMAGE_TYPE_ENUM::PNG;
			else
				return (int)RESULT_ENUM::RS_NOT_OK;//Unknown image type
			FILE *pFile = NULL;
			fopen_s(&pFile, url, "rb");
			if (pFile == NULL) return false;
			fseek(pFile, 0, SEEK_END);
			long fsize = ftell(pFile);
			fseek(pFile, 0, SEEK_SET);  /* same as rewind(f); */
			int avPacketLength = (fsize + AV_INPUT_BUFFER_PADDING_SIZE) / sizeof(uint64_t) + 1;
			uint8_t* buff = new uint8_t[avPacketLength * 8];
			try
			{
				fread(buff, sizeof(uint8_t), fsize, pFile);
				fclose(pFile);// Close file
				bool isReadFirstFrame = false;
				while (readThreadCtr.GetIsThreadRunning())
				{
					if (_camera->IsReconnect)
						break;
					if (!isReadFirstFrame) {
						//LogWriter::Instance->WriteLog(1, "AVPacketReadFrame Main successful", __LINE__, __FUNCDNAME__);
						isReadFirstFrame = true;
						AVPacketInfo* avPacketInfo = new AVPacketInfo();
						avPacketInfo->packetMode = PACKET_MODE_ENUM::INIT;
						avPacketInfo->codecPar = new AVCodecParameters();
						memset(avPacketInfo->codecPar, 0, sizeof(AVCodecParameters));
						if (imgType == IMAGE_TYPE_ENUM::JPEG)
							avPacketInfo->codecPar->codec_id = AV_CODEC_ID_MJPEG;
						else
							avPacketInfo->codecPar->codec_id = AV_CODEC_ID_PNG;
						int res = cpuDecoder.FFmpegCpuDecoderInit(avPacketInfo->codecPar);
						delete avPacketInfo;
						if (res != (int)RESULT_ENUM::RS_OK)
						{
							break;
						}
					}
					AVPacketInfo* avPacketInfo = new AVPacketInfo();
					uint8_t* packetBuff = new uint8_t[avPacketLength * 8];
					memcpy(packetBuff, buff, fsize);
					av_init_packet(&avPacketInfo->avPacket);
					avPacketInfo->avPacket.data = packetBuff;
					avPacketInfo->avPacket.size = fsize;
					avPacketInfo->isPacketRef = false;
					avPacketInfo->avPacket.flags = 1;		

					if (cpuDecoder.FFmpegRunAsynDecoder(&avPacketInfo->avPacket, avPacketInfo->isPacketRef) == E_OK)
					{
						if(cpuDecoder.avFrameList.size() > 0)
						{
							AVFrame* avFrame = (AVFrame*)cpuDecoder.avFrameList[0];
							while (readThreadCtr.GetIsThreadRunning())
							{
								if (_camera->IsReconnect)
									break;
								if (_camera->IsNDIOutput) {
									if (_camera->Name != lastCameraNameStr)
									{
										IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(_camera->Name);
										char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
										strncpy(cameraName, nativeString, _camera->Name->Length);
										cameraName[_camera->Name->Length] = STR_NULL;
										lastCameraNameStr = _camera->Name;
										ndiStream.StopSend();
									}
									if (ConvertToBRG(avFrame, &ndiStream)) {
										ndiStream.SendFrame(ndiStream.avFrameBGR, cameraName);
									}
								}
								RenderParamProcess(_camera, avFrame);
								Sleep(100); //10fps
							}
							cpuDecoder.ReleaseFrame();
						}
					}
					delete avPacketInfo;
				}
				delete[] buff;
				buff = NULL;
			}
			catch (Exception^ ex)
			{
				LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
			}
			finally
			{
				if (buff != NULL)
					delete[] buff;
			}
		}
		catch (Exception^ e) {
			String^ url = gcnew String(url);
			String^ log = "ReadProcessThread crash " + url + " " + e->Message + "\r\n" + e->StackTrace;
			LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
		}
		int sleepCount = 0;
		while (readThreadCtr.GetIsThreadRunning() && sleepCount < 10 && !_camera->IsReconnect) //Sleep 5s
		{
			Sleep(500);
			sleepCount++;
		}
		if (_camera->IsReconnect) {
			_camera->IsReconnect = false;
		}
	}
	return (int)RESULT_ENUM::RS_OK;
}

UINT32 ImageRenderDec::DecodeProcess(Camera^ _camera)
{
	WaitForSingleObject(decThreadCtr.hWakeUpThread, INFINITE);
	return (int)RESULT_ENUM::RS_OK;
}

UINT32 ImageRenderDec::RenderProcess(Camera^ _camera)
{
	WaitForSingleObject(renThreadCtr.hWakeUpThread, INFINITE);
	return (int)RESULT_ENUM::RS_OK;
}
