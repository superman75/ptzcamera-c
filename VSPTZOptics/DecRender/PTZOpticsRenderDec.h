#pragma once

#include "RenderDecCore.h"

class PTZOpticsRenderDec : public RenderDecCore
{
private:

public:
	PTZOpticsRenderDec();
	PTZOpticsRenderDec(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type);
	UINT32 ReadProcess(Camera^ _camera);
	UINT32 DecodeProcess(Camera^ _camera);
};
