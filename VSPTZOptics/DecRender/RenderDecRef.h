#pragma once

/* System headers */
#include <string.h>

/* Application headers */
#include "msclr/lock.h"
#include "RenderDecCore.h"
#include "PTZOpticsRenderDec.h"
#include "ImageRenderDec.h"
#include "NDIRenderDec.h"

using namespace std;
using namespace System;
using namespace msclr;
using namespace System::Diagnostics;
using namespace System::Security;
using namespace System::Runtime::ExceptionServices;
using namespace System::Runtime::InteropServices;
using namespace ShareLibrary::Base;
using namespace System::Collections::Generic;

namespace NsRenderHandlerRef
{
	using namespace std;
	using namespace System;
	using namespace System::Collections::Generic;
	public ref class RenderHandlerRef
	{
	private:
		IntPtr renderCoreObj;
		IntPtr handle;
	public:
		RenderHandlerRef(IntPtr _handle, DISPLAY_RATIO_ENUM _displayRatio, bool _bHWAccel);
		~RenderHandlerRef();
		!RenderHandlerRef();
		void Destroy();
		IntPtr GetRenderCoreObject();
		void SetSuppendRender(bool _isSuppendRender);
		void SetHWAccel(bool _bHWAccel);
		void SetDisplayRatio(DISPLAY_RATIO_ENUM _displayRatio);
		void SetZoomFactor(double _zoomFactor);
		void SetZoomPosition(int _zoomPosX, int _zoomPosY);
		System::Drawing::Rectangle^ GetDisplaySize();
		void GetDisplayRatio(float %_wRatio, float %_hRatio);
	};
};

namespace NsRenderDecRef
{
	using namespace std;
	using namespace System;
	using namespace System::Collections::Generic;
	using namespace NsRenderHandlerRef;
	public ref class RenderDecRef
	{
	private:
		IntPtr renderDecCoreObj;
		GCHandle cameraHandle;
	public:
		RenderDecRef(Camera^ _camera, CAMERA_TYPE_ENUM _type, CAMERA_STREAM_ENUM _stream, bool _bHWAccel);
		RenderDecRef(String^ _cameraID);
		~RenderDecRef();
		!RenderDecRef();
		IntPtr GetRenderDecCoreObj();
		void Destroy();
		void ClearAllQueue();
		void EnqueueDecode(cli::array<System::Byte>^ _buffer, int _startInd, int _dataLength, PACKET_MODE_ENUM _decodeMode);
		void AddRender(RenderHandlerRef^ _render);
		void RemoveRender(RenderHandlerRef^ _render);
		void ClearAllRender();
		void SetHWAccel(bool _bHWAccel);
		void Start(bool _isStartReadThread, bool _isStartDecodeThread, bool _isStartRenderThread);
		void Stop();
	};
};

