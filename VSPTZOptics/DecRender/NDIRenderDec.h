#pragma once
#include "RenderDecCore.h"
class NDIRenderDec : public RenderDecCore
{
private:

public:
	NDIRenderDec();
	NDIRenderDec(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type);
	UINT32 ReadProcess(Camera^ _camera);
	UINT32 DecodeProcess(Camera^ _camera);
	UINT32 RenderProcess(Camera^ _camera);
	bool CopyNDIData(NDIlib_video_frame_v2_t* _ndiFrame, NDIStreamCore* _pNDI);
};