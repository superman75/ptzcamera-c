#pragma once
#include "RenderDecCore.h"

class ImageRenderDec : public RenderDecCore
{
private:

public:
	ImageRenderDec();
	ImageRenderDec(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type);
	UINT32 ReadProcess(Camera^ _camera);
	UINT32 DecodeProcess(Camera^ _camera);
	UINT32 RenderProcess(Camera^ _camera);
};

