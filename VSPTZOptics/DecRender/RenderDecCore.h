/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = RenderDecCore.cpp                                           */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018_ ArethSoftware Ltd.        All rights reserved.					  */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class definations				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/*_ ArethSoftware Ltd. the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Areth be liable to the User for any incidental,          */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Areth shall not be liable for any services or products       */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Areth in connection with the Product(s) and/or     */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#pragma once

#include "ThreadCtl/ThreadController.h"
#include "AVInfo/AVInfo.h"
#include "CpuDec/FFmpegCpuDecoder.h"
#include "SDLRender/FramePresenterSDL.h"
#include "RenderInfParams.h"
#include "Stream/StreamCommon.h"
#include "NDIStreamCore.h"

using namespace std;
using namespace System;
using namespace System::Diagnostics;
using namespace System::Security;
using namespace System::Runtime::ExceptionServices;
using namespace ShareLibrary::Base;
using namespace System::Runtime::InteropServices;
using namespace System::IO;
using namespace System::Security::AccessControl;

/*******************************************************************************
**                      Class Defination Section							  **
*******************************************************************************/

class RenderParam
{
public:
	CriticalSection crsRamProtection;
	std::vector<UINT64> paramList;
	RenderParam();
	~RenderParam();
	template<class T> VOID AddRender(T* _param)
	{
		try
		{
			bool isExist = false;
			crsRamProtection.Enter();
			if (paramList.size() > 0)
			{
				vector<UINT64>::iterator it = paramList.begin();
				for (; it != paramList.end(); it++)
				{
					T* renderPar = (T*)*it;
					if (renderPar->handle == _param->handle)
					{
						isExist = true;
						return;
					}
				}
			}
			if (!isExist) {
				paramList.push_back((UINT64)_param);
			}
		}
		catch (Exception^ e)
		{
			LogWriter::Instance->WriteLog(1, "Add Handle crash for ", __LINE__, __FUNCDNAME__);
		}
		finally
		{
			crsRamProtection.Leave();
		}
	};
	template<class T> VOID RemoveRender(T* _param)
	{
		try
		{
			crsRamProtection.Enter();
			int index = 0;
			if (paramList.size() > 0)
			{
				vector<UINT64>::iterator it = paramList.begin();
				for (; it != paramList.end(); it++)
				{
					T* renderPar = (T*)*it;
					if (renderPar->handle == _param->handle)
					{
						it = paramList.erase(it);
						return;
					}
				}
			}
		}
		catch (Exception^ e)
		{
			LogWriter::Instance->WriteLog(1, "Add Handle crash for", __LINE__, __FUNCDNAME__);
		}
		finally
		{
			crsRamProtection.Leave();
		}
	};
	VOID ClearAllRender();
};

public class RenderDecCore
{
private:
	static void InitLib();
	static void AVPacketCopy(AVPacket * _dest, AVPacket* _src, bool _isLinkData = false);

public:
	static CriticalSection csrInitLib;
	static bool blInitLib;
	char	cameraID[64];
	bool	blHWAccel;
	bool    blFirstIFrameForDecode;
	CAMERA_STREAM_ENUM stream;
	CAMERA_TYPE_ENUM type;

	/* Define external objects used */
	FFmpegCpuDecoder cpuDecoder;

#ifdef HW_ACC_ENABLED
	CudaDecoder cuDecoder;
#endif

	RenderParam renderParam;
	NDIStreamCore ndiStream;
	FFmpegCpuEncoder cpuEncoder;
	StreamCommon streamCommon;

	/* Thread params for each thread process */
	ThreadController readThreadCtr;
	ThreadController decThreadCtr;
	ThreadController renThreadCtr;

	RenderDecCore();
	RenderDecCore(void* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type);
	~RenderDecCore();
	virtual void    ClearAllQueue();
	virtual void	EnqueueDecode(unsigned char* buffer, int buffSize, PACKET_MODE_ENUM _decodeMode);
	template<class T> void AddRender(T* _param)
	{
		renderParam.AddRender<T>(_param);
	};
	template<class T> void RemoveRender(T* _param)
	{
		renderParam.RemoveRender<T>(_param);
	};
	virtual void	ClearAllRender();
	virtual void Start(bool _isStartReadThread = true, bool _isStartDecodeThread = true, bool _isStartRenderThread = true);
	virtual void Stop();

	virtual void SaveFrame(AVFrame *pFrame, int width, int height, int iFrame);
	virtual bool SaveJPEGPacket(AVPacket* _pAvPacket, String^ _path);
	virtual bool ConvertToBRG(AVFrame* _avFrame, NDIStreamCore* _pNDI);
	virtual void DetectFPSTickCount(RenderInfParams* renderInfParams, String^ %_log, Stopwatch^ %swFrame);
	virtual void RenderParamProcess(Camera^ _camera, AVFrame* _avFrame);
	virtual UINT32 ReadProcess(Camera^ _camera);
	virtual UINT32 DecodeProcess(Camera^ _camera);
	virtual UINT32 RenderProcess(Camera^ _camera);
};

class RenderHandlerCore
{
private:
	bool	isInit;
	VOID	Destroy();
public:
	bool	bHWAccel;
	FramePresenterSDL sdlParam;
#ifdef HW_ACC_ENABLED
	FramePresenterD3D11 d3dRender;
#endif
	RenderHandlerCore(HWND _hwnd, DISPLAY_RATIO_ENUM _displayRatio, bool _bHWAccel);
	~RenderHandlerCore();
	VOID SetSuppendRender(bool _isSuppendRender);
	VOID SetZoomFactor(double _zoomFactor);
	VOID SetZoomPosition(int _zoomPosX, int _zoomPosY);
};