/*============================================================================*/
/* Project      = Video Management System                                     */
/* Module       = RenderDecCore.cpp                                           */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* (c) 2018 Areth Software Ltd.        All rights reserved.					  */
/*============================================================================*/
/* Purpose:                                                                   */
/* Class definations				                                          */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Areth Software Ltd., the following shall apply!							  */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Areth. Any          */
/* warranty is expressly disclaimed and excluded by Areth, either expressed   */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Areth shall not have any obligation to maintain, service or provide bug    */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Areth be liable to the User for any incidental,          */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Areth shall not be liable for any services or products       */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Areth in connection with the Product(s) and/or     */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#pragma once
/* Application header files */
#include "RenderDecCore.h"

/* System header files */
#include <fstream>

/*******************************************************************************
**                      Using Namespace Section                              **
*******************************************************************************/
using namespace ShareLibrary::Base;

using namespace std;
using namespace System;
using namespace System::Runtime::InteropServices;

extern UINT32 __stdcall DecodeProcessThread(void* pArguments);
extern UINT32 __stdcall RenderProcessThread(void* pArguments);
extern UINT32 __stdcall ReadProcessThread(void* pArguments);

/*******************************************************************************
**                      Static Symbols                                        **
*******************************************************************************/
bool RenderDecCore::blInitLib = false;
CriticalSection RenderDecCore::csrInitLib;

/******************************************************************************
**                   Class Methods Defination Section						 **
*******************************************************************************/
RenderParam::RenderParam()
{
	paramList.reserve(10);//Reserve 10 items
	paramList.clear();
}

RenderParam::~RenderParam()
{
	try
	{
		crsRamProtection.Enter();
		paramList.clear();
	}
	catch (Exception^ e)
	{
		LogWriter::Instance->WriteLog(2, "~SDL crash " + e->Message + e->StackTrace, __LINE__, __FUNCDNAME__);
	}
	finally
	{
		crsRamProtection.Leave();
	}
}

void RenderParam::ClearAllRender()
{
	try
	{
		crsRamProtection.Enter();
		paramList.clear();
	}
	catch (Exception^ e)
	{
		String^ log = "Add Handle crash for";
		if (e != nullptr)
			log += e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(1, log, __LINE__, __FUNCDNAME__);
	}
	finally
	{
		crsRamProtection.Leave();
	}
}

void RenderDecCore::InitLib()
{
	try {
		RenderDecCore::csrInitLib.Enter();
		if (!RenderDecCore::blInitLib) {
			std::ios_base::sync_with_stdio(false);

#ifdef  HW_ACC_ENABLED
			CudaDecoder::CuDaDeviceInit();
#endif			
			RenderDecCore::blInitLib = true;
		}
	}
	finally
	{
		csrInitLib.Leave();
	}
}

void RenderDecCore::AVPacketCopy(AVPacket * _dest, AVPacket* _src, bool _isLinkData)
{
	if (_src != NULL && _dest != NULL)
	{
		_dest->pts = _src->pts;
		_dest->dts = _src->dts;
		_dest->size = _src->size;
		_dest->stream_index = _src->stream_index;
		_dest->flags = _src->flags;
		_dest->duration = _src->duration;
		_dest->pos = _src->pos;
		_dest->buf = NULL;
		_dest->side_data = NULL;
		_dest->side_data_elems = 0;
		if (_isLinkData)
			_dest->data = _src->data;
		else
			_dest->data = NULL;
	}
}

RenderDecCore::RenderDecCore(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type)
{
	stream = _stream;
	type = _type;
	blHWAccel = false;
	blFirstIFrameForDecode = false;
	/* Init Decoder and Render libarary */
	RenderDecCore::InitLib();

	/* Create all controllers for all thread */
	readThreadCtr.SetThreadName("Reader Thread");
	decThreadCtr.SetThreadName("Decoder Thread");
	renThreadCtr.SetThreadName("Render Thread");
	//sVThreadCtr.SetThreadName("Supervisor for Render Decoder Core");

	UINT64 aaReaThreadPars[] = { (UINT64)this, (UINT64)_pCamera };
	UINT64 aaDecThreadPars[] = {(UINT64)this, (UINT64)_pCamera };
	UINT64 aaRenThreadPars[] = {(UINT64)this, (UINT64)_pCamera };
	//UINT64 aaSVThreadPars[] = { (UINT64)&SVThreadCtr, (UINT64)this};

	readThreadCtr.InitThreadArgs(aaReaThreadPars, (sizeof(aaReaThreadPars) / sizeof(UINT64)));
	decThreadCtr.InitThreadArgs(aaDecThreadPars, (sizeof(aaDecThreadPars)/sizeof(UINT64)));
	renThreadCtr.InitThreadArgs(aaRenThreadPars, (sizeof(aaRenThreadPars) / sizeof(UINT64)));
}

RenderDecCore::RenderDecCore()
{
	blHWAccel = false;
	blFirstIFrameForDecode = false;
	/* Init Decoder and Render libarary */
	RenderDecCore::InitLib();

	/* Create all controllers for all thread */
	readThreadCtr.SetThreadName("Reader Thread");
	decThreadCtr.SetThreadName("Decoder Thread");
	renThreadCtr.SetThreadName("Render Thread");
	//sVThreadCtr.SetThreadName("Supervisor for Render Decoder Core");

	UINT64 aaReaThreadPars[] = { (UINT64)this };
	UINT64 aaDecThreadPars[] = { (UINT64)this };
	UINT64 aaRenThreadPars[] = { (UINT64)this };
	//UINT64 aaSVThreadPars[] = { (UINT64)&SVThreadCtr, (UINT64)this};

	readThreadCtr.InitThreadArgs(aaReaThreadPars, (sizeof(aaReaThreadPars) / sizeof(UINT64)));
	decThreadCtr.InitThreadArgs(aaDecThreadPars, (sizeof(aaDecThreadPars) / sizeof(UINT64)));
	renThreadCtr.InitThreadArgs(aaRenThreadPars, (sizeof(aaRenThreadPars) / sizeof(UINT64)));
	
}

RenderDecCore::~RenderDecCore()
{
	readThreadCtr.Stop();
	decThreadCtr.Stop();
	renThreadCtr.Stop();
	ClearAllQueue();
}

void RenderDecCore::ClearAllQueue()
{
	decThreadCtr.crsBuffer.Enter();
	decThreadCtr.ClearQueue<AVPacketInfo>();
	decThreadCtr.crsBuffer.Leave();

	renThreadCtr.crsBuffer.Enter();
	renThreadCtr.ClearQueue<AVFrameInfo>();
	renThreadCtr.crsBuffer.Leave();
}

void RenderDecCore::EnqueueDecode(unsigned char* buffer, int buffSize, PACKET_MODE_ENUM _decodeMode)
{
	AVPacketInfo* packetInfo = new AVPacketInfo();
	if (_decodeMode == PACKET_MODE_ENUM::INIT) {
		packetInfo->packetMode = PACKET_MODE_ENUM::INIT;
		packetInfo->codecPar = new AVCodecParameters();
		if (buffSize >= sizeof(AVCodecParameters)) {
			memcpy(packetInfo->codecPar, buffer, sizeof(AVCodecParameters));
			if (packetInfo->codecPar->extradata_size + sizeof(AVCodecParameters) == buffSize)
			{
				if(packetInfo->codecPar->extradata_size > 0)
				{
					int length = (packetInfo->codecPar->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE) / sizeof(uint64_t) + 1;
					unsigned char* extra = new unsigned char[length * 8];
					memcpy(extra, buffer + sizeof(AVCodecParameters), packetInfo->codecPar->extradata_size);
					packetInfo->codecPar->extradata = extra;
				}
			}
			else
			{
				LogWriter::Instance->WriteLog(2, "Unexpected buffSize of AVCodecParameters = " + buffSize, __LINE__, __FUNCDNAME__);
				delete packetInfo;
				packetInfo = NULL;
				return;
			}
		}
		else {
			LogWriter::Instance->WriteLog(2, "Unexpected buffSize of AVCodecParameters = " + buffSize, __LINE__, __FUNCDNAME__);
			delete packetInfo;
			packetInfo = NULL;
			return;
		}
	}
	else { //data
		packetInfo->packetMode = PACKET_MODE_ENUM::DRAW_DATA;
		AVPacketCopy(&packetInfo->avPacket, (AVPacket*)buffer);
		if (packetInfo->avPacket.size != (buffSize - sizeof(AVPacket)))
		{
			LogWriter::Instance->WriteLog(2, "Unexpected AVpacket size = " + buffSize + " packet.size = " + packetInfo->avPacket.size, __LINE__, __FUNCDNAME__);
			delete packetInfo;
			packetInfo = NULL;
			return;
		}
		int length = (packetInfo->avPacket.size + AV_INPUT_BUFFER_PADDING_SIZE) / sizeof(uint64_t) + 1;
		unsigned char* data = new unsigned char[length * 8];
		memcpy(data, buffer + sizeof(AVPacket), packetInfo->avPacket.size);
		packetInfo->avPacket.data = data;
	}
	try {
		decThreadCtr.crsBuffer.Enter();
		decThreadCtr.queueBuffer.push_back((UINT64)packetInfo);
	}
	finally{
		decThreadCtr.crsBuffer.Leave();
	}
	SetEvent(decThreadCtr.hWakeUpThread);
}

void RenderDecCore::ClearAllRender()
{

}

void RenderDecCore::Start(bool _isStartReadThread, bool _isStartDecodeThread, bool _isStartRenderThread)
{
	if(_isStartRenderThread)
		renThreadCtr.Start(RenderProcessThread);
	if(_isStartDecodeThread)
		decThreadCtr.Start(DecodeProcessThread);
	if(_isStartReadThread)
		readThreadCtr.Start(ReadProcessThread);
}

void RenderDecCore::Stop()
{
	readThreadCtr.Stop();
	decThreadCtr.Stop();
	renThreadCtr.Stop();
	ClearAllQueue();
}

void RenderDecCore::SaveFrame(AVFrame *pFrame, int width, int height, int iFrame)
{
	FILE *pFile;
	char szFilename[32];
	int  y;

	sprintf_s(szFilename, "frame%d.ppm", iFrame);// Open file
	fopen_s(&pFile, szFilename, "wb");
	if (pFile == NULL) return;
	fprintf_s(pFile, "P6\n%d %d\n255\n", width, height);// Write header
	for (y = 0; y < height; y++) fwrite(pFrame->data[0] + y * pFrame->linesize[0],
		1, width * 3, pFile);// Write pixel data
	fclose(pFile);// Close file
}//SaveFrame

bool RenderDecCore::SaveJPEGPacket(AVPacket* _pAvPacket, String^ _path)
{
	try
	{
		FILE *pFile = NULL;
		char fileName[STR_LENGTH_256];
		sprintf_s(fileName, "%s", _path);// Open file
		remove(fileName);
		fopen_s(&pFile, fileName, "wb");
		if (pFile == NULL) return false;
		fwrite(_pAvPacket->data, sizeof(uint8_t), _pAvPacket->size, pFile);
		fclose(pFile);// Close file
		return true;
	}
	catch (Exception^ e)
	{
		return false;
	}

}//SaveJPEGPacket

bool RenderDecCore::ConvertToBRG(AVFrame* _avFrame, NDIStreamCore* _pNDI)
{
	try
	{
		_pNDI->frameInd++;
		_pNDI->avFrameBGR = _pNDI->frameInd % 2 == 0 ? _pNDI->ppAvFrameBGR[0] : _pNDI->ppAvFrameBGR[1];

		if (_pNDI->avFrameBGR == NULL)
		{
			_pNDI->avFrameBGR = av_frame_alloc();
			_pNDI->frameInd % 2 == 0 ? _pNDI->ppAvFrameBGR[0] = _pNDI->avFrameBGR : _pNDI->ppAvFrameBGR[1] = _pNDI->avFrameBGR;
			av_image_alloc(_pNDI->avFrameBGR->data, _pNDI->avFrameBGR->linesize, _avFrame->width, _avFrame->height, AV_PIX_FMT_BGRA, 1);
			_pNDI->avFrameBGR->width = _avFrame->width;
			_pNDI->avFrameBGR->height = _avFrame->height;
		}

		if (_pNDI->swsCtx == NULL)
		{
			_pNDI->swsCtx = sws_getContext(_avFrame->width, _avFrame->height,
				(AVPixelFormat)_avFrame->format, _avFrame->width, _avFrame->height,
				AV_PIX_FMT_BGRA, SWS_BICUBIC, 0, 0, 0);
		}
		sws_scale(_pNDI->swsCtx,
			_avFrame->data,
			_avFrame->linesize,
			0,
			_avFrame->height,
			_pNDI->avFrameBGR->data,
			_pNDI->avFrameBGR->linesize);
		return true;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return false;
	}
}


void RenderDecCore::DetectFPSTickCount(RenderInfParams* renderInfParams, String^ %_log, Stopwatch^ %swFrame)
{
	DWORD tEndTick = GetTickCount();
#pragma region calculate fps for display
	if (renderInfParams->tDisplayTick == 0)
	{
		renderInfParams->tDisplayTick = GetTickCount();
	}
	else
	{
		renderInfParams->nFramesDisplay++;
		renderInfParams->fpsDisplay = renderInfParams->nFramesDisplay * 1000 / (double)(tEndTick - renderInfParams->tDisplayTick);
		if (renderInfParams->maxFps > 0 && renderInfParams->fpsDisplay > renderInfParams->maxFps) //Unlimited if maxFps = 0
		{
			renderInfParams->nFramesDisplay--;
			renderInfParams->fpsDisplay -= 1;
			renderInfParams->isRender = false;
		}
	}
#pragma endregion calculate fps for display
	if (renderInfParams->delay_buffer == -1)//for auto buffer mode
	{
		if (renderInfParams->timeStartTick == 0)
		{
			renderInfParams->timeStartTick = GetTickCount();
			if (swFrame == nullptr)
				swFrame = Stopwatch::StartNew();
		}
		else
		{
			renderInfParams->nFrames++;
			double duarationMiliSecond = (double)(tEndTick - renderInfParams->timeStartTick);
#pragma region adjust avgFrameRate for auto mode
			if (renderInfParams->avgFrameRate == 0)
			{
				if (duarationMiliSecond >= MIN_TIME_THRESHOLD2)
				{
					if (0 == renderInfParams->numAdjustFrameRate)
						renderInfParams->numAdjustFrameRate++;
					else
					{
						renderInfParams->fps = renderInfParams->nFrames * 1000 / duarationMiliSecond;
						renderInfParams->avgFrameRate = (int)(renderInfParams->fps + 0.5);
						if (renderInfParams->isEnableWriteLog)
						{
							String^ log = "Camera = " + renderInfParams->cameraInd + " AUTO_MODE assign avgFrameRate = " + renderInfParams->avgFrameRate;
							LogWriter::Instance->WriteLog(1, log, __LINE__, __FUNCDNAME__);
						}
					}
					renderInfParams->ResetFrameRate();
					goto exit;
				}
			}
			else
			{
				if (renderInfParams->avgFrameRate > 0)
				{
					if (0 == renderInfParams->tAvgFrameRateTick)
					{
						renderInfParams->tAvgFrameRateTick = GetTickCount();
					}
					else
					{
						DWORD tEndAvgFrameRateTick = GetTickCount();
						double duarationT = (double)(tEndAvgFrameRateTick - renderInfParams->tAvgFrameRateTick);
						if (renderInfParams->avgFrameRate > 0 && duarationT >= MIN_TIME_THRESHOLD) //check to update fps every MIN_TIME_THRESHOLD
						{
							int minBufferSize = MIN_BUFFER_SIZE;
							int maxBufferSize = MAX_BUFFER_SIZE;
							if (renderInfParams->currentBufferSize > maxBufferSize) //buffer dang tang dan -> tang fps
							{
								renderInfParams->numOfCheckDecreaseAvgFrameRate = 0;
								renderInfParams->numOfCheckIncreaseAvgFrameRate++;
								double frameRateGap = 0.25 + (double)1 / (double)(renderInfParams->numOfCheckIncreaseAvgFrameRate + 1);
								if (frameRateGap > 0)
								{
									renderInfParams->avgFrameRate += frameRateGap;
									if (renderInfParams->isEnableWriteLog)
									{
										_log += "\r\n" + "Camera = " + renderInfParams->cameraInd + " increase frame rate = " + renderInfParams->avgFrameRate + " current buffer size = " + renderInfParams->currentBufferSize + " lastBufferSize = " + renderInfParams->lastBufferSize;
									}
									renderInfParams->lastBufferSize = renderInfParams->currentBufferSize;
									renderInfParams->ResetFrameRate();
								}
							}
							else if (renderInfParams->currentBufferSize < minBufferSize) //buffer dang giam dan -> giam fps
							{
								renderInfParams->numOfCheckIncreaseAvgFrameRate = 0;
								renderInfParams->numOfCheckDecreaseAvgFrameRate++;
								double frameRateGap = 0.25 + (double)1 / (double)(renderInfParams->numOfCheckDecreaseAvgFrameRate + 1);
								if (frameRateGap > 0)
								{
									renderInfParams->avgFrameRate -= frameRateGap;
									if (renderInfParams->isEnableWriteLog)
									{
										_log += "\r\n" + "Camera = " + renderInfParams->cameraInd + " decrease frame rate = " + renderInfParams->avgFrameRate + " current buffer size = " + renderInfParams->currentBufferSize + " lastBufferSize = " + renderInfParams->lastBufferSize;
									}
									renderInfParams->lastBufferSize = renderInfParams->currentBufferSize;
									renderInfParams->ResetFrameRate();
								}
							}
							else if (renderInfParams->numOfCheckIncreaseAvgFrameRate > 0)
							{
								double frameRateGap = 0.25 + (double)1 / (double)(renderInfParams->numOfCheckIncreaseAvgFrameRate + 1);
								renderInfParams->avgFrameRate -= frameRateGap;
								renderInfParams->numOfCheckIncreaseAvgFrameRate--;
							}
							else if (renderInfParams->numOfCheckDecreaseAvgFrameRate > 0)
							{
								double frameRateGap = 0.25 + (double)1 / (double)(renderInfParams->numOfCheckDecreaseAvgFrameRate + 1);
								renderInfParams->avgFrameRate += frameRateGap;
								renderInfParams->numOfCheckDecreaseAvgFrameRate--;
							}
							renderInfParams->tAvgFrameRateTick = GetTickCount();
						}
					}
					if (renderInfParams->timeStartWriteLog == 0)
						renderInfParams->timeStartWriteLog = GetTickCount();
					else
					{
						DWORD tEndWriteLog = GetTickCount();
						if (tEndWriteLog - renderInfParams->timeStartWriteLog > 30000)
						{
							if (renderInfParams->isEnableWriteLog)
							{
								_log += "\r\n" + "Camera = " + renderInfParams->cameraInd + " frame rate = " + renderInfParams->avgFrameRate + " current buffer size = " + renderInfParams->currentBufferSize + " lastBufferSize = " + renderInfParams->lastBufferSize;
								LogWriter::Instance->WriteLog(1, _log, __LINE__, __FUNCDNAME__);
								_log = nullptr;
							}
							renderInfParams->timeStartWriteLog = tEndWriteLog;
						}
					}
				}
			}
#pragma endregion adjust  avgFrameRate				
		}
	exit:
#pragma region sleep by avgFrameRate
		if (renderInfParams->avgFrameRate != 0)
		{
			swFrame->Stop();
			double spanMiliSecond = swFrame->ElapsedMilliseconds;// (double)(tEndTick - renderInfParams->timeStartForFrameTick);
			double timeSleep = renderInfParams->avgFrameRate > 0 ? (double)1000 / (double)(renderInfParams->avgFrameRate) : 0;
			renderInfParams->timeNeedToSleep = timeSleep - spanMiliSecond;
		}
		if (renderInfParams->timeNeedToSleep > 0)
			Sleep(int(renderInfParams->timeNeedToSleep));
		else
		{
			if (renderInfParams->timeNeedToSleep < 0)
				renderInfParams->isRender = false;
		}
		swFrame->Reset();
		swFrame->Start();
#pragma endregion sleep by avgFrameRate			
	}
}

void RenderDecCore::RenderParamProcess(Camera^ _camera, AVFrame* _avFrame)
{
	renderParam.crsRamProtection.Enter();
	if (renderParam.paramList.size() > 0)
	{
		for (auto var : renderParam.paramList) // access by reference to avoid copying
		{
			if (!blHWAccel)
			{
				if (_avFrame != NULL)
				{
					FramePresenterSDL* renderPar = (FramePresenterSDL*)var;
					if (renderPar->isSuppendReder)
						continue;
					if (_camera->StageList != nullptr)
					{
						int size = renderParam.paramList.size();
						renderPar->RenderAVFrameInfo(_avFrame, false);
#pragma region Bounding Box
						SDL_Rect* pRect = renderPar->GetDisplaySize();
						if (pRect->w * pRect->h != 0)
						{
							for each (Stage^ stage in _camera->StageList)
							{
								if (stage->IsShowOnStageView)
								{
									bool isResize = false;
									double resizeWidthRatio = 1;
									double resizeHeightRatio = 1;
									if (stage->WindowW * stage->WindowH == 0)
									{
										stage->WindowW = pRect->w;
										stage->WindowH = pRect->h;
									}
									else
									{
										if (stage->WindowW != pRect->w || stage->WindowH != pRect->h)
										{
											isResize = true;
											resizeWidthRatio = (double)pRect->w / (double)stage->WindowW;
											resizeHeightRatio = (double)pRect->h / (double)stage->WindowH;
											stage->WindowW = pRect->w;
											stage->WindowH = pRect->h;
										}
									}
									if (stage->Cl == Color::Black)
										stage->Cl = System::Drawing::Color::FromArgb(rand() % 128 + 50, rand() % 128 + 50, rand() % 128 + 50);
									if (stage->Rect.Width * stage->Rect.Height == 0)
										stage->Rect = System::Drawing::Rectangle(stage->Rect.X, stage->Rect.Y, 80, 80); //change bounding box by display size
									if (isResize)
										stage->Rect = System::Drawing::Rectangle(stage->Rect.X * resizeWidthRatio, stage->Rect.Y * resizeHeightRatio, stage->Rect.Width* resizeWidthRatio, stage->Rect.Height * resizeHeightRatio); //change bounding box by display size

									int newX = 0, newY = 0;
									if (pRect->w > 0 && stage->Rect.X > pRect->w)
									{
										newX = pRect->w - 100;
										newY = stage->Rect.Y;
									}
									if (pRect->h > 0 && stage->Rect.Y > pRect->h)
									{
										newY = pRect->h - 100;
										newX = stage->Rect.X;
									}
									if (newX || newY)
									{
										stage->Rect = System::Drawing::Rectangle(newX, newY, stage->Rect.Width, stage->Rect.Height);
									}
									SDL_Rect r;
									r.x = stage->Rect.X;
									r.y = stage->Rect.Y;
									r.w = stage->Rect.Width;
									r.h = stage->Rect.Height;
									char name[STR_LENGTH_256];
									sprintf(name, "%s", stage->Name);
									name[stage->Name->Length] = STR_NULL;
									renderPar->RenderRects(&r, stage->Cl.R, stage->Cl.G, stage->Cl.B, name, stage->Index);
								}
							}
						}
						renderPar->RenderPresent(); //Present to screen
#pragma endregion Bounding Box									
					}
					else
						renderPar->RenderAVFrameInfo(_avFrame);
				}
			}
			else
			{
				//not supported
			}
		}
	}
	renderParam.crsRamProtection.Leave();
}

UINT32 RenderDecCore::RenderProcess(Camera^ _camera)
{
	char strError[STR_LENGTH_256];
	AVFrameInfo* avFrameInfo = NULL;
	RenderInfParams renderInfParams;
	String^ log = nullptr;
	Stopwatch^ swFrame = nullptr;
	while (renThreadCtr.GetIsThreadRunning())
	{
		renThreadCtr.crsBuffer.Enter();
#pragma region init params
		if (!renThreadCtr.blIsDataInit)
		{
			renderInfParams.nFrames = 0;
			renderInfParams.nFramesDisplay = 0;
			renderInfParams.fpsDisplay = 0;
			renderInfParams.fps = 0;
			renderInfParams.timeStart.QuadPart = 0;
			renderInfParams.tDisplay.QuadPart = 0;
			renderInfParams.tDisplayTick = 0;
			renderInfParams.timeStartTick = 0;
			renderInfParams.timeStartForFrameTick = 0;
			renThreadCtr.blIsDataInit = true;
			if (renderInfParams.isEnableWriteLog)
			{
				String^ log = "Camera = " + renderInfParams.cameraInd + " int param";
				LogWriter::Instance->WriteLog(1, log, __LINE__, __FUNCDNAME__);
			}
		}
#pragma endregion init params
#pragma region get avframe info
		int size = renThreadCtr.queueBuffer.size();
		if (0 == size)
		{
			renThreadCtr.crsBuffer.Leave();
			DWORD timeStart = GetTickCount();
			DWORD res = WaitForSingleObject(renThreadCtr.hWakeUpThread, 2000);
			DWORD timeEnd = GetTickCount();
			if (res == WAIT_TIMEOUT || (timeEnd - timeStart >= 1500))
			{
				if (res == WAIT_TIMEOUT)
				{
					renderParam.crsRamProtection.Enter();
					for (auto var : renderParam.paramList) // access by reference to avoid copying
					{
						if (!blHWAccel)
						{
							FramePresenterSDL* renderPar = (FramePresenterSDL*)var;
							renderPar->RenderDefaultFrame();
						}
					}
					renderParam.crsRamProtection.Leave();
				}
				renThreadCtr.blIsDataInit = false;
			}
			continue;
		}
		_camera->CameraStatus = STATUS_ENUM::CONNECTED;
		UINT64 address = renThreadCtr.queueBuffer.front();
		renThreadCtr.queueBuffer.pop_front();
		avFrameInfo = (AVFrameInfo*)address;
		renThreadCtr.crsBuffer.Leave();
#pragma endregion get avframe info			
		try
		{
#pragma region render
			if ((avFrameInfo->avFrame != NULL)
#ifdef  HW_ACC_ENABLED
				|| (avFrameInfo->cuFrame.pFrame != NULL)
#endif
				)
			{
				renderInfParams.isRender = true;
				renderInfParams.currentBufferSize = size;
				//DetectFPSTickCount(&renderInfParams, log, swFrame);//Render buffer
				if (renderInfParams.isRender)
				{
					RenderParamProcess(_camera, avFrameInfo->avFrame);
				}
			}
			if (avFrameInfo != NULL)
			{
				delete avFrameInfo;
				avFrameInfo = NULL;
			}
#pragma endregion render
		}
		catch (Exception^ e)
		{
			String^ log = e->Message + "\r\n" + e->StackTrace;
			LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
			return E_NOT_OK;
		}
		finally
		{
			if (avFrameInfo != NULL)
			{
				delete avFrameInfo;
				avFrameInfo = NULL;
			}
		}
	}
}

UINT32 RenderDecCore::DecodeProcess(Camera^ _camera)
{
	char cameraName[STR_LENGTH_256];
	String^ lastCameraNameStr = nullptr;
	while (decThreadCtr.GetIsThreadRunning())
	{
		decThreadCtr.crsBuffer.Enter();
		if (decThreadCtr.queueBuffer.size() > 0)
		{
			AVPacketInfo* packetInfo = (AVPacketInfo*)decThreadCtr.queueBuffer.front();
			decThreadCtr.queueBuffer.pop_front();
			decThreadCtr.crsBuffer.Leave();
			if (packetInfo->packetMode == PACKET_MODE_ENUM::INIT
				|| !decThreadCtr.blIsDataInit) {
				cpuDecoder.FFmpegCpuDecoderInit(packetInfo->codecPar);
				decThreadCtr.blIsDataInit = true;
				blFirstIFrameForDecode = false;
				renThreadCtr.crsBuffer.Enter();
				renThreadCtr.ClearQueue<AVFrameInfo>(); //Reset queue;
				renThreadCtr.crsBuffer.Leave();
			}
			else {
				if (!blFirstIFrameForDecode && packetInfo->avPacket.flags == 1)
					blFirstIFrameForDecode = true;
				if (blFirstIFrameForDecode)
				{
					if (blHWAccel)
					{
						//not supported
					}
					else
					{
						if (cpuDecoder.FFmpegRunAsynDecoder(&packetInfo->avPacket, packetInfo->isPacketRef) == E_OK)
						{
							stream == CAMERA_STREAM_ENUM::MAIN ? _camera->MainStreamStatus = STATUS_ENUM::CONNECTED : _camera->SubStreamStatus = STATUS_ENUM::CONNECTED;
							renThreadCtr.crsBuffer.Enter();
							for (int i = 0; i < cpuDecoder.avFrameList.size(); i++)
							{
								AVFrameInfo* avFrameInfo = new AVFrameInfo();
								avFrameInfo->avFrame = (AVFrame*)cpuDecoder.avFrameList[i];
								//NDI stream - begin
								if (_camera->IsNDIOutput && stream == CAMERA_STREAM_ENUM::MAIN) {
									if (_camera->Name != lastCameraNameStr)
									{
										IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(_camera->Name);
										char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
										strncpy(cameraName, nativeString, _camera->Name->Length);
										cameraName[_camera->Name->Length] = STR_NULL;
										lastCameraNameStr = _camera->Name;
										ndiStream.StopSend();
									}
									if (ConvertToBRG(avFrameInfo->avFrame, &ndiStream)) {
										ndiStream.SendFrame(ndiStream.avFrameBGR, cameraName);
									}
								}
								else
									ndiStream.StopSend();
								//NDI stream - end
								//Snapshot - begin
								if (_camera->IsSnapshot && stream == CAMERA_STREAM_ENUM::SUB)
								{
									if (!cpuEncoder.isInit)
									{
										cpuEncoder.FFmpegCpuEncoderInit(cpuDecoder.GetAVCodecPar(), AV_CODEC_ID_MJPEG);
									}
									if (cpuEncoder.isInit)
									{
										int res = cpuEncoder.FFmpegRunAsynEncoder((AVFrame*)cpuDecoder.avFrameList[i]);
										if (res == E_OK)
										{
											for (int j = 0; j < cpuEncoder.avPacketList.size(); j++)
											{
												if (SaveJPEGPacket((AVPacket*)cpuEncoder.avPacketList[j], _camera->ImagePath))
												{
													cpuEncoder.ReleasePacket();
													_camera->IsSnapshot = false; //snapshot successfully
													break;
												}
											}
										}
									}
								}
								//Snapshot - end
								renThreadCtr.queueBuffer.push_back((UINT64)avFrameInfo);
								int bufferSize = renThreadCtr.queueBuffer.size();
								if (bufferSize > MAX_BUFFER_SIZE_APPEND)
								{
									avFrameInfo = (AVFrameInfo*)renThreadCtr.queueBuffer.front();
									renThreadCtr.queueBuffer.pop_front();
									delete avFrameInfo;
									avFrameInfo = NULL;
								}
							}
							cpuDecoder.avFrameList.clear();
							renThreadCtr.crsBuffer.Leave();
							SetEvent(renThreadCtr.hWakeUpThread);
						}
					}
				}
			}
			delete packetInfo;
		}
		else {
			decThreadCtr.crsBuffer.Leave();
			WaitForSingleObject(decThreadCtr.hWakeUpThread, INFINITE);
		}
	}
	return (int)RESULT_ENUM::RS_OK;
}

UINT32 RenderDecCore::ReadProcess(Camera^ _camera)
{
	WaitForSingleObject(readThreadCtr.hWakeUpThread, INFINITE);
	return (int)RESULT_ENUM::RS_OK;
}

#pragma region RenderHandleCore
void RenderHandlerCore::Destroy()
{

}

RenderHandlerCore::RenderHandlerCore(HWND _hwnd, DISPLAY_RATIO_ENUM _displayRatio, bool _bHWAccel)
{
	isInit = false;
	bHWAccel = _bHWAccel;
	if (_bHWAccel)
	{
#ifdef  HW_ACC_ENABLED
		d3dRender.handle = _hwnd;	
		d3dRender.displayRatio = _displayRatio;
#endif
	}
	else
	{
		sdlParam.handle = _hwnd;
		sdlParam.displayRatio = _displayRatio;
	}
	
}

RenderHandlerCore::~RenderHandlerCore()
{
	Destroy();
}

VOID RenderHandlerCore::SetSuppendRender(bool _isSuppendRender)
{
	sdlParam.isSuppendReder = _isSuppendRender;
	if (sdlParam.isSuppendReder)
		sdlParam.isInit = false;
}

VOID RenderHandlerCore::SetZoomFactor(double _zoomFactor)
{
	sdlParam.SetZoomFactor(_zoomFactor);
}

VOID RenderHandlerCore::SetZoomPosition(int _zoomPosX, int _zoomPosY)
{
	sdlParam.SetZoomPosition(_zoomPosX, _zoomPosY);
}

#pragma endregion RenderHandleCore
