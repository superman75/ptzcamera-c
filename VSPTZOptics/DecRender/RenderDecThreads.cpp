#include "RenderDecCore.h"
#include <corecrt_io.h>

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
UINT32 __stdcall RenderProcessThread(void* pArguments)
{
	UINT64* params = (UINT64*)pArguments;
	RenderDecCore* pRenderDecCore = (RenderDecCore*)params[0];
	void* cameraPtr = (void*)params[1];
	IntPtr pointer(cameraPtr);
	GCHandle cameraHandle = GCHandle::FromIntPtr(pointer);
	Camera^ camera = (Camera^)cameraHandle.Target;

	try
	{

		if (pRenderDecCore == NULL)
			return E_NOT_OK;
		pRenderDecCore->RenderProcess(camera);
		//LogWriter::Instance->WriteLog(2, "Start RenderProcessThread camera " + cameraObj->Index + " stream " + (int)pRedDecCore->stream, __LINE__, __FUNCDNAME__);
		
	}
	catch (Exception^ e)
	{
		LogWriter::Instance->WriteLog(2, e->Message + "\r\n" + e->StackTrace, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally
	{
		pRenderDecCore->stream == CAMERA_STREAM_ENUM::MAIN ? camera->MainStreamStatus = STATUS_ENUM::DISCONNECTED : camera->SubStreamStatus = STATUS_ENUM::DISCONNECTED;
		pRenderDecCore->renThreadCtr.SetIsThreadRunning(false);
		//LogWriter::Instance->WriteLog(2, "Exit RenderProcessThread camera " + cameraObj->Index + " stream " + (int)pRedDecCore->stream, __LINE__, __FUNCDNAME__);
	}
}

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
UINT32 __stdcall DecodeProcessThread(void* pArguments)
{
	UINT64* params = (UINT64*)pArguments;
	RenderDecCore* pRenderDecCore = (RenderDecCore*)params[0];
	void* cameraPtr = (void*)params[1];
	IntPtr pointer(cameraPtr);
	GCHandle cameraHandle = GCHandle::FromIntPtr(pointer);
	Camera^ camera = (Camera^)cameraHandle.Target;
	try
	{
		if (pRenderDecCore == NULL)
			return E_NOT_OK;
		pRenderDecCore->DecodeProcess(camera);
		//LogWriter::Instance->WriteLog(2, "Start DecodeProcessThread camera " + camera->Index + " stream " + (int)pRenderDecCore->stream, __LINE__, __FUNCDNAME__);
		
	}
	catch (Exception^ e) {
		String^ log = "";
		if (e != nullptr)
			log += "Camera " + camera->Index + " " + pRenderDecCore->stream.ToString() + " " + e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally{
		pRenderDecCore->decThreadCtr.blIsDataInit = false;
		pRenderDecCore->blFirstIFrameForDecode = false;
		pRenderDecCore->stream == CAMERA_STREAM_ENUM::MAIN ? camera->MainStreamStatus = STATUS_ENUM::DISCONNECTED : camera->SubStreamStatus = STATUS_ENUM::DISCONNECTED;
		pRenderDecCore->decThreadCtr.SetIsThreadRunning(false);
		//LogWriter::Instance->WriteLog(2, "Exit DecodeProcessThread camera " + camera->Index + " stream " + (int)pRenderDecCore->stream, __LINE__, __FUNCDNAME__);
	}
}

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
UINT32 __stdcall ReadProcessThread(void* pArguments)
{
	UINT64* params = (UINT64*)pArguments;
	RenderDecCore* pRenderDecCore = (RenderDecCore*)params[0];
	void* cameraPtr = (void*)params[1];
	IntPtr pointer(cameraPtr);
	GCHandle cameraHandle = GCHandle::FromIntPtr(pointer);
	Camera^ camera = (Camera^)cameraHandle.Target;
	try
	{
		if (pRenderDecCore == NULL)
			return E_NOT_OK;
		pRenderDecCore->ReadProcess(camera);
		//LogWriter::Instance->WriteLog(2, "Start ReadProcessThread camera " + camera->Index + " stream " + (int)pRenderDecCore->stream, __LINE__, __FUNCDNAME__);
		
	}
	catch (Exception^ e) {
		String^ log = "";
		if (e != nullptr)
			log += " " + e->Message + "\r\n" + e->StackTrace;
		LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
		return E_NOT_OK;
	}
	finally{
		pRenderDecCore->stream == CAMERA_STREAM_ENUM::MAIN ? camera->MainStreamStatus = STATUS_ENUM::DISCONNECTED : camera->SubStreamStatus = STATUS_ENUM::DISCONNECTED;
		//LogWriter::Instance->WriteLog(2, "Exit ReadProcessThread camera " + camera->Index + " stream " + (int)pRenderDecCore->stream, __LINE__, __FUNCDNAME__);
		pRenderDecCore->readThreadCtr.SetIsThreadRunning(false);
	}
}
