#include "stdafx.h"
#include "PTZOpticsRenderDec.h"

PTZOpticsRenderDec::PTZOpticsRenderDec()
	: RenderDecCore()
{

}

PTZOpticsRenderDec::PTZOpticsRenderDec(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type)
	: RenderDecCore(_pCamera, _stream, _type)
{

}


UINT32 PTZOpticsRenderDec::ReadProcess(Camera^ _camera)
{
	char url[STR_LENGTH_256];
	String^ lastUrlStr = nullptr;
	while (readThreadCtr.GetIsThreadRunning()) {
		try {
			String^ urlStr = nullptr;
			urlStr = stream == CAMERA_STREAM_ENUM::MAIN ? _camera->MainUrl : _camera->SubUrl;
			if (urlStr != lastUrlStr)
			{
				IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(urlStr);
				char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
				strncpy(url, nativeString, urlStr->Length);
				url[urlStr->Length] = STR_NULL;
				lastUrlStr = urlStr;
			}
			if (streamCommon.OpenVideoRTPStream(url) == (int)RESULT_ENUM::RS_OK) {
				//LogWriter::Instance->WriteLog(2, "OpenVideoRTPStream successful camera " + camera->Index, __LINE__, __FUNCDNAME__);
				bool isReadFirstFrame = false;
				while (readThreadCtr.GetIsThreadRunning())
				{
					if (_camera->IsReconnect)
						break;
					int res = streamCommon.AVPacketReadFrame();
					if (res == (int)RESULT_ENUM::RS_CONTINUE)
						continue;
					else if (res == (int)RESULT_ENUM::RS_NOT_OK)
						break;
					else //Read frame ok here
					{
						if (!isReadFirstFrame) {
							//LogWriter::Instance->WriteLog(2, "AVPacketReadFrame successful camera " + camera->Index, __LINE__, __FUNCDNAME__);
							isReadFirstFrame = true;
							AVPacketInfo* avPacketInfo = new AVPacketInfo();
							avPacketInfo->packetMode = PACKET_MODE_ENUM::INIT;
							avPacketInfo->codecPar = new AVCodecParameters();
							memcpy(avPacketInfo->codecPar, &streamCommon.avCodecParameters, sizeof(AVCodecParameters));
							if (streamCommon.avCodecParameters.extradata_size > 0)
							{
								int length = (streamCommon.avCodecParameters.extradata_size + AV_INPUT_BUFFER_PADDING_SIZE) / sizeof(uint64_t) + 1;
								unsigned char* extra = new unsigned char[length * 8];
								memcpy(extra, streamCommon.avCodecParameters.extradata, streamCommon.avCodecParameters.extradata_size);
								avPacketInfo->codecPar->extradata = extra;
							}
							decThreadCtr.crsBuffer.Enter();
							decThreadCtr.ClearQueue<AVPacketInfo>(); //Reset queue;
							decThreadCtr.queueBuffer.push_back((UINT64)avPacketInfo);
							decThreadCtr.crsBuffer.Leave();
							SetEvent(decThreadCtr.hWakeUpThread);
						}
						if (streamCommon.packet.data != NULL && streamCommon.packet.size > 0)
						{
							AVPacketInfo* avPacketInfo = new AVPacketInfo();
							av_packet_ref(&avPacketInfo->avPacket, &streamCommon.packet);
							avPacketInfo->isPacketRef = true;
							decThreadCtr.crsBuffer.Enter();
							decThreadCtr.queueBuffer.push_back((UINT64)avPacketInfo);
							decThreadCtr.crsBuffer.Leave();
							SetEvent(decThreadCtr.hWakeUpThread);
						}
					}
				}
			}
			else {
				stream == CAMERA_STREAM_ENUM::MAIN ? _camera->MainStreamStatus = STATUS_ENUM::DISCONNECTED : _camera->SubStreamStatus = STATUS_ENUM::DISCONNECTED;
				LogWriter::Instance->WriteLog(2, "Connect stream failure camera " + _camera->Index + " url: " + urlStr, __LINE__, __FUNCDNAME__);
				int sleepCount = 0;
				while (readThreadCtr.GetIsThreadRunning() && sleepCount < 4) //Sleep 2s
				{
					Sleep(500);
					sleepCount++;
				}
			}
		}
		catch (Exception^ e) {
			String^ url = gcnew String(url);
			String^ log = "ReadProcessThread crash " + url + " " + e->Message + "\r\n" + e->StackTrace;
			LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
		}
		int sleepCount = 0;
		while (readThreadCtr.GetIsThreadRunning() && sleepCount < 10 && !_camera->IsReconnect) //Sleep 5s
		{
			Sleep(500);
			sleepCount++;
		}
		if (_camera->IsReconnect) {
			_camera->IsReconnect = false;
		}
	}
	return (int)RESULT_ENUM::RS_OK;
}

UINT32 PTZOpticsRenderDec::DecodeProcess(Camera^ _camera)
{
	char cameraName[STR_LENGTH_256];
	String^ lastCameraNameStr = nullptr;
	while (decThreadCtr.GetIsThreadRunning())
	{
		decThreadCtr.crsBuffer.Enter();
		if (decThreadCtr.queueBuffer.size() > 0)
		{
			AVPacketInfo* packetInfo = (AVPacketInfo*)decThreadCtr.queueBuffer.front();
			decThreadCtr.queueBuffer.pop_front();
			decThreadCtr.crsBuffer.Leave();
			if (packetInfo->packetMode == PACKET_MODE_ENUM::INIT
				|| !decThreadCtr.blIsDataInit) {
				cpuDecoder.FFmpegCpuDecoderInit(packetInfo->codecPar);
				decThreadCtr.blIsDataInit = true;
				blFirstIFrameForDecode = false;
				renThreadCtr.crsBuffer.Enter();
				renThreadCtr.ClearQueue<AVFrameInfo>(); //Reset queue;
				renThreadCtr.crsBuffer.Leave();
			}
			else {
				if (!blFirstIFrameForDecode && packetInfo->avPacket.flags == 1)
					blFirstIFrameForDecode = true;
				if (blFirstIFrameForDecode)
				{
					if (blHWAccel)
					{
						//not supported
					}
					else
					{
						if (cpuDecoder.FFmpegRunAsynDecoder(&packetInfo->avPacket, packetInfo->isPacketRef) == E_OK)
						{
							stream == CAMERA_STREAM_ENUM::MAIN ? _camera->MainStreamStatus = STATUS_ENUM::CONNECTED : _camera->SubStreamStatus = STATUS_ENUM::CONNECTED;
							renThreadCtr.crsBuffer.Enter();
							for (int i = 0; i < cpuDecoder.avFrameList.size(); i++)
							{
								AVFrameInfo* avFrameInfo = new AVFrameInfo();
								avFrameInfo->avFrame = (AVFrame*)cpuDecoder.avFrameList[i];
								//NDI stream - begin
								if (_camera->IsNDIOutput && stream == CAMERA_STREAM_ENUM::MAIN) {
									if (_camera->Name != lastCameraNameStr)
									{
										IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(_camera->Name);
										char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
										strncpy(cameraName, nativeString, _camera->Name->Length);
										cameraName[_camera->Name->Length] = STR_NULL;
										lastCameraNameStr = _camera->Name;
										ndiStream.StopSend();
									}
									if (ConvertToBRG(avFrameInfo->avFrame, &ndiStream)) {
										ndiStream.SendFrame(ndiStream.avFrameBGR, cameraName);
									}
								}
								else
									ndiStream.StopSend();
								//NDI stream - end
								//Snapshot - begin
								if (_camera->IsSnapshot && stream == CAMERA_STREAM_ENUM::SUB)
								{
									if (!cpuEncoder.isInit)
									{
										cpuEncoder.FFmpegCpuEncoderInit(cpuDecoder.GetAVCodecPar(), AV_CODEC_ID_MJPEG);
									}
									if (cpuEncoder.isInit)
									{
										int res = cpuEncoder.FFmpegRunAsynEncoder((AVFrame*)cpuDecoder.avFrameList[i]);
										if (res == E_OK)
										{
											for (int j = 0; j < cpuEncoder.avPacketList.size(); j++)
											{
												if (SaveJPEGPacket((AVPacket*)cpuEncoder.avPacketList[j], _camera->ImagePath))
												{
													cpuEncoder.ReleasePacket();
													_camera->IsSnapshot = false; //snapshot successfully
													break;
												}
											}
										}
									}
								}
								//Snapshot - end
								renThreadCtr.queueBuffer.push_back((UINT64)avFrameInfo);
								int bufferSize = renThreadCtr.queueBuffer.size();
								if (bufferSize > MAX_BUFFER_SIZE_APPEND)
								{
									avFrameInfo = (AVFrameInfo*)renThreadCtr.queueBuffer.front();
									renThreadCtr.queueBuffer.pop_front();
									delete avFrameInfo;
									avFrameInfo = NULL;
								}
							}
							cpuDecoder.avFrameList.clear();
							renThreadCtr.crsBuffer.Leave();
							SetEvent(renThreadCtr.hWakeUpThread);
						}
					}
				}
			}
			delete packetInfo;
		}
		else {
			decThreadCtr.crsBuffer.Leave();
			WaitForSingleObject(decThreadCtr.hWakeUpThread, INFINITE);
		}
	}
	return (int)RESULT_ENUM::RS_OK;
}

