#include "stdafx.h"
#include "NDIRenderDec.h"

NDIRenderDec::NDIRenderDec()
	: RenderDecCore()
{

}

NDIRenderDec::NDIRenderDec(VOID* _pCamera, CAMERA_STREAM_ENUM _stream, CAMERA_TYPE_ENUM _type)
	: RenderDecCore(_pCamera, _stream, _type)
{

}

bool NDIRenderDec::CopyNDIData(NDIlib_video_frame_v2_t* _ndiFrame, NDIStreamCore* _pNDI)
{
	try
	{
		_pNDI->frameInd++;
		_pNDI->avFrameBGR = _pNDI->frameInd % 2 == 0 ? _pNDI->ppAvFrameBGR[0] : _pNDI->ppAvFrameBGR[1];

		if (_pNDI->avFrameBGR == NULL)
		{
			_pNDI->avFrameBGR = av_frame_alloc();
			_pNDI->frameInd % 2 == 0 ? _pNDI->ppAvFrameBGR[0] = _pNDI->avFrameBGR : _pNDI->ppAvFrameBGR[1] = _pNDI->avFrameBGR;
			av_image_alloc(_pNDI->avFrameBGR->data, _pNDI->avFrameBGR->linesize, _ndiFrame->xres, _ndiFrame->yres, AV_PIX_FMT_BGRA, 1);
			_pNDI->avFrameBGR->width = _ndiFrame->xres;
			_pNDI->avFrameBGR->height = _ndiFrame->yres;
		}
		memcpy(_pNDI->avFrameBGR->data[0], _ndiFrame->p_data, 4 * _ndiFrame->xres * _ndiFrame->yres);
		return true;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return false;
	}
}

UINT32 NDIRenderDec::ReadProcess(Camera^ _camera)
{
	char url[STR_LENGTH_256];
	char cameraName[STR_LENGTH_256];
	String^ lastUrlStr = nullptr;
	String^ lastCameraNameStr = nullptr;
	while (readThreadCtr.GetIsThreadRunning()) {
		try {
			String^ urlStr = _camera->NDICameraName;
			if (urlStr != lastUrlStr)
			{
				IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(urlStr);
				char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
				strncpy(url, nativeString, urlStr->Length);
				url[urlStr->Length] = STR_NULL;
				lastUrlStr = urlStr;
			}			
			if (ndiStream.NDIConnect(url)) {
				//LogWriter::Instance->WriteLog(2, "OpenVideoRTPStream successful camera " + camera->Index, __LINE__, __FUNCDNAME__);
				bool isReadFirstFrame = false;
				while (readThreadCtr.GetIsThreadRunning())
				{
					if (_camera->IsReconnect)
						break;
					NDIlib_video_frame_v2_t video_frame;
					AVFrame* avFrame = av_frame_alloc();

					if (ndiStream.NDIRecvFrame(&video_frame))
					{
						stream == CAMERA_STREAM_ENUM::MAIN ? _camera->MainStreamStatus = STATUS_ENUM::CONNECTED : _camera->SubStreamStatus = STATUS_ENUM::CONNECTED;
						if (_camera->IsNDIOutput)
						{
							if (_camera->Name != lastCameraNameStr)
							{
								IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(_camera->Name);
								char* nativeString = static_cast<char*>(ptrToNativeString.ToPointer());
								strncpy(cameraName, nativeString, _camera->Name->Length);
								cameraName[_camera->Name->Length] = STR_NULL;
								lastCameraNameStr = _camera->Name;
								ndiStream.StopSend();
							}
							ndiStream.ReSendFrame(&video_frame, cameraName);
						}
						else
							ndiStream.StopSend();
						bool isRender = false;
						if (video_frame.FourCC == NDIlib_FourCC_type_BGRA)
						{
							avFrame->width = video_frame.xres;
							avFrame->height = video_frame.yres;
							avFrame->linesize[0] = 4 * video_frame.xres;
							avFrame->data[0] = video_frame.p_data;
							avFrame->format = AV_PIX_FMT_BGRA;
							isRender = true;							
						}
						else if (video_frame.FourCC == NDIlib_FourCC_type_UYVY)
						{
							avFrame->width = video_frame.xres;
							avFrame->height = video_frame.yres;
							avFrame->linesize[0] = 2 * video_frame.xres;
							avFrame->data[0] = video_frame.p_data;
							avFrame->format = AV_PIX_FMT_UYVY422;
							isRender = true;
						}
						else
						{
							LogWriter::Instance->WriteLog(2, "Unexpected fourcc " + (int)video_frame.FourCC + " camera " + _camera->Index, __LINE__, __FUNCDNAME__);
						}
						if (isRender)
						{
							RenderParamProcess(_camera, avFrame);
							//Snapshot - begin
							if (_camera->IsSnapshot)
							{
								if (!cpuEncoder.isInit)
								{
									AVCodecParameters* codecPar = cpuDecoder.GetAVCodecPar();
									codecPar->width = avFrame->width;
									codecPar->height = avFrame->height;
									cpuEncoder.FFmpegCpuEncoderInit(cpuDecoder.GetAVCodecPar(), AV_CODEC_ID_MJPEG);
								}
								if (cpuEncoder.isInit)
								{
									int res = cpuEncoder.FFmpegRunAsynEncoder(avFrame);
									if (res == E_OK)
									{
										for (int j = 0; j < cpuEncoder.avPacketList.size(); j++)
										{
											if (SaveJPEGPacket((AVPacket*)cpuEncoder.avPacketList[j], _camera->ImagePath))
											{
												cpuEncoder.ReleasePacket();
												_camera->IsSnapshot = false; //snapshot successfully
												break;
											}
										}
									}
								}
							}
							//Snapshot - end
						}
						ndiStream.NDIFreeFrame(&video_frame);
						av_frame_free(&avFrame);
					}
					else
					{
						LogWriter::Instance->WriteLog(2, "receive frame failure camera " + _camera->Index + " url: " + urlStr, __LINE__, __FUNCDNAME__);
						av_frame_free(&avFrame);
						break;
					}
				}
			}
			else
			{
				stream == CAMERA_STREAM_ENUM::MAIN ? _camera->MainStreamStatus = STATUS_ENUM::DISCONNECTED : _camera->SubStreamStatus = STATUS_ENUM::DISCONNECTED;
				LogWriter::Instance->WriteLog(2, "Connect stream failure camera " + _camera->Index + " url: " + urlStr, __LINE__, __FUNCDNAME__);
				int sleepCount = 0;
				while (readThreadCtr.GetIsThreadRunning() && sleepCount < 4) //Sleep 2s
				{
					Sleep(500);
					sleepCount++;
				}
			}
		}
		catch (Exception^ e) {
			String^ url = gcnew String(url);
			String^ log = "ReadProcessThread crash " + url + " " + e->Message + "\r\n" + e->StackTrace;
			LogWriter::Instance->WriteLog(2, log, __LINE__, __FUNCDNAME__);
		}
		int sleepCount = 0;
		while (readThreadCtr.GetIsThreadRunning() && sleepCount < 10 && !_camera->IsReconnect) //Sleep 5s
		{
			Sleep(500);
			sleepCount++;
		}
		if (_camera->IsReconnect) {
			_camera->IsReconnect = false;
		}
	}
	return (int)RESULT_ENUM::RS_OK;
}

UINT32 NDIRenderDec::DecodeProcess(Camera^ _camera)
{
	WaitForSingleObject(decThreadCtr.hWakeUpThread, INFINITE);
	return (int)RESULT_ENUM::RS_OK;
}

UINT32 NDIRenderDec::RenderProcess(Camera^ _camera)
{
	WaitForSingleObject(renThreadCtr.hWakeUpThread, INFINITE);
	return (int)RESULT_ENUM::RS_OK;
}
