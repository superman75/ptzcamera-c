#include "RenderDecRef.h"

using namespace NsRenderHandlerRef;

#pragma region NsRenderDecRef
NsRenderDecRef::RenderDecRef::RenderDecRef(String^ _cameraID)
{
	RenderDecCore* core = new RenderDecCore();
	renderDecCoreObj = (IntPtr)core;

}

NsRenderDecRef::RenderDecRef::RenderDecRef(Camera^ _camera, CAMERA_TYPE_ENUM _type, CAMERA_STREAM_ENUM _stream, bool _bHWAccel)
{
	//TienVo added to handle camera object
	cameraHandle = GCHandle::Alloc(_camera);
	IntPtr intPtrCamera = GCHandle::ToIntPtr(cameraHandle);
	RenderDecCore* core = NULL;
	switch (_type)
	{
	case CAMERA_TYPE_ENUM::PTZ_OPTICS:
		core = new PTZOpticsRenderDec(intPtrCamera.ToPointer(), _stream, _type);
		break;
	case CAMERA_TYPE_ENUM::ZCAM:
		core = new PTZOpticsRenderDec(intPtrCamera.ToPointer(), _stream, _type);
		break;
	case CAMERA_TYPE_ENUM::STATIC_IMAGE:
		core = new ImageRenderDec(intPtrCamera.ToPointer(), _stream, _type);
		break;
	case CAMERA_TYPE_ENUM::NDI:
		core = new NDIRenderDec(intPtrCamera.ToPointer(), _stream, _type);
		break;
	default:
		core = new RenderDecCore(intPtrCamera.ToPointer(), _stream, _type);
		break;
	}
	
	renderDecCoreObj = (IntPtr)core;
	core->blHWAccel = _bHWAccel;
}

NsRenderDecRef::RenderDecRef::~RenderDecRef() //Release managed Resources
{
	//Should Remove managed Resource here
	//....
	this->!RenderDecRef();
}

NsRenderDecRef::RenderDecRef::!RenderDecRef() //Release Unmanaged Resources
{
	Destroy();
}

void NsRenderDecRef::RenderDecRef::Destroy()
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
	{
		delete core;
		renderDecCoreObj = (IntPtr)0;
	}
	cameraHandle.Free(); //TienVo added to handle camera object
}

void NsRenderDecRef::RenderDecRef::ClearAllQueue()
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
	{
		core->ClearAllQueue();
	}
}

void NsRenderDecRef::RenderDecRef::EnqueueDecode(cli::array<System::Byte>^ _buffer, int _startInd, int _dataLength, PACKET_MODE_ENUM _decodeMode)
{
	pin_ptr<System::Byte> p = &_buffer[0];
	unsigned char* bufferC = p + _startInd;
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
		core->EnqueueDecode(bufferC, _dataLength, _decodeMode);
}

void NsRenderDecRef::RenderDecRef::AddRender(RenderHandlerRef^ _render)
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	RenderHandlerCore* renderHandleCore = (RenderHandlerCore*)_render->GetRenderCoreObject().ToPointer();
	if (core != NULL && renderHandleCore != NULL)
	{
		if(!core->blHWAccel)
			core->AddRender<FramePresenterSDL>(&renderHandleCore->sdlParam);
#ifdef  HW_ACC_ENABLED
		else
			core->AddRender<FramePresenterD3D11>(&renderHandleCore->d3dRender);
#endif
	}
}

void NsRenderDecRef::RenderDecRef::RemoveRender(RenderHandlerRef^ _render)
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	RenderHandlerCore* renderHandleCore = (RenderHandlerCore*)_render->GetRenderCoreObject().ToPointer();
	if (core != NULL && renderHandleCore != NULL)
	{
		if (!core->blHWAccel)
			core->RemoveRender<FramePresenterSDL>(&renderHandleCore->sdlParam);
#ifdef  HW_ACC_ENABLED
		else
			core->RemoveRender<FramePresenterD3D11>(&renderHandleCore->d3dRender);
#endif
	}
}

void NsRenderDecRef::RenderDecRef::ClearAllRender()
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
	{
		core->ClearAllRender();
	}
}

void NsRenderDecRef::RenderDecRef::SetHWAccel(bool _bHWAccel)
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
	{
		core->blHWAccel = _bHWAccel;
		core->blFirstIFrameForDecode = false;
	}
}

void NsRenderDecRef::RenderDecRef::Start(bool _isStartReadThread, bool _isStartDecodeThread, bool _isStartRenderThread)
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
	{
		core->Start(_isStartReadThread, _isStartDecodeThread, _isStartRenderThread);
	}
}

void NsRenderDecRef::RenderDecRef::Stop()
{
	RenderDecCore* core = (RenderDecCore*)renderDecCoreObj.ToPointer();
	if (core != NULL)
	{
		core->Stop();
	}
}

System::IntPtr NsRenderDecRef::RenderDecRef::GetRenderDecCoreObj()
{
	return renderDecCoreObj;
}
#pragma endregion RenderDecRef


NsRenderHandlerRef::RenderHandlerRef::RenderHandlerRef(IntPtr _handle, DISPLAY_RATIO_ENUM _displayRatio, bool _bHWAccel)
{
	handle = _handle;	
	RenderHandlerCore* core = new RenderHandlerCore((HWND)handle.ToPointer(), _displayRatio, _bHWAccel);
	renderCoreObj = (IntPtr)core;
}

#pragma region NsRenderHandlerRef
NsRenderHandlerRef::RenderHandlerRef::~RenderHandlerRef()
{
	this->!RenderHandlerRef();
}


NsRenderHandlerRef::RenderHandlerRef::!RenderHandlerRef()
{
	Destroy();
}


void NsRenderHandlerRef::RenderHandlerRef::Destroy()
{
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		delete core;
		renderCoreObj = (IntPtr)0;
	}
}

System::IntPtr NsRenderHandlerRef::RenderHandlerRef::GetRenderCoreObject()
{
	return renderCoreObj;
}

void NsRenderHandlerRef::RenderHandlerRef::SetSuppendRender(bool _isSuppendRender)
{
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		core->SetSuppendRender(_isSuppendRender);
	}
}

void NsRenderHandlerRef::RenderHandlerRef::SetHWAccel(bool _bHWAccel)
{
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		core->bHWAccel = _bHWAccel;
	}
}

void NsRenderHandlerRef::RenderHandlerRef::SetDisplayRatio(DISPLAY_RATIO_ENUM _displayRatio)
{
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		core->sdlParam.displayRatio = _displayRatio;
		core->sdlParam.windowW = core->sdlParam.windowH = 0;
#ifdef HW_ACC_ENABLED
		core->d3dRender.displayRatio = _displayRatio;
#endif
	}
}

void NsRenderHandlerRef::RenderHandlerRef::SetZoomFactor(double _zoomFactor)
{
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		core->SetZoomFactor(_zoomFactor);
#ifdef HW_ACC_ENABLED
		//do stuff
#endif
	}
}

void NsRenderHandlerRef::RenderHandlerRef::SetZoomPosition(int _zoomPosX, int _zoomPosY)
{
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		core->SetZoomPosition(_zoomPosX, _zoomPosY);
#ifdef HW_ACC_ENABLED
		//do stuff
#endif
	}
}

System::Drawing::Rectangle^ NsRenderHandlerRef::RenderHandlerRef::GetDisplaySize()
{
	System::Drawing::Rectangle^ rect = gcnew System::Drawing::Rectangle();
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		SDL_Rect* pRect = core->sdlParam.GetDisplaySize();
		rect->X = pRect->x;
		rect->Y = pRect->y;
		rect->Width = pRect->w;
		rect->Height = pRect->h;
	}
	return rect;
}

void NsRenderHandlerRef::RenderHandlerRef::GetDisplayRatio(float %_wRatio, float %_hRatio)
{
	float wRatio, hRatio = 0;
	RenderHandlerCore* core = (RenderHandlerCore*)renderCoreObj.ToPointer();
	if (core != NULL)
	{
		core->sdlParam.GetDisplayRatio(&wRatio, &hRatio);
		_wRatio = wRatio;
		_hRatio = hRatio;
	}
}

#pragma endregion NsRenderHandlerRef
