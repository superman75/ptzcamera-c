#pragma once
#include <Processing.NDI.Lib.h>
#include "ThreadCtl/CriticalSection.h"
#include "AVInfo/AVInfo.h"
#include "NDIStreamExportMacro.h"
#include "vector"
using namespace System::Collections::Generic;
using namespace System;
using namespace std;
using namespace System::Diagnostics;
using namespace System::Security;
using namespace System::Runtime::ExceptionServices;
using namespace ShareLibrary::Base;
using namespace System::Runtime::InteropServices;
using namespace System::IO;
using namespace System::Security::AccessControl;

class NDI_API NDIStreamCore
{
private:
	NDIlib_video_frame_v2_t NDI_video_frame;
	NDIlib_send_instance_t pNDI_send;
	void Destroy();
public:
	NDIStreamCore();
	~NDIStreamCore();

	AVFrame* avFrameBGR;
	AVFrame* ppAvFrameBGR[2];
	uint8_t* ndiBuffer[2];

	const NDIlib_v3* p_NDILib;
	const NDIlib_source_t* pNDIsources;
	NDIlib_recv_instance_t pNDIRecv;
	bool isConnected;
	char ndiName[STR_LENGTH_256];

	long long frameInd;
	SwsContext* swsCtx;
	void SendFrame(AVFrame* _avFrame, char* _channelName);
	void ReSendFrame(NDIlib_video_frame_v2_t* _pNDIFrame, char* _channelName);
	void StopSend();
	void GetNDISources(std::vector<std::string> &_ndiSources);
	bool NDIConnect(char* _ndiName);
	bool NDIPTZConnect(char* _ndiName);
	void NDIDisConnect();
	bool NDIRecvFrame(NDIlib_video_frame_v2_t* _frame);
	void NDIFreeFrame(NDIlib_video_frame_v2_t* _frame);
	bool NDIControlReq(int _controlType, int _value1, int _value2);
};
