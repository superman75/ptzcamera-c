#include "stdafx.h"
#include "NDIStreamCore.h"
#include <xstring>


/*******************************************************************************
**                      Static Symbols                                        **
*******************************************************************************/

/******************************************************************************
**                   Class Methods Defination Section						 **
*******************************************************************************/

void NDIStreamCore::Destroy()
{
	if(pNDI_send && p_NDILib)
	{
		p_NDILib->NDIlib_send_send_video_async_v2(pNDI_send, NULL);
		// Destroy the NDI sender
		p_NDILib->NDIlib_send_destroy(pNDI_send);
		p_NDILib->NDIlib_destroy();
		pNDI_send = NULL;
	}
	for (int i = 0; i < 2; i++)
	{
		avFrameBGR = ppAvFrameBGR[i];
		if (avFrameBGR != NULL)
		{
			av_freep(&avFrameBGR->data[0]);
			av_frame_free(&avFrameBGR);
		}
		if (ndiBuffer[i] != NULL)
		{
			free(ndiBuffer[i]);
			ndiBuffer[i] = NULL;
		}
	}
	if (swsCtx != NULL)
	{
		sws_freeContext(swsCtx);
		swsCtx = NULL;
	}
	if (pNDIRecv)
	{
		// Destroy the receiver
		NDIlib_recv_destroy(pNDIRecv);
		pNDIRecv = NULL;
	}
	// Free the video frame
}

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
void NDIStreamCore::SendFrame(AVFrame* _avFrame, char* _channelName)
{
	try
	{
		if (!pNDI_send)
		{
			if (!p_NDILib)
				p_NDILib = NDIlib_v3_load();
			if (p_NDILib)
				p_NDILib->NDIlib_initialize();
			if (p_NDILib)
			{
				char ndiGroup[STR_LENGTH_16];
				sprintf(ndiGroup, "GROUP");
				NDIlib_send_create_t ndi((const char*)_channelName, NULL, false, true);
				pNDI_send = p_NDILib->NDIlib_send_create(&ndi);
				NDI_video_frame.xres = _avFrame->width;
				NDI_video_frame.yres = _avFrame->height;
				NDI_video_frame.FourCC = NDIlib_FourCC_type_BGRA;
				NDI_video_frame.line_stride_in_bytes = 4 * _avFrame->width;
			}
		}
		if (pNDI_send && p_NDILib)
		{
			NDI_video_frame.p_data = (uint8_t*)_avFrame->data[0];
			p_NDILib->NDIlib_send_send_video_async_v2(pNDI_send, &NDI_video_frame);
		}
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}

}

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
void NDIStreamCore::ReSendFrame(NDIlib_video_frame_v2_t* _pNDIFrame, char* _channelName)
{
	try
	{
		if (!pNDI_send)
		{
			if (!p_NDILib)
				p_NDILib = NDIlib_v3_load();
			if (p_NDILib)
				p_NDILib->NDIlib_initialize();
			if (p_NDILib)
			{
				char ndiGroup[STR_LENGTH_16];
				sprintf(ndiGroup, "GROUP");
				NDIlib_send_create_t ndi((const char*)_channelName, NULL, false, true);
				pNDI_send = p_NDILib->NDIlib_send_create(&ndi);
				NDI_video_frame.xres = _pNDIFrame->xres;
				NDI_video_frame.yres = _pNDIFrame->yres;
				NDI_video_frame.FourCC = _pNDIFrame->FourCC;
				if (_pNDIFrame->FourCC == NDIlib_FourCC_type_BGRA)
				{
					NDI_video_frame.line_stride_in_bytes = 4 * _pNDIFrame->xres;
					if (ndiBuffer[0] == NULL)
						ndiBuffer[0] = (uint8_t*)malloc(_pNDIFrame->xres * _pNDIFrame->yres * 4);
					if (ndiBuffer[1] == NULL)
						ndiBuffer[1] = (uint8_t*)malloc(_pNDIFrame->xres * _pNDIFrame->yres * 4);
				}
				else if (_pNDIFrame->FourCC == NDIlib_FourCC_type_UYVY)
				{
					NDI_video_frame.line_stride_in_bytes = 2 * _pNDIFrame->xres;
					if (ndiBuffer[0] == NULL)
						ndiBuffer[0] = (uint8_t*)malloc(_pNDIFrame->xres * _pNDIFrame->yres * 2);
					if (ndiBuffer[1] == NULL)
						ndiBuffer[1] = (uint8_t*)malloc(_pNDIFrame->xres * _pNDIFrame->yres * 2);
				}
			}
		}
		if (pNDI_send && p_NDILib)
		{
			if (_pNDIFrame->FourCC == NDI_video_frame.FourCC)
			{
				if (_pNDIFrame->FourCC == NDIlib_FourCC_type_BGRA)
				{
					uint8_t* buffer = ndiBuffer[frameInd & 1];
					memcpy(buffer, _pNDIFrame->p_data, _pNDIFrame->xres * _pNDIFrame->yres * 4);
					NDI_video_frame.p_data = (uint8_t*)buffer;
					p_NDILib->NDIlib_send_send_video_async_v2(pNDI_send, &NDI_video_frame);
				}
				else if (_pNDIFrame->FourCC == NDIlib_FourCC_type_UYVY)
				{
					uint8_t* buffer = ndiBuffer[frameInd & 1];
					memcpy(buffer, _pNDIFrame->p_data, _pNDIFrame->xres * _pNDIFrame->yres * 2);
					NDI_video_frame.p_data = (uint8_t*)buffer;
					p_NDILib->NDIlib_send_send_video_async_v2(pNDI_send, &NDI_video_frame);
				}
			}
			else
				StopSend();
		}
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}
}

void NDIStreamCore::StopSend()
{
	try
	{
		if (pNDI_send && p_NDILib)
		{
			p_NDILib->NDIlib_send_send_video_async_v2(pNDI_send, NULL);
			// Destroy the NDI sender
			p_NDILib->NDIlib_send_destroy(pNDI_send);
			pNDI_send = NULL;
		}
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}	
}

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
void NDIStreamCore::GetNDISources(vector<std::string> &_ndiNameSources)
{
	NDIlib_find_instance_t pNDI_find = NULL;
	try
	{
		// Create a finder
		pNDI_find = NDIlib_find_create_v2();
		if (!pNDI_find) return;

		// Wait until there is one source
		uint32_t no_sources = 0;
		const NDIlib_source_t* p_sources = NULL;
		int count = 0;
		bool loop = true;
		while (loop && count < 3)//!no_sources)
		{	// Wait until the sources on the nwtork have changed
			printf("Looking for sources ...\n");
			NDIlib_find_wait_for_sources(pNDI_find, 5000/* One second */);
			p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
			if (p_sources != NULL && no_sources)
			{
				for (int i = 0; i < no_sources; i++)
				{
					NDIlib_source_t source = p_sources[i];
					if (source.p_ndi_name != NULL)
					{
						_ndiNameSources.push_back(source.p_ndi_name);
					}
				}
				break;
			}
			Sleep(100);
			count++;
		}
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}
	finally
	{
		if(pNDI_find)
		{
			// Destroy the NDI finder. We needed to have access to the pointers to p_sources[0]
			NDIlib_find_destroy(pNDI_find);
		}
	}
}

[SecurityCritical]
[HandleProcessCorruptedStateExceptions]
bool NDIStreamCore::NDIConnect(char* _ndiName)
{
	// Create a finder
	NDIlib_find_instance_t pNDI_find = NULL;
	try
	{
		// Create a finder
		pNDI_find = NDIlib_find_create_v2();
		if (!pNDI_find) return false;
		// Wait until there is one source
		uint32_t no_sources = 0;
		const NDIlib_source_t* p_sources = NULL;
		int count = 0;
		pNDIsources = NULL;
		bool loop = true;
		strcpy(ndiName, _ndiName);
		while (loop && count < 3)//!no_sources)
		{	// Wait until the sources on the nwtork have changed
			NDIlib_find_wait_for_sources(pNDI_find, 5000/* One second */);
			p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
			if (p_sources != NULL && no_sources)
			{
				//LogWriter::Instance->WriteLog(2, "NDIlib_find_get_current_sources successfully no source " + no_sources + " " + gcnew String(_ndiName), __LINE__, __FUNCDNAME__);
				for (int i = 0; i < no_sources; i++)
				{
					NDIlib_source_t source = p_sources[i];
					if (!strcmp(source.p_ndi_name, _ndiName))
					{
						pNDIsources = &p_sources[i];
						loop = false;
						//LogWriter::Instance->WriteLog(2, "NDIConnect found resource " + gcnew String(_ndiName), __LINE__, __FUNCDNAME__);
						break;
					}
				}
			}
			count++;
		}
		if (pNDIsources)
		{
			// We now have at least one source, so we create a receiver to look at it.
			if (pNDIRecv)
			{
				// Destroy the receiver
				NDIlib_recv_destroy(pNDIRecv);
			}
			pNDIRecv = NDIlib_recv_create_v3();
			if (!pNDIRecv) return false;
			// Connect to our sources
			NDIlib_recv_connect(pNDIRecv, pNDIsources);
			isConnected = true;
			return true;
		}
		else
			return false;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return false;
	}	
	finally
	{
		// Destroy the NDI finder. We needed to have access to the pointers to p_sources[0]
		if(pNDI_find)
			NDIlib_find_destroy(pNDI_find);
	}
}

bool NDIStreamCore::NDIPTZConnect(char* _ndiName)
{
	try
	{
		if (NDIConnect(_ndiName))
		{
			if (pNDIRecv)
			{
				NDIlib_video_frame_v2_t video_frame;
				NDIlib_frame_type_e ndiRes = NDIlib_recv_capture_v2(pNDIRecv, &video_frame, NULL, nullptr, 5000);
				switch (ndiRes)
				{	// No data
				case NDIlib_frame_type_none:
					break;
					// Video data
				case NDIlib_frame_type_video:
					NDIlib_recv_free_video_v2(pNDIRecv, &video_frame);
					break;
				case NDIlib_frame_type_status_change:
					if (NDIlib_recv_ptz_is_supported(pNDIRecv))
					{
						LogWriter::Instance->WriteLog(2, "NDIPTZConnect successfully " + gcnew String(_ndiName), __LINE__, __FUNCDNAME__);
						return true;
					}
					break;

				default:
					break;
				}
				return false;
			}
			else
				return false;
		}
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}
	return false;
}

void NDIStreamCore::NDIDisConnect()
{
	try
	{
		if (pNDIRecv)
		{
			// Destroy the receiver
			NDIlib_recv_destroy(pNDIRecv);
			pNDIRecv = NULL;
		}
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}
	
}

bool NDIStreamCore::NDIRecvFrame(NDIlib_video_frame_v2_t* _frame)
{
	try
	{
		bool res = false;
	reCapture:
		NDIlib_frame_type_e ndiRes = NDIlib_recv_capture_v2(pNDIRecv, _frame, NULL, nullptr, 5000);
		switch (ndiRes)
		{	// No data
		case NDIlib_frame_type_none:
			LogWriter::Instance->WriteLog(2, "No data receive", __LINE__, __FUNCDNAME__);
			break;

			// Video data
		case NDIlib_frame_type_video:
			res = true;
			//NDIlib_recv_free_video_v2(pNDIRecv, &video_frame);
			break;
		case NDIlib_frame_type_status_change:
			if (_frame->xres * _frame->yres != 0)
				res = true;
			else
			{
				LogWriter::Instance->WriteLog(2, "NDIlib_frame_type_status_change no data received", __LINE__, __FUNCDNAME__);
				goto reCapture;
			}
			//NDIlib_recv_free_video_v2(pNDIRecv, &video_frame);
			break;

		default:
			LogWriter::Instance->WriteLog(2, "unexpected receive " + ndiRes, __LINE__, __FUNCDNAME__);
			break;
		}
		return res;
	}
	catch (Exception^ ex)
	{
		return false;
	}	
}

void NDIStreamCore::NDIFreeFrame(NDIlib_video_frame_v2_t* _frame)
{
	try
	{
		if (pNDIRecv != NULL)
			NDIlib_recv_free_video_v2(pNDIRecv, _frame);
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
	}
	
}

bool NDIStreamCore::NDIControlReq(int _controlType, int _value1, int _value2)
{
	try
	{
		bool res = false;
		/*if (pNDIRecv == NULL || !isConnected)
			NDIPTZConnect(ndiName);*/
		if (pNDIRecv)
		{
			PTZ_CONTROL_ENUM controlType = (PTZ_CONTROL_ENUM)_controlType;
			switch (controlType)
			{
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::MOVE_LEFT:
				res = NDIlib_recv_ptz_pan_tilt_speed(pNDIRecv, _value1, 0);
				LogWriter::Instance->WriteLog(2, "Move left value = " + _value1, __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::MOVE_RIGHT:
				res = NDIlib_recv_ptz_pan_tilt_speed(pNDIRecv, -_value1, 0);
				LogWriter::Instance->WriteLog(2, "Move right value = " + _value1, __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::MOVE_TOP:
				res = NDIlib_recv_ptz_pan_tilt_speed(pNDIRecv, 0, _value2);
				LogWriter::Instance->WriteLog(2, "Move top value = " + _value1, __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::MOVE_BOTTOM:
				res = NDIlib_recv_ptz_pan_tilt_speed(pNDIRecv, 0, -_value2);
				LogWriter::Instance->WriteLog(2, "Move bottom value = " + _value1, __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::MOVE_HOME:
				res = NDIlib_recv_ptz_pan_tilt(pNDIRecv, 0, 0);
				LogWriter::Instance->WriteLog(2, "Move home value = " + _value1, __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::STOP_MOVING:
				res = NDIlib_recv_ptz_pan_tilt_speed(pNDIRecv, 0, 0);
				LogWriter::Instance->WriteLog(2, "Move stop value = ", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::ZOOM_IN:
				res = NDIlib_recv_ptz_zoom_speed(pNDIRecv, _value1);
				LogWriter::Instance->WriteLog(2, "zoom in value = ", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::ZOOM_OUT:
				res = NDIlib_recv_ptz_zoom_speed(pNDIRecv, -_value1);
				LogWriter::Instance->WriteLog(2, "zoom out value = ", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::STOP_ZOOMING:
				res = NDIlib_recv_ptz_zoom_speed(pNDIRecv, 0);
				LogWriter::Instance->WriteLog(2, "stop zooming", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::FOCUS_IN:
				res = NDIlib_recv_ptz_focus_speed(pNDIRecv, _value1);
				LogWriter::Instance->WriteLog(2, "focus in", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::FOCUS_OUT:
				res = NDIlib_recv_ptz_focus_speed(pNDIRecv, -_value1);
				LogWriter::Instance->WriteLog(2, "focus out", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::STOP_FOCUSING:
				res = NDIlib_recv_ptz_focus_speed(pNDIRecv, 0);
				LogWriter::Instance->WriteLog(2, "stop focusing", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::AUTO_FOCUS:
				res = NDIlib_recv_ptz_auto_focus(pNDIRecv);
				LogWriter::Instance->WriteLog(2, "set auto focus", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::SET_PRESET:
				res = NDIlib_recv_ptz_store_preset(pNDIRecv, _value1);
				LogWriter::Instance->WriteLog(2, "set preset", __LINE__, __FUNCDNAME__);
				break;
			case ShareLibrary::Base::PTZ_CONTROL_ENUM::RECAL_PRESET:
				res = NDIlib_recv_ptz_recall_preset(pNDIRecv, _value1, 0.5);
				LogWriter::Instance->WriteLog(2, "recall preset", __LINE__, __FUNCDNAME__);
				break;
			default:
				break;
			}
		}
		return res;
	}
	catch (Exception^ ex)
	{
		LogWriter::Instance->WriteLog(2, ex->Message + ex->StackTrace, __LINE__, __FUNCDNAME__);
		return false;
	}
}

NDIStreamCore::NDIStreamCore()
{
	//InitLib();
	swsCtx = NULL;
	pNDI_send = NULL;
	frameInd = 0;
	avFrameBGR = NULL;
	ppAvFrameBGR[0] = NULL;
	ppAvFrameBGR[1] = NULL;
	pNDIsources = NULL;
	pNDIRecv = NULL;
	ndiBuffer[0] = NULL;
	ndiBuffer[1] = NULL;
	isConnected = false;
	ndiName[0] = STR_NULL;
}

NDIStreamCore::~NDIStreamCore()
{
	Destroy();
}

