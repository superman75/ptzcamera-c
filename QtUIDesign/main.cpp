#include "mainwindow.h"
#include "armulticamapp.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setDesktopSettingsAware(true);
    ARMultiCamApp a(argc, argv);

    return a.exec();
}
