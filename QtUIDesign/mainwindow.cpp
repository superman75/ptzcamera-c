#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QGridLayout>
#include "camerasetupdlg.h"
#include "presetsetupdlg.h"
#include "stagepresetsetupdlg.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMenus();
    adjustSize();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupMenus()
{
    connect(ui->actionCamera_Setup , SIGNAL(triggered(bool)) , this , SLOT(showCameraSetupDlg()));
    connect(ui->actionPreset_Setup , SIGNAL(triggered(bool)) , this , SLOT(showPresetSetupDlg()));
    connect(ui->actionStage_Preset_Setup , SIGNAL(triggered(bool)) , this , SLOT(showStagePresetSetupDlg()));
}

void MainWindow::showCameraSetupDlg()
{
    CameraSetupDlg camSetupDlg;
    camSetupDlg.exec();
}

void MainWindow::showPresetSetupDlg()
{
    PresetSetupDlg presetSetupDlg;
    presetSetupDlg.exec();
}

void MainWindow::showStagePresetSetupDlg()
{
    StagePresetSetupDlg stagePresetSetupDlg;
    stagePresetSetupDlg.exec();
}
