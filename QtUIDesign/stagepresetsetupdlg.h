#ifndef STAGEPRESETSETUPDLG_H
#define STAGEPRESETSETUPDLG_H

#include <QDialog>

namespace Ui {
class StagePresetSetupDlg;
}

class StagePresetSetupDlg : public QDialog
{
    Q_OBJECT

public:
    explicit StagePresetSetupDlg(QWidget *parent = 0);
    ~StagePresetSetupDlg();

private:
    Ui::StagePresetSetupDlg *ui;
};

#endif // STAGEPRESETSETUPDLG_H
