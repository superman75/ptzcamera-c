# PTZOptics_AR_Multicam_Controller

Project Title: PTZOptics AR MultiCam Controller

<<<<<<< HEAD
General Overview:? We would like to build a unique AR multi camera
=======
General Overview:​ We would like to build a unique AR multi camera
>>>>>>> ffdefacadebbeb2308325a79cfd4877c95c1f122
control solution that is rich in features but simple to operate. We
would like to offer live previews of the cameras, an area to upload a
photo / panoramic shot maintaining the aspect ratio but scaling to
the available area. The ability to drag and drop presets onto the
image and when clicked the presets can offer camera preset recall
commands to multiple cameras. The application should allow you to
setup presets for each camera in case none have been set. The
application should re-broadcast each incoming RTSP feed as an NDI
output from the application if enabled. We would like the application
to be in a 16:10 aspect ratio, to match displays, even though the
example images are not.

Scope of Controller:
<<<<<<< HEAD
? Main Interface
? Menu Bar
� File
? Allows end user to fully ?Exit ?the application
? Allows end user to reach the application
Settings
? Allows end user to reach ?Camera Setup? directly
? Allows end user to reach ?Camera Preset Setup
directly
? Allows end user to reach ?Stage Preset Setup
directly
? Allows end user to use a guided ?Setup Wizard
that starts with Camera Setup ? Camera Preset
Setup ? Stage Preset Setup ? Return to Main
Interface
� Profiles
? Allows end user to ?Save? the current
configuration as a file
? Should contain all current cameras
? Should contain all current presets
? Should contain all current images
? Should contain any custom names
? Should contain snapshots
? Should contain Stage Presets
? Should contain if XBox control is enabled? Should contain if NDI outputs are enabled
? Allows end user to ?Save As� ?with the current
file to rename it
? Allows end user to ?Load ?an existing profile
? Allows end user to see the eight (8) most ?Recent
Profiles ?used and quickly select from them by
clicking
� NDI Output (IMPORTANT)
? This will allow you to quickly see the status
and enable or disable NDI output for any
CONNECTED source using the checkboxes
? Note this list should be dynamic and ONLY
show the cameras that are currently
connected
� Camera Tuning ?(Future)
? We will eventually want to work all of the
detailed camera tuning capabilities you have
built into the Advanced Section of the PTZOptics
as a tuning tool within this application
? Live Preview Area ?(Top area under menu bar)
� This area is used for viewing only connected cameras
in a dynamic grid that changes depending on how many
cameras are connected
� This area should show the PTZOptics logo when no
cameras are currently setup with a �+� bubble you can
click to immediately navigate to the �Camera Setup�
and add a camera
? Note if we can have it handle a �quick� camera
add via a small pop-up window instead of
navigating to the entire Camera Setup that
would be better
� Each dynamic live view grid should have a �+� to add
additional cameras from the Camera Setup page until
you reach eight (8) cameras and the �+� should
disappear.
? Note if we can have it handle a �quick� camera
add via a small pop-up window instead of
navigating to the entire Camera Setup that
would be better
� Double clicking on ANY live preview should take you
to the Camera Setup page directly
? Note if we can have it handle a �quick� camera
edit via a right click and click �Edit� to geta pop-up window instead of navigating to the
entire Camera Setup that would be better
? Augmented Reality Image / Live Preview Section? (Area under
live previews)
� This area is used for viewing an uploaded static
image of the �stage� OR to view a live preview of
your stage IF you have a PTZOptics ZCam (based on
serial number query)
� This is the area that you will have the ability to
click on �Stage Presets� color coded overlays that
will issue control commands to multiple cameras.
� If you right click area should show a pop-up for
�Edit Stage Source�, �Edit Camera Presets� or �Edit
Stage Presets� that will take you directly to either
�Camera Preset Setup� or �Stage Preset Setup�
directly and for the �Edit Stage Source� it will take
you to �Camera Setup� directly.
? Note if we can have the �Edit Stage Source� be
a quick add pop-up window to change to either
an image, change the image or change to a live
view from a ZCam that would be better
? Direct Camera Control? (Area under stage preview)
� This area is used for taking control of ONE camera at
a time that is currently connected
� This area should showcase the Camera # or Device ID
name of any camera currently connected for direct
control
� The box in the lower left should be dynamic and match
the live preview grid so that as you click on a box
it allows you to control that specific camera in the
same box in the live preview
� This area should allow for Pan, Tilt, Zoom, Focus,
=======
● Main Interface
○ Menu Bar
■ File
● Allows end user to fully ​Exit ​the application
● Allows end user to reach the application
Settings
● Allows end user to reach ​Camera Setup​ directly
● Allows end user to reach ​Camera Preset Setup
directly
● Allows end user to reach ​Stage Preset Setup
directly
● Allows end user to use a guided ​Setup Wizard
that starts with Camera Setup → Camera Preset
Setup → Stage Preset Setup → Return to Main
Interface
■ Profiles
● Allows end user to ​Save​ the current
configuration as a file
○ Should contain all current cameras
○ Should contain all current presets
○ Should contain all current images
○ Should contain any custom names
○ Should contain snapshots
○ Should contain Stage Presets
○ Should contain if XBox control is enabled○ Should contain if NDI outputs are enabled
● Allows end user to ​Save As… ​with the current
file to rename it
● Allows end user to ​Load ​an existing profile
● Allows end user to see the eight (8) most ​Recent
Profiles ​used and quickly select from them by
clicking
■ NDI Output (IMPORTANT)
● This will allow you to quickly see the status
and enable or disable NDI output for any
CONNECTED source using the checkboxes
○ Note this list should be dynamic and ONLY
show the cameras that are currently
connected
■ Camera Tuning ​(Future)
● We will eventually want to work all of the
detailed camera tuning capabilities you have
built into the Advanced Section of the PTZOptics
as a tuning tool within this application
○ Live Preview Area ​(Top area under menu bar)
■ This area is used for viewing only connected cameras
in a dynamic grid that changes depending on how many
cameras are connected
■ This area should show the PTZOptics logo when no
cameras are currently setup with a “+” bubble you can
click to immediately navigate to the “Camera Setup”
and add a camera
● Note if we can have it handle a “quick” camera
add via a small pop-up window instead of
navigating to the entire Camera Setup that
would be better
■ Each dynamic live view grid should have a “+” to add
additional cameras from the Camera Setup page until
you reach eight (8) cameras and the “+” should
disappear.
● Note if we can have it handle a “quick” camera
add via a small pop-up window instead of
navigating to the entire Camera Setup that
would be better
■ Double clicking on ANY live preview should take you
to the Camera Setup page directly
● Note if we can have it handle a “quick” camera
edit via a right click and click “Edit” to geta pop-up window instead of navigating to the
entire Camera Setup that would be better
○ Augmented Reality Image / Live Preview Section​ (Area under
live previews)
■ This area is used for viewing an uploaded static
image of the “stage” OR to view a live preview of
your stage IF you have a PTZOptics ZCam (based on
serial number query)
■ This is the area that you will have the ability to
click on “Stage Presets” color coded overlays that
will issue control commands to multiple cameras.
■ If you right click area should show a pop-up for
“Edit Stage Source”, “Edit Camera Presets” or “Edit
Stage Presets” that will take you directly to either
“Camera Preset Setup” or “Stage Preset Setup”
directly and for the “Edit Stage Source” it will take
you to “Camera Setup” directly.
● Note if we can have the “Edit Stage Source” be
a quick add pop-up window to change to either
an image, change the image or change to a live
view from a ZCam that would be better
○ Direct Camera Control​ (Area under stage preview)
■ This area is used for taking control of ONE camera at
a time that is currently connected
■ This area should showcase the Camera # or Device ID
name of any camera currently connected for direct
control
■ The box in the lower left should be dynamic and match
the live preview grid so that as you click on a box
it allows you to control that specific camera in the
same box in the live preview
■ This area should allow for Pan, Tilt, Zoom, Focus,
>>>>>>> ffdefacadebbeb2308325a79cfd4877c95c1f122
Focus Lock or Auto Focus + Focus Unlock, Preset
Recall, Pan Speed, Tilt Speed, Zoom Speed, Focus
Speed and the ability to enable basic P/T/Z control
via an XBOX USB controller.
<<<<<<< HEAD
? Camera Setup Interface
? Main eight (8) cameras Section
� This is the area that will allow you to define the
cameras that will be available for control and to
connect to.
� It would be nice if we could ingest either unicast
(RTSP) or multicast (RTP) from the cameras here� The end user should be able to enter their RTSP
string or RTP string into the text box and click
�connect� to make the source available
? Note we want end users to be able to setup
eight (8) cameras but possibly only connect
three (3) of them� at which point regardless of
their camera # they should show in the live
preview using the three (3) camera dynamic grid
(as one example)
? Note to accept multicast we will have to add a
textbox for IP addresses as well since the
multicast address will not provide insight
about the camera IP for control
? If we can make an assignable control port for
outgoing commands for each camera in this same
interface and an ability to select TCP or UDP
it would be very beneficial
� Once added and you click �Connect� button the green
status light should illuminate if a successful
connection is establish
? If a connection is unable to be establish the
red status light should illuminate until
resolved
? A pop-up error message of �Unable to contact
camera� should showcase
� You should be able to tell each input at this point
if the application should re-broadcast the video &
audio feed as a unique NDI output from the
application using the NDI check boxes
? Stage Setup Section
� In this area we will allow an end user to have a live
preview of their stage using a PTZOptics ZCam
(verified using serial number query) OR to use a
static image that they upload
? Note that both of these sources should also
have the ability to output as a unique NDI
output from the application.
� The image or live preview should NEVER have the
aspect ratio changed and should fill the available
stage preview area while maintaining the aspect
ratio.
? Ability to assign a custom name to each unique NDI output
would be very useful? Camera Preset Setup
? In this area the user should be able to setup and test
presets for each connected camera
� If a camera is NOT connected it should not be
displayed in the list of cameras to setup presets for
? This area should allow the end user to select a camera
from the list on the left
� Using the P/T/Z, Home, OSD, OSD Enter, OSD Back,
Focus, Auto Focus + Focus Unlock and Speed Settings
allow a user to get presets set for each camera
? The �Preset Name� when available should
overwrite the traditional �Preset #� for preset
drop downs and instead of showing �Preset #�
should show the custom name instead. This is
set when setting a preset.
� Camera 9 is �special� and should only show if a ZCam
is connected
? This should have the same interface as above
camera preset setups
? Stage Preset Setup
? The top area should show a live preview, in the dynamic
grid, for any cameras currently connected
? The middle section, Augmented Reality Image or Live
Preview, should show a single clear bounding box that can
be moved and has a colored border that will correspond to
the setup tab for that Stage Preset.
� It would be nice if the bounding boxes were able to
be resized into a larger or smaller box
? The Stage Presets should be available as tabs with a �+�
to add more Stage Presets as needed (Limit to number of
Stage Presets?)
? The �Stage Preset #� titles should be able to be double
clicked and the title changed which will show everywhere.
? Stage Preset Setup Section
� Once you have selected a �Stage Preset� to setup you
should see a dynamic list of every CONNECTED camera
you can control
� You should see a drop down list of presets for each
camera in the �Stage Preset�
? There needs to be a default Preset option of
�No Movement� that can always be selected if
they do not want a camera to move when the
Stage Preset is called.? Note if a custom name has been applied to a
preset it should show up using that custom name
instead of Preset #
? Once a preset has been selected from the drop
down the camera should move to that preset and
the live preview should show this. Once the
action completes a jpeg snapshot should be
taken and should show in the �Preset Snapshot�
area as shown beside the preset drop down
� You should be able to define a custom HTTP trigger
=======
● Camera Setup Interface
○ Main eight (8) cameras Section
■ This is the area that will allow you to define the
cameras that will be available for control and to
connect to.
■ It would be nice if we could ingest either unicast
(RTSP) or multicast (RTP) from the cameras here■ The end user should be able to enter their RTSP
string or RTP string into the text box and click
“connect” to make the source available
● Note we want end users to be able to setup
eight (8) cameras but possibly only connect
three (3) of them… at which point regardless of
their camera # they should show in the live
preview using the three (3) camera dynamic grid
(as one example)
● Note to accept multicast we will have to add a
textbox for IP addresses as well since the
multicast address will not provide insight
about the camera IP for control
● If we can make an assignable control port for
outgoing commands for each camera in this same
interface and an ability to select TCP or UDP
it would be very beneficial
■ Once added and you click “Connect” button the green
status light should illuminate if a successful
connection is establish
● If a connection is unable to be establish the
red status light should illuminate until
resolved
● A pop-up error message of “Unable to contact
camera” should showcase
■ You should be able to tell each input at this point
if the application should re-broadcast the video &
audio feed as a unique NDI output from the
application using the NDI check boxes
○ Stage Setup Section
■ In this area we will allow an end user to have a live
preview of their stage using a PTZOptics ZCam
(verified using serial number query) OR to use a
static image that they upload
● Note that both of these sources should also
have the ability to output as a unique NDI
output from the application.
■ The image or live preview should NEVER have the
aspect ratio changed and should fill the available
stage preview area while maintaining the aspect
ratio.
○ Ability to assign a custom name to each unique NDI output
would be very useful● Camera Preset Setup
○ In this area the user should be able to setup and test
presets for each connected camera
■ If a camera is NOT connected it should not be
displayed in the list of cameras to setup presets for
○ This area should allow the end user to select a camera
from the list on the left
■ Using the P/T/Z, Home, OSD, OSD Enter, OSD Back,
Focus, Auto Focus + Focus Unlock and Speed Settings
allow a user to get presets set for each camera
● The “Preset Name” when available should
overwrite the traditional “Preset #” for preset
drop downs and instead of showing “Preset #”
should show the custom name instead. This is
set when setting a preset.
■ Camera 9 is “special” and should only show if a ZCam
is connected
● This should have the same interface as above
camera preset setups
● Stage Preset Setup
○ The top area should show a live preview, in the dynamic
grid, for any cameras currently connected
○ The middle section, Augmented Reality Image or Live
Preview, should show a single clear bounding box that can
be moved and has a colored border that will correspond to
the setup tab for that Stage Preset.
■ It would be nice if the bounding boxes were able to
be resized into a larger or smaller box
○ The Stage Presets should be available as tabs with a “+”
to add more Stage Presets as needed (Limit to number of
Stage Presets?)
○ The “Stage Preset #” titles should be able to be double
clicked and the title changed which will show everywhere.
○ Stage Preset Setup Section
■ Once you have selected a “Stage Preset” to setup you
should see a dynamic list of every CONNECTED camera
you can control
■ You should see a drop down list of presets for each
camera in the “Stage Preset”
● There needs to be a default Preset option of
“No Movement” that can always be selected if
they do not want a camera to move when the
Stage Preset is called.● Note if a custom name has been applied to a
preset it should show up using that custom name
instead of Preset #
● Once a preset has been selected from the drop
down the camera should move to that preset and
the live preview should show this. Once the
action completes a jpeg snapshot should be
taken and should show in the “Preset Snapshot”
area as shown beside the preset drop down
■ You should be able to define a custom HTTP trigger
>>>>>>> ffdefacadebbeb2308325a79cfd4877c95c1f122
that also has a Test button to allow you to test the
HTTP Trigger
