#ifndef ARMUTILCAMAPP_H
#define ARMUTILCAMAPP_H

#include <QApplication>
#include "mainwindow.h"


class ARMultiCamApp : public QApplication
{
    Q_OBJECT
public:
    ARMultiCamApp(int argc, char *argv[]);
    MainWindow *mainWin;
};

#endif // ARMUTILCAMAPP_H
