#include "stagepresetsetupdlg.h"
#include "ui_stagepresetsetupdlg.h"

StagePresetSetupDlg::StagePresetSetupDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StagePresetSetupDlg)
{
    ui->setupUi(this);
    adjustSize();
}

StagePresetSetupDlg::~StagePresetSetupDlg()
{
    delete ui;
}
